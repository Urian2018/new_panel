<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
    $cod_vcard_cliente = $_SESSION['cod_vcard'];
    $ref_empre_cliente = $_SESSION['rfc_empresa'];
    $rango_imgs = $_SESSION['rango_imagenes'];
    ?>
    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
    <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">


            <!----js para mostrar msj--->
        <!--<script  src="asset/js/jquery.min.js"></script>-->
            <script  src="https://code.jquery.com/jquery-2.2.4.js"></script>
            <script src="asset/js/msj.js"></script>
            <script>
                $(function () {
                    $("input[name^=img_vcard]").on("change", function () {
                        var id = $(this).attr('rel');
                        var formData = new FormData($("#formulariocero" + id)[0]);
                        var ruta = "cambiar_img_slider.php";
                        $.ajax({
                            url: ruta,
                            type: "POST",
                            data: formData,
                            contentType: false,
                            processData: false,
                            success: function (datos)
                            {
                                $("#respuesta").html(datos);
                            }
                        });
                    });

                });
            </script>
        </head>

        <body id="mimin" class="dashboard">
    <?php include('menu_header_user.php'); ?>

            <div class="container-fluid mimin-wrapper">
            <?php include('menu_lateral_escritorio.php'); ?>
            <?php
            $Consultar = ("SELECT * FROM slider_cliente WHERE cod_vcard='" . $cod_vcard_cliente . "' ORDER BY id ASC LIMIT 20");
            $numero_servicios = mysqli_query($con, $Consultar);
            $total_img_permitidas = mysqli_num_rows($numero_servicios);
            ?>

                <div id="content">
                    <br>
                    <div class="col-md-12">
                        <div class="col-md-12 panel">
                            <div class="col-md-12 panel-heading">
                                <h4 style="text-align: center; color: black;"> Actualizar las Imágenes del <strong style="color:crimson;">"CARRUSEL"</strong></h4>
                                <br><br>
                            </div><p>&nbsp;</p>			<div style="border: solid 2px #000000; ">										<h5 style="text-align: center; color: red;">                                     <strong>REFERENCIA</strong>  <strong style='color:crimson;font-size: 16px;'></strong>                                </h5>														<h6 style="text-align: center; color: black;">                                    - Tipo de archivos permitidos: <strong>".jpg, .jpeg, .png"</strong><strong style='color:crimson;font-size: 14px;'></strong>                                </h6>		        </div><p>&nbsp;</p>
                            <div class="col-md-12 panel-body">
                                <div class="col-md-12">
                                    <form  enctype="multipart/form-data" method="post" action="recib_add_carrusel.php">
                                        <div class="col-md-6">
                                            <div class="form-group form-animate-text">
                                                <div class="input-group fileupload-v1">
                                                    <!--<input type="file" id="miarchivo[]" name="miarchivo[]" multiple="">-->
                                                    <input type="file" name="file[]" id="file[]" multiple="" required="required" class="fileupload-v1-file hidden"  accept="image/*">
                                                    <input type="text" class="form-control fileupload-v1-path" placeholder="Imagen . . . ." disabled>
                                                    <span class="input-group-btn">
                                                        <button class="btn fileupload-v1-btn" type="button"><i class="fa fa-folder"></i> Presione Examinar</button>
                                                    </span>
                                                </div>
                                                <input type="hidden" name="cod_vcard_cliente" value="<?php echo $cod_vcard_cliente; ?>">
                                                <input type="hidden" name="rf_cempre_cliente" value="<?php echo $ref_empre_cliente; ?>">

                                            </div>
                                        </div>

    <?php if ($total_img_permitidas < $rango_imgs) { ?>
                                            <div class="col-md-6">
                                                <div class="form-group form-animate-text">
                                                    <button class="btn ripple btn-raised btn-success" name="enviar">
                                                        <div>
                                                            <span>Agregar Imagen</span>
                                                        </div>
                                                    </button>
                                                </div>
    <?php } else { ?>

                                                <div class="parpadea text">
                                                    <strong>Ya excedio el Limite de Imagenes, debe borra para poder agregar de nuevo.</strong>
                                                </div>
    <?php } ?>

                                        </div>
                                    </form>
                                </div>
                            </div>



                            <div class="col-md-12 top-20 padding-0">
                                <div class="col-md-12">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h3  style="text-align: center;">Lista de Imágenes del
                                                <strong style="color: crimson;">"CARRUSEL"</strong>
                                            </h3>
                                            <div id="cont_total">
                                                <h5 style="border-bottom: 1px dashed #0099cc; width:150px; margin-top:-40px;">Total de Im&aacute;genes <strong><?php echo $total_img_permitidas; ?></strong></h5>
                                                <h5 style='color:crimson; border-bottom: 1px dashed #0099cc; width:150px;'>Limite de Im&aacute;genes <strong><?php echo $rango_imgs; ?></strong></h5> 
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-responsive table-bordered table-hover" id="datatables-example" width="100%" cellspacing="0">
                                                    <thead>
                                                        <tr>
                                                            <th>Im&aacute;gen</th>
                                                            <th>Opción</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
    <?php
    while ($vcard = mysqli_fetch_array($numero_servicios)) {
        $id = $vcard['id'];
        $directorio = "Galeria_Clientes/" . $cod_vcard_cliente;
        ?>
                                                            <tr>
                                                                <td style="text-align: center;" id="respuesta">
                                                                    <img src="<?php echo $directorio; ?>/<?php echo $vcard['filename']; ?>" style='width:150px; height: 80px; object-fit: cover; text-align: center;'>
                                                                </td>
                                                                <td style="text-align: center;">
                                                                    <div id="chekee" style='float:left;'>
                                                                        <form enctype="multipart/form-data" id="formulariocero<?php echo $id; ?>" method="post">
                                                                            <input  type="hidden" name="id" id="id" value="<?php echo $id; ?>">
                                                                            <input  type="hidden" name="filename" id="filename" value="<?php echo $vcard['filename']; ?>">
                                                                            <label style='float:left;'>Cambiar Imagen</label><br>
                                                                            <input type="file" id="img_vcard<?php echo $id; ?>" name="img_vcard" accept="image/*" rel="<?php echo $id; ?>" style="color: transparent;">
                                                                        </form>
                                                                    </div>

                                                                    <a href="delet_img_slider.php?id=<?php echo $id; ?>&directorio=<?php echo $directorio; ?>&nameImg=<?php echo $vcard['filename']; ?>">
                                                                        <span class="fa fa-trash" style="font-size: 25px" title="Eliminar Imagen"></span>
                                                                    </a>
                                                                </td>
                                                            </tr>
    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
    <?php
    @mysqli_close($numero_servicios);
    ?>  
                            </div>

                        </div>
                    </div>
                    <div class="contenedor_flotante">   
                    <?php
                    if (!empty($_GET['msjs'])) {
                ?>
                <div class='col-md-12'>
                <div class='alert alert-success col-md-12 col-sm-12  alert-icon alert-dismissible fade in' role='alert'>
                  <div class='col-md-2 col-sm-2 icon-wrapper text-center'>
                    <span class='fa fa-check fa-2x'></span></div>
                    <div class='col-md-10 col-sm-10'>
                      <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                      <span aria-hidden='true'>x</span></button>
                      <p><strong>Felicitaciones el Registro fue un Exito.</strong></p>
                    </div>
                  </div>
                </div>
                <?php }

    echo isset($msj_exito) ? utf8_decode($msj_exito) : ''; 
    if (!empty($_GET['msj'])) {
        ?>
                            <div class='col-md-12'>
                                <div class='alert col-md-12 col-sm-12 alert-icon alert-danger alert-dismissible fade in' role='alert'>
                                    <div class='col-md-2 col-sm-2 icon-wrapper text-center'>
                                        <span class='fa fa-flash fa-2x'></span></div>
                                    <div class='col-md-10 col-sm-10'>
                                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>
                                        <p><strong>Felicitaciones el Registro fue Borrado Correctamente</strong></p>
                                    </div>
                                </div>
                            </div> 

    <?php }
    ?> 
                    </div>
                </div> 
            </div>    


            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse" > 
    <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

    <?php include('js.html'); ?>
        </body>
    </html>
            <?php
        } else {
            include('error.php');
        }
        ?>