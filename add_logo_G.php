<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
$cod_vcard_cliente = $_SESSION['cod_vcard'];

//almacenoel ids con la variable session ya que se pierde de repente
$ids = !empty($_REQUEST['id']) ? $_REQUEST['id'] : $_SESSION["ids"];
$_SESSION["ids"] = $ids;

foreach ($ids as $key => $val) {
if ($key == 0) {
$res = " ( ";
}
$res .= $val;
if (count($ids) == ($key + 1)) {
$res.=" )";
} else {
$res.=" , ";
}
}

if (isset($_POST['enviar'])) {
    
        $file = $_FILES['file']['name'];
        //extraer la solo la extension dela imagen
        $archivo = $_FILES['file']['name'];
        $explode = explode('.', $archivo);
        $extension = array_pop($explode);
        
        //contar imagen de un directorio
        $total_imagenes = count(glob('logos_g/{*.jpg,*.gif,*.png,*.jpeg,*.PNG,*.JPG}',GLOB_BRACE));
        $new_total_img = $total_imagenes + 1;
        
        $name_archivo = "logoG_".$new_total_img.'.';
        $mi_file = $name_archivo.$extension;
        
        $file = $_FILES['file']['name'];
        $file_loc = $_FILES['file']['tmp_name'];
        $file_size = $_FILES['file']['size'];
        $file_type = $_FILES['file']['type'];
        $folder="logos_g/";
        
	//exit;
	if(move_uploaded_file($file_loc,$folder.$mi_file))
	{
            $update_logo_G = ("UPDATE myclientes SET logo_grand='" .$mi_file. "' WHERE id IN " .$res);
            $result = mysqli_query($con, $update_logo_G);
            header("location:add_logo_G.php"); 
            exit();
	} else {
            echo 'ERROR EN EL REGISTRO';    
        }
}
?>

<!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
        <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
    </head>
    <body id="mimin" class="dashboard">
        <?php include('menu_header.php'); ?>

        <div class="container-fluid mimin-wrapper">
            <?php include('menu_lateral_escritorio.php'); ?>
             <div id="content">

<br><br>
<div class="col-md-12">
<div class="col-md-12 panel">
    <div class="col-md-12 panel-heading">
        <h4 style="text-align: center; color: black;"> Actualizar <strong style="color:crimson;">"LOGO GRANDE"</strong> para la empresa.</h4>
        <br><br>
        <?php
        $Consultar = ("SELECT * FROM myclientes WHERE id ='" .$ids[0]."' ");
        $numero_servicios = mysqli_query($con, $Consultar);
        while ($vcard = mysqli_fetch_array($numero_servicios)) {            
           if(empty($vcard['logo_grand'])){ 
                echo "<center><span style='color:red; font-size:13px;text-align:center;'>Sin Imagen</span></center>";
           } else { ?>
               <center>
                    <img src="logos_g/<?php echo $vcard['logo_grand'] ?>" style='width:100px; height: 80px;object-fit: cover; text-align: center;'>
               </center>
          <?php } }
        @mysqli_close($numero_servicios);
        ?>
        <p style="text-align:right;"><a href="vcard.php" title="Volver">
                <span class="icon-action-undo" title="Volver">Volver</span>
            </a>
        </p>
    </div>
    <form  enctype="multipart/form-data" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <div class="col-md-12 panel-body">
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="input-group fileupload-v1">
                        <input type="file" name="file"  required="required" class="fileupload-v1-file hidden"  accept="image/*">
                        <input type="text" class="form-control fileupload-v1-path" placeholder="Imagen . . . ." disabled>
                        <span class="input-group-btn">
                            <button class="btn fileupload-v1-btn" type="button"><i class="fa fa-folder"></i> Presione Examinar</button>
                        </span>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-6">
                        <button class="btn ripple btn-raised btn-success" name="enviar">
                            <div>
                                <span>Agregar Imagen</span>
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
</div>
</div>


<!-- start: Mobile -->
        <div id="mimin-mobile" class="reverse" > 
            <?php include('menu_movil.php'); ?>
        </div>
        <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
            <span class="fa fa-bars"></span>
        </button>
        <!-- end: Mobile -->

        <?php include('js.html'); ?>
    </body>
</html>
    <?php
} else {
    include('error.php');
}
?>