<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
    $id_user    = $_SESSION['id'];
    $rango_user = $_SESSION['rango_users'];
    $rfc_empresa = $_SESSION['rfc_empresa'];
    ?>
<!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">

            <!----js para mostrar msj--->
            <script  src="asset/js/jquery.min.js"></script>
            <script src="asset/js/msj.js"></script>
        </head>

        <body id="mimin" class="dashboard">
            <?php 
                if ($rango_user !="Administrador") {
                    include('menu_header_user.php');
                } else{
                    include('menu_header.php');
                }
             ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>

                <div id="content">
                    <br><br>
                    <?php
                    if (isset($_POST['enviar'])) {
                        $msj_exito = "";
                        $user     = $_POST['user'];
                        $pass     = $_POST['pass'];


                        $update = ("UPDATE users SET user='" .$user. "', pass='" .$pass. "' WHERE id='".$id_user."'");
                        $result_update = mysqli_query($con, $update);
                        $msj_exito = "
                            <div class='col-md-12'>
                            <div class='alert alert-success col-md-12 col-sm-12  alert-icon alert-dismissible fade in' role='alert'>
                            <div class='col-md-2 col-sm-2 icon-wrapper text-center'>
                                <span class='fa fa-check fa-2x'></span></div>
                                <div class='col-md-10 col-sm-10'>
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                <span aria-hidden='true'>x</span></button>
                                <p><strong>Felicitaciones la Clave fue cambiada con Exito.</strong></p>
                                </div>
                            </div>
                            </div>";
                            //header("location:new_clave.php");
                            // exit();
                    }
                    ?>

                    <div class="col-md-12">
                        <div class="col-md-12 panel">
                            <div class="col-md-12 panel-heading">
                                <h4 style="text-align: center; color: black;"> Actualizar mi <strong style="color:crimson;">"CLAVE"</strong></h4>
                                <br>
                            </div>
                            <?php
                            $sql = ("SELECT * FROM users WHERE id='".$id_user."'");
                            $mostar = mysqli_query($con, $sql);
                              while ($row = mysqli_fetch_array($mostar)) { ?>
                            <form  enctype="multipart/form-data" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                                <div class="col-md-12 panel-body">
                                    <div class="col-md-12">

                                        <div class="col-md-12 panel-body">
                                            <div class="col-md-6">
                                            <label>USUARIO</label>
                                                <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                    <input type="text" class="form-text" id="validate_firstname" name="user" value="<?php echo $row['user']; ?>" required autocomplete="off" readonly>
                                                    <span class="bar"></span>
                                                    <br>
                                                    
                                                </div>
                                            </div>
                                        

                                            <div class="col-md-6">
                                            <label>CLAVE</label>
                                                <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                    <input type="text" class="form-text" id="validate_firstname" name="pass" value="<?php echo $row['pass']; ?>" required autocomplete="off">
                                                    <span class="bar"></span>
                                                   
                                                </div>
                                            </div>
                                        
                                    </div>
                                </div>
                                </div>
                                
                                 <div class="col-md-12 panel-body">
                                     <div class="col-md-12">
                                        <div class="col-md-6"  style="float:right;">
                                                <button class="btn ripple btn-raised btn-success" name="enviar">
                                                    <div>
                                                        <span>Cambiar Clave</span>
                                                    </div>
                                                </button>
                                            </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                            <?php } ?>
                    </div>
                </div>

                <div class="contenedor_flotante">                         
                <?php
                    echo isset($msj_exito) ? utf8_decode($msj_exito) : ''; ?>
                </div>

            </div>


            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse" > 
    <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

    <?php include('js.html'); ?>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>