<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
    ?>
    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
            <style>
            .fa-credit-card:hover{
                cursor: pointer;
                color: black;
            }
            </style>
        </head>

        <body id="mimin" class="dashboard">
            <?php include('menu_header.php'); ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>

                <div id="content">
                    <div class="col-md-12">
                    <br>
                    <div class="col-md-12">
                        <div>
                            <span>
                                <h5 style="text-align: right;">
                                    <a href="descargar_clientes.php" class="ripple btn-outline btn-primary">Descargar Todos mis Clientes</a>    
                                </h5>
                            </span>
                        </div>
                    </div>
                    <br>

                    <?php
                    $Consultar = ("SELECT * FROM historial_registros ORDER BY id DESC");
                    $numero_servicios = mysqli_query($con, $Consultar);
                    ?>
                    <div class="col-md-12 top-20 padding-0">
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-heading"><h3 style="text-align: center;">Historial de Registros Subidos al
                                        <strong style="color: crimson;">"SERVIDOR"</strong></h3></div>
                                <div class="panel-body">
                                    <div class="responsive-table">
                                        <table  class="table table-striped table-bordered" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>Inicio</th>
                                                    <th>Hasta</th>
                                                    <th>Total Subidos</th>
                                                    <th>Fecha</th>
                                                    <th>Opción</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                while ($vcard = mysqli_fetch_array($numero_servicios)) {
                                                    $table = "historial_registros";
                                                    $id = $vcard['id'];
                                                    $desde = $vcard['desde'];
                                                    $hasta = $vcard['hasta'];
                                                    ?>
                                                    <tr>
                                                        <td style="text-align: center;"><?php echo $desde; ?></td>
                                                        <td style="text-align: center;"><?php echo $hasta; ?></td>
                                                        <td style="text-align: center;"><?php echo $vcard['total_subidos']; ?></td>
                                                        <td style="text-align: center;"><?php echo $vcard['fecha']; ?></td>
                                                        <td style="text-align: center; font-size: 25px;">
                                                        <a href="descargar_historial_excel.php?id_principio=<?php echo $desde; ?>&id_final=<?php echo $hasta; ?>"> 
                                                        <span class="fa fa-download" title="Descargar Excel"></span>
                                                        </a>
                                                            <a href="delete.php?id=<?php echo $id; ?>&table=<?php echo $table;?>">
                                                                <span class="fa fa-trash" style="font-size: 25px" title="Eliminar Historial"></span>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <?php
                        @mysqli_close($numero_servicios);
                        ?>  
                    </div> 

                </div>
            </div>
            </div>


            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse" > 
                <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

            <?php include('js.html'); ?>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>