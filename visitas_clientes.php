<?php
session_start();
if (isset($_SESSION['user']) != "") {
header("Content-Type: text/html;charset=utf-8");
include('config.php');
$rfc_empresa = $_SESSION['rfc_empresa'];
    ?>
    
    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
            <style type="text/css">
                h3{
                    text-align: center;
                    color: crimson;
                }
                .fa-eye:hover{
                    color: black;
                }
                h5 a:hover{
                    color: black;
                }

            </style>
            <script  src="https://code.jquery.com/jquery-2.2.4.js"></script>
            <script type="text/javascript" src="asset/js/ajax.js"></script>
            <script language="javascript">
            $(document).ready(function () {
                 $("#mes").change(function () {
                    $('#meses').html('<center><img src="img/cargandoo.gif"/><br/>Un momento, por favor...</center>');
                    $('#cbx_localidad').find('option').remove().end().append('<option value="whatever"></option>').val('whatever');
                        mes = $(this).val();
                        $.get("recib_mes_visita.php", {mes: mes}, function (data) {
                             $("#meses").html(data); 
                        });
                });
            });
            </script>
        </head>


        <body id="mimin" class="dashboard">
            <?php include('menu_header_user.php'); ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>
                <div id="content">
                    <?php
                    $Consultar_dos = ("SELECT 
                        id,
                        nombre,
                        cod_vcard,
                        rfc_empresa,
                        mes,
                         SUM( visita_pag1 ) AS visita_pag1,
                         SUM( visita_pag2 ) AS visita_pag2,
                         SUM( visita_pag3 ) AS visita_pag3
                    FROM visitas
                    WHERE rfc_empresa = '".$rfc_empresa."'
                    GROUP BY cod_vcard");
                    $numero_servi = mysqli_query($con, $Consultar_dos);
                    ?>
                    <div class="col-md-12 top-20 padding-0">
                        <div class="col-md-12">
                            <div class="panel">

                                <div class="panel-heading">
                                    <?php
                                        $query = ("SELECT mes FROM visitas  WHERE rfc_empresa='".$rfc_empresa."' GROUP BY mes");
                                        $res = mysqli_query($con, $query);
                                        ?>
                                         <div class="col-md-12">
                                        <div class="col-md-3">
                                        <center><label for=""style="color: #333; font-size: 13px; text-align: center; font-weight: bold;">Meses</label></center>
                                          <select name="mes" id="mes" class="form-control">
                                            <option value="" selected>Seleccione</option>
                                            <?php
                                              while ($valores = mysqli_fetch_array($res)) {
                                                $mes  = $valores['mes'];
                                                if($mes == '01'){
                                                    $name_mes = 'ENERO';
                                                }elseif ($mes == '02') {
                                                    $name_mes = 'FEBRERO';
                                                }elseif ($mes == '03') {
                                                    $name_mes = 'MARZO';
                                                }elseif ($mes == '04') {
                                                    $name_mes = 'ABRIL';
                                                }elseif ($mes == '05') {
                                                    $name_mes = 'MAYO';
                                                }elseif ($mes == '06') {
                                                    $name_mes = 'JUNIO';
                                                }elseif ($mes == '07') {
                                                    $name_mes = 'JULIO';
                                                }elseif ($mes == '08') {
                                                    $name_mes = 'AGOSTO';
                                                }elseif ($mes == '09') {
                                                    $name_mes = 'SEPTIEMBRE';
                                                }elseif ($mes == '10') {
                                                    $name_mes = 'OCTUBRE';
                                                }elseif ($mes == '11') {
                                                    $name_mes = 'NOVIEMBRE';
                                                }else{
                                                    $name_mes = 'DICIEMBRE';
                                                }
                                            ?>
                                                <option value="<?php echo $mes; ?>"><?php echo $name_mes; ?></option>
                                                <?php
                                              }
                                            @mysqli_close($res);
                                            ?>
                                          </select>

                                      </div>

                                    <div class="col-md-9">
                                                <h5 style="text-align: right;">
                                                    <a href="descargar_metricas.php?rfc_empresa=<?php echo $rfc_empresa; ?>" class="ripple btn-outline btn-primary">Descargar mis M&eacute;tricas</a>    
                                                
                                                <a href="visitas_clientes.php">
                                                    <span class="icon-action-undo"></span>
                                                        Volver
                                                </a>
                                                </h5>

                                        </div>
                                    </div>

                                    <h5>
                                     <?php
                                    $total_mes = ("SELECT
                                        visita_pag1,
                                        visita_pag2,
                                        visita_pag3, 
                                        rfc_empresa,
                                         SUM( visita_pag1 ) AS visita_pag1,
                                         SUM( visita_pag2 ) AS visita_pag2,
                                         SUM( visita_pag3 ) AS visita_pag3,
                                         Sum(visita_pag1 + visita_pag2 + visita_pag3) AS total_mes
                                    FROM visitas
                                    WHERE rfc_empresa = '".$rfc_empresa."'");
                                    $resul_total = mysqli_query($con, $total_mes);
                                    $row_total = mysqli_fetch_array($resul_total);
                                    //echo $row_total['total_mes'];
                                    ?>
                                    <br>

                                    <!---Contenedor del ajax para cuando se ejecute el select, esta es la capa---->
                                    <div id="meses">
                                    <?php
                                    $sql_total = ("SELECT 
                                        id,
                                        nombre,
                                        cod_vcard,
                                        rfc_empresa,
                                        mes,
                                         SUM( visita_pag1 ) AS visita_pag1,
                                         SUM( visita_pag2 ) AS visita_pag2,
                                         SUM( visita_pag3 ) AS visita_pag3
                                    FROM visitas
                                    WHERE rfc_empresa = '".$rfc_empresa."'
                                    GROUP BY rfc_empresa");
                                    $total_visit = mysqli_query($con, $sql_total);

                                    while ($visitas = mysqli_fetch_array($total_visit)) { ?>
                                        <h4 style="color: #00afef; text-align: center;">RESUMEN DE INTERACCIONES</h4>
                                        <div style="margin: 0 auto; text-align: center;">
                                            <span>Total Descargas VCard </span>
                                                <strong style="color: #00afef"><?php echo $visitas['visita_pag1']; ?></strong><br>
                                            <span>Total VCard Compartidas</span>
                                                <strong style="color: #00afef"><?php echo $visitas['visita_pag2']; ?></strong><br>
                                            <span>Total Visitas Galeria </span> 
                                                <strong style="color: #00afef"><?php echo $visitas['visita_pag3']; ?></strong><br>
                                        </div>
                                           
                                        <?php   }
                                    ?>
                                    <br>
                <center style='color: crimson; font-size: 16px;' font-weight:bold;>Total de Visitas <strong><?php echo $row_total['total_mes']; ?></strong></center></h5>


                                <div class="panel-body" style="background-color: white !important;">
                                   <div class="table-responsive">
                                        <table class="table table-responsive table-bordered table-hover" id="datatables-example" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>C&oacute;digo</th>
                                                    <th>Nombre</th>
                                                    <th>Descargas de VCard</th>
                                                    <th>Compartió VCard</th>
                                                    <th>Visitas en Galeria</th>
                                                    <!--<th>Opci&oacute;n</th>-->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                while ($visitas = mysqli_fetch_array($numero_servi)) {
                                                    $cod_vcard_cliente = $visitas['cod_vcard'];
                                                    ?>
                                                    <tr>
                                                        <td style="text-align: center;"><?php echo $visitas['cod_vcard']; ?></td>
                                                        <td style="text-align: center;"><?php echo $visitas['nombre']; ?></td>
                                                        <td style="text-align: center;"><?php echo $visitas['visita_pag1']; ?></td>
                                                        <td style="text-align: center;"><?php echo $visitas['visita_pag2']; ?></td>
                                                        <td style="text-align: center;"><?php echo $visitas['visita_pag3']; ?></td>
                                                        <!--<td style="text-align: center;">
                                                            <a href="ver_visita_clientes.php?cod_vcard=<?php //echo $cod_vcard_cliente; ?>">
                                                                <span class="fa fa-eye" style="font-size: 25px" title="Ver Resumen de Metricas"></span>
                                                            </a>
                                                        </td>-->
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                </div>


                            </div>
                        </div>  
                        <?php
                        @mysqli_close($numero_servi);
                        ?>  
                    </div> 
                </div>
            </div>


            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse" > 
                <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

            <?php include('js.html'); ?>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#datatables-example').DataTable();
                });
            </script>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>