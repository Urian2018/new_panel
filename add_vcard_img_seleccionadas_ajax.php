<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
    $cod_vcard_cliente = $_SESSION['cod_vcard'];
                $ids = !empty($_REQUEST['id']) ? $_REQUEST['id'] : $_SESSION["ids"];
                $_SESSION["ids"] = $ids;

                foreach ($ids as $key => $val) {
                    if ($key == 0) {
                        $res = " ( ";
                    }
                    $res .= $val;
                    if (count($ids) == ($key + 1)) {
                        $res.=" )";
                    } else {
                        $res.=" , ";
                    }
                }

                if (isset($_POST['enviar'])) {
                   $check = getimagesize($_FILES["img_vcard"]["tmp_name"]);
                   if ($check !== false) {
                    $data = base64_encode(file_get_contents($_FILES["img_vcard"]["tmp_name"]));
                    $img_vcard = "," . $data . "";
                    $q = ("UPDATE myclientes SET img_vcard='" . $img_vcard . "' WHERE id IN " . $res);
                    $result = mysqli_query($con, $q);
                    header("location:mis_clientes.php");
                    exit();
                }
                }
                ?>
                <br><br>
                <div class="col-md-12">
                    <div class="col-md-12 panel">
                        <div class="col-md-12 panel-heading">
                            <h4 style="text-align: center; color: black;"> Actualizar Imágen <strong style="color:crimson;">"VCARD"</strong> para los registros seleccionados.</h4>
                            <br><br>
                            <br><br>
                            <?php
                            $Consultar_vcard = ("SELECT * FROM myclientes WHERE id = " . $ids[0] );
                            $numero_img_vcard = mysqli_query($con, $Consultar_vcard);
                            while ($resu_vcard = mysqli_fetch_array($numero_img_vcard)) {
                                $table = "myclientes";
                                $img_vcard = $resu_vcard['img_vcard'];
                                
                                $var = "data:image/jpg/png;base64";
                                $ruta_img_vcard = $var . $img_vcard;
                                if (empty($img_vcard)) {
                                    echo "<center><span style='color:red; font-size:13px;text-align: center;'>No tengo Imgen</span></center>";
                                } else {
                                    $img = file_get_contents($ruta_img_vcard);
                                    $imdata = base64_encode($img);
                                    echo "<center><img src='data:image/jpg/png;base64," . $imdata . "' style='width:40px;'/></center>";
                                }

                            }
                            @mysqli_close($numero_img_vcard);
                            ?>
                            <p style="text-align:right;"><a href="mis_clientes.php" title="Volver">
                                    <span class="icon-action-undo" title="Volver">Volver</span>
                                </a>
                            </p>
                        </div>
                        <form  enctype="multipart/form-data" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                            <div class="col-md-12 panel-body">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="input-group fileupload-v1">
                                            <input type="file" name="img_vcard" id="img_vcard" required="required" class="fileupload-v1-file hidden"  accept="image/*">
                                            <input type="text" class="form-control fileupload-v1-path" placeholder="Imagen . . . ." disabled>
                                            <span class="input-group-btn">
                                                <button class="btn fileupload-v1-btn" type="button"><i class="fa fa-folder"></i> Presione Examinar</button>
                                            </span>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="col-md-6">
                                            <button class="btn ripple btn-raised btn-success" name="enviar">
                                                <div>
                                                    <span>Agregar Imagen</span>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            </div>

    <?php include('js.html'); ?>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>