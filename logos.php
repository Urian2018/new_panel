<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {

    if (isset($_POST['enviar'])) {
        $msj_exito = "";
        $fileName = $_FILES['file']['name'];
        $rtOriginal = $_FILES['file']['tmp_name'];
        $fileExtension = substr(strrchr($fileName, '.'), 1);
        $g = "_G";
        $p = "_P";
        $cod_vcard_cliente = strtoupper($_POST['cod_vcard']);


        $png = 'png';
        $PNG = 'PNG';
        /*if ($fileExtension = $PNG) {
            $fileExtension = $png;
        }*/
        if ($fileExtension == $png || $fileExtension == $PNG) {
            $original = imagecreatefrompng($rtOriginal);
            $logo_Grand_png = $cod_vcard_cliente . $g . '.' . $fileExtension;
//Ancho y alto máximo
            $max_ancho = 400;
            $max_alto = 300;

//Medir la imagen
            list($ancho, $alto) = getimagesize($rtOriginal);
//Ratio
            $x_ratio = $max_ancho / $ancho;
            $y_ratio = $max_alto / $alto;

//Proporciones
            if (($ancho <= $max_ancho) && ($alto <= $max_alto)) {
                $ancho_final = $ancho;
                $alto_final = $alto;
            } else if (($x_ratio * $alto) < $max_alto) {
                $ancho_final = $max_ancho;
                $alto_final = ceil($x_ratio * $alto);
            } else {
                $ancho_final = $max_ancho;
                $alto_final = $max_alto;
            }

            $lienzo = imagecreatetruecolor($ancho_final, $alto_final);
            imagecopyresampled($lienzo, $original, 0, 0, 0, 0, $ancho_final, $alto_final, $ancho, $alto);
            imagedestroy($original);
            imagepng($lienzo, "Logos/" . $logo_Grand_png);
            include('logo_png_respaldo.php');
        } else {
            $original = imagecreatefromjpeg($rtOriginal);
            $logo_Grand_jpg = $cod_vcard_cliente . $g . '.' . $fileExtension;

            $max_ancho = 400;
            $max_alto = 300;

            list($ancho, $alto) = getimagesize($rtOriginal);
            $x_ratio = $max_ancho / $ancho;
            $y_ratio = $max_alto / $alto;
//Proporciones
            if (($ancho <= $max_ancho) && ($alto <= $max_alto)) {
                $ancho_final = $ancho;
                $alto_final = $alto;
            } else if (($x_ratio * $alto) < $max_alto) {
                $alto_final = ceil($x_ratio * $alto);
                $ancho_final = $max_ancho;
            } else {
                $ancho_final = ceil($y_ratio * $ancho);
                $alto_final = $max_alto;
            }

//Crear un lienzo
            $lienzo = imagecreatetruecolor($ancho_final, $alto_final);
            imagecopyresampled($lienzo, $original, 0, 0, 0, 0, $ancho_final, $alto_final, $ancho, $alto);
            imagedestroy($original);
            imagejpeg($lienzo, "Logos/" . $logo_Grand_jpg);
            include('logo_jpg_respaldo.php');
        }
    }
    ?>

    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
    <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
            <!----js para mostrar msj--->
            <script  src="asset/js/jquery.min.js"></script>
            <script src="asset/js/msj.js"></script>
        </head>

        <body id="mimin" class="dashboard">
    <?php include('menu_header.php'); ?>

            <div class="container-fluid mimin-wrapper">
            <?php include('menu_lateral_escritorio.php'); ?>
                <div id="content">
                    <br>
                    <div class="col-md-12">
                        <div class="col-md-12 panel">
                            <div class="col-md-12 panel-heading">
                                <h4 style="text-align: center; color: black;"> Crear <strong style="color:crimson;">Logo Grande y Pequeño</strong> para Respaldo.</h4>
                                <br><br>
                            </div>
                            <form  enctype="multipart/form-data" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                                <div class="col-md-12 panel-body">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <label>CODIGO VCARD</label>
                                            <div class="form-group form-animate-text">
                                                <input type="text" class="form-text" name="cod_vcard" maxlength="8" style="text-transform:uppercase;" required autocomplete="off">
                                                <span class="bar"></span>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="input-group fileupload-v1">
                                                <input type="file" name="file" required="required" class="fileupload-v1-file hidden"  accept="image/*">
                                                <input type="text" class="form-control fileupload-v1-path" placeholder="Imagen . . . ." disabled>
                                                <span class="input-group-btn">
                                                    <button class="btn fileupload-v1-btn" type="button"><i class="fa fa-folder"></i> Presione Examinar</button>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <button class="btn ripple btn-raised btn-success" name="enviar">
                                                <div>
                                                    <span>Agregar Imagen</span>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
    <?php
    /* Total logos tanto grande como pequeño de todas las empresas */
    $total_imagenes = count(glob('Logos/{*.jpg,*.jpeg,*.gif,*.png}', GLOB_BRACE));
    ?>
                    <div class="col-md-12 top-20 padding-0">
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 style="text-align: center;">
                                        Todos los <strong style="color: crimson;">"LOGO GRANDE Y PEQUEÑOS"</strong> 
                                        de las Empresas.</h3> Total <strong style="color: crimson;">(<?php echo $total_imagenes; ?>)</strong></div>
                                <div class="panel-body">
                                <?php
                                $ruta = "Logos/"; 
                                $filehandle = opendir($ruta); // Abrir archivos
                                while ($file = readdir($filehandle)) {
                                    if ($file != '.' && $file != '..') {
                                        $tamanyo = GetImageSize($ruta . $file); ?>
                                        <div class="col-md-3">
                                            <img src="<?php echo $ruta . $file; ?>" style="width:220px; min-width: 220px; max-width: 220px; min-height:100px; max-height:100px;"> <br>
                                            <div style="width: 100%; padding: 3px 0px 15px  0px">
                                                <a href="download_logo.php?file=<?php echo $file; ?>"  download="Logo" title="Descargar Logo"><?php echo $file; ?></a></td>
                                                <a href="delete_logo_resp.php?name=<?php echo $file; ?>" style="margin-left: 50px;">Eliminar</a>
                                            </div>
                                        </div>                        
                                            <?php
                                        }
                                    }
                                    closedir($filehandle); // Fin lectura archivos
                                ?>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>


            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse" > 
                <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

            <?php include('js.html'); ?>
        </body>
    </html>

    <?php
} else {
    include('error.php');
}
?>