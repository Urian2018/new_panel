<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
    $rango_usuario = $_SESSION['rango_users'];
    $nameUser      = $_SESSION['nombre_apellido'];
    ?>
    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">

            <script  src="https://code.jquery.com/jquery-2.2.4.js"></script>
            <script src="asset/js/msj.js"></script>
        </head>

        <body id="mimin" class="dashboard">
            <?php 
                include('menu_header.php'); ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>

                <div id="content">
                    <div class="panel box-shadow-none content-header">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <h3 class="animated fadeInLeft" style="text-align: center">Cargar Winrar con Información de los Expositores de la Expo</h3>
                            </div>
                        </div>
                    </div>

                  
                    <div class="col-md-12">
                        <div class="col-md-12 panel">
                            <div class="col-md-12 panel-heading">
                                <h4> &nbsp;</h4>
                            </div>
                            <form action="recibe_excelClienteStand.php" method="post" enctype="multipart/form-data">
                                <div class="col-md-12 panel-body">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                        <div class="form-group form-animate-text">
                                            <div class="input-group fileupload-v1">
                                                <input type="file" id="archivo[]" name="archivo[]"  required="required" class="fileupload-v1-file hidden">
                                                <input type="text" class="form-control fileupload-v1-path" placeholder="Archivo . . . ." disabled>
                                                <span class="input-group-btn">
                                                    <button class="btn fileupload-v1-btn" type="button"><i class="fa fa-folder"></i> Presione Examinar</button>
                                                </span>

                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="userCliente" value="<?php echo $nameUser; ?>">
                                        <div class="col-md-6">
                                            <div class="col-md-6">
                                                <button class="btn ripple btn-raised btn-success">
                                                    <div>
                                                        <span>Subir Archivo</span>
                                                    </div>
                                                </button>
                                            </div>
                                            <br><br>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="contenedor_flotante">                         
                        <?php
                        if (!empty($_GET['msj'])) { ?>
                    <div class='col-md-12'>
                        <div class='alert alert-success col-md-12 col-sm-12  alert-icon alert-dismissible fade in' role='alert'>
                        <div class='col-md-2 col-sm-2 icon-wrapper text-center'>
                            <span class='fa fa-check fa-2x'></span></div>
                            <div class='col-md-10 col-sm-10'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                            <span aria-hidden='true'>x</span></button>
                            <p><strong>Felicitaciones fue enviado correctamente.</strong></p>
                            </div>
                        </div>
                    </div> 
                        <?php } ?>
                    </div>

                </div>
                </div>
           
            


            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse"> 
                <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

            <?php include('js.html'); ?>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>