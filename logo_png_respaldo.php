<?php
$png = 'png';
$PNG = 'PNG';
if ($fileExtension == $png || $fileExtension == $PNG ) {
$original =  imagecreatefrompng($rtOriginal);
$logo_pequeño_png = $cod_vcard_cliente.$p.'.'.$fileExtension;

//Ancho y alto máximo
$max_ancho = 300;
$max_alto = 300;
list($ancho,$alto)=getimagesize($rtOriginal);

$x_ratio = $max_ancho / $ancho;
$y_ratio = $max_alto / $alto;

if(($ancho <= $max_ancho) && ($alto <= $max_alto) ){
    $ancho_final = $ancho;
    $alto_final = $alto;
}
else if(($x_ratio * $alto) < $max_alto){
    $alto_final = ceil($x_ratio * $alto);
    $ancho_final = $max_ancho;
}
else {
    $ancho_final = $max_ancho;
    $alto_final = $max_alto;
}

$lienzo=imagecreatetruecolor($ancho_final,$alto_final);
imagecopyresampled($lienzo,$original,0,0,0,0,$ancho_final, $alto_final,$ancho,$alto);
imagedestroy($original);
imagepng($lienzo,"Logos/".$logo_pequeño_png);

    } 
?>
