<?php
require_once('config.php');
/* * LIMPIANDO TABLA TEMPORAL** */
mysqli_query($con, "TRUNCATE TABLE clientes_temporales");

if ((!empty($_POST['id']))) {
    foreach ($_POST['id'] as $id_cliente) {
        $checkemail = mysqli_query($con, "SELECT * FROM myclientes WHERE id='" . $id_cliente . "' ");
        $resultado_vcard = mysqli_num_rows($checkemail);

        while ($row = mysqli_fetch_array($checkemail)) {

            $correlativo = 0;
            $ce = mysqli_query($con, "SELECT max(codigo) as last FROM empresas WHERE ciudad = '" . trim($row['cod_ciudad']) . "' ");
            $data = mysqli_fetch_array($ce);

            $ced = mysqli_query($con, "SELECT max(codigo) as last FROM empresas WHERE rfc_empresa = '" . trim($row['rfc_empresa']) . "' ");
            $data2 = mysqli_fetch_array($ced);

            $co = mysqli_query($con, "SELECT max(correlativo) as correlativo FROM myclientes WHERE rfc_empresa='" . trim($row['rfc_empresa']) . "' AND cod_ciudad= " . trim($row['cod_ciudad']));
            $cor = mysqli_fetch_array($co);

            $cod = 0;

            //echo "<pre>" . trim($row['empresa']) . ' ' . trim($row['cod_ciudad']) . '   Correlativo - ' . $cor['correlativo'] . "</pre>";

            if (empty($data['last']) && empty($data2['last'])) {
                $correlativo = 1;
                $cod = 1;
                $a = "INSERT INTO empresas(name_city,empresa,ciudad,codigo,rfc_empresa) VALUES ('" . trim($row['ciudad']) . "','" . trim($row['empresa']) . "', '" . trim($row['cod_ciudad']) . "', " . $cod . ", '" . trim($row['rfc_empresa']) . "')";
                $resultado = mysqli_query($con, $a);
            } else if (!empty($data['last']) && empty($data2['last'])) {
                $correlativo = 1;

                $cod = (int) $data['last'] + 1;
                $b = "INSERT INTO empresas(name_city,empresa,ciudad,codigo,rfc_empresa) VALUES ('" . trim($row['ciudad']) . "','" . trim($row['empresa']) . "', '" . trim($row['cod_ciudad']) . "', " . $cod . ", '" . trim($row['rfc_empresa']) . "')";
                $resultado = mysqli_query($con, $b);
            } else if (empty($data['last']) && !empty($data2['last'])) {
                $correlativo = 1;
                $cod = 1;
                $c = "INSERT INTO empresas(name_city,empresa,ciudad,codigo,rfc_empresa) VALUES ('" . trim($row['ciudad']) . "','" . trim($row['empresa']) . "', '" . trim($row['cod_ciudad']) . "', " . $cod . ", '" . trim($row['rfc_empresa']) . "')";
                $resultado = mysqli_query($con, $c);
            } else if (!empty($data['last']) && !empty($data2['last'])) {
                $correlativo = $cor['correlativo'] + 1;
            }


            $cd = mysqli_query($con, "SELECT max(codigo) as codigo FROM empresas WHERE rfc_empresa='" . trim($row['rfc_empresa']) . "' AND ciudad = '" . trim($row['cod_ciudad']) . "' ");
            $cliente = mysqli_fetch_array($cd);

            $cliente = !empty($cliente['codigo']) ? $cliente['codigo'] : 0;
            $cliente = str_pad(trim($cliente), 2, "0", STR_PAD_LEFT);

            $correlativo = str_pad(trim($correlativo), 2, "0", STR_PAD_LEFT);
            $codciudad = ($row['cod_ciudad'] == 'NULL' || $row['cod_ciudad'] == '' ) ? 0 : $row['cod_ciudad'];
            $codciudad = str_pad(trim($codciudad), 2, "0", STR_PAD_LEFT);

            $cod_vcard = 'VC' . $codciudad . $cliente . $correlativo;
            $cod_distribuidor   = $row['cod_distribuidor'];
            $name               = $row['nombre'];
            $cargo              = $row['cargo'];
            $empresa            = $row['empresa'];
            $email              = $row['email'];
            $telefono           = $row['telefono'];
            $logo_grand         = $row['logo_grand'];
            $web_cliente        = $row['web_cliente'];
            $direccion          = $row['direccion'];
            $dir_l2             = $row['dir_l2'];
            $dir_l3             = $row['dir_l3'];
            $foto_vcard         = $row['img_vcard'];
            $tlf_dos            = $row['tlf_dos'];
            $tlf_tres           = $row['tlf_tres'];
            $tlf_cuatro         = $row['tlf_cuatro'];
            $movil              = $row['tlf_movil'];
            $web_dos_cliente    = $row['web_cliente_dos'];
            $facebook           = $row['facebook'];
            $twitter            = $row['twitter'];
            $instagram          = $row['instagram'];
            $youtube            = $row['youtube'];
            $linkedin           = $row['linkedin'];
            $google_mas         = $row['gogle_mas'];
            $nota               = $row['nota'];
            $cod_ciudad         = $row['cod_ciudad'];
            $city               = $row['ciudad'];
            $ciudad_compa       = $row['ciudad_compa'];
            $cp                 = $row['cp'];
            $estatus_metrica    = $row['estatus_metrica'];
            
            $rango_img              = $row['rango_imagenes'];
            $rango_video            = $row['rango_video'];
            $rango_audios           = $row['rango_audios'];
            $rango_arch_descargable = $row['rango_arch_descargables'];

            $rfc           = $row['rfc'];
            $rfc_empresa   = $row['rfc_empresa'];

            $pagina1 = 'https://vcard.mx/_/1.php?vc=' . $cod_vcard;
            $pagina2 = 'https://vcard.mx/_/2.php?vc=' . $cod_vcard;
            $pagina3 = 'https://vcard.mx/_/3.php?vc=' . $cod_vcard;
            //$redireccionamiento_contacto = 'https://vcard.mx/_/Compartir-Contacto/index.php?vc=' . $cod_vcard;
            //$redireccionamiento_galeria  = 'https://vcard.mx/_/Galeria-de-imagenes/index.php?vc=' . $cod_vcard;

            $redireccionamiento_contacto  = 'http://Compartir-Contacto.vcard.mx/c.php?vc=' . $cod_vcard;
            $redireccionamiento_galeria   = 'http://Galeria-de-Imagenes.vcard.mx/g.php?vc=' . $cod_vcard;

            /*
              $pagina1 = 'http://localhost:8080/SISTEMA_VCARD/1.php?vc=' . $cod_vcard;
              $pagina2 = 'http://localhost:8080/SISTEMA_VCARD/2.php?vc=' . $cod_vcard;
              $pagina3 = 'http://localhost:8080/SISTEMA_VCARD/3.php?vc=' . $cod_vcard;
              $redireccionamiento_contacto = 'http://localhost:8080/SISTEMA_VCARD/Compartir-Contacto/index.php?vc=' . $cod_vcard;
              $redireccionamiento_galeria = 'http://localhost:8080/SISTEMA_VCARD/Galeria-de-imagenes/index.php?vc=' . $cod_vcard;
             */

            /* $mi_directorio = 'http://vcard.mx/_/';
              $url_redireccionamiento_galeria = $mi_directorio."Compartir-Contacto/";
              $url_redireccionamiento_contacto = $mi_directorio."Galeria-de-imagenes/";

              $pagina1 = $mi_directorio.'1.php?vc=' . $cod_vcard;
              $pagina2 = $mi_directorio.'2.php?vc=' . $cod_vcard;
              $pagina3 = $mi_directorio.'3.php?vc=' . $cod_vcard;
              $redireccionamiento_contacto = $url_redireccionamiento_contacto.'index.php?vc=' . $cod_vcard;
              $redireccionamiento_galeria  = $url_redireccionamiento_galeria.'index.php?vc=' . $cod_vcard; */


            //if( empty( $row['cod_vcard'] ) ) {
            $var_asoc_vcard = "VCard_Asociada";
            $var_cod_asc_vcard = $rfc_empresa.'_'.$var_asoc_vcard;
            $update = ("UPDATE myclientes SET cod_vcard='" . $cod_vcard . "', migrar_descarga_vcard='" . $var_cod_asc_vcard . "', pagina_descarga='" . $pagina1 . "', pagina_compartir='" . $pagina2 . "', pagina_galeria='" . $pagina3 . "', redireccionamiento_contacto='" . $redireccionamiento_contacto . "', redireccionamiento_galeria='" . $redireccionamiento_galeria . "', correlativo = " . $correlativo . "  WHERE id=" . $row['id']);
            $resultado = mysqli_query($con, $update);
            //}
            //Creando Usuarios al Sistema
            $checkemail = ("SELECT rfc, tipo_cliente_empresa FROM users WHERE rfc='" . trim($row['rfc']) . "' LIMIT 1 ");
            $ca = mysqli_query($con, $checkemail);
            $cant = mysqli_num_rows($ca);
            $pass = substr("$cod_vcard", 0, -2);
            $rango_users = "Cliente";
            $tipo_cliente = "Jefe";
            if ($cant == 0) {

                $dat = mysqli_fetch_array($ca);
                if (trim($rfc) !='') {
                    $creando_user = ("INSERT INTO users
                     (
                        user,
                        pass,
                        nombre_apellido, 
                        rfc,
                        rango_imagenes, 
                        rango_video, 
                        rango_audios,
                        rango_arch_descargables,
                        rango_users,
                        estatus_metrica,
                        empresa,
                        tipo_cliente_empresa,
                        ciudad_cliente,
                        cod_vcard, 
                        rfc_empresa ) VALUES ('$cod_vcard','$pass','$name','$rfc','$rango_img','$rango_video','$rango_audios','$rango_arch_descargable','$rango_users','$estatus_metrica','$empresa','$tipo_cliente','$city','$cod_vcard','$rfc_empresa')");
                    $result_users = mysqli_query($con, $creando_user);
                }
            }

        }

        /*************AGREGANDO REGISTROS TEMPORALES****************/
        $reg_temp = ("INSERT INTO clientes_temporales (user, pass, name,  empresa, cod_vcard, cod_distribuidor, cargo, direccion,  dir_l2, dir_l3, cp, ciudad_compa, telefono, tlf_movil, logo_grand,  web_cliente, email, pag1, pag2, pag3) VALUES ('$cod_vcard','$pass','$name','$empresa','$cod_vcard','$cod_distribuidor','$cargo','$direccion','$dir_l2','$dir_l3','$cp','$ciudad_compa','$telefono','$movil','$logo_grand','$web_cliente','$email','$pagina1','$pagina2','$pagina3')");
        $result_ = mysqli_query($con, $reg_temp);

        /*************AGREGANDO EL CLIENTE A LA TABLA ASOCIAR VCARD***************/
        if($rfc !=""){
        $cliente_asc_vcard = ("INSERT INTO asociar_vcard (cod_vcard, rfc_empresa, migrar_descarga_vcard) VALUES ('$cod_vcard','$rfc_empresa','$var_cod_asc_vcard')");
        $result_cliente_asc_vcard = mysqli_query($con, $cliente_asc_vcard);
    } } 
} else {
    echo "<h2 style='text-align:center;'>Debe Seleccinar un Registro.</h2>";
}
header('Location: home_ventana_modal.php');
exit;
?>
