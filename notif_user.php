<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
    $id_user_online = $_SESSION['id'];
    $cod_vcard_client = $_SESSION['cod_vcard'];
    ?>
    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">

            <!----js para mostrar msj--->
            <script  src="asset/js/jquery.min.js"></script>
            <script src="asset/js/msj.js"></script>

        </head>

        <body id="mimin" class="dashboard">
            <?php include('menu_header_user.php'); ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>

                <div id="content">
                    <br>
                    <?php
                    $no = 'NO';
                    $si  = 'SI';
                    $Consultar = ("SELECT * FROM notificaciones WHERE (para='".$cod_vcard_client."' OR para='Todos')  ORDER BY id ASC");
                    $numero_servicios = mysqli_query($con, $Consultar);
                    ?>
                    <div class="col-md-12 top-20 padding-0">
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-heading"><h3 style="text-align: center;">MIS 
                                        <strong style="color: crimson;">"NOTIFICACIONES"</strong></h3></div>
                                <div class="panel-body">
                                    <div class="responsive-table">
                                        <table  class="table table-striped table-bordered" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>N°</th>
                                                    <th>Notificación</th>
                                                    <th>Fecha</th>
                                                    <th>Opción</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                while ($vcard = mysqli_fetch_array($numero_servicios)) {
                                                    $table = "notificaciones";
                                                    $id = $vcard['id'];
                                                    $us ="user";
                                                    ?>
                                                    <tr>
                                                        <td style="text-align: center;"><?php echo $vcard['id']; ?></td>
                                                        <td style="text-align: center;"><?php echo $vcard['notificacion']; ?></td>
                                                        <td style="text-align: center;"><?php echo $vcard['fecha']; ?></td>
                                                        <td style="text-align: center; font-size: 25px;"><a href="view_notif.php?id=<?php echo $id; ?>"> 
                                                        <span class="fa icon-eye" title="Ver Notificación"></span></a>
                                                        <a href="delete.php?id=<?php echo $id; ?>&table=<?php echo $table; ?>&delete=<?php echo $us; ?>"> 
                                                        <span class="fa fa-trash" title="Eliminar Notificación"></span></a>
                                                        <?php
                                                        if($vcard['leido']=='NO'){
                                                        ?>
                                                            <span class="fa icon-close" style="color:red;" title="Notificación sin Leer"></span>
                                                        <?php } else { ?>
                                                            <span class="fa icon-check" style="color:green;" title="Notificación Leida"></span>
                                                      <?php  } ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <?php
                        @mysqli_close($numero_servicios);
                        ?>  
                    </div> 

                    <div class="contenedor_flotante">                         
                    <?php
                        if(!empty($_GET['msj'])){ ?>
                        <div class='col-md-12'>
                        <div class='alert col-md-12 col-sm-12 alert-icon alert-danger alert-dismissible fade in' role='alert'>
                            <div class='col-md-2 col-sm-2 icon-wrapper text-center'>
                            <span class='fa fa-flash fa-2x'></span></div>
                            <div class='col-md-10 col-sm-10'>
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>
                                <p><strong>Felicitaciones la Notificacion fue Borrado Correctamente.</strong></p>
                            </div>
                            </div>
                        </div> 
                <?php } ?>
                    </div>
                    
                </div>
            </div>


            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse" > 
                <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

            <?php include('js.html'); ?>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>