<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
    $cod_vcard_cliente  = $_SESSION['cod_vcard'];
    $rfc_empresa        = $_SESSION['rfc_empresa'];
    $rango_arch_desc    = $_SESSION['rango_arch_descargables'];

    if (isset($_POST['enviar'])) {
        $msj_exito = "";
        $msj_borrar = "";
        //Validamos que el archivo exista
        foreach ($_FILES["archivo"]['tmp_name'] as $key => $tmp_name) {
            //Validamos que el archivo exista
            if ($_FILES["archivo"]["name"][$key]) {

                $titulo_arch = $_POST['titulo_archivo'];
                $estado = $_POST['estado'];
                $archivo = $_FILES['archivo']['name'][$key];
                $explode = explode('.', $archivo);
                $extension = array_pop($explode);

                $filename = $_FILES["archivo"]["name"][$key]; //Obtenemos el nombre original del archivo
                $source = $_FILES["archivo"]["tmp_name"][$key]; //Obtenemos un nombre temporal del archivo
                //Validamos si la ruta de destino existe, en caso de no existir la creamos
                $carp = "/Arch_Descargable/";
                $directorio = "Galeria_Clientes/";
                $folder = $directorio . $cod_vcard_cliente . '/' . $carp;


                if (!file_exists($folder)) {
                    mkdir($folder, 0777, true);
                }

                $total_arch = count(glob($folder . '/{*.xlsx,*.docx,*.pdf,*.pptx,*.zip,*.rar}', GLOB_BRACE));
                $new_filename = $total_arch + 1;
                $name_archivo = "archivo_" . $total_arch . '.';
                $mi_file = $name_archivo . $extension;


                $dir = opendir($folder); //Abrimos el directorio de destino
                $target_path = $folder . '/' . $mi_file; //Indicamos la ruta de destino, como el nombre del archivo
                //Movemos y validamos que el archivo se haya cargado correctamente
                //El primer campo es el origen y el segundo el destino
                if (move_uploaded_file($source, $target_path)) {
                    $add = "INSERT INTO archivos_descargables (filename,titulo_archivo,cod_vcard,rfc_empresa,estado) VALUES ('$mi_file','$titulo_arch','$cod_vcard_cliente','$rfc_empresa','$estado')";
                    $result = mysqli_query($con, $add);
                    $msj_exito = "
                        <div class='col-md-12'>
                        <div class='alert alert-success col-md-12 col-sm-12  alert-icon alert-dismissible fade in' role='alert'>
                        <div class='col-md-2 col-sm-2 icon-wrapper text-center'>
                            <span class='fa fa-check fa-2x'></span></div>
                            <div class='col-md-10 col-sm-10'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                            <span aria-hidden='true'>x</span></button>
                            <p><strong>Felicitaciones el Registro fue un Exito.</strong></p>
                            </div>
                        </div>
                        </div>";
                   // header("location:arch_descargable.php");
                   // exit();
                } else {
                    echo "Error en el Registro . .";
                }
                closedir($dir); //Cerramos el directorio de destino
            }
        }
    }
    ?>

    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">

                        <!----js para mostrar msj--->
        <script  src="asset/js/jquery.min.js"></script>
        <script src="asset/js/msj.js"></script>
        </head>
        <body id="mimin" class="dashboard">
            <?php include('menu_header_user.php'); ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>
                 <?php
                    $Consultar = ("SELECT * FROM archivos_descargables WHERE rfc_empresa='" . $rfc_empresa . "' ORDER BY id ASC ");
                    $numero_servicios = mysqli_query($con, $Consultar);
                    $total_arch_permitidos = mysqli_num_rows($numero_servicios);
                ?>
                <div id="content">
                    <br>
                    <div class="col-md-12">
                        <div class="col-md-12 panel">
                            <div class="col-md-12 panel-heading">
                                <h4 style="text-align: center; color: black;"> Subir Archivo<strong style="color:crimson;" id="arch"> PDF, Word, PowerPoint, Excel, Winrar </strong> para descargar.</h4>
                                <br><br>

                                <p style="text-align:right;"><a href="home.php" title="Volver">
                                        <span class="icon-action-undo" title="Volver">Volver</span>
                                    </a>
                                </p>
                            </div>
                            
                            
                            <div class="col-md-12 panel-body">
                                    <div class="col-md-12">
                            <form  enctype="multipart/form-data" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                                    <div class="col-md-6">
                                        <div class="form-group form-animate-text">
                                            <div class="input-group fileupload-v1">
                                                <input type="file" id="archivo[]" name="archivo[]"  required="required" class="fileupload-v1-file hidden">
                                                <input type="text" class="form-control fileupload-v1-path" placeholder="Archivo . . . ." disabled>
                                                <span class="input-group-btn">
                                                    <button class="btn fileupload-v1-btn" type="button"><i class="fa fa-folder"></i> Presione Examinar</button>
                                                </span>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                            <label id="title_label">NOMBRE O TITULO DEL ARCHIVO</label>
                                            <div class="form-group form-animate-text">
                                                <input type="text" class="form-text" id="validate_firstname" name="titulo_archivo" autocomplete="off">
                                            </div>
                                        </div>

                                    <br><br>
                                    <br><br><br>
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <p style="text-align: center; color: crimson">Si tu Archivo es un PDF debes elejir una opci&oacute;n</p>

                                            <div class="input-group fileupload-v1">
                                                <input type="radio" name="estado" value="solodescarga" checked> Solo Mostrar Descarga<br>
                                                <input type="radio" name="estado" value="vistaprevia"> Mostra Vista Previa del PDF<br>
                                            </div>
                                        </div>
                                        <?php if($total_arch_permitidos < $rango_arch_desc){ ?>
                                        <div class="col-md-6">
                                                <button class="btn ripple btn-raised btn-success" name="enviar">
                                                    <div>
                                                        <span>Agregar Archivo</span>
                                                    </div>
                                                </button>
                                            
                                         <?php }  else{ ?>

                                        <div class="parpadea text">
                                            <strong>Ya excedio el Limite de Archivos, debe borra para poder agregar de nuevo.</strong>
                                        </div>
                                        </div>
                                  <?php  } ?>
                                    </div>
                            </form>
                        </div>
                    </div>



                    <div class="col-md-12 top-20 padding-0">
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 style="text-align: center;">Lista de Archivos <strong style="color: crimson;">"Descargables"</strong></h3>
                                    <div id="cont_total">
                                    <h5 style="border-bottom: 1px dashed #0099cc; width:150px; margin-top:-40px;">Total de Archivos <strong><?php echo $total_arch_permitidos; ?></strong></h5>
                                    <h5 style='color:crimson; border-bottom: 1px dashed #0099cc; width:150px;'>Limite de Archivos <strong><?php echo $rango_arch_desc; ?></strong></h5> 
                                    </div>
                                </div>
                                <div class="panel-body">
                                   <div class="table-responsive">
                                        <table class="table table-responsive table-bordered table-hover" id="datatables-example" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>Nombre del Archivo</th>
                                                    <th>Tipo de Archivo</th>
                                                    <th>Opción</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                while ($vcard = mysqli_fetch_array($numero_servicios)) {
                                                    $id = $vcard['id'];
                                                    $carpeta = "Arch_Descargable/";
                                                    $midirectorio = "Galeria_Clientes/";
                                                    $folder_fin = $midirectorio . $cod_vcard_cliente . '/' . $carpeta;
                                                    
                                                    $archivo = $vcard['filename'];
                                                    $explode = explode('.', $archivo);
                                                    $extension_arch = array_pop($explode);
                                                    ?>
                                                    <tr>
                                                        <td>
                                                         <?php echo $vcard['titulo_archivo']; ?>   
                                                        </td>
                                                        <td style="text-align: center">
                                                            <?php
                                                                if($extension_arch =="docx" || $extension_arch =="doc"){?>
                                                            <img src="asset/img/word.png" alt="" width="40px">
                                                               <?php }
                                                               if($extension_arch =="pptx" || $extension_arch =="ppt"){ ?>
                                                            <img src="asset/img/powerpoint.png" alt="" width="40px">
                                                               <?php }
                                                                if($extension_arch =="xlsx" || $extension_arch =="xls"){ ?>
                                                            <img src="asset/img/excel.png" alt="" width="40px">
                                                               <?php }
                                                                if($extension_arch =="pdf"){  ?>
                                                            <img src="asset/img/pdf.png" alt="" width="40px">
                                                               <?php }
                                                                if($extension_arch =="zip" || $extension_arch =="rar"){ ?>
                                                            <img src="asset/img/winrar.png" alt="" width="40px">
                                                               <?php }
                                                            ?>
                                                        </td>
                                                        <td style="text-align: center;">
                                                            <a href="delete_archivo_desc.php?id=<?php echo $id; ?>&directorio=<?php echo $folder_fin; ?>&namearchi=<?php echo $vcard['filename']; ?>">
                                                                <span class="fa fa-trash" style="font-size: 25px" title="Eliminar Imagen"></span>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <?php
                        @mysqli_close($numero_servicios);
                        ?>  
                    </div>

                    <div class="contenedor_flotante">                         
                        <?php
                             echo isset($msj_exito) ? utf8_decode($msj_exito) : ''; 
                             //isset($_GET['msj']) ? utf8_decode($_GET['msj']) : ''; 
                             if(!empty($_GET['msj'])){ ?>
                               <div class='col-md-12'>
                                <div class='alert col-md-12 col-sm-12 alert-icon alert-danger alert-dismissible fade in' role='alert'>
                                    <div class='col-md-2 col-sm-2 icon-wrapper text-center'>
                                    <span class='fa fa-flash fa-2x'></span></div>
                                    <div class='col-md-10 col-sm-10'>
                                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>
                                        <p><strong>Felicitaciones el Archivo fue Borrado Correctamente.</strong></p>
                                    </div>
                                    </div>
                                </div> 

                            <?php } 
                        ?> 
                    </div>

                            
                    </div>          
                </div>
            </div>


            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse" > 
                <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

            <?php include('js.html'); ?>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>