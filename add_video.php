<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
$cod_vcard_cliente = $_SESSION['cod_vcard'];
$ref_empre_cliente = $_SESSION['rfc_empresa'];
$rango_videos       = $_SESSION['rango_video'];

if (isset($_POST['enviar'])) {
    $msj_exito = "";
    $url = $_POST['url'];
	
//https://www.youtube.com/watch?v=J_6TUYXi0PQ&feature=youtu.be  == 60 
//https://m.youtube.com/watch?v=J_6TUYXi0PQ&feature=youtu.be  == 58  desde el movil no funciona
//https://www.youtube.com/watch?v=A44i5rzysuY == 43 normalfunciona
//https://m.youtube.com/watch?v=J_6TUYXi0PQ   == 41
//https://www.youtube.com/watch?v=J_6TUYXi0PQ&app=desktop ==
//https://youtu.be/J_6TUYXi0PQ  == 28

	$cantidad_url_video = strlen($url);
    if ($cantidad_url_video == '28') {
        $cortar_url = str_replace ('https://youtu.be/','',$url);
        $url_final_video = 'https://www.youtube.com/embed/' .$cortar_url; 
        
    }elseif ($cantidad_url_video == '41') {
        $cortar_url = str_replace ('https://m.youtube.com/watch?v=','',$url);
        $url_final_video = 'https://www.youtube.com/embed/' .$cortar_url; 
       
    }elseif ($cantidad_url_video == '43') {
        $cortar_url = str_replace ('https://www.youtube.com/watch?v=','',$url);
        $url_final_video = 'https://www.youtube.com/embed/' .$cortar_url; 

    }elseif ($cantidad_url_video == '58') {
        $cortar_url = str_replace ('https://m.youtube.com/watch?v=','',$url);
        $url_final_video = 'https://www.youtube.com/embed/' .$cortar_url; 
		
    }elseif ($cantidad_url_video == '60') {
        $cortar_url = str_replace ('https://www.youtube.com/watch?v=','',$url);
        $url_final_video = 'https://www.youtube.com/embed/' .$cortar_url; 
		
    }else{
        echo "URL INVALIDA";
    }

    $query = "INSERT INTO videos (url , cod_vcard,rfc_empresa) VALUES ('$url_final_video','$cod_vcard_cliente','$ref_empre_cliente')";
    $result = mysqli_query($con, $query);
    $msj_exito = "
<div class='col-md-12'>
<div class='alert alert-success col-md-12 col-sm-12  alert-icon alert-dismissible fade in' role='alert'>
  <div class='col-md-2 col-sm-2 icon-wrapper text-center'>
    <span class='fa fa-check fa-2x'></span></div>
    <div class='col-md-10 col-sm-10'>
      <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
      <span aria-hidden='true'>x</span></button>
      <p><strong>Felicitaciones el Video fue Registrado Correctamente.</strong></p>
    </div>
  </div>
</div>";

}
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="VCARD">
<meta name="author" content="ALEJANDRO TORRES">
<meta name="keyword" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/png" href="../favicon.png" />
<title>VCARD</title>
<?php include('css.html'); ?>
<link rel="stylesheet" type="text/css" href="asset/css/my_style.css">

<!----js para mostrar msj--->
<script  src="asset/js/jquery.min.js"></script>
<script src="asset/js/msj.js"></script>
</head>

<body id="mimin" class="dashboard">
<?php include('menu_header_user.php'); ?>

<div class="container-fluid mimin-wrapper">
<?php include('menu_lateral_escritorio.php'); ?>
<?php
$Consultar = ("SELECT * FROM videos WHERE cod_vcard='" . $cod_vcard_cliente . "' ORDER BY id ASC LIMIT 3");
$numero_servicios = mysqli_query($con, $Consultar);
$total_video_permitidos = mysqli_num_rows($numero_servicios);
?>

<div id="content">
<br>
<div class="col-md-12">
    <div class="col-md-12 panel">
        <div class="col-md-12 panel-heading">
            <h4 style="text-align: center; color: black;">
                Registrar Nuevo <strong style="color:crimson;">"VIDEO"</strong></h4>
            <br>
        </div>
        <form  enctype="multipart/form-data" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
            <div class="col-md-12 panel-body">
                <div class="col-md-12">
<p>&nbsp;</p>			<div style="border: solid 2px #000000; ">										<h5 style="text-align: center; color: red;">                                     <strong>REFERENCIA</strong>  <strong style='color:crimson;font-size: 16px;'></strong>                                </h5>														<h6 style="text-align: center; color: black;">                                    - Copia la URL de un video de YOUTUBE y pega en el espacio<strong style='color:crimson;font-size: 14px;'></strong>                                </h6>		        </div><p>&nbsp;</p>
                    <div class="col-md-12 panel-body">
                        <div class="col-md-12">
                            <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                <input type="text" class="form-text" id="validate_firstname" name="url" required autocomplete="off">
                                <span class="bar"></span>
                                <label id="title_label">PEGAR LA URL DEL VIDEO DE YOUTUBE</label>
                            </div>
                        </div>
                        <?php if($total_video_permitidos < $rango_videos){ ?>
                        <div class="col-md-12">
                                <div class="col-md-6"  style="float:right;">
                                    <button class="btn ripple btn-raised btn-success" name="enviar">
                                        <div>
                                            <span>Registrar Video</span>
                                        </div>
                                    </button>
                                </div>
                            </div>
                        <?php }  else{ ?>
                            <div class="parpadea text">
                                <strong>Ya excedio el Limite de Video, debe borra para poder agregar de nuevo.</strong>
                            </div>
                      <?php  } ?>
                        
                    </div>
                </div>
            </div>

                
        </form>
    </div>
</div>

<div class="col-md-12 top-20 padding-0">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <h3 style="text-align: center;">Lista de <strong style="color: crimson;">"VIDEOS"</strong></h3>
                <div id="cont_total">
                <h5 style="border-bottom: 1px dashed #0099cc; width:150px; margin-top:-40px;">Total de Videos <strong><?php echo $total_video_permitidos; ?></strong></h5>
                <h5 style='color:crimson; border-bottom: 1px dashed #0099cc; width:150px;'>Limite de Videos <strong><?php echo $rango_videos; ?></strong></h5> 
            </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-responsive table-bordered table-hover" id="datatables-example" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Video</th>
                                <th>Opción</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            while ($vcard = mysqli_fetch_array($numero_servicios)) {
                                $table = "videos";
                                $id = $vcard['id'];
                                ?>
                                <tr>
                                    <td style="text-align: center;"><iframe src="<?php echo $vcard['url']; ?>" frameborder="0" id="tamano_img_table"></iframe></td>
                                    <td style="text-align: center; font-size: 25px;"><a href="delete.php?id=<?php echo $id; ?>&table=<?php echo $table; ?>"> <span class="fa fa-trash" title="Eliminar Video"></span></a></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>  
    <?php
    @mysqli_close($numero_servicios);
    ?>  
</div> 

<div class="contenedor_flotante">                         
<?php
    echo isset($msj_exito) ? utf8_decode($msj_exito) : ''; 
    if(!empty($_GET['msj'])){ ?>
        <div class='col-md-12'>
         <div class='alert col-md-12 col-sm-12 alert-icon alert-danger alert-dismissible fade in' role='alert'>
             <div class='col-md-2 col-sm-2 icon-wrapper text-center'>
             <span class='fa fa-flash fa-2x'></span></div>
             <div class='col-md-10 col-sm-10'>
                 <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>
                 <p><strong>Felicitaciones el Video fue Borrado Correctamente</strong></p>
             </div>
             </div>
         </div> 

     <?php } 
?>
</div>
</div>
</div>


<!-- start: Mobile -->
<div id="mimin-mobile" class="reverse" > 
<?php include('menu_movil.php'); ?>
</div>
<button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
<span class="fa fa-bars"></span>
</button>
<!-- end: Mobile -->

<?php include('js.html'); ?>
</body>
</html>
<?php
} else {
include('error.php');
}
?>