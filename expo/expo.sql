-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 19-10-2019 a las 14:20:26
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `new_card`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `expo`
--

CREATE TABLE IF NOT EXISTS `expo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_expo` varchar(100) NOT NULL,
  `estatusCliente` varchar(100) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `nombre_paterno` varchar(100) NOT NULL,
  `nombre_materno` varchar(100) NOT NULL,
  `edad` varchar(20) NOT NULL,
  `profesion` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `tfl_movil` varchar(20) NOT NULL,
  `tlf_fijo` varchar(30) NOT NULL,
  `empresa` varchar(250) NOT NULL,
  `cargo` varchar(100) NOT NULL,
  `ciudad` varchar(100) NOT NULL,
  `pais` varchar(100) NOT NULL,
  `interesado` varchar(200) NOT NULL,
  `comentario` text NOT NULL,
  `stand` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Volcado de datos para la tabla `expo`
--

INSERT INTO `expo` (`id`, `cod_expo`, `estatusCliente`, `nombre`, `nombre_paterno`, `nombre_materno`, `edad`, `profesion`, `email`, `tfl_movil`, `tlf_fijo`, `empresa`, `cargo`, `ciudad`, `pais`, `interesado`, `comentario`, `stand`) VALUES
(1, 'YXZ', 'ClienteVisitante', 'nombre', 'paterno', 'mate', '444', 'inge', 'ema@gmail.com', '4', '4', 'em', 'cargo', 'city', 'pais', 'inte', 'comen', ''),
(2, 'YXZ', 'ClienteVisitante', '7', '7', '7', '7', '7', '7', '7', '7', '7', '7', '7', '7', '7', '7', ''),
(5, 'YXZ', 'ClienteVisitante', 'm', 'm', 'm', '', 'm', 'm', 'm', 'm', '', 'm', 'm', 'm', 'm', 'm', ''),
(7, 'YXZ', 'ClienteVisitante', 'nnn,', 'n,', 'n', '', ',n', ',', 'n', 'nn', 'n', 'n', 'n', 'n', 'n', 'n', ''),
(20, '', 'ClienteJefeStand', 'Alex Rondon', 'apellido', 'apellido materno', '', '', 'VCARD', '21', '1', 'Alejandro Torres', 'gerente', '', '', '', '', 'N1\r\n'),
(21, '', 'ClienteJefeStand', 'Alexandra J.', 'apellido', 'apellido materno', '', '', 'SAMSUNG', '5445', '212', 'Blanca de Torres', 'aseo', '', '', '', '', 'N2\r\n'),
(22, 'matias', 'ClienteVisitante', 'u', 'u', 'u', '', 'u', 'uu', '', '', 'u', 'uu', 'u', 'uu', 'u', 'u', ''),
(23, 'hola', 'ClienteVisitante', 'k', 'k', 'k', '', 'k', 'k', '', '', 'k', 'k', 'k', 'k', 'k', 'k', ''),
(24, 'matias', 'ClienteVisitante', 'n', 'n', 'nn', '', 'n', 'n', '', '', '', 'n', 'n', 'n', 'n', 'n', ''),
(25, '0', 'ClienteVisitante', 'Andres ', 'h', 'n', '8', 'medico', 'u@gmail.com', '678', '856', 'samsung', 'obrero', 'xy', 'Argentina', '', '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
