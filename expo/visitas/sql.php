<div id="resultado">
<?php
$cd = !empty($_REQUEST['cd']) ? $_REQUEST['cd'] : '';
$st = !empty($_REQUEST['st']) ? $_REQUEST['st'] : '';

$sql = ("SELECT
    e.id, 
    e.nombre, 
    e.empresa,
    f.id,
    f.cod_evento,
    f.stan,
    f.id_cliente_visita_expo 
FROM expo e, filtro_visitas_expo f
WHERE e.id = f.id_cliente_visita_expo AND f.stan='".$st."'  ");

$mostar = mysqli_query($con, $sql);
$total_client = mysqli_num_rows($mostar); ?>
    
<div class="col-md-7">
        <div class="col-md-12 panel" style="padding: 0px 5px 5px 5px">
            <div class="col-md-12 panel-heading" style="background-color: lightgrey;">
                <h4 style="text-align: center;color:#1cb39b;font-weight: bold; "> 
                    MIS VISITANTES <strong style="color: #333; float: right;">Total (<?php echo $total_client ?>)</strong>&nbsp;</h4>
            </div>
            <br><br>
            <br><br>

    <table class="table  table-responsive table-bordered table-hover" id="datatables-example">
         <thead>
                <tr>
                    <th>ID VISITANTE</th>
                    <th>EVENTO</th>
                    <th>NOMBRE</th>
                    <th>EMPRESA</th>
                    <th>STAND</th>
                </tr>
            </thead>
            <tbody>
            <?php
                while ($row_expo = mysqli_fetch_array($mostar)) { ?>
                    <tr>
                        <td><?php echo $row_expo['id_cliente_visita_expo']; ?></td>
                        <td><?php echo $row_expo['cod_evento']; ?></td>
                        <td><?php echo $row_expo['nombre']; ?></td>
                        <td>--</td>
                        <td><?php echo $row_expo['stan']; ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        
    </div>
</div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        $('#datatables-example').DataTable();
    });
</script>