<?php
    $cd = !empty($_REQUEST['cd']) ? $_REQUEST['cd'] : '';
    $st = !empty($_REQUEST['st']) ? $_REQUEST['st'] : '';
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="VCARD">
    <meta name="author" content="ALEJANDRO TORRES">
    <meta name="keyword" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="atandrastoth,belfast,uk,northern ireland,programing,developer,web,design,webdesign,php,html,css,jquery,javascript,sql,mssql,mysql,visual basic,nails,fashion,c#,wpf,xaml" />

    <link rel="shortcut icon" type="image/png" href="../favicon.png" />
    <title>VCARD</title>
    <link rel="stylesheet" type="text/css" href="https://www.vcard.mx/_/asset/css/bootstrap.min.css">

    <!-- plugins -->
    <link rel="stylesheet" type="text/css" href="https://www.vcard.mx/_/asset/css/plugins/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://www.vcard.mx/_/asset/css/plugins/simple-line-icons.css"/>
    <link rel="stylesheet" type="text/css" href="https://www.vcard.mx/_/asset/css/plugins/animate.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://www.vcard.mx/_/asset/css/plugins/datatables.bootstrap.min.css"/>
    <link href="https://www.vcard.mx/_/asset/css/style.css" rel="stylesheet">

    <!-----css del scan--->
    <link rel="stylesheet" href="css/stylewebcam.css">

    <!-- end: Css -->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
</head>
<body id="mimin" class="dashboard">

<!----MENU HEARD---->
<nav class="navbar navbar-default header navbar-fixed-top">
<div class="col-md-12 nav-wrapper">
    <div class="navbar-header" style="width:100%;">
        <div class="opener-left-menu is-open">
            <span class="top"></span>
            <span class="middle"></span>
            <span class="bottom"></span>
        </div>
        <a href="scan_visitas.php" class="navbar-brand"> 
            <b>VCARD</b>
        </a>

        <ul class="nav navbar-nav navbar-right user-nav">
            <li class="user-name"><span>VCard</span></li>
            <li class="dropdown avatar-dropdown">
                <img src="https://www.vcard.mx/_/asset/img/img_vcard.png"style="width: 50px; border: 2px solid #108DB6 !important;" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"/>
            </li>
        </ul>
    </div>
</div>
</nav>
<!--fin-->

<br>
<br>
<div class="container-fluid mimin-wrapper">
<div id="content" class="center" style="padding-left: 0px !important;">

    <div class="col-md-5"  id="QR-Code">
        <div class="col-md-12 panel">
            <div class="col-md-12 panel-heading" style="background-color: lightgrey;">
                <h4 style="text-align: center;color:#333;font-weight: bold; "> ESCANEAR VISITANTE&nbsp;</h4>
            </div>
            <br><br>
            <br><br>

            <div class="col-md-12">
                  <div class="col-md-12">
                    <div class="form-group form-animate-text">

                    <div class="well">
                    <canvas width="420" height="240" id="webcodecam-canvas"></canvas>
                    <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
                    <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
                    <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
                    <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>

                </div>
                <br>
                <input type="text" name="cd"id="cd" value="<?php echo $cd; ?>" style="display: none;">
                <input type="text" name="st" id="st" value="<?php echo $st; ?>" style="display: none;">
                <div class="well" style="position: relative;display: inline-block;">
                   <select class="form-control" id="camera-select"></select>
                    <button title="Decode Image" class="btn btn-default btn-sm" id="decode-img" type="button" style="display: none;" data-toggle="tooltip"><span class="glyphicon glyphicon-upload"></span></button>
                    <button title="Image shoot" class="btn btn-info btn-sm disabled" id="grab-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-picture"></span></button>
                    <button title="Play" class="btn btn-success btn-sm" id="play" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-play"></span></button>
                    <button title="Pause" class="btn btn-warning btn-sm" id="pause" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-pause"></span></button>
                    <button title="Stop streams" class="btn btn-danger btn-sm" id="stop" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-stop"></span></button>
                 </div>

                    </div>
                </div>


                    <div class="col-md-6">
                      <p style="text-align: center;font-size: 17px; color: green;" id="scanned-QR" style="overflow-wrap: anywhere !important;"></p>
                        <div class="thumbnail" style="webkit-box-shadow: none; box-shadow: none;" id="result">
                            <div class="well" style="overflow: hidden;">
                                <img src="user.png" width="420" height="240" id="scanned-img">
                            </div>
                        </div>
                    </div>                
            </div>

            </div>
        </div>



<div id="resultado">
<?php
include('config.php');
include('sql.php');
?>
</div>



</div>
</div>
        



<!-- start: Javascript -->
<!--<script type="text/javascript" src="https://www.vcard.mx/_/asset/js/jquery.min.js"></script>
<script type="text/javascript" src="https://www.vcard.mx/_/asset/js/jquery.ui.min.js"></script>
<script type="text/javascript" src="https://www.vcard.mx/_/asset/js/bootstrap.min.js"></script>-->

<!---js para scaner---->
<script type="text/javascript" src="js/webcam/qrcodelib.js"></script>
<script type="text/javascript" src="js/webcam/webcodecamjs.js"></script>
<script type="text/javascript" src="js/webcam/main.js"></script>


<!-- plugins -->
<!--<script type="text/javascript" src="https://www.vcard.mx/_/asset/js/plugins/moment.min.js"></script>-->
<script type="text/javascript" src="https://www.vcard.mx/_/asset/js/plugins/jquery.datatables.min.js"></script>
<script type="text/javascript" src="https://www.vcard.mx/_/asset/js/plugins/datatables.bootstrap.min.js"></script>

<!-- custom -->
<!--<script type="text/javascript" src="https://www.vcard.mx/_/asset/js/main.js"></script>--->


        </body>
    </html>