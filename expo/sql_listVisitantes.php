<?php 
$sql = ("SELECT * FROM expo ");
$mostar = mysqli_query($con, $sql);
$total_client = mysqli_num_rows($mostar); ?>
    
<div class="col-md-5">
        <div class="col-md-12 panel" style="padding: 0px 5px 5px 5px">
             <div class="col-md-12 panel-heading" style="background-color: lightgrey;">
                <h4 style="text-align: center;color:#1cb39b;font-weight: bold; "> 
                    TABLA DE VISITANTES <strong style="color: #333; float: right;">Total (<?php echo $total_client ?>)</strong>&nbsp;</h4>
            </div>
            <br><br>
            <br><br>

    <table class="table  table-responsive table-bordered table-hover" id="datatables-example">
         <thead>
                <tr>
                    <th>ID</th>
                    <th>NOMBRE</th>
                    <th>EMPRESA</th>
                </tr>
            </thead>
            <tbody>
            <?php
                while ($row_expo = mysqli_fetch_array($mostar)) { ?>
                    <tr>
                        <td><?php echo $row_expo['id']; ?></td>
                        <td><?php echo $row_expo['nombre']; ?></td>
                        <td><?php echo $row_expo['empresa']; ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        
    </div>
</div>
</div>


<script type="text/javascript">
$(document).ready(function () {
    $('#datatables-example').DataTable();
});
</script>