<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="VCARD">
<meta name="author" content="ALEJANDRO TORRES">
<meta name="keyword" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/png" href="../favicon.png" />
<title>VCARD</title>
<link rel="stylesheet" type="text/css" href="http://vcard.mx/_/asset/css/bootstrap.min.css">

<!-- plugins -->
<link rel="stylesheet" type="text/css" href="http://vcard.mx/_/asset/css/plugins/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="http://vcard.mx/_/asset/css/plugins/simple-line-icons.css"/>
<link rel="stylesheet" type="text/css" href="http://vcard.mx/_/asset/css/plugins/animate.min.css"/>
<link rel="stylesheet" type="text/css" href="http://vcard.mx/_/asset/css/plugins/datatables.bootstrap.min.css"/>
<link href="http://vcard.mx/_/asset/css/style.css" rel="stylesheet">
<!-- end: Css -->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script  src="http://vcard.mx/_/asset/js/jquery.min.js"></script>
</head>
<body id="mimin" class="dashboard">

<!----MENU HEARD---->
<nav class="navbar navbar-default header navbar-fixed-top">
<div class="col-md-12 nav-wrapper">
<div class="navbar-header" style="width:100%;">
<div class="opener-left-menu is-open">
    <span class="top"></span>
    <span class="middle"></span>
    <span class="bottom"></span>
</div>
<a href="index.php" class="navbar-brand"> 
    <b>VCARD</b>
</a>

<ul class="nav navbar-nav navbar-right user-nav">
    <li class="user-name"><span>VCard</span></li>
    <li class="dropdown avatar-dropdown">
        <img src="http://vcard.mx/_/asset/img/img_vcard.png"style="width: 50px; border: 2px solid #108DB6 !important;" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"/>
    </li>
</ul>
</div>
</div>
</nav>
<!--fin-->
<br>
<br>
<div class="container-fluid mimin-wrapper">
<div id="content" class="center" style="padding-left: 0px !important;">

<div class="col-md-12">
<div class="col-md-12 panel">
    <div class="col-md-12 panel-heading" style="background-color: lightgrey;">
        <h4 style="text-align: center;color:#333;font-weight: bold; "> REGISTRO DE VISITANTES&nbsp;</h4>
    </div>
    <br><br>
    <br><br>



<form   method="post" action="recib_editFormVisitante.php">
            <div class="col-md-12 panel-body">
                <div class="col-md-12">
                    <div id="dynamicDiv">

            <?php 
            include('config.php');
            $codEvento ="EX-02";
            $queryCampos = ("SELECT * FROM visitantexpo WHERE cod_expo='".$codEvento. "'  ORDER BY id DESC Limit 1");
            $resulCampos = mysqli_query($con, $queryCampos);
            $CantidadCampos = mysqli_num_rows($mostar);
            while ($filacampos = mysqli_fetch_array($resulCampos)) {
                $CantidadCamposUsados =$filacampos["cantCamposUsudos"];
                for ($i = 1; $i <= $CantidadCamposUsados; ++$i)
                {
                 $t = ($filacampos["campo".$i]);  ?>
                    <p>
                        <div class="col-md-6" id="eliminar">
                            <label>CAMPO <?php echo $i;?></label>
                                <div class="form-group form-animate-text">
                                    <input type="text" name=campo[] value="<?php echo $t;?>" style="width:60%; color:#333; font-weight:bold;" />
                                        <a style="margin:0px 0px 0px 5px;" class="btn btn-danger" href="javascript:void(0)" id="removerInput">
                                        <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
                                        Eliminar
                                    </a>
                            </div>
                        </div>
                    </p>
                   <?php    }
                    }  ?>

                                           
                </div>
                                           
                <input type="hidden" name="cod_expo" value="<?php echo $codEvento?>">
                <div id="botonEnviar">
                   <div><input type="submit" class="btn ripple btn-raised btn-success" value="Guardar Modificaciones del Formulario para Visitantes" name="Guardar Modificaciones del Formulario para Visitantes"/></div>
                                    
                </div>
                     
                    </div>
                    </div>
                </form>












<?php
$sqlForm = ("SELECT * FROM visitantexpo ORDER BY id DESC Limit 1");
$QueryForm = mysqli_query($con, $sqlForm);
?>
<form method="post" id="formulario_visitante" name="formulario_visitante">  
<?php
while ($valores = mysqli_fetch_array($QueryForm)) { ?>

<div class="col-md-12 panel-body">
<div class="col-md-12">
<div class="col-md-12 panel-body">
    <div class="col-md-6">
    <label>
    <?php if (!empty($valores['campo1'])) {
        echo $valores['campo1'];
    } ?></label>
        <div class="form-group form-animate-text">
            <input type="text" class="form-text" id="validate_firstname" name="campo1" autocomplete="off">
            <span class="bar"></span>
            <input type="hidden" name="cod_expo" value="<?php echo $valores['cod_expo']; ?>">
        </div>
    </div>


    <div class="col-md-6">
    <label>
    <?php if (!empty($valores['campo2'])) {
        echo $valores['campo2'];
    } ?>
    </label>
        <div class="form-group form-animate-text">
            <input type="text" class="form-text" id="validate_firstname" name="campo2"  autocomplete="off">
            <span class="bar"></span>
        </div>
    </div>

</div>
<div class="col-md-12 panel-body">
    <div class="col-md-6">
    <label>
    <?php if (!empty($valores['campo3'])) {
        echo $valores['campo3'];
    } ?>
    </label>
        <div class="form-group form-animate-text">
            <input type="text" class="form-text" id="validate_firstname" name="campo3" autocomplete="off">
            <span class="bar"></span>
        </div>
    </div>

    <div class="col-md-6">
    <label>
    <?php if (!empty($valores['campo4'])) {
        echo $valores['campo4'];
    } ?>
    </label>
        <div class="form-group form-animate-text">
            <input type="text" class="form-text" id="validate_firstname" name="campo4"  autocomplete="off">
            <span class="bar"></span>
        </div>
    </div>

</div>
<div class="col-md-12 panel-body">
    <div class="col-md-6">
    <label>
    <?php if (!empty($valores['campo5'])) {
        echo $valores['campo5'];
    } ?>
    </label>
        <div class="form-group form-animate-text">
            <input type="text" class="form-text" id="validate_firstname" name="campo5" autocomplete="off">
            <span class="bar"></span>
        </div>
    </div>


    <div class="col-md-6">
    <label>
    <?php if (!empty($valores['campo6'])) {
        echo $valores['campo6'];
    } ?>
    </label>
        <div class="form-group form-animate-text">
            <input type="text" class="form-text" id="validate_firstname" name="campo6"  autocomplete="off">
            <span class="bar"></span>
           
        </div>
    </div>

</div>

</div>
</div>

<div class="col-md-12 panel-body">
<div class="col-md-12">
<div class="col-md-6">
     <input type="button" id="botonocultamuestra" value="Crear Mas Campos" class="btn ripple btn-raised btn-success"/>
</div>

<div class="col-md-6"  style="float:right;">
        <button class="btn ripple btn-raised btn-success" name="enviar">
            <div>
                <span>Crear Formulario de Visitantes</span>
            </div>
        </button>
    </div>
</div>
</div>

<?php } ?>

</form>
</div>
</div> 

</div>
</div>




<!-- start: Javascript -->
<script type="text/javascript" src="http://vcard.mx/_/asset/js/jquery.min.js"></script>
<script type="text/javascript" src="http://vcard.mx/_/asset/js/jquery.ui.min.js"></script>
<script type="text/javascript" src="http://vcard.mx/_/asset/js/bootstrap.min.js"></script>

<!-- plugins -->
<script type="text/javascript" src="http://vcard.mx/_/asset/js/plugins/moment.min.js"></script>
<script type="text/javascript" src="http://vcard.mx/_/asset/js/plugins/jquery.datatables.min.js"></script>
<script type="text/javascript" src="http://vcard.mx/_/asset/js/plugins/datatables.bootstrap.min.js"></script>
<script type="text/javascript" src="http://vcard.mx/_/asset/js/plugins/jquery.nicescroll.js"></script>
<script type="text/javascript" src="http://vcard.mx/_/asset/js/plugins/jquery.vmap.min.js"></script>
<script type="text/javascript" src="http://vcard.mx/_/asset/js/plugins/jquery.vmap.sampledata.js"></script>

<!-- custom -->
<script type="text/javascript" src="http://vcard.mx/_/asset/js/main.js"></script>

<script type="text/javascript">
$(document).ready(function () {
$('#datatables-example').DataTable();
});
</script>
</body>
</html>