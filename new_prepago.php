<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
    ?>
<!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">

            <!----js para mostrar msj--->
            <script  src="asset/js/jquery.min.js"></script>
            <script src="asset/js/msj.js"></script>
        </head>

        <body id="mimin" class="dashboard">
            <?php 
               include('menu_header.php');
             ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>

                <div id="content">
                    <br>
    <?php
    if (isset($_POST['enviar'])) {
        $cantida_prepago   = $_POST['cantidad'];
        $tipo_prepago      = $_POST['radiobotton'];
        $cod_distribuidor  = $_POST['cod_distribuidor'];
        //start = VC91
        //elite = VC96

        $suma = ("SELECT * FROM myclientes WHERE cod_vcard  LIKE '%$tipo_prepago%' ORDER BY cod_vcard DESC LIMIT 1 "); 
        $numero_vis = mysqli_query($con, $suma);
        $total = mysqli_fetch_array($numero_vis);
        $codigo_prepago = $total["cod_vcard"];
        //$codigo_prepago = "VC910001";
    for ($valor = 1; $valor <= $cantida_prepago; $valor++){
        $resultado = (int) substr($codigo_prepago, 2,6);
        
        $resultado += $valor;
        $var = "VC"; 
        $valor_resultado = $var.$resultado;
        //$resultado += 1; incrementando el valor de $resultado
        //$resultado = "910001"; 
        
        $correlativo     = "1";
        $status_vcard    = "Inactiva";
        $status_metricas = "NO";
        $rango_imgs      = "20";
        $rango_video     = "3";
        $rango_audio     = "3";
        $rango_users     = "Cliente";
        $rango_arch_descargable = "3";
        $pagina1         = 'https://vcard.mx/_/1.php?vc=' . $valor_resultado;
        $pagina2         = 'https://vcard.mx/_/2.php?vc=' . $valor_resultado;
        $pagina3         = 'https://vcard.mx/_/3.php?vc=' . $valor_resultado;
        $texto_whatsapp  = "Pongo a su disposición mi tarjeta de presentación electrónica. Favor de dar *click* en el siguiente enlace:";
        $texto           = "Abrir el archivo descargado y Click en Guardar";
        $redirc_c        = "http://Compartir-Contacto.vcard.mx/c.php?vc=";
        $redirc_g        = "http://Galeria-de-Imagenes.vcard.mx/g.php?vc=";
        $redireccionamiento_contacto     = $redirc_c.$valor_resultado;
        $redireccionamiento_galeria      = $redirc_g.$valor_resultado;

        //General clave de forma aleatoria
        $longitud = "5";
        $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        $password = "";
        for($i=0;$i<$longitud;$i++) {
          $password .= substr($str,rand(0,62),1);
        }

        if($tipo_prepago !="VC91"){
        //VCARD tipo ELITE
        $queryvc96 = "INSERT INTO myclientes (cod_vcard,cod_distribuidor,correlativo,rfc,rfc_empresa,estatus_vcard,estatus_metrica,rango_imagenes,rango_video,rango_audios,rango_arch_descargables,texto,pagina_descarga,pagina_compartir,pagina_galeria,texto_whatsapp,redireccionamiento_contacto,redireccionamiento_galeria) VALUES ('" .$valor_resultado. "','" .$cod_distribuidor. "','" .$correlativo. "','" .$valor_resultado. "','" .$valor_resultado. "','" .$status_vcard. "','" .$status_metricas. "','".$rango_imgs."','".$rango_video."','".$rango_audio."','".$rango_arch_descargable."','".$texto."','".$pagina1."','".$pagina2."','".$pagina3."','".$texto_whatsapp."','".$redireccionamiento_contacto."','".$redireccionamiento_galeria."')";
        $resultvc96 = mysqli_query($con, $queryvc96);

        $insert_uservc96 = "INSERT INTO users (user,pass,rfc,rfc_empresa,rango_imagenes,rango_video,rango_audios,rango_arch_descargables,rango_users,estatus_metrica,cod_vcard) VALUES ('" .$valor_resultado. "','" .$password. "','" .$valor_resultado. "','" .$valor_resultado. "','".$rango_imgs."','".$rango_video."','".$rango_audio."','".$rango_arch_descargable."','".$rango_users."','" .$status_metricas. "','" .$valor_resultado. "')";
        $result_user = mysqli_query($con, $insert_uservc96);
        }else{
        $queryvc91 = "INSERT INTO myclientes (cod_vcard,cod_distribuidor,correlativo,rfc,rfc_empresa,estatus_vcard,estatus_metrica,texto,pagina_descarga,pagina_compartir,texto_whatsapp,redireccionamiento_contacto) VALUES ('" .$valor_resultado. "','" .$cod_distribuidor. "','" .$correlativo. "','" .$valor_resultado. "','" .$valor_resultado. "','" .$status_vcard. "','" .$status_metricas. "','" .$texto. "','" .$pagina1. "','".$pagina2."','" .$texto_whatsapp. "','".$redireccionamiento_contacto."')";
        $resultvc91 = mysqli_query($con, $queryvc91);

        $insert_uservc91 = "INSERT INTO users (user,pass,rfc,rfc_empresa,rango_users,estatus_metrica,cod_vcard) VALUES ('" .$valor_resultado. "','" .$password. "','" .$valor_resultado. "','" .$valor_resultado. "','".$rango_users."','" .$status_metricas. "','" .$valor_resultado. "')";
        $result_uservc91 = mysqli_query($con, $insert_uservc91);
            }
        }
    }
    ?>

                    <div class="col-md-12">
                        <div class="col-md-12 panel">
                            <div class="col-md-12 panel-heading">
                                <h4 style="text-align: center; color: black;"> Registrar <strong style="color:crimson;">"PREPAGO"</strong></h4>
                                <br>
                            </div>
                            
                            <form  method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                                <div class="col-md-12 panel-body">
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <label>CODIGO DISTRIBUIDOR</label>
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text" name="cod_distribuidor" autocomplete="off">
                                                    <span class="bar"></span>
                                                </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label>CANTIDAD DE VCARD A CREAR</label>
                                                <div class="form-group form-animate-text">
                                                    <input type="number" class="form-text" name="cantidad" autocomplete="off">
                                                    <span class="bar"></span>
                                                </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-check">
                                            <!-- Group of default radios - option 2 -->
                                            <div class="custom-control custom-radio">
                                              <input type="radio" class="custom-control-input" id="VC91" name="radiobotton" value="VC91" checked>
                                              <label class="custom-control-label" for="defaultGroupExample2">START</label>
                                            </div>

                                            <!-- Group of default radios - option 3 -->
                                            <div class="custom-control custom-radio">
                                              <input type="radio" class="custom-control-input" id="VC96" name="radiobotton" value="VC96">
                                              <label class="custom-control-label" for="defaultGroupExample3">ELITE</label>
                                            </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="col-md-6"  style="float:right;">
                                                <button class="btn ripple btn-raised btn-success" name="enviar">
                                                    <div>
                                                        <span>Crear Prepago</span>
                                                    </div>
                                                </button>
                                            </div>
                                    </div>
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>


            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse" > 
    <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

    <?php include('js.html'); ?>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>