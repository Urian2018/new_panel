<?php

require 'phpqrcode/qrlib.php';
$dir = 'temp/';

if(!file_exists($dir))
	mkdir($dir);

$filename = $dir.'test.png';

$tamanio = 15;  //Tamaño de Pixel
$level = 'H'; //Precisión Baja
$frameSize = 1; //Tamaño en blanco
$contenido = 'hola uriany';

QRcode::png($contenido, $filename, $level, $tamanio, $frameSize);

echo '<img src="'.$filename.'" />';

?>