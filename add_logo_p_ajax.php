<?php
$original_lg = imagecreatefromjpeg($rtOriginal);
$original_lp = imagecreatefromjpeg($rtOriginal);
$max_ancho_lg = 400; 
$max_alto_lg = 400;

list($ancho_lg,$alto_lg)=getimagesize($rtOriginal);
$x_ratio_lg = $max_ancho_lg / $ancho_lg;
$y_ratio_lg = $max_alto_lg / $alto_lg;

if(($ancho_lg <= $max_ancho_lg) && ($alto_lg <= $max_alto_lg) ){
    $ancho_final_lg = $ancho_lg;
    $alto_final_lg = $alto_lg;
}
else if(($x_ratio_lg * $alto_lg) < $max_alto_lg){
    $alto_final_lg = ceil($x_ratio_lg * $alto_lg);
    $ancho_final_lg = $max_ancho_lg;
}
else {
    $ancho_final_lg = ceil($y_ratio_lg * $ancho_lg);
    $alto_final_lg = $max_alto_lg;
}

$lienzo_lg=imagecreatetruecolor($ancho_final_lg,$alto_final_lg);
imagecopyresampled($lienzo_lg,$original_lg,0,0,0,0,$ancho_final_lg, $alto_final_lg,$ancho_lg,$alto_lg);
imagedestroy($original_lg);
imagejpeg($lienzo_lg,"logos_g/".$mi_file_p);
?>
<META HTTP-EQUIV="REFRESH" CONTENT="0;URL=vcard.php">
<?php 
exit();
?>