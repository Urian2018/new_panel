<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
    $id_empleado = $_GET['id'];
    $cod_vcard   = $_GET['cod_vcard'];
    $empresa     = $_GET['empresa'];
    $name        = $_GET['name'];
    ?>
    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
        </head>

        <body id="mimin" class="dashboard">
            <?php include('menu_header.php'); ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>

                <div id="content">
                    <br><br>
                    <div class="col-md-12">
                        <div class="col-md-12 panel">
                            <div class="col-md-12 panel-heading">
                                <h4 style="text-align: center; color: black;"> MIGRAR <strong style="color:crimson;">"CLIENTE JEFE O EMPLEADO"</strong></h4>
                                <br>
                            </div>
                            <form  method="post" action="alerta_migrar_empleado.php">
                                <div class="col-md-12 panel-body">
                                    <div class="col-md-12">
                                        <input type="hidden" name="cod_vcard" value="<?php echo $cod_vcard; ?>">
                                        <input type="hidden" name="empresa" value="<?php echo $empresa; ?>">
                                        <input type="hidden" name="name" value="<?php echo $name; ?>">
                                        <input type="hidden" name="id" value="<?php echo $id_empleado; ?>">
                                        <div class="col-md-12 panel-body">
                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text">
                                                    <strong> Codigo VCard:</strong> <?php echo $cod_vcard; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text">
                                                    <strong> Nombre y Apellido:</strong> <?php echo $name; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text">
                                                    <strong> Empresa:</strong> <?php echo $empresa; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                    <input type="text" class="form-text"  name="cod_vcard_cliente_jefe" autocomplete="off" required>
                                                    <span class="bar"></span>
                                                    <label style="font-size: 12px;">CODIGO VCARD DEL NUEVO CLIENTE</label>
                                                </div>
                                            </div>

                                                <div class="col-md-6"  style="float:right; margin-top: 40px;">
                                                    <button class="btn ripple btn-raised btn-success" name="enviar">
                                                        <div>
                                                            <span>Siguiente</span>
                                                        </div>
                                                    </button>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>                     

                </div>
            </div>


            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse" > 
                <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

            <?php include('js.html'); ?>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>