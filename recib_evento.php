<?php
include('config.php');
date_default_timezone_set("America/Mexico_City");

$nameEvent      = $_POST['nameEvent'];
$fecha_alta     = $_POST['fecha_alta'];
$fechaInicio    = $_POST['fechaInicio'];
$fecha_fin      = $_POST['fecha_fin'];
$fecha_baja     = date("d/m/Y");
$direccionEvent = $_POST['direccionEvent'];
$gpsEvent       = $_POST['gpsEvent'];
$name           = $_POST['name'];
$apePaterno     = $_POST['apePaterno'];
$apeMater       = $_POST['apeMater'];
$empresa        = $_POST['empresa'];
$cargo          = $_POST['cargo'];
$email          = $_POST['email'];
$tlf_fijo       = $_POST['tlf_fijo'];
$tlf_movil      = $_POST['tlf_movil'];
$webEvento      = $_POST['webEvento'];

$filename  		= $_FILES["logoEvento"]["name"]; 
$source    		= $_FILES["logoEvento"]["tmp_name"];

$directorio = "Archivo_ClientesExpo/LogosExpo/";

    if (!file_exists($directorio)) {
    mkdir($directorio, 0777, true);
}


$rangoUser      ="AdminExpo";
$logitudpass = 5;
$pass = substr( md5(microtime()), 1, $logitudpass);

$sqlCodExpot = ("SELECT * FROM eventos ORDER BY id DESC");
$QueryEvent = mysqli_query($con, $sqlCodExpot);
$cdExpot = mysqli_fetch_array($QueryEvent);
$CodExpot = $cdExpot['codExpot'];

if ($CodExpot !="") {
	$cortar_url = str_replace ('EX-','',$CodExpot);
	$IncrementoCodExpot = ($cortar_url + 1);
	$newCodExpot = "EX-0".$IncrementoCodExpot;
}else{
	$newCodExpot = "EX-01";
}


$explode = explode('.', $filename);
$extension_arch = array_pop($explode);
$namelogoevento = $newCodExpot.'.'.$extension_arch;


$dir = opendir($directorio); 
$target_path = $directorio . '/' . $namelogoevento; 
if (move_uploaded_file($source, $target_path)) {

$query = "INSERT INTO eventos(
	nameEvent,
	codExpot,
	fecha_alta,
	fechaInicio,
	fecha_baja,
	fecha_fin,
	direccionEvent,
	gpsEvent,
	name,
	apePaterno,
	apeMater,
	empresa,
	cargo,
	email,
	tlf_fijo,
	tlf_movil,
	webEvento,
	logoEvento
	)  
VALUES (
	'" .$nameEvent. "',
	'" .$newCodExpot. "',
	'" .$fecha_alta. "',
	'" .$fechaInicio. "',
	'" .$fecha_fin. "',
	'" .$fecha_baja. "',
	'" .$direccionEvent. "',
	'" .$gpsEvent. "',
	'" .$name. "',
	'" .$apePaterno. "',
	'" .$apeMater. "',
	'" .$empresa. "',
	'" .$cargo. "',
	'" .$email. "',
	'" .$tlf_fijo. "',
	'" .$tlf_movil. "',
	'" .$webEvento. "',
	'" .$namelogoevento. "'
)";
        $result = mysqli_query($con, $query);
if ($result > 0) {
      	$insertUser = "INSERT INTO users (user,pass,nombre_apellido,rango_users)
      	 VALUES ('" .$newCodExpot. "','" .$pass. "','" .$name. "','" .$rangoUser. "')";
        $result_uservc91 = mysqli_query($con, $insertUser);	
    }


//ENVIANDO INFORMACION AL EMAIL DEL CLIENTE JEFE DE LA EXPO

//enviando enlace de descarga al correo
$Panel   = "https://vcard.mx/_/";

$para = $email;
$titulo = 'Tienes un mensaje de VCard';
$mensaje = "<html>".
    "<head><title>Email desde VCard</title>".
        "<style>* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}
body {
    font-family: 'Nunito', sans-serif;
    color: #333;
    font-size: 14px;
    background:#222; 
}
.contenedor{
    width: 80%;
    min-height:auto;
    text-align: center;
    margin: 0 auto;
    padding: 40px;
    background: #ececec;
    }
    .hola{
    color:#333;
    font-size:25px;
    font-weight:bold;
    }
    img{
    margin-left: auto;
    margin-right: auto;
    display: block;
    padding:0px 0px 20px 0px;
   }
    </style></head>".
            "<body>" .
                "<div class='contenedor'>" .
                "<img src='$url_logo' style='width:120px;'>" .
                "<p>&nbsp;</p>" .
                    "<span class='hola'>Hola, <strong>" . $name . "</strong></span>" .
                    "<p>&nbsp;</p>" .
                    "<p>VCard te ha enviado todos los datos para acceder a tu cuenta personal de la Expo <span style='color:#00afef; font-weight:bold;'>VC</span>ard</p> " .
                    "<p>&nbsp;</p>" .
                    "<p>Link del Panel de Control: <strong> " . $Panel . "</strong>" .
                    "<p>El Codigo del evento es: <strong>" . $newCodExpot . "</strong> " .
                    "<p>Usuario del Panel: <strong>" . $newCodExpot . "</strong> " .
                    "<p>Clave del Panel: <strong> " . $pass . "</strong>" .
                    "<p>&nbsp;</p>" .
                    "<p>La Fecha inicio del evento es <strong> " . $fechaInicio . "</strong> y el mismo culminará en la fecha <strong>" . $fecha_fin . "</strong>" .
                    "<p>&nbsp;</p>" .
                "<p>Felicitaciones! ya puedes empezar a usar tu Panel Vcard</p>" .
                "<img src='$img_welcome' alt='' style='width:50%; margin:0 auto;'>".
                "<p><a title='VCard' href='https://vcard.mx' target='_blank'><img src='https://vcard.mx/vc.png' alt='' width='22px' /></p>" .
                "</div>" .
            "</body>" .
        "</html>";
        
$cabeceras = 'MIME-Version: 1.0' . "\r\n";
$cabeceras .= 'Content-type: text/html; charset=utf-8' . "\r\n";
$cabeceras .= 'From: VCard de México<info@vcard.mx>';
$enviado = mail($para, $titulo, $mensaje, $cabeceras);


}  

header("Location: add_evento.php");      
?>