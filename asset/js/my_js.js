//FUNCION CHEXBOX SELECCION MULTIPLE
function marcar(source)
{
    checkboxes = document.getElementsByTagName('input'); //obtenemos todos los controles del tipo Input
    for (i = 0; i < checkboxes.length; i++) //recoremos todos los controles
    {
        if (checkboxes[i].type == "checkbox") //solo si es un checkbox entramos
        {
            checkboxes[i].checked = source.checked; //si es un checkbox le damos el valor del checkbox que lo llamó (Marcar/Desmarcar Todos)
        }
    }
}


//CREAR VCARD Y URLS CON RESPECTO A LOS ID SELECCIONADOS.
$(function () {
    /*$("input[name^=img_vcard]").on("change", function () {
        var id = $(this).attr('rel');
        var formData = new FormData($("#formulariocero" + id)[0]);
        var ruta = "img_vcard.php";
        $.ajax({
            url: ruta,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function (datos)
            {
                $("#respuesta").html(datos);
            }
        });
    });*/

//CREAR LOGOS GRANDE DE LA EMPRESA CON RESPECTO A LOS ID SELECCIONADOS
    $("#logoG").on("click", function () {
        var id = $(this).attr('rel');
        var formData = new FormData($("#form")[0]);
        var ruta = "add_logo_G.php";
        $.ajax({
            url: ruta,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function (datos)
            {
                $("#capalogos").html(datos);
            }
        });
    });
    
//ASIGNAR LOGO PEQUEÑO DE LA EMPRESA CON RESPECTO A LOS ID SELECCIONADOS
    $("#logoP").on("click", function () {
        var id = $(this).attr('rel');
        var formData = new FormData($("#form")[0]);
        var ruta = "add_logo_P.php";
        $.ajax({
            url: ruta,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function (datos)
            {
                $("#capalogos").html(datos);
            }
        });
    });
    
    
//CREAR IMG VCARD DE LA EMPRESA CON RESPECTO A LOS ID SELECCIONADOS
    $("#img_vcards").on("click", function () {
        var id = $(this).attr('rel');
        var formData = new FormData($("#form")[0]);
        var ruta = "add_vcard_img_seleccionadas.php";
        $.ajax({
            url: ruta,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function (datos)
            {
                $("#capalogos").html(datos);
            }
        });
    });


    //ASIGNAR LOGO PEQUEÑO, LOGO GRANDE MAS IMAGEN VCAR A UNO O VARIOS REGISTROS SELECCIONADOS
    $("#grupal").on("click", function () {
        var id = $(this).attr('rel');
        var formData = new FormData($("#form")[0]);
        var ruta = "img_grupal.php";
        $.ajax({
            url: ruta,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function (datos)
            {
                $("#capalogos").html(datos);
            }
        });
    });

    //ELIMINAR REGISTROS
    /*$("#eliminarRegis").on("click", function () {
        var id = $(this).attr('rel');
        var formData = new FormData($("#form")[0]);
        var ruta = "eliminar_regist.php";
        $.ajax({
            url: ruta,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function (datos)
            {
                $("#capalogos").html(datos);
            }
        });
    });*/
    $("#eliminarRegis").on("click", function () {
        var id = $(this).attr('rel');
        var formData = new FormData($("#form")[0]);
        var ruta = "alert_delete.php";
        $.ajax({
            url: ruta,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function (datos)
            {
                $("#capalogos").html(datos);
            }
        });
    });
    
});