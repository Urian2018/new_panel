$(function(){
$('body').on('click', '#botonenviar_tab1', function(e){
$('#tab1').html('<center><img src="img/cargandoo.gif"/><br/>Espere un momento, por favor...</center>');
    e.preventDefault();
    var formData = new FormData($(this).parents('form')[0]);
    var botonenviar_tab1 = $("#botonenviar_tab1");

    $.ajax({
        url: 'recib_tab1_edit_empleado.php',
        type: 'POST',
        xhr: function() {
            var myXhr = $.ajaxSettings.xhr();
            return myXhr;
        },  

       data: $("#formulario_tab1").serialize(), // Adjuntar los campos del formulario enviado.
       beforeSend: function(){
            botonenviar_tab1.val("Actualizando Información . . ."); // Para input de tipo button
            botonenviar_tab1.attr("disabled","disabled");
        },
        complete:function(data){
        $("#exito").delay(500).fadeIn("slow"); 
        $("#exito").delay(3500).fadeOut("slow");

          botonenviar_tab1.val("Actualizar Información");
          botonenviar_tab1.removeAttr("disabled");
        },
        success: function(data){
            $("#tab1").html(data); // Mostrar la respuestas del script PHP.
        },
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    });
    return false;
    });
 });


/*******FUNCION PARA GUARDAR INFORMACION DEL TAB2**********/
$(function(){
$("#botonenviar_tab2").click(function(){
var url = "recib_tab2_edit_empleado.php"; // El script a dónde se realizará la petición.
$('#tab2').html('<center><img src="img/cargandoo.gif"/><br/>Espere un momento, por favor...</center>');
var botonenviar_tab2 = $("#botonenviar_tab2");
$.ajax({
     type: "POST",
     url: url,
     data: $("#formulario_tab2").serialize(), // Adjuntar los campos del formulario enviado.
     beforeSend: function(){
          botonenviar_tab2.val("Actualizando Información . . ."); // Para input de tipo button
          botonenviar_tab2.attr("disabled","disabled");
      },
      complete:function(data){
          $("#exito").delay(500).fadeIn("slow"); 
          $("#exito").delay(3500).fadeOut("slow");

          botonenviar_tab2.val("Actualizar Información");
          botonenviar_tab2.removeAttr("disabled");
      },
      success: function(data){
          $("#tab2").html(data); // Mostrar la respuestas del script PHP.
      },
      error: function(data){
          alert("Problemas al tratar de enviar el formulario");
      }
   });

return false; // Evitar ejecutar el submit del formulario.
});
});


/*******FUNCION PARA GUARDAR INFORMACION DEL TAB3**********/
$(function(){
$("#botonenviar_tab3").click(function(){
var url = "recib_tab3_edit_empleado.php"; // El script a dónde se realizará la petición.
$('#tab3').html('<center><img src="img/cargandoo.gif"/><br/>Espere un momento, por favor...</center>');
var botonenviar_tab3 = $("#botonenviar_tab3");
$.ajax({
     type: "POST",
     url: url,
     data: $("#formulario_tab3").serialize(), // Adjuntar los campos del formulario enviado.
     beforeSend: function(){
          botonenviar_tab3.val("Actualizando Información . . ."); // Para input de tipo button
          botonenviar_tab3.attr("disabled","disabled");
      },
      complete:function(data){
          $("#exito").delay(500).fadeIn("slow"); 
          $("#exito").delay(3500).fadeOut("slow");

          botonenviar_tab3.val("Actualizar Información");
          botonenviar_tab3.removeAttr("disabled");
      },
      success: function(data){
          $("#tab3").html(data); // Mostrar la respuestas del script PHP.
      },
      error: function(data){
          alert("Problemas al tratar de enviar el formulario");
      }
   });

return false; // Evitar ejecutar el submit del formulario.
});
});



/*******FUNCION PARA GUARDAR INFORMACION DEL TAB4**********/
$(function(){
$("#botonenviar_tab4").click(function(){
var url = "recib_tab4_edit_empleado.php"; // El script a dónde se realizará la petición.
$('#tab4').html('<center><img src="img/cargandoo.gif"/><br/>Espere un momento, por favor...</center>');
var botonenviar_tab4 = $("#botonenviar_tab4");
$.ajax({
     type: "POST",
     url: url,
     data: $("#formulario_tab4").serialize(), // Adjuntar los campos del formulario enviado.
     beforeSend: function(){
          botonenviar_tab4.val("Actualizando Información . . ."); // Para input de tipo button
          botonenviar_tab4.attr("disabled","disabled");
      },
      complete:function(data){
          $("#exito").delay(500).fadeIn("slow"); 
          $("#exito").delay(3500).fadeOut("slow");

          botonenviar_tab4.val("Actualizar Información");
          botonenviar_tab4.removeAttr("disabled");
      },
      success: function(data){
          $("#tab4").html(data); // Mostrar la respuestas del script PHP.
      },
      error: function(data){
          alert("Problemas al tratar de enviar el formulario");
      }
   });

return false; // Evitar ejecutar el submit del formulario.
});
});








/*****************************************************/
/******CONFIGURACION WEB PAGINA GALERIA**************/
/************CAMBIAR COLOR DE LA WEB VISTA CLIENTE*******************/
$(function(){
$("#config_botonenviar_tab2").click(function(){
var url = "recib_config_tab2.php"; // El script a dónde se realizará la petición.
$('#capa_tab2_config_cliente_color').html('<center><img src="img/cargandoo.gif"/><br/>Espere un momento, por favor...</center>');
var config_botonenviar_tab2 = $("#config_botonenviar_tab2");
$.ajax({
     type: "POST",
     url: url,
     data: $("#config_formulario_tab2").serialize(), // Adjuntar los campos del formulario enviado.
     beforeSend: function(){
          config_botonenviar_tab2.val("Cambiando el Color . . ."); // Para input de tipo button
          config_botonenviar_tab2.attr("disabled","disabled");
      },
      complete:function(data){
          $("#exito_color_fondo_insert").delay(500).fadeIn("slow"); 
          $("#exito_color_fondo_insert").delay(3500).fadeOut("slow");

          config_botonenviar_tab2.val("Guardar Color");
          config_botonenviar_tab2.removeAttr("disabled");
      },
      success: function(data){
          $("#capa_tab2_config_cliente_color").html(data); // Mostrar la respuestas del script PHP.
      },
      error: function(data){
          alert("Problemas al tratar de enviar el formulario");
      }
   });

return false; // Evitar ejecutar el submit del formulario.
});
});





/*********************TAB1 CONFIGURATION CAMBIAR IMG EXAMINAR VISTA ADMINISTRADOR*****************/
$(function(){
$('.error').hide();
$('body').on('click', '#config_botonenviar_tab1_admin', function(e){
    e.preventDefault();
    var formData = new FormData($(this).parents('form')[0]);
    var config_botonenviar_tab1_admin = $("#config_botonenviar_tab1_admin");

    //validando el boton examinar que no este vacio
    var img_fondo = $("input#img_fondo").val();
    if (img_fondo == "") {
      $("div#name_error").show();
      $("input#img_fondo").focus();
      return false;
    }

    $.ajax({
        url: 'recib_config_tab1_admin.php',
        type: 'POST',
        xhr: function() {
            var myXhr = $.ajaxSettings.xhr();
            return myXhr;
        },  
       data: $("#config_formulario_tab1_admin").serialize(), // Adjuntar los campos del formulario enviado.
       beforeSend: function(){
        $('#capa_tab1_config_admin').html('<center><img src="img/cargandoo.gif"/><br/>Espere un momento, por favor...</center>');
            config_botonenviar_tab1_admin.val("Guardando Theme . . ."); // Para input de tipo button
            config_botonenviar_tab1_admin.attr("disabled","disabled");
        },
        complete:function(data){
        $("#msj_exito_img_fondo_insert").delay(500).fadeIn("slow"); 
        $("#msj_exito_img_fondo_insert").delay(3500).fadeOut("slow");

          config_botonenviar_tab1_admin.val("Guardar Theme");
          config_botonenviar_tab1_admin.removeAttr("disabled");
        },
        success: function(data){
            $("#capa_tab1_config_admin").html(data); // Mostrar la respuestas del script PHP.
        },
       
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    });
    return false;
    });
 });


/************CAMBIAR COLOR DE LA WEB DESDE LA VISTA ADMINISTRADOR*******************/
$(function(){
$("#config_botonenviar_tab2_admin").click(function(){
var url = "recib_config_tab2_admin.php"; // El script a dónde se realizará la petición.
var config_botonenviar_tab2_admin = $("#config_botonenviar_tab2_admin");
$.ajax({
     type: "POST",
     url: url,
     data: $("#config_formulario_tab2_admin").serialize(), // Adjuntar los campos del formulario enviado.
     beforeSend: function(){
      $('#capa_tab2_config_admin_color').html('<center><img src="img/cargandoo.gif"/><br/>Espere un momento, por favor...</center>');
          config_botonenviar_tab2_admin.val("Cambiando el Color . . ."); // Para input de tipo button
          config_botonenviar_tab2_admin.attr("disabled","disabled");
      },
      complete:function(data){
          $("#exito_color_fondo_insert").delay(500).fadeIn("slow"); 
          $("#exito_color_fondo_insert").delay(3500).fadeOut("slow");

          config_botonenviar_tab2_admin.val("Guardar Color");
          config_botonenviar_tab2_admin.removeAttr("disabled");
      },
      success: function(data){
          $("#capa_tab2_config_admin_color").html(data); // Mostrar la respuestas del script PHP.
      },
      error: function(data){
          alert("Problemas al tratar de enviar el formulario");
      }
   });

return false; // Evitar ejecutar el submit del formulario.
});
});

