<?php 
    if (isset($_POST['enviar'])) {
    	include('config.php');

        foreach ($_FILES["file"]['tmp_name'] as $key => $tmp_name) {
        if ($_FILES["file"]["name"][$key]) {
            $file = $_FILES["file"]["name"][$key]; //Obtenemos el nombre original del archivo
            $rtOriginal = $_FILES["file"]["tmp_name"][$key]; //Obtenemos un nombre temporal del archivo

    $cod_vcard_cliente = $_POST['cod_vcard_cliente'];
    $ref_empre_cliente = $_POST['rf_cempre_cliente'];

        $msj_exito = "";
        $msj_error = "";
        $msj_borrar = "";

        //$file = $_FILES['file']['name'];
        //$rtOriginal = $_FILES['file']['tmp_name'];
        $folder = "Galeria_Clientes/" . $cod_vcard_cliente . '/';

                //contar imagen de un directorio
        $sql_contar_theme = ("SELECT MAX(id) AS id FROM slider_cliente");
        $query_theme = mysqli_query($con, $sql_contar_theme);
        $themes = mysqli_fetch_array($query_theme);
        $id_theme = $themes['id'];
        $new_total_img = $id_theme + 1;

        //contando el total de imagenes de ese cliente

        $total_imagenes = count(glob($folder . '/{*.jpg,*.gif,*.png,*.jpeg,*.PNG,*.JPG}', GLOB_BRACE));
        $new_total_img = $total_imagenes + 1;

        $fileExtension = substr(strrchr($file, '.'), 1);
        $mi_file = $new_total_img . '.' . $fileExtension;

        //VERIFANDO SINO EXISTE LA CARPETA LA CREO
        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
        }


        $png = 'png';
        $PNG = 'PNG';
        if ($fileExtension == $png || $fileExtension == $PNG) {
            $original = imagecreatefrompng($rtOriginal);

//Ancho y alto maximo
		$max_ancho = 900;
            $max_alto = 900;

//Medir la imagen
            list($ancho, $alto) = getimagesize($rtOriginal);

//Ratio
            $x_ratio = $max_ancho / $ancho;
            $y_ratio = $max_alto / $alto;

//Proporciones
            if (($ancho <= $max_ancho) && ($alto <= $max_alto)) {
                $ancho_final = $ancho;
                $alto_final = $alto;
            } else if (($x_ratio * $alto) < $max_alto) {
                $alto_final = ceil($x_ratio * $alto);
                $ancho_final = $max_ancho;
            } else {
                $ancho_final = ceil($y_ratio * $ancho);
                $alto_final = $max_alto;
            }

//Crear un lienzo
            $lienzo = imagecreatetruecolor($ancho_final, $alto_final);
//Copiar original en lienzo
            imagecopyresampled($lienzo, $original, 0, 0, 0, 0, $ancho_final, $alto_final, $ancho, $alto);
//Destruir la original
            imagedestroy($original);
//Crear la imagen y guardar en directorio logos_g/
            imagepng($lienzo, $folder . $mi_file);

            $query = "INSERT INTO slider_cliente (filename, cod_vcard,rfc_empresa) VALUES ('$mi_file', '$cod_vcard_cliente','$ref_empre_cliente')";
            $result = mysqli_query($con, $query);
			$msj_exito = '';
			if($result){
				$msj_exito ="fine";
			}

//header("location:add_carrusel.php?msjs=".$msj_exito);
//echo '<META HTTP-EQUIV="Refresh" Content="0; URL=add_carrusel.php">'; 
//include 'add_carrusel.php';
//header("location:add_carrusel.php?msj=".$msj_exito);
        } else {
            $original = imagecreatefromjpeg($rtOriginal);

            $max_ancho = 900;
            $max_alto = 900;

            list($ancho, $alto) = getimagesize($rtOriginal);
            $x_ratio = $max_ancho / $ancho;
            $y_ratio = $max_alto / $alto;
            //Proporciones
            if (($ancho <= $max_ancho) && ($alto <= $max_alto)) {
                $ancho_final = $ancho;
                $alto_final = $alto;
            } else if (($x_ratio * $alto) < $max_alto) {
                $alto_final = ceil($x_ratio * $alto);
                $ancho_final = $max_ancho;
            } else {
                $ancho_final = ceil($y_ratio * $ancho);
                $alto_final = $max_alto;
            }

            //Crear un lienzo
            $lienzo = imagecreatetruecolor($ancho_final, $alto_final);
            imagecopyresampled($lienzo, $original, 0, 0, 0, 0, $ancho_final, $alto_final, $ancho, $alto);
            imagedestroy($original);
            imagejpeg($lienzo, $folder . $mi_file);

            $query_jpg = "INSERT INTO slider_cliente (filename, cod_vcard,rfc_empresa) VALUES ('$mi_file', '$cod_vcard_cliente','$ref_empre_cliente')";
            $result = mysqli_query($con, $query_jpg);
			$msj_exito = '';
			if($result){
				$msj_exito ="fine";
			}
           
        }
    }
}
	 header("location:add_carrusel.php?msjs=".$msj_exito);
}

?>