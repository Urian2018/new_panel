<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
    ?>
<!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
            <!----js para mostrar msj--->
            <script  src="asset/js/jquery.min.js"></script>
            <script src="asset/js/msj.js"></script>
        </head>

        <body id="mimin" class="dashboard">
            <?php 
               include('menu_header.php');
             ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>

                <div id="content">
                    <br>
                    <?php
                    if (isset($_POST['enviar'])) {
                        $msj_exito  = "";
                        $user       = $_POST['user'];
                        $pass       = $_POST['pass'];
                        $name_ape   = $_POST['nombre_apellido'];
                        $rango_user = $_POST['rango_users'];
       

                        $insert_user = ("INSERT users (user, pass, nombre_apellido, rango_users) VALUES ('".$user."','".$pass."','".$name_ape."','".$rango_user."')");
                        $result_insert = mysqli_query($con, $insert_user);
                        $msj_exito = "
                            <div class='col-md-12'>
                            <div class='alert alert-success col-md-12 col-sm-12  alert-icon alert-dismissible fade in' role='alert'>
                            <div class='col-md-2 col-sm-2 icon-wrapper text-center'>
                                <span class='fa fa-check fa-2x'></span></div>
                                <div class='col-md-10 col-sm-10'>
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                <span aria-hidden='true'>x</span></button>
                                <p><strong>Felicitaciones la Clave fue cambiada con Exito.</strong></p>
                                </div>
                            </div>
                            </div>";
                            //header("location:new_clave.php");
                            // exit();
                    }
                    ?>

                    <div class="col-md-12">
                        <div class="col-md-12 panel">
                            <div class="col-md-12 panel-heading">
                                <h4 style="text-align: center; color: black;"> Registrar Nuevo Usuario al <strong style="color:crimson;">"SISTEMA"</strong></h4>
                                <br>
                            </div>
                            
                            <form  method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                                <div class="col-md-12 panel-body">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <label>USUARIO</label>
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text" name="user" autocomplete="off">
                                                    <span class="bar"></span>
                                                </div>
                                        </div>
                                        
                                        <div class="col-md-6">
                                            <label>CLAVE</label>
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text" name="pass" autocomplete="off">
                                                    <span class="bar"></span>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <label>NOMBRE Y APELLIDO</label>
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text" name="nombre_apellido" autocomplete="off">
                                                    <span class="bar"></span>
                                                </div>
                                        </div>
                                        
                                        <div class="col-md-6">
                                            <label>TIPO DE USUARIO</label>
                                                <div class="form-group form-animate-text" style="margin-top:10px !important;">
                                                    <select name="rango_users" id="rango_users" class="form-control">
                                                        <option value="Administrador">Administrador</option>
                                                        <option value="Operador">Operador</option>
                                                    </select>
                                                 </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                
                                 <div class="col-md-12 panel-body">
                                     <div class="col-md-12">
                                        <div class="col-md-6"  style="float:right;">
                                                <button class="btn ripple btn-raised btn-success" name="enviar">
                                                    <div>
                                                        <span>Registrar Usario</span>
                                                    </div>
                                                </button>
                                            </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>


            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse" > 
    <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

    <?php include('js.html'); ?>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>