<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
    if (isset($_POST['enviar'])) {
        
        date_default_timezone_set("America/Mexico_City");
        $hora = date('h:i A', time() - 3600 * date('I'));
        $fecha = date("d/m/Y"); 
        $result_fecha = $fecha. " : ". $hora;
        $para = $_POST['para'];
        $notificacion = $_POST['notificacion'];
        $leido = "NO";

        $query = "INSERT INTO notificaciones (para,notificacion,fecha,leido) VALUES ('" . $_POST['para'] . "','" . $_POST['notificacion'] . "','" . $result_fecha . "','" . $leido . "')";
        $result = mysqli_query($con, $query);
        header("location:notificacion.php");
        exit();
        @mysqli_close($con);
    }
    ?>
    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
        </head>

        <body id="mimin" class="dashboard">
            <?php include('menu_header.php'); ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>

                <div id="content">
                    <br><br>
                    <div class="col-md-12">
                        <div class="col-md-12 panel">
                            <div class="col-md-12 panel-heading">
                                <h4 style="text-align: center; color: black;"> AGREGAR <strong style="color:crimson;">"NOTIFICACIÓN"</strong> A LOS USUARIOS DEL SISTEMA</h4>
                                <br>
                            </div>
                            <form  enctype="multipart/form-data" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                                <div class="col-md-12 panel-body">
                                    <div class="col-md-12">

                                        <div class="col-md-12 panel-body">
                                            <div class="col-md-8">
                                                <div class="form-group form-animate-text">
                                                    <textarea type="text" class="form-text" id="validate_firstname" name="notificacion" required></textarea>
                                                    <span class="bar"></span>
                                                    <label>ESCRIBA LA NOTIFICACIÓN AQUI</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                    <input type="text" class="form-text" id="validate_firstname" name="para" value="Todos" required autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>PARA</label>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-12 panel-body">
                                    <div class="col-md-12">
                                        <div class="col-md-6"  style="float:right;">
                                            <button class="btn ripple btn-raised btn-success" name="enviar">
                                                <div>
                                                    <span>Agregar Notificación</span>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <?php
                    $Consultar = ("SELECT * FROM notificaciones ORDER BY id DESC");
                    $numero_servicios = mysqli_query($con, $Consultar);
                    ?>
                    <div class="col-md-12 top-20 padding-0">
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-heading"><h3 style="text-align: center;">Lista de 
                                        <strong style="color: crimson;">"NOTIFICACIONES"</strong></h3></div>
                                <div class="panel-body">
                                    <div class="responsive-table">
                                        <table  class="table table-striped table-bordered" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>N°</th>
                                                    <th>Para</th>
                                                    <th>Notificación</th>
                                                    <th>Fecha</th>
                                                    <th>Opción</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                while ($vcard = mysqli_fetch_array($numero_servicios)) {
                                                    $table = "notificaciones";
                                                    $id    = $vcard['id'];
                                                    $delet ="admin";
                                                    ?>
                                                    <tr>
                                                        <td style="text-align: center;"><?php echo $vcard['id']; ?></td>
                                                        <td style="text-align: center;"><?php echo $vcard['para']; ?></td>
                                                        <td style="text-align: center;"><?php echo $vcard['notificacion']; ?></td>
                                                        <td style="text-align: center;"><?php echo $vcard['fecha']; ?></td>
                                                        <td style="text-align: center; font-size: 25px;"><a href="delete.php?id=<?php echo $id; ?>&table=<?php echo $table; ?>&delete=<?php echo $delet; ?>"> <span class="fa fa-trash" title="Eliminar Notificación"></span></a></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <?php
                        @mysqli_close($numero_servicios);
                        ?>  
                    </div> 

                </div>
            </div>


            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse" > 
                <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

            <?php include('js.html'); ?>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>