<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
    ?>
    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <style>
                .fa-trash{
                    font-size: 20px;
                }
                .fa-trash:hover{
                    cursor: pointer;
                    color: black;
                }
                .icon-action-undo{
                    font-size: 20px;
                }
                .icon-action-undo:hover{
                    cursor: pointer;
                    color: black;
                }
            </style>
        </head>

        <body id="mimin" class="dashboard">
            <?php include('menu_header_user.php'); ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>

                <div id="content">

                    <br>
                    <?php
                    $notif_selecc = isset($_POST['id']) ? $_POST['id'] : $_GET['id'];
                    $Consultar = ("SELECT * FROM notificaciones WHERE id='$notif_selecc'");
                    $numero_servicios = mysqli_query($con, $Consultar);
                    ?>
                    <div class="col-md-12 top-20 padding-0">
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 style="text-align: center;"> 
                                        <strong style="color: crimson;">"NOTIFICACIÓN"</strong>
                                    </h3>
                                </div>
                                <div class="panel-body">

                                    <?php
                                    while ($noti = mysqli_fetch_array($numero_servicios)) {
                                        $table = "notificaciones";
                                        $id = $noti['id'];
                                        $delet ="user";
                                        ?>
                                        <p style="text-align: center;">
                                            <?php echo $noti['notificacion']; ?>
                                        </p>
                                        <br>
                                        <p style="text-align:right;">
                                            <?php echo $noti['fecha']; ?>     
                                            <a href="notif_user.php" title="Volver">
                                                <span class="icon-action-undo" title="Volver">
                                                    Volver
                                                </span>
                                            </a>
                                            
                                             <a href="delete.php?id=<?php echo $id; ?>&table=<?php echo $table; ?>&delete=<?php echo $delet; ?>"> 
                                                <span class="fa fa-trash" title="Eliminar Notificación">
                                                </span>
                                            </a>
                                        </p>
                                    <?php 
                                    if ($noti['leido'] != "SI") {
                                    $update_leido = ("UPDATE notificaciones SET leido='SI' WHERE id='" . $notif_selecc . "'");
                                    $result_update = mysqli_query($con, $update_leido);
                                    }
                                    } ?>

                                </div>
                            </div>
                        </div>
                    </div>  
                    <?php
                    @mysqli_close($numero_servicios);
                   ?>  
                </div> 

            </div>
        </div>


        <!-- start: Mobile -->
        <div id="mimin-mobile" class="reverse" > 
            <?php include('menu_movil.php'); ?>
        </div>
        <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
            <span class="fa fa-bars"></span>
        </button>
        <!-- end: Mobile -->

        <?php include('js.html'); ?>
    </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>