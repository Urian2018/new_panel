<style type="text/css" media="screen">
    table th{
        font-size: 13px;
    }
    table tr td{
        font-size: 13px;
    }
</style>
<?php
header("Content-Type: text/html;charset=utf-8");
//include('config.php');
$sql = ("SELECT 
    e.id,
    e.nameEvent,
    e.codExpot,
    e.fecha_alta,
    e.fechaInicio,
    e.fecha_fin,
    e.fecha_baja,
    e.direccionEvent,
    e.gpsEvent,
    e.name,
    e.apePaterno,
    e.apeMater,
    e.empresa,
    e.cargo,
    e.email,
    e.tlf_fijo,
    e.tlf_movil,
    e.logoEvento,
    u.user,
    u.pass

     FROM eventos e, users u WHERE u.user=e.codExpot");
    if($mostar = mysqli_query($con, $sql)){
    $total_client = mysqli_num_rows($mostar) ;
?>
<div id="content">
<div class="col-md-12 top-20 padding-0" id="capa">
<div class="col-md-12">
    <div class="panel">
        <div class="panel-heading">
            <h4 style="text-align: center;">
                <?php echo " Lista de Eventos <strong style='color:green; text-align: center;'>(" .$total_client. ')</strong>'; ?>
            </h4>
        </div>
        <div class="panel-body">
            <div class="responsive-table">
                <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Codigo Evento</th>
                            <th>Nombre de Evento</th>
                            <th>Jefe del Evento</th>
                            <th>Logo</th>
                            <th>Usuario</th>
                            <th>Clave</th>
                            <th>Fecha Alta</th>
                            <th>Fecha Baja</th>
                            <th>Fecha Fin</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        while ($expot = mysqli_fetch_array($mostar)) {
                           ?>
                            <tr>
                                <td><?php echo $expot['codExpot']; ?></td>
                                <td><?php echo $expot['nameEvent']; ?></td>
                                <td><?php echo $expot['name']; ?></td>
                                <td>
                                    <?php 
                                    $directorio = "Archivo_ClientesExpo/LogosExpo/".$expot['logoEvento']; ?>
                                    <img src="<?php echo $directorio; ?>" alt="" style="width: 80px;"></td>
                                <td><?php echo $expot['user']; ?></td>
                                <td><?php echo $expot['pass']; ?></td>
                                <td><?php echo $expot['fecha_alta'];?></td>
                                <td><?php echo $expot['fecha_baja'];?></td>
                                <td><?php echo $expot['fecha_fin'];?></td>
                                <td style="text-align: center; font-size: 25px;">
                                <a href="edit_Evento.php?id=<?php echo $expot['id'];?>"> 
                                    <span style="text-align: center; font-size: 20px;" class="fa icon-pencil" title="Editar Datos del Evento"></span>
                                </a>
                                <a href="FuncionesCrud.php?id=<?php echo $expot['id']; ?>&codExpo=<?php echo $expot['codExpot']; ?>">
                                    <span class="fa fa-trash" style="font-size: 25px" title="Eliminar Evento"></span>
                                </a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>  
</div>
<?php
}
@mysqli_close($mostar);
?> 

</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#datatables-example').DataTable();
    });
</script>