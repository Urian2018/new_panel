<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>VCARD</title>
<script  src="asset/js/jquery.min.js"></script>
<script type="text/javascript">
/********JQUE QUE AGREGA LA CLASE HOVE AL COLOR QUE ESTA SELECCIONADA*********/
$(function() {
$(".img_fondo_view div").click(function() {
    $('.seleccion').removeClass("seleccion").addClass("noseleccion");
    $(this).addClass("seleccion");
  });
});    

/************JQUERY QUE CAPTURA EL ID DEL DIV LISTA DE COLORES PARA ENVIARLO A PHP*************/
$(document).ready(function() {
$(".img_fondo_view li div").on("click", function () {
var id = $(this).attr('id');
var dataString = 'id=' + id;

var ruta = "recib_config_color_fondo_admin.php";
$('#tab1').html('<center><img src="img/cargandoo.gif"/><br/>Espere un momento, por favor...</center>');

$.ajax({
    url: ruta,
    type: "POST",
    data: dataString,
    complete:function(data){
      $("#exito_color_fondo").delay(500).fadeIn("slow"); 
      $("#exito_color_fondo").delay(3500).fadeOut("slow");
    },
    success: function(data){
          $("#tab1").html(data); // Mostrar la respuestas del script PHP.
    }
});
return false;
});
});
</script>
</head>
<body>
<div id="capa_tab1_config_admin_color">
    <?php
    $tipo = "admin";
    $sql_color_config = ("SELECT id,tipo,color,estatus FROM color WHERE tipo='".$tipo."' ");
    $query_config_color = mysqli_query($con, $sql_color_config); ?>

    <ul class="img_fondo_view" style="display: flex; flex-wrap: wrap;">

    <?php  while ($color_fondo = mysqli_fetch_array($query_config_color)) { 
    $color_fondo_bd = $color_fondo['color']; 
    $estado_color   = $color_fondo['estatus'];
    if($estado_color !='Activo'){  ?>
    <li>
        <div class="noseleccion" id="<?php echo $color_fondo['id']; ?>" style=" background-color:<?php echo $color_fondo_bd; ?>" title="Activar Color"> </div>  
    </li>
    <?php } else{ ?>
    <li>
      <div class="seleccion" id="<?php echo $color_fondo['id']; ?>" style="background-color:<?php echo $color_fondo_bd; ?> " title="Color Activado"> </div>  
    </li>

    <?php }  } ?>
    </ul>
</div>
    
</body>
</html>