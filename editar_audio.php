<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
    $id_user = $_SESSION['id'];
    $id_audio     = $_GET['id'];
    ?>
<!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
        </head>

        <body id="mimin" class="dashboard">
            <?php include('menu_header.php'); ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>

                <div id="content">
                    <br><br>

                    <div class="col-md-12">
                        <div class="col-md-12 panel">
                            <div class="col-md-12 panel-heading">
                                <h4 style="text-align: center; color: black;"> Actualizar Datos del 
                                    <strong style="color:crimson;">"AUDIO"</strong> 
                                </h4>
                                <h5 style="text-align: right;"><a href="add_audio.php">
                                        <span class="icon-action-undo"></span>
                                        Volver
                                    </a>
                                </h5>
                                <br>
                            </div>
                            <?php
                            $sql = ("SELECT * FROM archivos_audio WHERE id='".$id_audio."' LIMIT 1");
                            $mostar = mysqli_query($con, $sql);
                              while ($row = mysqli_fetch_array($mostar)) {?>
                            <form  enctype="multipart/form-data" method="post" action="recib_update_audio.php">
                                <div class="col-md-12 panel-body">
                                        <div class="col-md-6">
                                            <div class="input-group fileupload-v1">
                                                <input type="file" id="archivo[]" name="archivo[]"  required="required" class="fileupload-v1-file hidden">
                                                <input type="text" class="form-control fileupload-v1-path" placeholder="Audio . . . ." disabled>
                                                <span class="input-group-btn">
                                                    <button class="btn fileupload-v1-btn" type="button"><i class="fa fa-folder"></i> Presione Examinar</button>
                                                </span>

                                            </div>
                                        </div>
                                        <input type="hidden" name="id" value="<?php echo $row['id']; ?>" >
                                                    
                                            <div class="col-md-6">
                                                <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                    <input type="text" class="form-text" id="validate_firstname" name="titulo_archivo" value="<?php echo $row['titulo_archivo']; ?>" required autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>NOMBRE O TITULO DEL AUDIO</label>
                                                </div>
                                            </div>

                                 <div class="col-md-12 panel-body">
                                     <div class="col-md-12">
                                        <div class="col-md-6"  style="float:right;">
                                                <button class="btn ripple btn-raised btn-success" name="enviar">
                                                    <div>
                                                        <span>Actualizar </span>
                                                    </div>
                                                </button>
                                            </div>
                                    </div>
                                </div>

                                </div>
                            </form>
                            <?php } ?>
                    </div>
                </div>
            </div>
            </div>
            </div>


            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse" > 
    <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

    <?php include('js.html'); ?>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>