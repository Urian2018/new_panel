<?php
session_start();
include('config.php');

mysqli_query($con," TRUNCATE TABLE `temporal_vcard_pdf` ");

/*$pdo = new PDO('mysql:host=localhost;dbname=new_panel', 'root', '12345678');
$sql = "TRUNCATE TABLE `temporal_vcard_pdf`";
$statement = $pdo->prepare($sql);
$statement->execute();
*/

if (isset($_SESSION['user']) != "") {
    ?>
<!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
            <style type="text/css" media="screen">
                 .fa-file-pdf-o:hover{
                    color: black;
                 }
             </style> 

            <!----js para mostrar msj--->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
            <script src="asset/js/ajax.js"></script>
            <script type="text/javascript">
            $(function(){
              $(".miform").keypress(function(e) {
                if(e.which == 13) {
                    enviarDatosEmpleado();
                }
              });
            });
        </script>
        
        </head>

        <body id="mimin" class="dashboard">
            <?php 
               include('menu_header.php');
             ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>

                <div id="content">
                    <br>
                    <div class="col-md-12">
                        <div class="col-md-12 panel">
                            <div class="col-md-12 panel-heading">
                                <h4 style="text-align: center; color: black;"> General <strong style="color:crimson;">"PDF"</strong> de VCard</h4>
                                <br>
                            </div>
                            
                            <form name="formulario" class="miform" id="formulario" action="" onsubmit="enviarDatosEmpleado(); return false">
                                <div class="col-md-12 panel-body">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <label>CODIGO VCARD</label>
                                                <div class="form-group form-animate-text">
                                                    <input type="text" name="cod_vcard" id="cod_vcard" style="text-transform:uppercase; width: 60%;" autocomplete="off"  autofocus/>
                                                    <span class="bar"></span>
                                                </div>
                                        </div>
                                        <div class="col-md-3">
                                            <label>CANTIDAD</label>
                                                <div class="form-group form-animate-text">
                                                    <input type="text" name="cantidad" id="cantidad" style="width: 60%;" value="1" />
                                                    <span class="bar"></span>
                                                </div>
                                        </div>
                                        <div class="col-md-3">
                                                <div class="form-group form-animate-text">
                                                    <a href="pdf_vcard.php" target="_blank">
                                                        <span style="font-size: 20px;" class="fa fa-file-pdf-o" title="Desacargar VCard en PDF"></span> 
                                                    Descargar VCard en PDF</a>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                                </form>

                        <div id="resultado">


                        </div>

                        </div>
                    </div>
                </div>
            </div>


            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse" > 
    <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

<!-- start: Javascript -->
<script type="text/javascript" src="asset/js/jquery.ui.min.js"></script>
<script type="text/javascript" src="asset/js/bootstrap.min.js"></script>

<!-- plugins -->
<script type="text/javascript" src="asset/js/plugins/moment.min.js"></script>
<script type="text/javascript" src="asset/js/plugins/jquery.datatables.min.js"></script>
<script type="text/javascript" src="asset/js/plugins/datatables.bootstrap.min.js"></script>
<script type="text/javascript" src="asset/js/plugins/jquery.nicescroll.js"></script>
<script type="text/javascript" src="asset/js/plugins/jquery.vmap.min.js"></script>
<script type="text/javascript" src="asset/js/plugins/jquery.vmap.sampledata.js"></script>

<!-- custom -->
<script type="text/javascript" src="asset/js/main.js"></script>


<!----BUSCADOR AUTOMATICO----->
<script type="text/javascript">
    $(document).ready(function(){
     $('#cod_vcard').typeahead({
      source: function(cod_vcard, result)
      {
       $.ajax({
        url:"busqueda_form_vcard_pdf.php",
        method:"POST",
        data:{cod_vcard:cod_vcard},
        dataType:"json",
        success:function(data)
        {
         result($.map(data, function(item){
          return item;
         }));
        }
       })
      }
     });
     
    });
</script>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>