<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
$cod_cliente_jefe = $_POST['cod_vcard_cliente_jefe'];

$id_empleado = $_POST['id'];
$cod_vcard   = $_POST['cod_vcard'];
$empresa     = $_POST['empresa'];
$name        = $_POST['name'];

$verificando_existencia = ("SELECT * FROM myclientes WHERE cod_vcard='" . $cod_cliente_jefe . "' LIMIT 1 ");
$query_existencia = mysqli_query($con, $verificando_existencia);
$total_empleado = mysqli_num_rows($query_existencia) ;

while ($row = mysqli_fetch_array($query_existencia)) {
$new_empresa     = $row['empresa'];
$new_rfc_empresa = $row['rfc_empresa'];
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="VCARD">
<meta name="author" content="ALEJANDRO TORRES">
<meta name="keyword" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/png" href="../favicon.png" />
<title>VCARD</title>
<?php include('css.html'); ?>
<link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
</head>

<body id="mimin" class="dashboard">
<?php include('menu_header.php'); ?>

<div class="container-fluid mimin-wrapper">
    <?php include('menu_lateral_escritorio.php'); ?>

    <div id="content">
        <br><br>
        <div class="col-md-12">
            <div class="col-md-12 panel">
                <div class="col-md-12 panel-heading">
                    <h4 style="text-align: center; color: black;"><strong style="color:crimson;">"ALERTA"</strong> PARA MIGRAR <strong style="color:crimson;">"EMPLEADO"</strong></h4>
                    <br>
                </div>
                <form  method="post" action="recib_migrar.php">
                    <div class="col-md-12 panel-body">
                        <div class="col-md-12">
                            <input type="hidden" name="cod_vcard" value="<?php echo $cod_vcard; ?>">
                            <input type="hidden" name="cod_vcard_cliente_jefe" value="<?php echo $cod_cliente_jefe; ?>">
                            <div class="col-md-12 panel-body">
                                <div class="col-md-12">
                                    <div class="form-group form-animate-text">
                                    <p style="text-align: center;">El empleado <strong style="color: #333;"><?php echo $name; ?>,</strong>
                                        el cual coresponde al Codigo VCard <strong style="color: #333;"><?php echo $cod_vcard; ?></strong> 
                                        y actualmente pertenece ala Empresa <strong style="color: #333;"><?php echo $empresa; ?></strong>
                                    </p>
                                    <?php
                                    if($total_empleado !=''){ ?>
                                        <p style="text-align:center;">Dicho empleado fue elejido para pertenecer desde ahora ala
                                        empresa <strong style="color: #333;"><?php echo $new_empresa; ?></strong>
                                         la misma corresponde al RFC <strong style="color: #333;"><?php echo $new_rfc_empresa; ?></strong>
                                    </p>

                                    <h3 style="text-align: center; color: crimson;">Esta Seguro que desea migrar este registro ?</h3>
                                    
                                    <div class="col-md-12 panel-body">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                          <a href="busqueda_myclientes.php"  class="btn btn-raised btn-danger" style="width: 100%;">
                                                <div>
                                                    <span>Salir</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-6"  style="float:right;">
                                            <button class="btn ripple btn-raised btn-success" name="enviar">
                                                <div>
                                                    <span>Migrar Empleado</span>
                                                </div>
                                            </button>
                                        </div>
                                        </div>
                                    </div>
                    
                                    <?php }else{ ?>
                                    <h3 style='text-align: center; color: crimson;'>
                                        Por Favor verifique este Codigo VCard no existe 
                                    </h3> 
                                    <h3 style='color:#333; text-align: center;'>
                                        <strong><?php echo $cod_cliente_jefe; ?></strong>
                                    </h3>

                                    <div class="col-md-12">
                                      <a href="busqueda_myclientes.php"  class="btn btn-raised btn-danger" style="width: 100%;">
                                            <div>
                                                <span>Salir</span>
                                            </div>
                                        </a>
                                    </div>

                                    <?php }  ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>                     

    </div>
</div>


<!-- start: Mobile -->
<div id="mimin-mobile" class="reverse" > 
    <?php include('menu_movil.php'); ?>
</div>
<button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
    <span class="fa fa-bars"></span>
</button>
<!-- end: Mobile -->

<?php include('js.html'); ?>
</body>
</html>
<?php
} else {
include('error.php');
}
?>