<?php
require_once('config.php');
$codigo_vcard = isset($_POST['vc']) ? $_POST['vc'] : $_GET['vc'];

$sql = mysqli_query($con, "SELECT * FROM myclientes WHERE cod_vcard='" . $codigo_vcard . "' LIMIT 1 ");
$resultado_vcard = mysqli_num_rows($sql);

if($resultado_vcard > 0){
while ($row = mysqli_fetch_array($sql)) {
    $id_logo_cliente = $row['id'];

    $cod_vcard      = $row['cod_vcard'];
    $name           = $row['nombre'];
    $name_client    = $row['nombre'];
    $cargo          = $row['cargo'];
    $empresa        = $row['empresa'];
    $ciudad         = $row['ciudad'];
	$ciudad_comp    = $row['ciudad_compa'];
    $telefono       = $row['telefono'];
    $tlf_movil      = $row['tlf_movil'];
    $email          = $row['email'];
    $cp             = $row['cp'];
    $logo_peq       = $row['logo_peque'];
    $logo_grande    = $row['logo_grand'];
    $pagina_descarga        = $row['pagina_descarga'];
    $pagina_compartir       = $row['pagina_compartir'];
    $pagina_galeria         = $row['pagina_galeria'];
    $ruta_icono_compartir   = $row['ruta_icono_compartir'];
    $logo_galeria           = $row['logo_galeria'];
    $texto_whasapp          = $row['texto_whatsapp'];
    $web_cliente            = $row['web_cliente'];
    $codigo_pais            = $row['codigo_pais'];
    $pais                   = $row['pais'];
    $direccion              = $row['direccion'];
	$dir_l2                 = $row['dir_l2'];
	$dir_l3                 = $row['dir_l3'];
    $rfc_empresa            = $row['rfc_empresa'];
	$facebook               = $row['facebook'];
	$twitter                = $row['twitter'];
	$instagram              = $row['instagram'];
	$youtube                = $row['youtube'];
	$linkedin               = $row['linkedin'];
	$googleplus             = $row['gogle_mas'];
	$nota                   = $row['nota'];
	$actividad1             = $row['actividad1'];
	$actividad2             = $row['actividad2'];
	$actividad3             = $row['actividad3'];
	$actividad4             = $row['actividad4'];
	$actividad5             = $row['actividad5'];
	$actividad6             = $row['actividad6'];
	$acercade_txt           = $row['acercade_txt'];
    $actividad1_breve       = $row['actividad1_breve'];
	$actividad2_breve       = $row['actividad2_breve'];
	$actividad3_breve       = $row['actividad3_breve'];
	$actividad4_breve       = $row['actividad4_breve'];
	$actividad5_breve       = $row['actividad5_breve'];
	$actividad6_breve       = $row['actividad6_breve'];
	$blog1_titulo           = $row['blog1_titulo'];
	$blog2_titulo           = $row['blog2_titulo'];
	$blog3_titulo           = $row['blog3_titulo'];
	$blog1_desc             = $row['blog1_desc'];
	$blog2_desc             = $row['blog2_desc'];
	$blog3_desc             = $row['blog3_desc'];
}

//MIS VARIABLES
$web_vcard = "https://vcard.mx/";
$folder = "https://vcard.mx/_/logos_p/" . $logo_peq;
$ruta_logo_peque = $folder;
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-50408658-5"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag("js", new Date());
            gtag("config", "UA-50408658-5");
        </script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" type="image/png" href="../favicon.png" />

        <!-- METAS FACEBOOK -->
        <meta content='<?php echo $name . ' - ' . $empresa; ?>' property='og:title'/>
        <meta content='<?php echo $ruta_logo_peque; ?>'   property='og:image'/>
        <meta content='Portafolio VCard' property='og:description'/>

        <link rel="apple-touch-icon-precomposed" sizes="72x72"   href="https://vcard.mx/apple-touch-icon-72x72-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="https://vcard.mx/apple-touch-icon-114x114precomposed.png" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="https://vcard.mx/apple-touch-icon-144x144-precomposed.png" />
        <title>Portafolio <?php echo $name . ' - ' . $empresa; ?></title>

        <!-- Facebook Opengraph integration: https://developers.facebook.com/docs/sharing/opengraph -->
        <meta property="og:title" content="">
        <meta property="og:image" content="">
        <meta property="og:url"   content="">
        <meta property="og:site_name"   content="">
        <meta property="og:description" content="">

        <!-- Twitter Cards integration: https://dev.twitter.com/cards/  -->
        <meta name="twitter:card"  content="summary">
        <meta name="twitter:title" content="<?php echo $name . ' - ' . $empresa; ?>">
        <meta name="twitter:site"  content="@Vcardmx">
        <meta name="twitter:description" content="Portafolio de Trabajo en nuestra VCard">
        <meta name="twitter:image" content="logos_g/<?php echo $logo_grande; ?>">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i|Playfair+Display:400,400i,700,700i,900,900i" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="https://vcard.mx/_/tema-p/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="https://vcard.mx/_/tema-p/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="https://vcard.mx/_/tema-p/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="https://vcard.mx/_/tema-p/lib/magnific-popup/magnific-popup.css" rel="stylesheet">
  <link href="https://vcard.mx/_/tema-p/lib/hover/hover.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="https://vcard.mx/_/tema-p/css/style.css" rel="stylesheet">

  <!-- Responsive css -->
  <link href="https://vcard.mx/_/tema-p/css/responsive.css" rel="stylesheet">

  <!-- Favicon -->
  <link rel="shortcut icon" href="https://vcard.mx/favicon.png">

  <!-- =======================================================
    Theme Name: Folio
    Theme URL: https://bootstrapmade.com/folio-bootstrap-portfolio-template/
    Author: BootstrapMade.com
    Author URL: https://bootstrapmade.com
  ======================================================= -->
</head>

<body>

  <!-- start section navbar -->
  <nav id="main-nav">
    <div class="row">
      <div class="container">

        <div class="logo">
          <a href="1p.html"><img src="<?php echo $ruta_logo_peque; ?>" alt="logo"></a>
        </div>

        <div class="responsive"><i data-icon="m" class="ion-navicon-round"></i></div>

        <ul class="nav-menu list-unstyled">
          <li><a href="#header" class="smoothScroll">Inicio</a></li>
          <li><a href="#about" class="smoothScroll">Acerca de</a></li>
          <li><a href="#portfolio" class="smoothScroll">Portafolio</a></li>
          <li><a href="#journal" class="smoothScroll">Blog</a></li>
          <li><a href="#contact" class="smoothScroll">Contacto</a></li>
        </ul>

      </div>
    </div>
  </nav>
  <!-- End section navbar -->


  <!-- start section header -->
  <div id="header" class="home">

    <div class="container">
      <div class="header-content">
        <h1>- <span class="typed"></span></h1>
        <p><a href="<?php echo $name . ' - ' . $actividad1 . ' - ' . $actividad2 . ' - ' . $actividad3 . ' - ' . $actividad4 . ' - ' . $actividad5 . ' - ' . $actividad6; ?>"></p>

        <ul class="list-unstyled list-social">
          <li><a href="<?php echo $facebook; ?>"><i class="ion-social-facebook"></i></a></li>
          <li><a href="<?php echo $twitter; ?>"><i class="ion-social-twitter"></i></a></li>
          <li><a href="<?php echo $instagram; ?>"><i class="ion-social-instagram"></i></a></li>
          <li><a href="<?php echo $googleplus; ?>"><i class="ion-social-googleplus"></i></a></li>
		  <li><a href="<?php echo $linkedin; ?>"><i class="ion-social-linkedin"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
  <!-- End section header -->


  <!-- start section about us -->
  <div id="about" class="paddsection">
    <div class="container">
      <div class="row justify-content-between">

        <div class="col-lg-4 ">
          <div class="div-img-bg">
            <div class="about-img">
              <img src="<?php echo $ruta_logo_peque; ?>" class="img-responsive" alt="me">
            </div>
          </div>
        </div>

        <div class="col-lg-7">
          <div class="about-descr">

            <p class="p-heading">Acerca de </p>
            <p class="separator"><?php echo $acercade_txt; ?></p>

          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- end section about us -->


  <!-- start section services --> <!-- LOS ICONOS DEBEN DE SER MODIFICABLES -->
  <div id="services">
    <div class="container">

        <div class="services-carousel owl-theme">

          <div class="services-block">

            <i class="ion-ios-browsers-outline"></i>
            <span><?php echo $actividad1; ?></span>
            <p class="separator"><?php echo $actividad1_breve; ?></p>

          </div>

          <div class="services-block">

            <i class="ion-ios-lightbulb-outline"></i>
            <span><?php echo $actividad2; ?></span>
            <p class="separator"><?php echo $actividad2_breve; ?></p>

          </div>

          <div class="services-block">

            <i class="ion-ios-color-wand-outline"></i>
            <span><?php echo $actividad3; ?></span>
            <p class="separator"><?php echo $actividad3_breve; ?></p>

          </div>

          <div class="services-block">

            <i class="ion-social-android-outline"></i>
            <span><?php echo $actividad4; ?></span>
            <p class="separator"><?php echo $actividad4_breve; ?> </p>

          </div>

          <div class="services-block">

            <i class="ion-ios-analytics-outline"></i>
            <span><?php echo $actividad5; ?></span>
            <p class="separator"><?php echo $actividad5_breve; ?></p>

          </div>

          <div class="services-block">

            <i class="ion-ios-camera-outline"></i>
            <span><?php echo $actividad6; ?></span>
            <p class="separator"><?php echo $actividad6_breve; ?></p>

          </div>

        </div>

    </div>

  </div>
  <!-- end section services -->


  <!-- start section portfolio -->
  <div id="portfolio" class="text-center paddsection">

    <div class="container">
      <div class="section-title text-center">
        <h2>Portafolio <?php echo $name . ' - ' . $empresa; ?></h2>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-md-12">

          <div class="portfolio-list">

            <ul class="nav list-unstyled" id="portfolio-flters">
              <li class="filter filter-active" data-filter=".all">TODO</li>
              <li class="filter" data-filter=".branding"><?php echo $actividad1; ?></li>
              <li class="filter" data-filter=".mockups"><?php echo $actividad2; ?></li>
              <li class="filter" data-filter=".uikits"><?php echo $actividad3; ?></li>
              <li class="filter" data-filter=".webdesign"><?php echo $actividad4; ?></li>
              <li class="filter" data-filter=".photography"><?php echo $actividad5; ?></li>
			  <li class="filter" data-filter=".photography"><?php echo $actividad6; ?></li>
            </ul>

          </div>

         <!-- AQUI EN CONTENEDOR DE IMAGENES DEL PORTAFOLIO... HACER COINCIDIR -->
          <div class="portfolio-container">

            <div class="col-lg-4 col-md-6 portfolio-thumbnail all branding uikits webdesign">
              <a class="popup-img" href="https://vcard.mx/_/tema-p/images/portfolio/1.jpg">
                <img src="https://vcard.mx/_/tema-p/images/portfolio/1.jpg" alt="img">
              </a>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-thumbnail all mockups uikits photography">
              <a class="popup-img" href="https://vcard.mx/_/tema-p/images/portfolio/2.jpg">
                <img src="https://vcard.mx/_/tema-p/images/portfolio/2.jpg"  alt="img">
              </a>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-thumbnail all branding webdesig photographyn">
              <a class="popup-img" href="https://vcard.mx/_/tema-p/images/portfolio/3.jpg">
                <img src="https://vcard.mx/_/tema-p/images/portfolio/3.jpg" alt="img">
              </a>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-thumbnail all mockups webdesign photography">
              <a class="popup-img" href="https://vcard.mx/_/tema-p/images/portfolio/4.jpg">
                <img src="https://vcard.mx/_/tema-p/images/portfolio/4.jpg" alt="img">
              </a>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-thumbnail all branding uikits photography">
              <a class="popup-img" href="https://vcard.mx/_/tema-p/images/portfolio/5.jpg">
                <img src="https://vcard.mx/_/tema-p/images/portfolio/5.jpg" alt="img">
              </a>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-thumbnail all mockups uikits webdesign">
              <a class="popup-img" href="https://vcard.mx/_/tema-p/images/portfolio/6.jpg">
                <img src="https://vcard.mx/_/tema-p/images/portfolio/6.jpg" alt="img">
              </a>
            </div>
			
			<div class="col-lg-4 col-md-6 portfolio-thumbnail all mockups uikits webdesign">
              <a class="popup-img" href="https://vcard.mx/_/tema-p/images/portfolio/6.jpg">
                <img src="https://vcard.mx/_/tema-p/images/portfolio/6.jpg" alt="img">
              </a>
            </div>

          </div>
        </div>
      </div>
    </div>

  </div>
  <!-- End section portfolio -->


  <!-- start section journal -->
  <div id="journal" class="text-left paddsection">

    <div class="container">
      <div class="section-title text-center">
        <h2>BLOG</h2>
      </div>
    </div>

    <div class="container">
      <div class="journal-block">
        <div class="row">

          <div class="col-lg-4 col-md-6">
            <div class="journal-info">

              <a href="blog-single.html"><img src="https://vcard.mx/_/tema-p/images/blog-post-1.jpg" class="img-responsive" alt="img"></a>

              <div class="journal-txt">

                <h4><a href="blog-single.html"><?php echo $blog1_titulo; ?></a></h4>
                <p class="separator"><?php echo $blog1_desc; ?>
                </p>

              </div>

            </div>
          </div>

          <div class="col-lg-4 col-md-6">
            <div class="journal-info">

              <a href="blog-single.html"><img src="https://vcard.mx/_/tema-p/images/blog-post-2.jpg" class="img-responsive" alt="img"></a>

              <div class="journal-txt">

                <h4><a href="#blog-single.html"><?php echo $blog2_titulo; ?></a></h4>
                <p class="separator"><?php echo $blog2_desc; ?>
                </p>

              </div>

            </div>
          </div>

          <div class="col-lg-4 col-md-6">
            <div class="journal-info">

              <a href="blog-single.html"><img src="https://vcard.mx/_/tema-p/images/blog-post-3.jpg" class="img-responsive" alt="img"></a>

              <div class="journal-txt">

                <h4><a href="blog-single.html"><?php echo $blog3_titulo; ?></a></h4>
                <p class="separator"><?php echo $blog3_desc; ?>
                </p>

              </div>

            </div>
          </div>

        </div>
      </div>
    </div>

  </div>
  <!-- End section journal -->


  <!-- start sectoion contact -->
  <div id="contact" class="paddsection">
    <div class="container">
      <div class="contact-block1">
        <div class="row">

          <div class="col-lg-6">
            <div class="contact-contact">

              <h2 class="mb-30">CONTACTO</h2>

              <ul class="contact-details">
                <li><span><?php echo $direccion; ?></span></li>
                <li><span><?php echo $dir_l2 . ', ' . $dir_l3; ?></span></li>
				<li><span><?php echo $ciudad_comp . ', ' . $pais; ?></span></li>
                <li><span><?php echo $tlf_movil; ?></span></li>
                <li><span><?php echo $email; ?></span></li>
              </ul>

            </div>
          </div>

          <div class="col-lg-6">
            <form action="" method="post" role="form" class="contactForm">
              <div class="row">

                <div id="sendmessage">Tu mensaje ha sido enviado. Gracias!</div>
                <div id="errormessage"></div>

                <div class="col-lg-6">
                  <div class="form-group contact-block1">
                    <input type="text" name="name" class="form-control" id="name" placeholder="Nombre" data-rule="minlen:4" data-msg="Entre al menos 4 caracteres" />
                    <div class="validation"></div>
                  </div>
                </div>

                <div class="col-lg-6">
                  <div class="form-group">
                    <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" data-rule="email" data-msg="Ingrese un e-mail válido" />
                    <div class="validation"></div>
                  </div>
                </div>

                <div class="col-lg-12">
                  <div class="form-group">
                    <input type="text" class="form-control" name="subject" id="subject" placeholder="Teléfono" data-rule="minlen:4" data-msg="Ingrese teléfono a 10 dígitos" />
                    <div class="validation"></div>
                  </div>
                </div>

                <div class="col-lg-12">
                  <div class="form-group">
                    <textarea class="form-control" name="message" rows="12" data-rule="required" data-msg="Escribe algo para nosotros" placeholder="Mensaje"></textarea>
                    <div class="validation"></div>
                  </div>
                </div>

                <div class="col-lg-12">
                  <input type="submit" class="btn btn-defeault btn-send" value="Enviar mensaje">
                </div>

              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- start sectoion contact -->


  <!-- start section footer -->
  <div id="footer" class="text-center">
    <div class="container">
      <div class="socials-media text-center">

        <ul class="list-unstyled">
          <li><a href="<?php echo $facebook; ?>"><i class="ion-social-facebook"></i></a></li>
          <li><a href="<?php echo $twitter; ?>"><i class="ion-social-twitter"></i></a></li>
          <li><a href="<?php echo $instagram; ?>"><i class="ion-social-instagram"></i></a></li>
          <li><a href="<?php echo $googleplus; ?>"><i class="ion-social-googleplus"></i></a></li>
          <li><a href="<?php echo $linkedin; ?>"><i class="ion-social-linkedin"></i></a></li>
        </ul>

      </div>

      <p>&copy; VCard Copyright - Todos los derechos reservados.</p>

      

    </div>
  </div>
  <!-- End section footer -->

  <!-- JavaScript Libraries -->
  <script src="https://vcard.mx/_/tema-p/lib/jquery/jquery.min.js"></script>
  <script src="https://vcard.mx/_/tema-p/lib/jquery/jquery-migrate.min.js"></script>
  <script src="https://vcard.mx/_/tema-p/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="https://vcard.mx/_/tema-p/lib/typed/typed.js"></script>
  <script src="https://vcard.mx/_/tema-p/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="https://vcard.mx/_/tema-p/lib/magnific-popup/magnific-popup.min.js"></script>
  <script src="https://vcard.mx/_/tema-p/lib/isotope/isotope.pkgd.min.js"></script>

  <!-- Contact Form JavaScript File -->
  <script src="https://vcard.mx/_/tema-p/contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="https://vcard.mx/_/tema-p/js/main.js"></script>

</body>

</html>
<?php
date_default_timezone_set("America/Mexico_City");
$fecha = date("d/m/Y"); 
//Verificando si existe el Cod_vcard si existe
$verificando_cod_vcard = mysqli_query($con, "SELECT * FROM visitas WHERE cod_vcard='" . $codigo_vcard . "' LIMIT 1 ");
$result_verifcar = mysqli_num_rows($verificando_cod_vcard);
if ($result_verifcar != '') {
//Actualizando el campo visita
$contador_visita = "1";
$update_contador = ("UPDATE visitas SET visita_pag3=visita_pag3 + '".$contador_visita."' WHERE cod_vcard='".$codigo_vcard."'");
$result_update = mysqli_query($con, $update_contador);
 } else{
$add_cod_vcard = "INSERT INTO visitas (cod_vcard, nombre, empresa, rfc_empresa, fecha) VALUES ('$codigo_vcard','$name_client','$empresa', '$rfc_empresa','$fecha')";
$result_insert = mysqli_query($con, $add_cod_vcard);  
if($result_insert >0){
$contador_visita = "1";
$update_contador = ("UPDATE visitas SET visita_pag3=visita_pag3 + '".$contador_visita."' WHERE cod_vcard='".$codigo_vcard."'");
$result_update = mysqli_query($con, $update_contador);
        } 
    }
 } else{ 
    header("location:https://vcard.mx/inactiva/");
}  ?>
