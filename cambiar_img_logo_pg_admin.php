<?php
require('config.php');
$id_cliente_vcard   = $_REQUEST['id'];
$name_actual = $_POST['filename'];
$file = $_FILES['img_vcard']['name'];
$nombre_sin_extension = pathinfo($name_actual, PATHINFO_FILENAME); //funcion para sber solo el nombre de un archivo
$rtOriginal = $_FILES['img_vcard']['tmp_name'];

$explode = explode('.', $file);
$extension = array_pop($explode);

//contar imagen de un directorio de logos grandes
$total_imagenes = count(glob('logos_g/{*.jpg,*.gif,*.png,*.jpeg,*.PNG,*.JPG}',GLOB_BRACE));
$new_total_img = $total_imagenes + 1;

$name_archivo = "logoG_".$new_total_img.'.';
$mi_file = $name_archivo.$extension;


/*****contar imagenes del directorio de logos pequeños*******/
$total_imagenes_p = count(glob('logos_p/{*.jpg,*.gif,*.png,*.jpeg,*.PNG,*.JPG}',GLOB_BRACE));
$new_total_img_p = $total_imagenes_p + 1;

$name_archivo_p = "logoP_".$new_total_img_p.'.';
$mi_file_p = $name_archivo_p.$extension;


$png = 'png';
$PNG = 'PNG';
/**CREANDO LOGO GRANDE PNG */
if ($extension == $png || $extension == $PNG ) {
$original =  imagecreatefrompng($rtOriginal);
$originall =  imagecreatefrompng($rtOriginal);

$max_ancho = 400;
$max_alto = 400;

list($ancho,$alto)=getimagesize($rtOriginal);
$x_ratio = $max_ancho / $ancho;
$y_ratio = $max_alto / $alto;

if(($ancho <= $max_ancho) && ($alto <= $max_alto) ){
    $ancho_final = $ancho;
    $alto_final = $alto;
}
else if(($x_ratio * $alto) < $max_alto){
    $ancho_final = $max_ancho;
    $alto_final = ceil($x_ratio * $alto);
}
else {
    $ancho_final = $max_ancho;
    $alto_final = $max_alto;
}

$lienzo=imagecreatetruecolor($ancho_final,$alto_final);
imagecopyresampled($lienzo,$original,0,0,0,0,$ancho_final, $alto_final,$ancho,$alto);
imagedestroy($original);
imagepng($lienzo,"logos_g/".$mi_file);
        
$update_logo_G = ("UPDATE myclientes SET logo_grand='" .$mi_file. "', logo_peque='".$mi_file."'  WHERE id='".$id_cliente_vcard."' ");
$result = mysqli_query($con, $update_logo_G); 
include('add_logo_g_ajax.php'); 

}else{
/****CREANDO LOGO GRANDE JPG ****/    
$original_lg = imagecreatefromjpeg($rtOriginal);
$original_lp = imagecreatefromjpeg($rtOriginal);
$max_ancho_lg = 400; 
$max_alto_lg = 400;

list($ancho_lg,$alto_lg)=getimagesize($rtOriginal);
$x_ratio_lg = $max_ancho_lg / $ancho_lg;
$y_ratio_lg = $max_alto_lg / $alto_lg;

if(($ancho_lg <= $max_ancho_lg) && ($alto_lg <= $max_alto_lg) ){
    $ancho_final_lg = $ancho_lg;
    $alto_final_lg = $alto_lg;
}
else if(($x_ratio_lg * $alto_lg) < $max_alto_lg){
    $alto_final_lg = ceil($x_ratio_lg * $alto_lg);
    $ancho_final_lg = $max_ancho_lg;
}
else {
    $ancho_final_lg = ceil($y_ratio_lg * $ancho_lg);
    $alto_final_lg = $max_alto_lg;
}

$lienzo_lg=imagecreatetruecolor($ancho_final_lg,$alto_final_lg);
imagecopyresampled($lienzo_lg,$original_lg,0,0,0,0,$ancho_final_lg, $alto_final_lg,$ancho_lg,$alto_lg);
imagedestroy($original_lg);
imagejpeg($lienzo_lg,"logos_p/".$mi_file_p);

$update_logo_G = ("UPDATE myclientes SET logo_peque='" .$mi_file_p. "', logo_grand='" .$mi_file_p. "'  WHERE id='".$id_cliente_vcard."' ");
$result = mysqli_query($con, $update_logo_G); 
include('add_logo_p_ajax.php'); 
}
?>