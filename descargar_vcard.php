<?php
include('config.php');
$cod_vcard = $_GET['cod_vcard'];
$recib_nombre_vcard = $_GET['nombre'];
$nombre_file ="VCard ".$recib_nombre_vcard;

header('Cache-Control: public');
header('Content-Description: File Transfer');
header("Content-Disposition: attachment; filename=$nombre_file.vcf");
header('Content-Type: text/x-vcard');

$consulta_vcard = ("SELECT * FROM myclientes WHERE cod_vcard='" .$cod_vcard. "'");
$mostar = mysqli_query($con, $consulta_vcard);

while ($row = mysqli_fetch_array($mostar)) {
    $name           = $row['nombre'];
    $cargo          = $row['cargo'];
    $empresa        = $row['empresa'];
    $email          = $row['email'];		
    $email_dos      = $row['email_dos'];	
    $email_tres     = $row['email_tres'];
    $telefono       = $row['telefono'];
    $uso_telefono   = $row['uso_tlf'];
    $web_cliente    = $row['web_cliente'];
    $direccion      = $row['direccion'];
	$dir_l2         = $row['dir_l2'];
	$dir_l3         = $row['dir_l3'];
    $direccion_dos  = $row['direccion_dos'];
    $direccion_tres = $row['direccion_tres'];
	$direccion_cuatro = $row['direccion_cuatro'];
	$direccion_cinco = $row['direccion_cinco'];
    $foto_vcard     = $row['img_vcard'];
    $tlf_dos        = $row['tlf_dos'];
    $uso_tlf_dos    = $row['uso_tlf_dos'];
    $tlf_tres       = $row['tlf_tres'];
    $uso_tlf_tres   = $row['uso_tlf_tres'];
    $tlf_cuatro     = $row['tlf_cuatro'];
    $uso_tlf_cuatro = $row['uso_tlf_cuatro'];
    $movil          = $row['tlf_movil'];
    $web_dos_cliente = $row['web_cliente_dos'];
    $facebook       = $row['facebook'];
    $twitter        = $row['twitter'];
    $instagram      = $row['instagram'];
    $youtube        = $row['youtube'];
    $linkedin       = $row['linkedin'];
    $google_mas     = $row['gogle_mas'];
    $nota           = $row['nota'];
    $cod_ciudad     = $row['cod_ciudad'];
    $tipo_cliente   = $row['tipo_cliente'];
    $city           = $row['ciudad'];
    $rango_img      = $row['rango_imagenes'];
    $rfc            = $row['rfc'];
    $ciudad_completa = $row['ciudad_completa'];
    $cp              = $row['cp'];
    $pais            = $row['pais'];

    $redireccionamiento_contacto = 'Compartir-Contacto.vcard.mx/c.php?vc=' . $cod_vcard;
    $redireccionamiento_galeria  = 'Galeria-de-Imagenes.vcard.mx/g.php?vc=' . $cod_vcard;
     
    /******************
    $mi_directorio = 'https://vcard.mx/_/';
    $url_redireccionamiento_galeria = $mi_directorio."Compartir-Contacto/";
    $url_redireccionamiento_contacto = $mi_directorio."Galeria-de-imagenes/";

    $redireccionamiento_contacto = $url_redireccionamiento_contacto.'index.php?vc=' . $cod_vcard;
    $redireccionamiento_galeria  = $url_redireccionamiento_galeria.'index.php?vc=' . $cod_vcard;
    ************/
?>BEGIN:VCARD
VERSION:3.0
PHOTO;ENCODING=b;TYPE=JPEG/PNG:<?php echo($foto_vcard); ?>

N:<?php echo($name); ?>;;;;

FN:<?php echo($name); ?>

TITLE:<?php echo($cargo); ?>

ORG:<?php echo($empresa); ?>

TEL;TYPE=CELL:<?php echo($movil); ?>

TEL;TYPE=<?php echo($uso_telefono); ?>:<?php echo($telefono); ?>

TEL;TYPE=<?php echo($uso_tlf_dos); ?>:<?php echo($tlf_dos); ?>

TEL;TYPE=<?php echo($uso_tlf_tres); ?>:<?php echo($tlf_tres); ?>

TEL;TYPE=<?php echo($uso_tlf_cuatro); ?>:<?php echo($tlf_cuatro); ?>

EMAIL;TYPE=INTERNET;TYPE=WORK:<?php echo($email); ?>

EMAIL;TYPE=INTERNET;TYPE=WORK:<?php echo($email_dos); ?>

EMAIL;TYPE=INTERNET;TYPE=WORK:<?php echo($email_tres); ?>

ADR;type=WORK:;;<?php echo($direccion);?>, <?php echo($dir_l2);?>, <?php echo($dir_l3);?>;<?php echo($ciudad_completa);?>;;<?php echo($cp);?>;<?php echo($pais);?>

ADR;type=WORK:;;<?php echo($direccion_dos);?>

ADR;type=WORK:;;<?php echo($direccion_tres);?>

ADR;type=WORK:;;<?php echo($direccion_cuatro);?>

ADR;type=WORK:;;<?php echo($direccion_cinco);?>

URL;type=Galería:<?php echo($redireccionamiento_galeria); ?>

URL;type=Web:<?php echo($web_cliente); ?>

URL;type=Web:<?php echo($web_dos_cliente); ?>

URL;type=Facebook:<?php echo($facebook); ?>

URL;type=Twitter:<?php echo($twitter); ?>

URL;type=Instagram:<?php echo($instagram); ?>

URL;type=LinkedIn:<?php echo($linkedin); ?>

URL;type=Google+:<?php echo($google_mas); ?>

URL;type=Youtube:<?php echo($youtube); ?>

URL:<?php echo($redireccionamiento_contacto); ?>

NOTE:<?php echo($nota);?>

END:VCARD<?php } ?>