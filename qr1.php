<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="shortcut icon" type="image/png" href="../favicon.png" />
	<title>VCard</title>
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,700|Roboto:400,900" rel="stylesheet">
    <link href="https://vcard.mx/tema/3/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://vcard.mx/tema/3/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
   <!--<link href="https://vcard.mx/tema/3/css/style.css" rel="stylesheet">-->
    <link href="asset/style.css" rel="stylesheet"> <!----descargardado desde el servidor---->
    <link href="asset/css/fontello.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="container text-center">
<?php 
$pagina = $_REQUEST['pag1'];
if (! empty($pagina)) {
require 'QR/phpqrcode/qrlib.php';
$dir = 'QR/temp/';

if(!file_exists($dir))
	mkdir($dir);
$filename = $dir.'test.png';

$tamanio = 10;  //Tamaño de Pixel
$level = 'H'; //Precisión Baja
$frameSize = 1; //Tamaño en blanco
//$contenido = 'hola uriany';

QRcode::png($pagina, $filename, $level, $tamanio, $frameSize);
?>
<br><br>
<br><br>
<div class="row">
    <div class="col-md-12">
		<center><img class="img-fluid" src="<?php echo $filename; ?>"/></center>
	</div>
</div>
<?php
}else{
echo "NO TIENES URL";
}
 ?>	

</div>
</body>
</html>
