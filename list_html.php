<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
    $rfc_empresa =  $_SESSION['rfc_empresa'];
    ?>
    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
            <style type="text/css">
                .icon-pencil:hover{
                    color: #333;
                }
                img{
                    width: 100%;
                    max-width: 100px !important;
                }
            </style>
        </head>

        <body id="mimin" class="dashboard">
            <?php include('menu_header.php'); ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>

                <div id="content">
                    <br>
                    
                    <?php
                   $Consultar_editor = ("SELECT * FROM editor_html WHERE rfc_empresa='".$rfc_empresa."' ORDER BY id ASC");
                   $numero_editor = mysqli_query($con, $Consultar_editor);
                    ?>
                    <div class="col-md-12 top-20 padding-0">
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 style="text-align: center;"> 
                                    <p style="text-align:right;">
                                    <a href="iframe.php" title="Volver">
                                        <span class="icon-action-undo" title="Volver">Volver</span>
                                    </a>
                                    </p>    
                                       LISTA DE CONTENIDO HTML <strong style="color: crimson;">"REGISTRADOS"</strong>
                                    </h3>
                                </div>
                                <div class="panel-body">
                                    <div class="responsive-table">
                                        <table  class="table table-striped table-bordered" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>Contenido</th>
                                                    <th>Borrar</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    <?php 
                                     while ($resul_editor = mysqli_fetch_array($numero_editor)) {
                                     $id = $resul_editor['id']; 
                                     $table = "editor_html";
                                     $delet = "user"; ?>
                                    <tr>
                                        <td>
                                            <?php echo $resul_editor['content']; ?>
                                        </td>
                                        <td style="text-align: center; font-size: 25px;">
                                            <a href="iframe_editar.php?id=<?php echo $id; ?>"> 
                                                <span class="fa icon-pencil" title="Editar Contenido Html"></span>
                                            </a>
                                            <a href="delete.php?id=<?php echo $id; ?>&table=<?php echo $table; ?>&delete=<?php echo $delet; ?>"> 
                                                <span class="fa fa-trash" title="Eliminar Contenido Html"></span>
                                            </a>
                                                       
                                     </tr>
                                    <?php } ?>
                                     </tbody>
                                </table>
                                </div>
                            </div>
                        </div>   
                    </div> 

                </div>

                 <div class="contenedor_flotante">                         
                    <?php
                        if(!empty($_GET['msj'])){ ?>
                        <div class='col-md-12'>
                        <div class='alert col-md-12 col-sm-12 alert-icon alert-danger alert-dismissible fade in' role='alert'>
                            <div class='col-md-2 col-sm-2 icon-wrapper text-center'>
                            <span class='fa fa-flash fa-2x'></span></div>
                            <div class='col-md-10 col-sm-10'>
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>
                                <p><strong>Felicitaciones el Contenido Html fue Borrado Correctamente.</strong></p>
                            </div>
                            </div>
                        </div> 
                <?php } ?>
                    </div>
            </div>
    </div>

            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse" > 
                <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

            <?php include('js.html'); ?>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>