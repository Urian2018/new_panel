<?php
session_start();
header("Content-Type: text/html;charset=utf-8");
include('config.php');
if (isset($_SESSION['user']) != "") {
    ?>
    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <style>
                .icon-pencil:hover{
                    color: black;
                }
                .fa-paste:hover{
                    color: black;
                }
            </style>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
            <script  src="https://code.jquery.com/jquery-2.2.4.js"></script>
        </head>

        <body id="mimin" class="dashboard">
            <?php include('menu_header.php'); ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>

                <div id="content">
                    <br>
                    
                    <?php
                    $sql = ("SELECT * FROM users");
                   if($mostar = mysqli_query($con, $sql)){
                    $total_client = mysqli_num_rows($mostar) ;
                        ?>
                        <div class="col-md-12 top-20 padding-0">
                            <div class="col-md-12">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h4 style="text-align: center;">
                                            <?php echo " Cuentas de Usuarios <strong style='color:green; text-align: center;'>(" .$total_client. ')</strong>'; ?>
                                        </h4>
                                    </div>
                                    <div class="panel-body">
                                        <div class="responsive-table">
                                            <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                                                <thead>
                                                    <tr>
                                                        <th>Usuario</th>
                                                        <th>Clave</th>
                                                        <th>Nombre y Apellido</th>
                                                        <th>Rango Img</th>
                                                        <th>Rango Video</th>
                                                        <th>Rango Audio</th>
                                                        <th>Opci&oacute;n</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    while ($vcard = mysqli_fetch_array($mostar)) {
                                                       ?>
                                                        <tr>
                                                            <td><?php echo $vcard['user']; ?></td>
                                                            <td><?php echo $vcard['pass']; ?></td>
                                                            <td><?php echo $vcard['nombre_apellido'];?></td>
                                                            <td><?php echo $vcard['rango_imagenes']; ?> </td> 
                                                            <td><?php echo $vcard['rango_video']; ?></td> 
                                                            <td><?php echo $vcard['rango_audios']; ?></td>
                                                            <td style="text-align: center; font-size: 25px;">
                                                                <a href="new_clave_admin.php?id=<?php echo $vcard['id'];; ?>"> 
                                                                <span class="fa icon-pencil" title="Editar Datos del Cliente"></span>
                                                            </a>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div>
                        <?php
                    }
                    @mysqli_close($mostar);
                    ?>  
                </div>            
            </div>


            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse" > 
                <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

            <?php include('js.html'); ?>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#datatables-example').DataTable();
                });
            </script>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>