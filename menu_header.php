<nav class="navbar navbar-default header navbar-fixed-top">
    <div class="col-md-12 nav-wrapper">
        <div class="navbar-header" style="width:100%;">
            <div class="opener-left-menu is-open">
                <span class="top"></span>
                <span class="middle"></span>
                <span class="bottom"></span>
            </div>
            <a href="home.php" class="navbar-brand"> 
                <b>VCARD</b>
            </a>

            <ul class="nav navbar-nav navbar-right user-nav">
                <li class="user-name"><span><?php echo strtoupper($_SESSION['nombre_apellido']); ?></span></li>
                <li class="dropdown avatar-dropdown">
                    <img src="asset/img/img_vcard.png"style="width: 50px; border: 2px solid #108DB6 !important;" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"/>
                </li>
            </ul>
        </div>
    </div>
</nav>