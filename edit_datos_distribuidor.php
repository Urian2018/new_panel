<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
    $id_user = $_SESSION['id'];
    $id_distribuidor     = $_GET['id'];
    ?>
<!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
            <script  src="asset/js/jquery.min.js"></script>
        </head>

        <body id="mimin" class="dashboard">
        <?php include('menu_header.php'); ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>

                <div id="content">
                    <br>
                    <div class="col-md-12">
                        <div class="col-md-12 panel">
                            <div class="col-md-12 panel-heading">
                                <h4 style="text-align: center; color: black;"> 
                                Actualizar Datos del 
                                    <strong style="color:crimson;">"DISTRIBUIDOR"</strong> 
                                </h4>

                                <h5 style="text-align: right;"><a href="distribuidor.php">
                                        <span class="icon-action-undo"></span>
                                        Volver
                                    </a>
                                </h5>
                                <br>
                            </div>
                            <?php
                            $sql = ("SELECT * FROM distribuidores WHERE id='".$id_distribuidor."' LIMIT 1");
                            $mostar = mysqli_query($con, $sql);
                              while ($row = mysqli_fetch_array($mostar)) { ?>
                            <form   method="post" action="recib_update_distribuidor.php">
                                <div class="col-md-12 panel-body">
                                    <div class="col-md-12">
                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text" name="usuario" value="<?php echo $row['usuario']; ?>" autocomplete="off">
                                                    <input type="hidden" class="form-text" name="id" value="<?php echo $id_distribuidor ; ?>">
                                                    <span class="bar"></span>
                                                    <label>USUARIO</label>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text"  name="clave" value="<?php echo $row['clave']; ?>" autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>CLAVE</label>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text" >
                                                    <input type="text" class="form-text"  name="fecha_inicio" value="<?php echo $row['fecha_inicio']; ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>FECHA INICIO</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text" name="cod_distribuidor"  value="<?php echo $row['cod_distribuidor']; ?>" autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>CODIGO DISTRIBUIDOR</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                            <label>TIPO DISTRIBUIDOR</label>
                                                <div class="form-group form-animate-text" style="margin-top:-10px !important;">
                                                    <select name="tipo_distribuidor" id="tipo_distribuidor" class="form-control">
                                                        <option value="MASTER">MASTER</option>
                                                        <option value="SENIOR">SENIOR</option>
                                                    </select>
                                                 </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text" >
                                                    <input type="text" class="form-text"  name="nombre"  value="<?php echo $row['nombre']; ?>" autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>NOMBRE</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text" name="ape_paterno"  value="<?php echo $row['ape_paterno']; ?>" autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>APELLIDO PATERNO</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text"  name="ape_materno"  value="<?php echo $row['ape_materno']; ?>" autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>APELLIDO MATERNO</label>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text" >
                                                    <input type="text" class="form-text"  name="dir_contacto" value="<?php echo $row['dir_contacto']; ?>" autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>DIRECCION</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="col-md-3">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text" name="tlf_fijo"  value="<?php echo $row['tlf_fijo']; ?>" autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>TELEFONO FIJO</label>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text"  name="tlf_movil" value="<?php echo $row['tlf_movil']; ?>" autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>TELEFONO MOVIL</label>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group form-animate-text" >
                                                    <input type="text" class="form-text"  name="correo" value="<?php echo $row['correo']; ?>" autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>EMAIL</label>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text" name="correo_dos"  value="<?php echo $row['correo_dos']; ?>" autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>EMAIL OPCIONAL</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-5">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text"  name="dir_envio_paqueteria" value="<?php echo $row['dir_envio_paqueteria']; ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>DIRECCIÓN DE ENVIO</label>
                                                </div>
                                            </div>
                                            <div class="col-md-7" style="margin-top:10px !important;">
                                                <button class="btn ripple btn-raised btn-success" name="enviar">
                                                    <div>
                                                        <span>Actualizar Datos del Distribuidor</span>
                                                    </div>
                                                </button>
                                            </div>

                                        </div>
                                    </div>

                            </form>
                        </div>
                            <?php } ?>
                    </div>
                </div>
            </div>


        <!-- start: Mobile -->
        <div id="mimin-mobile" class="reverse" > 
            <?php include('menu_movil.php'); ?>
        </div>
        <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
            <span class="fa fa-bars"></span>
        </button>
        <!-- end: Mobile -->

        <?php include('js.html'); ?>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>