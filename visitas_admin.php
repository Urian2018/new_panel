<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
    ?>
    
    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">

                <!----js para mostrar msj--->
        <script  src="asset/js/jquery.min.js"></script>
        <script src="asset/js/msj.js"></script>
        </head>

        <body id="mimin" class="dashboard">
            <?php include('menu_header_user.php'); ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>

                <div id="content">

                    <br>
                    <?php
                    //comparar por el rcf y nombre por el nombre dela empresa
                    $Consultar_dos = ("SELECT * FROM visitas");
                    $numero_servi = mysqli_query($con, $Consultar_dos);
                    $total_client = mysqli_num_rows($numero_servi);
                    ?>
                    <div class="col-md-12 top-20 padding-0">
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 style="text-align: center;">VISITAS  DE TODAS LAS   
                                    <strong style="color: crimson;">"WEBS"</strong>  
                                    </h3>
                                </div>
                                <div class="panel-body">
                                    <div class="responsive-table">
                                        <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>C&oacute;digo</th>
                                                    <th>Nombre</th>
                                                    <th>Visitas en Pagina Descarga</th>
                                                    <th>Visitas en Pagina Compartir</th>
                                                    <th>Visitas en Pagina Galeria</th>
                                                    <th>Fecha</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                while ($visitas = mysqli_fetch_array($numero_servi)) {
                                                    ?>
                                                    <tr>
                                                        <td style="text-align: center;"><?php echo $visitas['cod_vcard']; ?></td>
                                                        <td style="text-align: center;"><?php echo $visitas['nombre']; ?></td>
                                                        <td style="text-align: center;"><?php echo $visitas['visita_pag1']; ?></td>
                                                        <td style="text-align: center;"><?php echo $visitas['visita_pag2']; ?></td>
                                                        <td style="text-align: center;"><?php echo $visitas['visita_pag3']; ?></td>
                                                        <td style="text-align: center;"><?php echo $visitas['fecha']; ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <?php
                        @mysqli_close($numero_servi);
                        ?>  
                    </div> 
                </div>
            </div>


            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse" > 
                <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

            <?php include('js.html'); ?>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#datatables-example').DataTable();
                });
            </script>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>