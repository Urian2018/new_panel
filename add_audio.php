<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
    $cod_vcard_cliente = $_SESSION['cod_vcard'];
    $rfc_empresa       = $_SESSION['rfc_empresa'];
    $rango_audios      = $_SESSION['rango_audios'];
    $rfc_cliente       = $_SESSION['rfc'];

    if (isset($_POST['enviar'])) {
        $msj_exito = "";
        $msj_borrar = "";
        //Validamos que el archivo exista
        foreach ($_FILES["archivo"]['tmp_name'] as $key => $tmp_name) {
            //Validamos que el archivo exista
            if ($_FILES["archivo"]["name"][$key]) {

                $titulo_arch = $_POST['titulo_archivo'];
                $archivo = $_FILES['archivo']['name'][$key];
                $explode = explode('.', $archivo);
                $extension = array_pop($explode);

                $filename = $_FILES["archivo"]["name"][$key]; //Obtenemos el nombre original del archivo
                $source = $_FILES["archivo"]["tmp_name"][$key]; //Obtenemos un nombre temporal del archivo
                //Validamos si la ruta de destino existe, en caso de no existir la creamos
                //$carp = "/Arch_Descargable/";
                $carp = "/Arch_Audios/";
                $directorio = "Galeria_Clientes/";
                $folder = $directorio . $cod_vcard_cliente . '/' . $carp;


                if (!file_exists($folder)) {
                    mkdir($folder, 0777, true);
                }

                $total_arch = count(glob($folder . '/{*.mp3}', GLOB_BRACE));
                $new_filename = $total_arch + 1;
                $name_archivo = "Audio_" . $total_arch . '.';
                $mi_file = $name_archivo . $extension;


                $dir = opendir($folder); //Abrimos el directorio de destino
                $target_path = $folder . '/' . $mi_file; //Indicamos la ruta de destino, como el nombre del archivo
                //Movemos y validamos que el archivo se haya cargado correctamente
                //El primer campo es el origen y el segundo el destino
                if (move_uploaded_file($source, $target_path)) {
                    $add = "INSERT INTO archivos_audio (filename,titulo_archivo,cod_vcard,rfc_empresa) VALUES ('$mi_file','$titulo_arch','$cod_vcard_cliente','$rfc_empresa')";
                    $result = mysqli_query($con, $add);
                    $msj_exito = "
                    <div class='col-md-12'>
                    <div class='alert alert-success col-md-12 col-sm-12  alert-icon alert-dismissible fade in' role='alert'>
                    <div class='col-md-2 col-sm-2 icon-wrapper text-center'>
                        <span class='fa fa-check fa-2x'></span></div>
                        <div class='col-md-10 col-sm-10'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                        <span aria-hidden='true'>x</span></button>
                        <p><strong>Felicitaciones el Audio se Registro con Exito.</strong></p>
                        </div>
                    </div>
                    </div>";
                    // header("location:add_audio.php");
                    // exit();
                } else {
                    echo "Error en el Registro . .";
                }
                closedir($dir); //Cerramos el directorio de destino
            }
        }
    }
    ?>

    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
            <!---para cargar la vista previa del audio-->
            <link rel="stylesheet" type="text/css" href="asset/css/plugins/mediaelementplayer.css"/>

            <!----js para mostrar msj--->
            <script  src="asset/js/jquery.min.js"></script>
            <script src="asset/js/msj.js"></script>
        </head>
        <body id="mimin" class="dashboard">
            <?php include('menu_header_user.php'); ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>
                <?php
                $Consultar = ("SELECT * FROM archivos_audio WHERE rfc_empresa='" . $rfc_empresa . "' ORDER BY id ASC ");
                $numero_servicios = mysqli_query($con, $Consultar);
                $total_audio_permitidos = mysqli_num_rows($numero_servicios);
                ?>
                <div id="content">
                    <br>
                    <div class="col-md-12">
                        <div class="col-md-12 panel">
                            <div class="col-md-12 panel-heading">
                                <h4 style="text-align: center; color: black;"> Subir Archivo tipo<strong style="color:crimson;"> AUDIO</strong>.</h4>
                                <br><br>

                                <p style="text-align:right;"><a href="home.php" title="Volver">
                                        <span class="icon-action-undo" title="Volver">Volver</span>
                                    </a>
                                </p>
                            </div>
<p>&nbsp;</p>			<div style="border: solid 2px #000000; ">										<h5 style="text-align: center; color: red;">                                     <strong>REFERENCIA</strong>  <strong style='color:crimson;font-size: 16px;'></strong>                                </h5>														<h6 style="text-align: center; color: black;">                                    - Tipo de archivo permitido: <strong>".MP3"</strong><strong style='color:crimson;font-size: 14px;'></strong>                                </h6>		        </div><p>&nbsp;</p>
                            <div class="col-md-12 panel-body">
                                <div class="col-md-12">
                                    <form  enctype="multipart/form-data" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                                        <div class="col-md-6">
                                            <div class="form-group form-animate-text">
                                                <div class="input-group fileupload-v1">
                                                    <input type="file" id="archivo[]" name="archivo[]"  required="required" class="fileupload-v1-file hidden">
                                                    <input type="text" class="form-control fileupload-v1-path" placeholder="Archivo . . . ." disabled>
                                                    <span class="input-group-btn">
                                                        <button class="btn fileupload-v1-btn" type="button"><i class="fa fa-folder"></i> Presione Examinar</button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>    
                                        <div class="col-md-6">
                                            <label>NOMBRE O TITULO DEL AUDIO</label>
                                            <div class="form-group form-animate-text">
                                                <input type="text" class="form-text" id="validate_firstname" name="titulo_archivo" required autocomplete="off">
                                            </div>
                                        </div>
                                </div>

                                <br><br>
                                <br><br><br>
                                <?php if ($total_audio_permitidos < $rango_audios) { ?>
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="form-group form-animate-text">
                                                <button class="btn ripple btn-raised btn-success" name="enviar">
                                                    <div>
                                                        <span>Agregar Audio</span>
                                                    </div>
                                                </button>
                                            </div>

                                        <?php } else { ?>

                                            <div class="parpadea text">
                                                <strong>Ya excedio el Limite de Audios, debe borra para poder agregar de nuevo.</strong>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>


                        <div class="col-md-12 top-20 padding-0">
                            <div class="col-md-12">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h3 style="text-align: center;">Lista de <strong style="color: crimson;">"Audios"</strong></h3>
                                        <div id="cont_total">
                                            <h5 style="border-bottom: 1px dashed #0099cc; width:150px; margin-top:-40px;">Total de Audios <strong><?php echo $total_audio_permitidos; ?></strong></h5>
                                            <h5 style='color:crimson; border-bottom: 1px dashed #0099cc; width:150px;'>Limite de Audios <strong><?php echo $rango_audios; ?></strong></h5> 
                                        </div>
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                        <table class="table table-responsive table-bordered table-hover" id="datatables-example" width="100%" cellspacing="0">
                                                    <thead>
                                                        <tr>
                                                            <th>Nombre del Archivo</th>
                                                            <th>Archivo</th>
                                                            <th>Opci&oacute;n</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        while ($vcard = mysqli_fetch_array($numero_servicios)) {
                                                            $id = $vcard['id'];
                                                            $carpeta = "Arch_Audios/";
                                                            $midirectorio = "Galeria_Clientes/";
                                                            $folder_fin = $midirectorio . $cod_vcard_cliente . '/' . $carpeta;

                                                            $archivo = $vcard['filename'];
                                                            $explode = explode('.', $archivo);
                                                            $extension_arch = array_pop($explode);

                                                            $ruta_audio = $folder_fin . $archivo;
                                                            ?>
                                                            <tr>
                                                                <td>
                                                                    <?php echo $vcard['titulo_archivo']; ?>   
                                                                </td>
                                                                <td style="text-align: center">
                                                        <center>
                                                            <audio class="ta_audio" src="<?php echo $ruta_audio; ?>"></audio>
                                                        </center>
                                                        </td>
                                                        <td style="text-align: center;">
                                                            <a href="editar_audio.php?id=<?php echo $id; ?>"> 
                                                                <span class="fa icon-pencil" style="font-size: 20px" title="Editar Nombre del Audio"></span>
                                                            </a>

                                                            <a href="delete_audio.php?id=<?php echo $id; ?>&directorio=<?php echo $folder_fin; ?>&namearchi=<?php echo $vcard['filename']; ?>">
                                                                <span class="fa fa-trash" style="font-size: 25px" title="Eliminar Imagen"></span>
                                                            </a>
                                                        </td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                                <?php
                                @mysqli_close($numero_servicios);
                                ?>  
                            </div>
                        </div>  
                        <div class="contenedor_flotante">                         
                            <?php
                            echo isset($msj_exito) ? utf8_decode($msj_exito) : '';
                            //isset($_GET['msj']) ? utf8_decode($_GET['msj']) : ''; 
                            if (!empty($_GET['msj'])) {
                                ?>
                                <div class='col-md-12'>
                                    <div class='alert col-md-12 col-sm-12 alert-icon alert-danger alert-dismissible fade in' role='alert'>
                                        <div class='col-md-2 col-sm-2 icon-wrapper text-center'>
                                            <span class='fa fa-flash fa-2x'></span></div>
                                        <div class='col-md-10 col-sm-10'>
                                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>
                                            <p><strong>Felicitaciones el Audio fue Borrado Correctamente.</strong></p>
                                        </div>
                                    </div>
                                </div> 

                            <?php }
                            if (!empty($_GET['msjupdate'])) {
                                ?>
                                <div class='col-md-12'>
                                    <div class='alert col-md-12 col-sm-12 alert-icon alert-danger alert-dismissible fade in' role='alert'>
                                        <div class='col-md-2 col-sm-2 icon-wrapper text-center'>
                                            <span class='fa fa-flash fa-2x'></span></div>
                                        <div class='col-md-10 col-sm-10'>
                                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>
                                            <p><strong>Felicitaciones el Audio fue Modificado Correctamente.</strong></p>
                                        </div>
                                    </div>
                                </div> 

    <?php } ?> 
                        </div>
                    </div>
                </div>        


                <!-- start: Mobile -->
                <div id="mimin-mobile" class="reverse" > 
    <?php include('menu_movil.php'); ?>
                </div>
                <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                    <span class="fa fa-bars"></span>
                </button>
                <!-- end: Mobile -->

    <?php include('js.html'); ?>

                <!--vista previa del audio--->
                <script src="asset/js/plugins/mediaelement-and-player.min.js"></script>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $('video,audio').mediaelementplayer({
                            alwaysShowControls: true,
                            videoVolume: 'vertical',
                            features: ['playpause', 'progress', 'volume', 'fullscreen']
                        });
                    });
                </script>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>