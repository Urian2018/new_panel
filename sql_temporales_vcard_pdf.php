    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <style type="text/css" media="screen">
                .fa-mail-reply-all:hover{
                    color: black;
                }
            </style>
    </head>
<body>

    
<?php
header("Content-Type: text/html;charset=utf-8");
include('config.php');

$verif = ("SELECT cod_vcard FROM temporal_vcard_pdf GROUP BY cod_vcard HAVING COUNT(*) > 1");
$res_verif = mysqli_query($con, $verif);

$repeats = array();
while ($d = mysqli_fetch_array( $res_verif ) ) {
    $repeats[] = $d['cod_vcard'];
}


$query_myclientes = ("SELECT * FROM temporal_vcard_pdf ");
$re_myclientes = mysqli_query($con, $query_myclientes);
$cant_clientes = mysqli_num_rows($re_myclientes);
if ($cant_clientes > 0) { ?>
<center>
    <h3 style="color: crimson;">Registros (<?php echo $cant_clientes; ?>)</h3>

    <table border="1" width="90%">
    <tr style="background-color: #2196F3; color: #fff;">
        <th>CODIGO VCARD</th>
        <th>NOMBRE Y APELLIDO</th>
        <th>RFC EMPRESA</th>
        <th>Acci&oacute;n</th>
    </tr>
<?php    while ($rows = mysqli_fetch_array($re_myclientes)) { 
            $id = $rows['id']; 
            $url = "restaurar_vcard_pdf.php?id=".$id;
        if( in_array( $rows['cod_vcard'], $repeats ) ){   ?>
        <tr style="background-color: grey; color: #fff;">
            <td><?php echo $rows['cod_vcard']; ?></td>
            <td><?php echo $rows['name']; ?></td>
            <td><?php echo $rows['rfc_empresa']; ?></td>
            <td  class="item" style="text-align: center; font-size: 20px;">
                <a href="javascript:Delete('resultado','<?php echo $url; ?>','')">
                    <span class="fa fa-mail-reply-all" title="Restaurar"></span>
                </a>
            </td>
        </tr>
        <?php }else{ ?>
            <tr>
            <td><?php echo $rows['cod_vcard']; ?></td>
            <td><?php echo $rows['name']; ?></td>
            <td><?php echo $rows['rfc_empresa']; ?></td>
            <td  class="item" style="text-align: center; font-size: 20px;">
                <a href="javascript:Delete('resultado','<?php echo $url; ?>','')">
                    <span class="fa fa-mail-reply-all" title="Restaurar"></span>
                </a>
            </td>
        </tr>

   <?php } } ?>
    </table>
</center>
<br><br>
<?php } ?>

</body>
</html>