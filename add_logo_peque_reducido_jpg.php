<?php 
$original = imagecreatefromjpeg($rtOriginal);

$max_ancho = 300; 
$max_alto = 300;

list($ancho,$alto)=getimagesize($rtOriginal);
$x_ratio = $max_ancho / $ancho;
$y_ratio = $max_alto / $alto;
//Proporciones
if(($ancho <= $max_ancho) && ($alto <= $max_alto) ){
    $ancho_final = $ancho;
    $alto_final = $alto;
}
else if(($x_ratio * $alto) < $max_alto){
    $alto_final = ceil($x_ratio * $alto);
    $ancho_final = $max_ancho;
}
else {
    $ancho_final = ceil($y_ratio * $ancho);
    $alto_final = $max_alto;
}


//Crear un lienzo
$lienzo=imagecreatetruecolor($ancho_final,$alto_final);
imagecopyresampled($lienzo,$original,0,0,0,0,$ancho_final, $alto_final,$ancho,$alto);
imagedestroy($original);
imagejpeg($lienzo,"logos_p/".$mi_file);

$update_logo_G = ("UPDATE myclientes SET logo_peque='" . $mi_file . "' WHERE rfc_empresa='".$_rfc_empresa."'");
$result = mysqli_query($con, $update_logo_G);

?>