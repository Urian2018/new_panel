<style>
    .style_tbody{
        font-size: 10px;
        color: black;
    }
    .color{
        background-color: #9BB;  
    }
</style>
<?php
require('config.php');
header("Content-Type: application/vnd.ms-excel");
header("Expires: 0");
$filename = "Todos_mis_Clientes." . date('m-d-Y') . ".xls";
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Disposition: attachment; filename=" . $filename . "");
                        
$sql = ("SELECT * FROM myclientes");
$mostar = mysqli_query($con, $sql);
?>
<table style="color:#000099; width:100px;" border=1 align="center" cellpadding=1 cellspacing=1>
    <thead>
        <tr>
            <th class="color">CODIGO VCARD</th>
            <th class="color">NOMBRE Y APELLIDO</th>
            <th class="color">EMPRESA</th>
            <th class="color">CARGO</th>
            <th class="color">CIUDAD</th>
            <th class="color">URL P&Aacute;GINA 1</th>
            <th class="color">URL P&Aacute;GINA 2</th>
            <th class="color">URL P&Aacute;GINA 3</th>
        </tr>
    </thead>
    <?php
    while ($row = mysqli_fetch_array($mostar)) {
        echo "<tbody>";
        echo "<tr>";
        echo "<td class='style_tbody'>" . $row['cod_vcard'] . "</td>";
        echo "<td class='style_tbody'>" . $row['nombre'] . "</td>";
        echo "<td class='style_tbody'>" . $row['empresa'] . "</td>";
        echo "<td class='style_tbody'>" . $row['cargo'] . "</td>";
        echo "<td class='style_tbody'>" . $row['ciudad'] . "</td>";
        echo "<td class='style_tbody'>" . $row['pagina_descarga'] . "</td>";
        echo "<td class='style_tbody'>" . $row['pagina_compartir'] . "</td>";
        echo "<td class='style_tbody'>" . $row['pagina_galeria'] . "</td>";
        echo "</tr>";
        echo "</tbody>";
    }
    ?>
</table>