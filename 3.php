<?php
require_once('config.php');
//header("Content-Type: text/html;charset=utf-8");

$codigo_vcard = isset($_POST['vc']) ? $_POST['vc'] : $_GET['vc'];
$sql = mysqli_query($con, "SELECT * FROM myclientes WHERE cod_vcard='" . $codigo_vcard . "' LIMIT 1 ");
$resultado_vcard = mysqli_num_rows($sql);


if($resultado_vcard > 0){
while ($row = mysqli_fetch_array($sql)) {
    $id_logo_cliente = $row['id'];
    $cod_vcard      = $row['cod_vcard'];
    
    $na              = ($row['nombre']);
   if (mb_detect_encoding($na, 'UTF-8', true) =='UTF-8') {
            $name    = ($row['nombre']);
        }else {
            $name    = utf8_encode($row['nombre']);
     }

    $carg            = $row['cargo'];
   if (mb_detect_encoding($carg, 'UTF-8', true) =='UTF-8') {
            $cargo   = ($row['cargo']);
        }else {
            $cargo   = utf8_encode($row['cargo']);
     }

    $empres          = ($row['empresa']);
   if (mb_detect_encoding($empres, 'UTF-8', true) =='UTF-8') {
           $empresa  = ($row['empresa']);
        }else {
           $empresa  = utf8_encode($row['empresa']);
     }

          $city      = ($row['ciudad']);
   if (mb_detect_encoding($city, 'UTF-8', true) =='UTF-8') {
           $ciudad   = ($row['ciudad']);
        }else {
           $ciudad   = utf8_encode($row['ciudad']);
     }

    $telefono       = $row['telefono'];
    $tlf_movil      = $row['tlf_movil'];
    $email          = $row['email'];
    $cp             = $row['cp'];
    $logo_peq       = $row['logo_peque'];
    $logo_grande    = $row['logo_grand'];
    $pagina_descarga        = $row['pagina_descarga'];
    $pagina_compartir       = $row['pagina_compartir'];
    $pagina_galeria         = $row['pagina_galeria'];
    $ruta_icono_compartir   = $row['ruta_icono_compartir'];
    $logo_galeria           = $row['logo_galeria'];
    $texto_whasapp          = $row['texto_whatsapp'];
    $web_cliente            = $row['web_cliente'];
    $codigo_pais            = $row['codigo_pais'];

    $country                = ($row['pais']);
   if (mb_detect_encoding($country, 'UTF-8', true) =='UTF-8') {
            $pais           = ($row['pais']);
        }else {
            $pais           = utf8_encode($row['pais']);
     }

        $addres             = ($row['direccion']);
   if (mb_detect_encoding($addres, 'UTF-8', true) =='UTF-8') {
           $direccion       = ($row['direccion']);
        }else {
           $direccion       = utf8_encode($row['direccion']);
     }

    $rfc_empresa            = $row['rfc_empresa'];
    $facebook               = $row['facebook'];
    $twitter                = $row['twitter'];
    $instagram              = $row['instagram'];
    $youtube                = $row['youtube'];
    $linkedin               = $row['linkedin'];
    $googleplus             = $row['gogle_mas'];
    $nota                   = $row['nota'];
}

include('variables_configuration.php'); //ESTE INCLUDE DEBE DE IR SIEMPRE AQUI YA QUE LAS VARIABLE ESTAN DECLARADAS ARRIBA
//MIS VARIABLES
$web_vcard = "https://vcard.mx/";
$folder = "https://vcard.mx/_/logos_p/" . $logo_peq;
$ruta_logo_peque = $folder;
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-50408658-5"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag("js", new Date());
            gtag("config", "UA-50408658-5");
        </script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" type="image/png" href="../favicon.png" />

        <!-- METAS FACEBOOK -->
        <meta content='<?php echo $name . ' - ' . $empresa; ?>' property='og:title'/>
        <meta content='<?php echo $ruta_logo_peque; ?>'   property='og:image'/>
        <meta content='Propuesta Comercial' property='og:description'/>

        <link rel="apple-touch-icon-precomposed" sizes="72x72"   href="https://vcard.mx/apple-touch-icon-72x72-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="https://vcard.mx/apple-touch-icon-114x114precomposed.png" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="https://vcard.mx/apple-touch-icon-144x144-precomposed.png" />
        <link rel="icon" sizes="144x144" href="https://vcard.mx/apple-touch-icon-144x144-precomposed.png">
        
        <meta name="theme-color" content="#171717">
        
        <title><?php echo $name . ' - ' . $empresa; ?></title>

        <!-- Facebook Opengraph integration: https://developers.facebook.com/docs/sharing/opengraph -->
        <meta property="og:title" content="">
        <meta property="og:image" content="">
        <meta property="og:url"   content="">
        <meta property="og:site_name"   content="">
        <meta property="og:description" content="">

        <!-- Twitter Cards integration: https://dev.twitter.com/cards/  -->
        <meta name="twitter:card"  content="summary">
        <meta name="twitter:title" content="<?php echo $name . ' - ' . $empresa; ?>">
        <meta name="twitter:site"  content="@Vcardmx">
        <meta name="twitter:description" content="Propuesta Comercial en nuestra VCard">
        <meta name="twitter:image" content="logos_g/<?php echo $logo_grande; ?>">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,700|Roboto:400,900" rel="stylesheet">
        <link href="https://vcard.mx/tema/3/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://vcard.mx/tema/3/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
       <!--<link href="https://vcard.mx/tema/3/css/style.css" rel="stylesheet">-->
        <link href="asset/style.css" rel="stylesheet"> <!----descargardado desde el servidor---->
        <link href="asset/css/fontello.css" rel="stylesheet" type="text/css">

        <style type="text/css" media=screen>
        /***********VARIABLES DE CONFIGURATION WEB**********/
            .hero {
            display: table;
            position: relative;
            background-image: url("<?php echo $img_fondo; ?>");
            background-size: cover;
            padding: 150px 0;
            color: #fff;
            width: 100%;
            height: 100vh;
            }
            #header {
            background: <?php echo $color_fondo; ?>;
            height: 70px;
            }
            .btn { 
            background-color: <?php echo $color_fondo; ?>;
            font-family: "Roboto", Helvetica, Arial, sans-serif;
            font-weight: 800;
            color: #fff;
            padding: 15px 45px;
            border-radius: 50px;
            }
            .btn:hover {
            opacity: 0.7;
            color: #fff !important;
            }
            .scrolltop {
            display: none;
            position: fixed;
            bottom: 15px;
            right: 15px;
            width: 42px;
            height: 42px;
            border-radius: 50%;
            background: <?php echo $color_fondo; ?>;
            color: #fff;
            text-align: center;
            font-size: 24px;
            }
            #btnEnviar{
            background: <?php echo $color_fondo; ?>;
            border: 0;
            padding: 10px 24px;
            color: #fff;
            transition: 0.4s;
            }
            #contact h2 {
            font-family: "Raleway", Helvetica, Arial, sans-serif;
            color: <?php echo $color_fondo; ?>;
            }
            #color_a{
            color: <?php echo $color_fondo; ?> !important;
            }
            .nav-menu ul li:hover {
            background: <?php echo $color_fondo; ?> !important;
            transition: 0.3s;
            }
            .features h2 {
              color: <?php echo $color_fondo; ?>;
            }
            .cta {
            background-color: <?php echo $color_fondo; ?>;
            padding: 25px 0;
            }
            .slider_info .flecha-info {
            font-size: 35px;
            line-height: 35px;
            color: <?php echo $color_fondo; ?>;
            position: absolute;
            top:40%;
            z-index: 1;
            }
            .slider_info .informacion .botones .active {
            background: <?php echo $color_fondo; ?>;
            }
            .jssora053 .a {
            fill:none;
            stroke:<?php echo $color_fondo; ?>;
            font-weight: bold;
            stroke-width:640;
            stroke-miterlimit:10;
            }
            .jssora053:hover{
            background-color: #eee;
            border-radius: 50%;
            border:2px solid <?php echo $color_fondo; ?>;
            }
            .slider_info .flecha-info:hover {
                color: <?php echo $color_fondo; ?>;
            }

    

    /*********FIN DE VARIABLES DECONFIGURATION******/    
            #linkSinHover:hover{
                cursor: auto;
                 }
            #div_video {
                width: 100%;
                height: 400px;
                margin: 0 auto;
                padding: 2px 0px 15px 0px;
            }
            textarea {
                resize: none;
            }
            .iframe {
                margin: 0 auto;
                width: 90%;
                height: 400px;
                /*width: 100%;*/
            }
            #btnEnviar:hover{
                opacity: 3;
                cursor: pointer;
            }
            /* jssor slider loading skin spin css */
            .jssorl-009-spin img {
                animation-name: jssorl-009-spin;
                animation-duration: 1.6s;
                animation-iteration-count: infinite;
                animation-timing-function: linear;
                object-fit: cover; /*importante acercal la imgn y le da calidad**/
            }

            @keyframes jssorl-009-spin {
                from {
                    transform: rotate(0deg);
                }

                to {
                    transform: rotate(360deg);
                }
            }


            .jssorb052 .i {position:absolute;cursor:pointer;}
            .jssorb052 .i .b {fill:#000;fill-opacity:0.3;}
            .jssorb052 .i:hover .b {fill-opacity:.7;}
            .jssorb052 .iav .b {fill-opacity: 1;}
            .jssorb052 .i.idn {opacity:.3;}
            .jssora053 {display:block;position:absolute;cursor:pointer;}
            #img_auto    img{
                width: auto;
                position: absolute;
                left: 50%;
                top: 50%;
                transform: translate(-50%, -50%);
                object-fit: cover; /*importante acercal la imgn y le da calidad**/
            }
            /*pa cambiar el style del scroll*/
            ::-webkit-scrollbar{
                width: 8px;
            }
            ::-webkit-scrollbar-thumb{
                border-radius: 5px;
                background:  #666666;
            }
            ::-webkit-scrollbar-thumb:hover{
                background:  grey;
            }
            /**********************************/

            /****STYLE VIDEO**********/
            .tarjeta {
                margin:40px 0;
                width: 100%;
                height: auto;
                /* background: #fafafa;*/
                border-radius: 5px;
                overflow: hidden;
            }
            .slider_info {
                text-align: center;
                overflow: hidden;
                padding: 20px 0;
                position: relative;
            }
            .slider_info .informacion {
                width: 100%;
                margin: auto;
                background-color: white;
                border-radius: 5px;
                padding: 25px 3px;
            }

            .slider_info .informacion article {
                width: 100%;
                overflow: hidden;
                position: relative;
            }

            .slider_info .informacion article .slide{
                width: 100%;
                left: 100%;
                /*padding: 2px 20px 80px 80px;*/
                position: absolute;
                background-color: white;
                border-radius: 5px;
            }
            /* Slider Flechas */

            .slider_info .flecha-info.anterior {
                left: 55px;
            }

            .slider_info .flecha-info.siguiente {
                right: 55px;
            }
            /* Botones Slider Info */
            .slider_info .informacion .botones {
                width: 100%;
            }

            .slider_info .informacion .botones span {
                width: 14px;
                height: 14px;
                display: inline-block;
                margin: 25px 10px;
                border-radius: 50%;
                background:white;
                border: 1px solid #39b54a;
            }

            @media screen and (max-width: 549px) {
                #img_auto    img{
                    max-width:100%;
                    max-height:100%;
                    left:46%;
                }
            }
        </style>

    </head>

    <body>

        <section class="hero">
            <div class="container text-center">
                <div class="row">
                    <div class="col-md-12">
                        <?php if($logo_grande !=""){  ?>
                        <p class="hero-brand">
                            <img alt="Logo" src="logos_g/<?php echo $logo_grande; ?>" style="width:25%">
                        </p>
                        <?php } ?>
                    </div>
                </div>

                <div class="col-md-12">
                    <h1>
                        <?php echo $name; ?>
                    </h1>
                    <h3>
                        <?php echo $cargo; ?>
                    </h3>
                    <p></p>
                    <a class="btn" href="#jssor_1">Iniciar</a>
                </div>
            </div>
        </section>
        <header id="header">
            <div class="container">

                <div id="logo" class="pull-left">
                    <?php if($logo_grande !=""){  ?>
                    <a href="index.html">
                        <img src="logos_g/<?php echo $logo_grande; ?>" alt="" title="Logo de la Empresa" />
                    </a>
                    <?php } ?>
                </div>

                <nav id="nav-menu-container">
                    <ul class="nav-menu">
                        <li class="menu-has-children"><a href="" id="linkSinHover">Mi VCard</a>
                            <ul>
                                <?php if(!empty($pagina_descarga)){ ?>
                                <li><a href="<?php echo $pagina_descarga; ?>" target="_blank">Guardar VCard</a></li>
                                 <?php } ?>

                                <?php if(!empty($pagina_compartir)){ ?>
                                <li><a href="<?php echo $pagina_compartir; ?>" target="_blank">Compartir VCard</a></li>
                                <?php } ?>
                            </ul>
                        <li class="menu-has-children"><a href="" id="linkSinHover">Multimedia</a>
                            <ul>
                                <li><a href="#img_auto">Im&aacute;genes</a></li>
                                <li><a href="#descargas">Descargas</a></li>
                                <li><a href="#audios">Audios</a></li>
                                <li><a href="#videos">Videos</a></li>
                            </ul>
                        </li>
                        <li class="menu-has-children"><a href="" id="linkSinHover">Informaci&oacute;n</a>
                            <ul>
                                <li><a href="https://www.google.com.mx/maps/place/<?php echo $direccion; ?>">Ruta GPS</a></li>
                                
                                <?php if(!empty($web_cliente)){ ?>
                                    <li><a href="<?php echo $web_cliente; ?>" target="_blank">P&aacute;gina Web</a></li>  
                                <?php } ?>
                                
                                <?php if(!empty($facebook)){ ?>
                                    <li><a href="<?php echo $facebook; ?>" target="_blank">Facebook</a></li>
                                <?php } ?>
                                <?php if(!empty($twitter)){ ?>
                                    <li><a href="<?php echo $twitter; ?>" target="_blank">Twitter</a></li>
                                <?php } ?>
                                <?php if(!empty($youtube)){ ?>
                                    <li><a href="<?php echo $youtube; ?>" target="_blank">Youtube</a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li class="menu-has-children"><a href="" id="linkSinHover">Contacto</a>
                            <ul>
                                <?php if(!empty($tlf_movil)){ ?>
                                <li><a href="tel:<?php echo $tlf_movil; ?>" target="_blank">Marcar a M&oacute;vil</a></li>
                                <?php } ?>

                                <?php if(!empty($telefono)){ ?>
                                <li><a href="tel:<?php echo $telefono;  ?>" target="_blank">Marcar a Oficina</a></li>
                                <?php } ?>

                                <?php if(!empty($email)){ ?>
                                <li><a href="mailto:<?php echo $email;  ?>" target="_blank">Enviar E-mail</a></li>
                                <?php } ?>

                                <?php if(!empty($tlf_movil)){ ?>
                                <li><a href="https://api.whatsapp.com/send?phone=<?php $wapp = substr($tlf_movil, 1);
                        echo $wapp ?>&text=Buen d&iacute;a, estoy interesado en recibir informaci&oacute;n de sus servicios." target="_blank">Enviar Whatsapp</a></li>
                                <?php } ?>
                                <li><a href="#contacto">Formulario de Contacto</a></li>
                            </ul>
                        </li>
                        <li class="menu-has-children"><a href="" id="linkSinHover">C&oacute;digos QR</a>
                            <ul>
                                <?php if(!empty($pagina_descarga)){ ?>
                                <li><a href="qr1.php?pag1=<?php echo $pagina_descarga; ?>" target="_blank">QR VCard</a></li>
                                <?php } ?>

                                <?php if(!empty($pagina_galeria)){ ?>
                                <li><a href="qr2.php?pag3=<?php echo $pagina_galeria; ?>" target="_blank">QR de Galer&iacute;a</a></li>
                                <?php } ?>
                            </ul>
                        </li>    
                    </ul>
                </nav>
                <nav class="nav social-nav pull-right d-none d-lg-inline">
                    <?php if(!empty($twitter)){ ?>
                    <a href="<?php echo $twitter; ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                    <?php } ?>

                    <?php if(!empty($facebook)){ ?>
                    <a href="<?php echo $facebook; ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                    <?php } ?>

                    <?php if(!empty($linkedin)){ ?>
                    <a href="<?php echo $linkedin; ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
                    <?php } ?>

                    <?php if(!empty($instagram)){ ?> 
                    <a href="<?php echo $instagram; ?>" target="_blank"><i class="fa fa-instagram"></i></a>
                    <?php } ?>
                </nav>
            </div>
        </header>

        <?php
           $Consultar_editor = ("SELECT * FROM editor_html WHERE rfc_empresa ='" . $rfc_empresa . "' ORDER BY id DESC LIMIT 1");
           $numero_editor = mysqli_query($con, $Consultar_editor); ?>
           <div class="container"> 
           <?php while ($resul_editor = mysqli_fetch_array($numero_editor)) {       ?>
               <div class="row"> 
                  <div class="col"><?php   echo  $resul_editor['content']; ?> </div> 
               </div>       
          <?php  } ?>
          </div>
        <!-- #header -->
        <?php 
            /**SQL PARA CONSULTAR LAS IMGS DEL SLIDER CON RESPECTO ALA EMPRESA**/
            $c = ("SELECT * FROM slider_cliente WHERE rfc_empresa ='" . $rfc_empresa . "' ORDER BY id ASC ");
            $num_imgs = mysqli_query($con, $c);
            $cant_slider = mysqli_num_rows($num_imgs); 
            if($cant_slider !="") { ?>
       
        <div id="jssor_1" style="background-color:#fff; position:relative;margin:0 auto;top:0px;left:0px;width:900px;height:900px;overflow:hidden;visibility:hidden;">
            <div data-u="loading" id="img_auto" class="jssorl-009-spin">
                <img  src="https://vcard.mx/_/asset/img/cargando_img.gif"/>
            </div>

            <div data-u="slides" id="img_auto" style="cursor:default;position:relative;top:0px;left:0px; width:980px;height:900px;overflow:hidden;">
                <?php
                while ($r = mysqli_fetch_array($num_imgs)) {
                    $cod_feje_empresa = $r['cod_vcard'];
                    $directorio = "Galeria_Clientes/".$cod_feje_empresa;
                    ?>
                    <div>
                        <img id='propiedad' src="<?php echo $directorio; ?>/<?php echo $r['filename']; ?>" style="max-width: 100%;">
                    </div>
                    <?php
                }
                ?>
            </div>

            <div data-u="navigator" class="jssorb052" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
                <div data-u="prototype" class="i" style="width:23px;height:23px;">
                    <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                    </svg>
                </div>
            </div>

            <!-- Arrow Navigator -->
            <div data-u="arrowleft" class="jssora053" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                <svg viewBox="0 0 16000 16000" style="background-color: rgba(255,255,255,.3);border-radius: 50%;padding: 10px; position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
                </svg>
            </div>
            <div data-u="arrowright" class="jssora053" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                <svg viewBox="0 0 16000 16000" style="background-color: rgba(255,255,255,.3); border-radius: 50%;padding: 10px;position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
                </svg>
            </div>
        </div>
    <?php } ?>
     <p>&nbsp;</p>
        <!-- Parallax -->

        <div class="block bg-primary block-pd-lg block-bg-overlay text-center" data-bg-img="https://vcard.mx/tema/3/img/parallax-bg.jpg" data-settings='{"stellar-background-ratio": 0.6}' data-toggle="parallax-bg">
            <h2>
                COMPARTIR
            </h2>
            <p>&nbsp;</p>
            <p style="text-align: center;">
                <?php if(!empty($pagina_galeria)){ ?> 
                <a href="https://www.facebook.com/sharer.php?u=<?php echo $pagina_galeria; ?>" target="_blank">
                    <img src="https://vcard.mx/f-b.png" alt="" width="8%" />
                </a>
                     
                &nbsp; &nbsp; &nbsp; &nbsp;
                <a href="whatsapp://send?text=Te recomiendo visitar este enlace! <?php echo $pagina_galeria; ?>" target="_blank">
                    <img src="https://vcard.mx/w-b.png" alt="" width="8%" />
                </a>

                &nbsp; &nbsp; &nbsp; &nbsp;
                <a href="https://twitter.com/intent/tweet?text=Te recomiendo visitar este enlace! <?php echo $pagina_galeria; ?>" target="_blank">
                    <img src="https://vcard.mx/t-b.png" alt="" width="8%" />
                </a>

                &nbsp; &nbsp; &nbsp; &nbsp;
                <a href="mailto:?subject=Propuesta%20comercial&body=Te%20recomiendo%20visitar%20este%20enlace!%20<?php echo $pagina_galeria; ?>">
                    <img src="https://vcard.mx/m-b.png" alt="" width="8%" />
                </a>
                <?php } ?>

            </p>
            <p></p>
        </div>
        <!-- /Parallax -->
        
        <?php
        /***SQL PARA CONSULTAR LOS ARCH DESCARGABLES***/
        $sql_archivos = mysqli_query($con, "SELECT * FROM archivos_descargables WHERE rfc_empresa='" . $rfc_empresa . "' ORDER BY id ASC LIMIT 3 ");
        $resultado_archivos = mysqli_num_rows($sql_archivos);
        if($resultado_archivos >0){
        ?>

        <section class="features" id="descargas">
            <div class="container">
                <h2 class="text-center">
                    Descargas
                </h2>

                <div class="row">
                    <?php
                    while ($row_arch = mysqli_fetch_array($sql_archivos)) {
                        $cod_vcard_cliente = $row_arch['cod_vcard'];
                        $archivo_name = $row_arch['filename'];
                        $estado_pdf = $row_arch['estado'];

                        $explode = explode('.', $archivo_name);
                        $extension_arch = array_pop($explode);

                        $midirectorio = "Galeria_Clientes/";
                        $carpeta = "Arch_Descargable/";
                        $folder_fin = $midirectorio . $cod_vcard_cliente . '/' . $carpeta . $archivo_name;
                        ?>
                                    <div class="col-sm">
                                        <?php if ($extension_arch == "docx" || $extension_arch == "doc") { ?>
                                        <p>&nbsp;</p>
                                        <p style="text-align: center;">
                                            <span style="font-size:17px;">
                                                <font style="vertical-align: inherit;">
                                                <h3 id="azul"><?php echo $row_arch['titulo_archivo']; ?></h3>
                                                </font>
                                            </span>
                                        <br>
                                        <a href="<?php echo $folder_fin; ?>" download="Archivo_Descargable">
                                            <img src="asset/img/word.png" alt="" width="100px">
                                        </a>
                                    </p>
                                    </div>
                                        <?php
                                    }
                                    if ($extension_arch == "pptx" || $extension_arch == "ppt") {
                                        ?>
                                        <div class="col-sm">
                                        <p>&nbsp;</p>
                                        <p style="text-align: center;">
                                            <span style="font-size:17px;">
                                                <font style="vertical-align: inherit;">
                                                <strong><?php echo $row_arch['titulo_archivo']; ?></strong>
                                                </font>
                                            </span>
                                            <br>
                                        
                                        <a href="<?php echo $folder_fin; ?>" download="Archivo_Descargable">
                                            <img src="asset/img/powerpoint.png" alt="" width="100px">
                                        </a>
                                        </p>
                                    </div>
                                        <?php
                                    }
                                    if ($extension_arch == "xlsx" || $extension_arch == "xls" || $extension_arch == "xlsm") {
                                        ?>
                                        <div class="col-sm">
                                        <p>&nbsp;</p>
                                        <p style="text-align: center;">
                                            <span style="font-size:17px;">
                                                <font style="vertical-align: inherit;">
                                                <strong><?php echo $row_arch['titulo_archivo']; ?></strong>
                                                </font>
                                            </span>
                                        <br>
                                        <a href="<?php echo $folder_fin; ?>" download="Archivo_Descargable">
                                            <img src="asset/img/excel.png" alt="" width="100px">
                                        </a>
                                        </p>
                                    </div>
                                        <?php
                                    }
                                    if ($extension_arch == "rar") {
                                        ?>
                                        <div class="col-sm">
                                        <p>&nbsp;</p>
                                        <p style="text-align: center;">
                                            <span style="font-size:17px;">
                                                <font style="vertical-align: inherit;">
                                                <strong><?php echo $row_arch['titulo_archivo']; ?></strong>
                                                </font>
                                            </span>
                                        <br>

                                        <a href="<?php echo $folder_fin; ?>" download="Archivo_Descargable">
                                            <img src="asset/img/winrar.png" alt="" width="100px">
                                        </a>
                                        </p>
                                    </div>
                                        <?php
                                    }
                                    if ($extension_arch == "zip") {
                                        ?>
                                        <div class="col-sm">
                                        <p>&nbsp;</p>
                                        <p style="text-align: center;">
                                            <span style="font-size:17px;">
                                                <font style="vertical-align: inherit;">
                                                <strong><?php echo $row_arch['titulo_archivo']; ?></strong>
                                                </font>
                                            </span>
                                        <br>
                                        <a href="<?php echo $folder_fin; ?>" download="Archivo_Descargable">
                                            <img src="asset/img/winzip.png" alt="" width="120px">
                                        </a>
                                    </p>
                                    </div>
                                        <?php
                                    }
                                    if (($extension_arch == "pdf" && $estado_pdf == "solodescarga")) {
                                        ?>
                                        <div class="col-sm">
                                        <p>&nbsp;</p>
                                        <p style="text-align: center;">
                                            <span style="font-size:17px;">
                                                <font style="vertical-align: inherit;">
                                                <strong><?php echo $row_arch['titulo_archivo']; ?></strong>
                                                </font>
                                            </span>
                                            <br>
                                        
                                        <a href="<?php echo $folder_fin; ?>" download="Archivo_Descargable"> 
                                            <img src="asset/img/pdf.png" alt="" width="60px">
                                        </a>
                                        </p>
                                    </div>
                                        <?php
                                    }
                                    if (($extension_arch == "pdf" && $estado_pdf == "vistaprevia")) {
                                        ?>
                                        <div class="col-sm">
                                        <p>&nbsp;</p>
                                        <p style="text-align: center;">
                                            <span style="font-size:17px; text-align: center;">
                                                <font style="vertical-align: inherit; text-align: center;">
                                                <center><strong><?php echo $row_arch['titulo_archivo']; ?></strong></center>
                                                </font>
                                            </span>
                                            <br>
                                        

                                        <p>&nbsp;</p>
                                        <p style="text-align:center;">
                    <embed src="https://docs.google.com/viewer?url=https://vcard.mx/_/<?php echo $folder_fin; ?>&embedded=true" style="width:95%; height:600px;" frameborder="0"></embed>
                                            <!--<embed src="<?php //echo $folder_fin; ?>" type="application/pdf" style="width: 95% !important;" height="600px"/>-->
                                        </p>
                                        <p>&nbsp;</p>
                                        <p>&nbsp;</p>
                                    </p>
                                    </div>
                                    <?php } ?>
                        </div>
                    <?php } ?>

                </div>
            </div>
        </section>
        <?php } ?>
        <!-- /Features -->
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <!-- /Features -->

    <?php
    /******SQL PARA CONSULTAR AUDIOS********/            
    $Consultar_audio = ("SELECT * FROM archivos_audio WHERE rfc_empresa ='" . $rfc_empresa . "' ORDER BY id DESC LIMIT 3");
    $numero_audio = mysqli_query($con, $Consultar_audio);
    $cant_audios = mysqli_num_rows($numero_audio);
    if($cant_audios > 0){ ?>
        <section class="cta" id="audios">
            <div class="container">
                <p></p>
                <p style="text-align: center;">
                    <span style="font-size:15px;">
                        <font style="vertical-align: inherit;">
                        <strong>AUDIOS</strong>
                        </font>
                    </span>
                </p> 
                <p>&nbsp;</p>
                <div class="grid-1-audio">
                    <?php
                    while ($audios = mysqli_fetch_array($numero_audio)) {
                        $cod_vcard_cliente_audio = $audios['cod_vcard'];
                        $archivo_name_audio = $audios['filename'];

                        $midirectorio_audio = "Galeria_Clientes/";
                        $carpeta_audio = "Arch_Audios/";
                        $folder_fin_audio = $midirectorio_audio . $cod_vcard_cliente_audio . '/' . $carpeta_audio . $archivo_name_audio;
                        ?>

                        <p>&nbsp;</p>
                        <p style="text-align: center;">
                            <span style="font-size:13px;">
                                <font style="vertical-align: inherit;">
                                <strong><?php echo $audios['titulo_archivo']; ?></strong>
                                </font>
                            </span>
                        </p>
                        <center>
                            <audio src="<?php echo $folder_fin_audio; ?>" controls="controls" type="audio/mpeg" preload="preload"></audio>
                        </center>
                    <?php } ?>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                </div>
            </div>
    </section>
    <?php } ?>    
    <!----SLIDER VIDEOS------------->

    <?php
    /**SQL PARA CONSULTAR AUDIOS***/
    $Consultar_videos = ("SELECT * FROM videos WHERE rfc_empresa ='" . $rfc_empresa . "' ORDER BY id DESC Limit 1");
    $numero_videos = mysqli_query($con, $Consultar_videos);
    $cant_videos = mysqli_num_rows($numero_videos);

    $Consultar_servicios_dos = ("SELECT * FROM videos WHERE rfc_empresa ='" . $rfc_empresa . "' ORDER BY id DESC Limit 1 ,3");
    $numero_servicios_dos = mysqli_query($con, $Consultar_servicios_dos);
    if($cant_videos >0){
    ?>
    <div class="tarjeta">
        <div class="slider_info">
            <!-- Flechas del Slider -->
            <a href="#" id="info-prev" class="flecha-info anterior" ><span class="icon-left-open"></span></a>
            <a href="#" id="info-next" class="flecha-info siguiente"><span class="icon-right-open"></span></a>

            <div class="informacion" id="videos">
                <article id="info">
                    <?php while ($servicios = mysqli_fetch_array($numero_videos)) { ?>
                        <div class="slide active">
                            <div id="div_video">
                                <iframe class="iframe"   src="<?php echo $servicios["url"]; ?>" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                        <?php } ?>

                    <?php while ($servicios_dos = mysqli_fetch_array($numero_servicios_dos)) { ?>
                        <div class="slide">
                            <p>
                            <div id="div_video">
                                <iframe class="iframe"   src="<?php echo $servicios_dos["url"]; ?>" frameborder="0" allowfullscreen></iframe>
                            </div>
                            </p>
                        </div>
                        <?php    } ?>
                </article>
                <div class="botones" id="botones"></div>
            </div>
        </div>
    </div>  
    <?php } ?>
    <!--------FIN VIDEOS------------->


<!-- Formulario de contacto ----->
<section id="contact">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h2 class="section-title">Contacto</h2>
      </div>
    </div>

    <div class="row justify-content-center">
      <div class="col-lg-3 col-md-4">
        <div class="info">
          <div>
            <?php if($direccion !=''){ ?>
            <i id="color_a" class="fa fa-map-marker"></i>
            <p>
            <a id="color_a" href="https://www.google.com.mx/maps/place/<?php echo $direccion; ?>" target="_blank">
                <?php echo $direccion; ?></a>
            </p>
            <?php } else { ?>
            <p id="color_a">Direcci&oacute;n</p>                 
              <?php  } ?>
          </div>

          <div>
              <?php if($email !=''){ ?>
            <i id="color_a" class="fa fa-envelope"></i>
            <p>
                <a id="color_a" href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
            </p>
              <?php } else { ?>
            <i id="color_a" class="fa fa-envelope"></i>
                <p id="color_a"> Email</p>
               <?php }  ?>
          </div>

          <div>
               <?php if($tlf_movil !=''){ ?>
            <i id="color_a" class="fa fa-phone"></i>
            <p>
                <a id="color_a" href="tel:<?php echo $tlf_movil; ?>"><?php echo $tlf_movil; ?></a>
            </p>
             <?php } else { ?>
             <i id="color_a" class="fa fa-phone"></i>
            <p id="color_a">Tel&eacute;fono</p>                 
              <?php  } ?>
          </div>

        </div>
      </div>

        <div class="col-lg-5 col-md-8" style="margin: 0px 60px;">
        <div class="form">
          <div id="sendmessage">Tu mensaje ha sido enviado. Gracias!</div>
          <form  name="register" id="formulario" method="post" action="send_email.php" class="contactForm" autocomplete="off" >
            <div class="form-group">
              <input type="text" name="nombre" class="form-control" placeholder="Nombre" data-rule="minlen:4" data-msg="Entre m&iacute;nimo 4 caracteres" required/>
              <input type="hidden" name="asunto" value="<?php echo $pagina_galeria; ?>">
              <input type="hidden" name="destinatario" value="<?php echo $email; ?>">
              <input type="hidden" name="name_cliente" value="<?php echo $name; ?>">
              
        <?php 
        $ruta_welcome = "https://vcard.mx/_/img_correo/img_correo.jpg";
              $logo_g ="https://vcard.mx/_/logos_g/";
              $ruta_log = $logo_g.$logo_grande; ?>
              <input type="hidden" name="logo" value="<?php echo $ruta_log; ?>">
              <input type="hidden" name="img_welcome" value="<?php echo $ruta_welcome; ?>">
              <div class="validation"></div>
            </div>
              <div class="form-group">
              <input type="text" name="telefono" class="form-control" placeholder="Telefono">
              <div class="validation"></div>
            </div>
            <div class="form-group">
              <input type="email" class="form-control" name="email" placeholder="Email" data-rule="email" data-msg="Favor de ingresar un E-mail v&aacute;lido" />
              <div class="validation"></div>
            </div>
            <div class="form-group">
              <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Cu�l es su mensaje?" placeholder="Mensaje"></textarea>
              <div class="validation"></div>
            </div>
            <div class="text-center">
               <input type="submit" id="btnEnviar" name="btnEnviar" value="Enviar Mensaje" >
            </div>
          </form>
        </div>
      </div>

        <p class="respuesta" style="background-color: #EEE;"> </p>
        
    </div>
  </div>
</section>


    <footer class="site-footer">
        <div class="bottom">
            <div class="container">
                <div class="row">

                    <div class="col-lg-6 col-xs-12 text-lg-right text-center">
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <a href="3.php">Inicio</a>
                            </li>

                            <li class="list-inline-item">
                                <a href="#imagenes">Im&aacute;genes</a>
                            </li>

                            <li class="list-inline-item">
                                <a href="#descargas">Descargas</a>
                            </li>

                            <li class="list-inline-item">
                                <a href="#audios">Audios</a>
                            </li>

                            <li class="list-inline-item">
                                <a href="#videos">Videos</a>
                            </li>

                            <li class="list-inline-item">
                                <a href="#contact">Contacto</a>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-lg-6 col-xs-12 text-lg-left text-center">
            <p class="copyright-text">
            &copy; VCard
            </p>
            <div class="credits">
                <a href="https://vcard.mx/">Todos los derechos reservados</a>
            </div>
        </div>
    </footer>
    <a class="scrolltop" href="#"><span class="fa fa-angle-up"></span></a>


    <script type="text/javascript" src="https://vcard.mx/menustilos/js/jquery-3.2.1.js"></script>
    <script type="text/javascript" src="https://vcard.mx/_/p_js/jssor.slider.min.js"></script>
    <script type="text/javascript" src="https://vcard.mx/menustilos/js/main.js"></script>
    <script type="text/javascript" src="https://vcard.mx/_/p_js/script.js"></script>
    
    <!-- Required JavaScript Libraries -->
    <script type="text/javascript" src="https://vcard.mx/tema/3/lib/jquery/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="https://vcard.mx/tema/3/lib/superfish/hoverIntent.js"></script>
    <script type="text/javascript" src="https://vcard.mx/tema/3/lib/superfish/superfish.min.js"></script>
    <script type="text/javascript" src="https://vcard.mx/tema/3/lib/tether/js/tether.min.js"></script>
    <script type="text/javascript" src="https://vcard.mx/tema/3/lib/stellar/stellar.min.js"></script>
    <script type="text/javascript" src="https://vcard.mx/tema/3/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="https://vcard.mx/tema/3/lib/counterup/counterup.min.js"></script>
    <script type="text/javascript" src="https://vcard.mx/tema/3/lib/waypoints/waypoints.min.js"></script>
    <script type="text/javascript" src="https://vcard.mx/tema/3/lib/easing/easing.js"></script>
    <script type="text/javascript" src="https://vcard.mx/tema/3/lib/stickyjs/sticky.js"></script>
    <script type="text/javascript" src="https://vcard.mx/tema/3/lib/parallax/parallax.js"></script>
    <script type="text/javascript" src="https://vcard.mx/tema/3/lib/lockfixed/lockfixed.min.js"></script>

    <!-- Template Specisifc Custom Javascript File -->
    <script type="text/javascript" src="https://vcard.mx/tema/3/js/custom.js"></script>
    <script type="text/javascript" src="https://vcard.mx/tema/3/contactform/contactform.js"></script>
    <script type="text/javascript" src="asset/js/main_slider_video.js"></script>

    <!----JS PARA ENVIAR EL FORMULARIO DE CONTACTO---->
            <script type="text/javascript">
            $(document).ready(function () {
                $("#formulario").bind("submit", function () {
                    var btnEnviar = $("#btnEnviar");
                    $.ajax({
                        type: $(this).attr("method"),
                        url:  $(this).attr("action"),
                        data: $(this).serialize(),
                        beforeSend: function () {
                            btnEnviar.val("Enviando . . . ."); // Para input de tipo button
                            btnEnviar.attr("disabled", "disabled");
                        },
                        complete: function (data) {
                            btnEnviar.val("Mensaje Enviado.");
                            btnEnviar.removeAttr("disabled");
                        },
                        success: function (data) {
                            $(".respuesta").html(data);
                            document.getElementById("formulario").reset(); //para limpiar el formulario
                        },
                        error: function (data) {
                            alert("Problemas al tratar de enviar el formulario");
                        }
                    });
                    return false;
                });
            });
        </script>
</body>
</html>
<?php
date_default_timezone_set("America/Mexico_City");
$fecha = date("d/m/Y"); 
$mes = date("m");
$contador_visita = "1";
$visita_pag3  = "1";

$consulta_visita_real = "SELECT * FROM visitas WHERE cod_vcard='" . $codigo_vcard . "' AND fecha='".$fecha."' ";
$rs_visita_real = mysqli_query($con, $consulta_visita_real);
    if (mysqli_num_rows($rs_visita_real) == 0) {
   $insert_real = "INSERT INTO visitas (cod_vcard, nombre, empresa, rfc_empresa, visita_pag3, fecha, mes) VALUES ('$codigo_vcard','$name','$empresa','$rfc_empresa','$visita_pag3', '$fecha', '$mes')";
   mysqli_query($con, $insert_real);
}else{
    $update_contador = ("UPDATE visitas SET visita_pag3=visita_pag3 + '".$contador_visita."' WHERE cod_vcard='".$codigo_vcard."' AND fecha='".$fecha."' ");
    $result_update = mysqli_query($con, $update_contador);
}

 } else{ 
    header("location:https://vcard.mx/inactiva/");
}  ?>
