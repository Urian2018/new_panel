<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
$cod_vcard_cliente = $_SESSION['cod_vcard'];


//almacenoel ids con la variable session ya que se pierde de repente
$ids = !empty($_REQUEST['id']) ? $_REQUEST['id'] : $_SESSION["ids"];
$_SESSION["ids"] = $ids;

foreach ($ids as $key => $val) {
if ($key == 0) {
$res = " ( ";
}
$res .= $val;
if (count($ids) == ($key + 1)) {
$res.=" )";
} else {
$res.=" , ";
}
}

if (isset($_POST['enviar'])) {
$archivo = $_FILES['file']['name'];
$rtOriginal = $_FILES['file']['tmp_name'];
$explode = explode('.', $archivo);
$extension = array_pop($explode);
      
//contar imagen de un directorio de logos grandes
$total_imagenes = count(glob('logos_g/{*.jpg,*.gif,*.png,*.jpeg,*.PNG,*.JPG}',GLOB_BRACE));
$new_total_img = $total_imagenes + 1;

$name_archivo = "logoG_".$new_total_img.'.';
$mi_file = $name_archivo.$extension;

/*****contar imagenes del directorio de logos pequeños*******/
$total_imagenes_p = count(glob('logos_p/{*.jpg,*.gif,*.png,*.jpeg,*.PNG,*.JPG}',GLOB_BRACE));
$new_total_img_p = $total_imagenes_p + 1;

$name_archivo_p = "logoP_".$new_total_img_p.'.';
$mi_file_p = $name_archivo_p.$extension;

        
$png = 'png';
$PNG = 'PNG';
/**CREANDO LOGO GRANDE PNG */
if ($extension == $png || $extension == $PNG ) {
$original =  imagecreatefrompng($rtOriginal);
$originall =  imagecreatefrompng($rtOriginal);

$max_ancho = 400;
$max_alto = 400;

list($ancho,$alto)=getimagesize($rtOriginal);
$x_ratio = $max_ancho / $ancho;
$y_ratio = $max_alto / $alto;

if(($ancho <= $max_ancho) && ($alto <= $max_alto) ){
    $ancho_final = $ancho;
    $alto_final = $alto;
}
else if(($x_ratio * $alto) < $max_alto){
    $ancho_final = $max_ancho;
    $alto_final = ceil($x_ratio * $alto);
}
else {
    $ancho_final = $max_ancho;
    $alto_final = $max_alto;
}

$lienzo=imagecreatetruecolor($ancho_final,$alto_final);
imagecopyresampled($lienzo,$original,0,0,0,0,$ancho_final, $alto_final,$ancho,$alto);
imagedestroy($original);
imagepng($lienzo,"logos_g/".$mi_file);
        

$update_logo_G = ("UPDATE myclientes SET logo_grand='" .$mi_file. "' WHERE id IN " .$res);
$result = mysqli_query($con, $update_logo_G);

//CREANDO LOGO PEQUEÑO PNG 
$max_anchoo = 300;
$max_altoo = 300;

list($anchoo,$altoo)=getimagesize($rtOriginal);
$x_ratioo = $max_anchoo / $anchoo;
$y_ratioo = $max_altoo / $altoo;

if(($anchoo <= $max_anchoo) && ($altoo <= $max_altoo) ){
    $ancho_finall = $anchoo;
    $alto_finall = $altoo;
}
else if(($x_ratioo * $altoo) < $max_altoo){
    $ancho_finall = $max_ancho;
    $alto_finall = ceil($x_ratioo * $altoo);
}
else {
    $ancho_finall = $max_anchoo;
    $alto_finall = $max_altoo;
}


$lienzoo=imagecreatetruecolor($ancho_finall,$alto_finall);
imagecopyresampled($lienzoo,$originall,0,0,0,0,$ancho_finall, $alto_finall,$anchoo,$altoo);
imagedestroy($originall);
imagepng($lienzoo,"logos_p/".$mi_file_p);

$update_logo_P = ("UPDATE myclientes SET logo_peque='" .$mi_file_p. "' WHERE id IN " .$res);
$result_P = mysqli_query($con, $update_logo_P);
header("location:add_logo_G.php"); 
exit();

} else {
/****CREANDO LOGO GRANDE JPG ****/    
$original_lg = imagecreatefromjpeg($rtOriginal);
$original_lp = imagecreatefromjpeg($rtOriginal);
$max_ancho_lg = 400; 
$max_alto_lg = 400;

list($ancho_lg,$alto_lg)=getimagesize($rtOriginal);
$x_ratio_lg = $max_ancho_lg / $ancho_lg;
$y_ratio_lg = $max_alto_lg / $alto_lg;

if(($ancho_lg <= $max_ancho_lg) && ($alto_lg <= $max_alto_lg) ){
    $ancho_final_lg = $ancho_lg;
    $alto_final_lg = $alto_lg;
}
else if(($x_ratio_lg * $alto_lg) < $max_alto_lg){
    $alto_final_lg = ceil($x_ratio_lg * $alto_lg);
    $ancho_final_lg = $max_ancho_lg;
}
else {
    $ancho_final_lg = ceil($y_ratio_lg * $ancho_lg);
    $alto_final_lg = $max_alto_lg;
}

$lienzo_lg=imagecreatetruecolor($ancho_final_lg,$alto_final_lg);
imagecopyresampled($lienzo_lg,$original_lg,0,0,0,0,$ancho_final_lg, $alto_final_lg,$ancho_lg,$alto_lg);
imagedestroy($original_lg);
imagejpeg($lienzo_lg,"logos_g/".$mi_file);

$update_logo_G = ("UPDATE myclientes SET logo_grand='" .$mi_file. "' WHERE id IN " .$res);
$result = mysqli_query($con, $update_logo_G);

/****LOGO PEQUEÑO ****/
$max_ancho_lp = 300; 
$max_alto_lp = 300;

list($ancho_lp,$alto_lp)=getimagesize($rtOriginal);
$x_ratio_lp = $max_ancho_lp / $ancho_lp;
$y_ratio_lp = $max_alto_lp / $alto_lp;

if(($ancho_lp <= $max_ancho_lp) && ($alto_lp <= $max_alto_lp) ){
    $ancho_finalp = $ancho_lp;
    $alto_finalp = $alto_lp;
}
else if(($x_ratio_lp * $alto_lp) < $max_alto_lp){
    $alto_finalp = ceil($x_ratio_lp * $alto_lp);
    $ancho_finalp = $max_ancho_lp;
}
else {
    $ancho_finalp = ceil($y_ratio_lp * $ancho_lp);
    $alto_finalp = $max_alto_lp;
}

$lienzo_lp=imagecreatetruecolor($ancho_finalp,$alto_finalp);
imagecopyresampled($lienzo_lp,$original_lp,0,0,0,0,$ancho_finalp, $alto_finalp,$ancho_lp,$alto_lp);
imagedestroy($original_lp);
imagejpeg($lienzo_lp,"logos_p/".$mi_file_p);

$update_logo_jpg_p = ("UPDATE myclientes SET logo_peque='" .$mi_file_p. "' WHERE id IN " .$res);
$result_logo_jpg_p = mysqli_query($con, $update_logo_jpg_p);

header("location:add_logo_G.php"); 
exit();
    }
}
?>

<!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
        <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
    </head>
    <body id="mimin" class="dashboard">
        <?php include('menu_header.php'); ?>

        <div class="container-fluid mimin-wrapper">
            <?php include('menu_lateral_escritorio.php'); ?>
             <div id="content">

<br><br>
<div class="col-md-12">
<div class="col-md-12 panel">
    <div class="col-md-12 panel-heading">
        <h4 style="text-align: center; color: black;"> Actualizar <strong style="color:crimson;">"LOGO GRANDE Y PEQUEÑO"</strong> para la empresa.</h4>
        <br><br>
        <?php
        $Consultar = ("SELECT * FROM myclientes WHERE id ='" .$ids[0]."' ");
        $numero_servicios = mysqli_query($con, $Consultar);
        while ($vcard = mysqli_fetch_array($numero_servicios)) {            
           if(empty($vcard['logo_grand'])){ 
                echo "<center><span style='color:red; font-size:13px;text-align:center;'>Sin Imagen</span></center>";
           } else { ?>
               <center>
                    <img src="logos_g/<?php echo $vcard['logo_grand'] ?>" style='width:100px; height: 80px;object-fit: cover; text-align: center;'>
               </center>
          <?php } }
        @mysqli_close($numero_servicios);
        ?>
        <p style="text-align:right;"><a href="vcard.php" title="Volver">
                <span class="icon-action-undo" title="Volver">Volver</span>
            </a>
        </p>
    </div>
    <form  enctype="multipart/form-data" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <div class="col-md-12 panel-body">
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="input-group fileupload-v1">
                        <input type="file" name="file"  required="required" class="fileupload-v1-file hidden"  accept="image/*">
                        <input type="text" class="form-control fileupload-v1-path" placeholder="Imagen . . . ." disabled>
                        <span class="input-group-btn">
                            <button class="btn fileupload-v1-btn" type="button"><i class="fa fa-folder"></i> Presione Examinar</button>
                        </span>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-6">
                        <button class="btn ripple btn-raised btn-success" name="enviar">
                            <div>
                                <span>Agregar Imagen</span>
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
</div>
</div>
</div>

<!-- start: Mobile -->
        <div id="mimin-mobile" class="reverse" > 
            <?php include('menu_movil.php'); ?>
        </div>
        <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
            <span class="fa fa-bars"></span>
        </button>
        <!-- end: Mobile -->

        <?php include('js.html'); ?>
    </body>
</html>
    <?php
} else {
    include('error.php');
}
?>