<?php
require_once('config.php');
$codigo_vcard = isset($_POST['vc']) ? $_POST['vc'] : $_GET['vc'];

$sql = mysqli_query($con, "SELECT * FROM myclientes WHERE cod_vcard='" . $codigo_vcard . "' LIMIT 1 ");
$resultado_vcard = mysqli_num_rows($sql);

while ($row = mysqli_fetch_array($sql)) {
    $id_logo_cliente = $row['id'];

    $cod_vcard = $row['cod_vcard'];
    $name = $row['nombre'];
    $cargo = $row['cargo'];
    $empresa = $row['empresa'];
    $ciudad = $row['ciudad'];
    $telefono = $row['telefono'];
    $tlf_movil = $row['tlf_movil'];
    $cp = $row['cp'];
    $logo_peq = $row['logo_peque'];
    $logo_grande = $row['logo_grand'];
    $pagina_descarga = $row['pagina_descarga'];
    $pagina_compartir = $row['pagina_compartir'];
    $pagina_galeria = $row['pagina_galeria'];
    $ruta_icono_compartir = $row['ruta_icono_compartir'];
    $logo_galeria = $row['logo_galeria'];
    $texto_whasapp = $row['texto_whatsapp'];
    $web_cliente = $row['web_cliente'];
    $pagina_compartir = $row['pagina_compartir'];
    $pagina_galeria = $row['pagina_galeria'];
    $codigo_pais = $row['codigo_pais'];
    $pais = $row['pais'];
    $direccion = $row['direccion'];
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-50408658-5"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag("js", new Date());
            gtag("config", "UA-50408658-5");
        </script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" type="image/png" href="../favicon.png" />

        <meta name="theme-color" content="#2196f3">
        <meta name="MobileOptimized" content="width">
        <meta name="HandheldFriendly" content="true">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
        <link rel="shortcut icon" type="image/png" href="./img_pwa/ProgramadorFitness.png">
        <link rel="apple-touch-icon" href="./img_pwa/ProgramadorFitness.png">
        <link rel="apple-touch-startup-image" href="./img_pwa/ProgramadorFitness.png">
        <link rel="manifest" href="manifest.json">

        <!-- METAS FACEBOOK -->
        <meta content='<?php echo $name . ' - ' . $empresa; ?>' property='og:title'/>
        <meta content='logos_g/<?php echo $logo_grande; ?>'   property='og:image'/>
        <meta content='Propuesta Comercial' property='og:description'/>

        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="https://vcard.mx/apple-touch-icon-72x72-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="https://vcard.mx/apple-touch-icon-114x114precomposed.png" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="https://vcard.mx/apple-touch-icon-144x144-precomposed.png" />
        <title><?php echo $empresa; ?></title>
        <link rel="stylesheet" href="https://vcard.mx/menustilos/css/estilos.css">
        <link rel="stylesheet" href="https://vcard.mx/menustilos/css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="asset/css/fontello.css">

        <script src="https://vcard.mx/menustilos/js/jquery-3.2.1.js"></script>
        <script type="text/javascript" type="text/javascript" src="p_js/jssor.slider.min.js"></script>
        <script src="https://vcard.mx/menustilos/js/main.js"></script>
        <script type="text/javascript" type="text/javascript" src="p_js/script.js"></script>
    </head>

    <body style="margin:0;padding:0;font-family: -apple-system, BlinkMacSystemFont, Arial, sans-serif; background-color: #FFFFFF;">

        <header>
            <span id="button-menu" class="fa fa-bars"></span>
            <nav class="navegacion">
                <ul class="menu">
                    <li class="title-menu">Menú</li>
                    <li><a href="<?php echo $pagina_descarga; ?>" target="_blank"><span class="fa fa-address-card icon-menu"></span>Guardar Contacto</a></li>
                    <li><a href="<?php echo $pagina_compartir; ?>" target="_blank"><span class="fa fa-share icon-menu"></span>Compartir Contacto</a></li>
                    <li><a href="https://www.google.com.mx/maps/place/$direccion" target="_blank"><span class="fa fa-map icon-menu"></span>Ruta GPS</a></li>

                    <li class="item-submenu" menu="1">
                        <a href="#"><span class="fa fa-globe icon-menu"></span>Información</a>
                        <ul class="submenu">
                            <li class="title-menu"><span class="fa fa-globe icon-menu"></span>Información</li>
                            <li class="go-back">Atras</li>
                            <li><a href="<?php echo $web_cliente; ?>" target="_blank">Sitio Web</a></li>
                        </ul>
                    </li>

                    <li class="item-submenu" menu="2">
                        <a href="#"><span class="fa fa-envelope icon-menu"></span>Contactar</a>
                        <ul class="submenu">
                            <li class="title-menu"><span class="fa fa-envelope icon-menu"></span>Contactar</li>
                            <li class="go-back">Atras</li>
                            <li><a href="tel:<?php echo $telefono; ?>">Marcar</a></li>														<li><a href="tel:<?php echo $tlf_movil; ?>">Marcar a Móvil</a></li>
                            <li><a href="mailto:<?php echo $email; ?>">Enviar Correo</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </header>
        <!-- FIN MENU -->
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p style="text-align: center;">
            <img src="logos_g/<?php echo $logo_grande; ?>" class="logo_G"  alt="" style='width:12%'/>
        <p>&nbsp;</p> 
<div>		
<?php
include "qrcode.php"; 
// Create QRcode object 
$qc = new QRCODE(); 

// URL QR code 
$qc->URL('<?php echo $pagina_descarga; ?>');

// render QR code
$qc->QRCODE(400,"sample.png");
?>
</div>
      

    <p>&nbsp;</p><center>
    <p style="text-align: right;">
        <a title="Vcard" href="https://vcard.mx" target="_blank">
            <img src="https://vcard.mx/vc.png" alt="" width="20px" />
        </a>
    </p></center>

<script src="./script.js"></script>
<script type="text/javascript" src="asset/js/main_slider_video.js"></script>
</body>
</html>