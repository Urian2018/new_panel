<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
    if (isset($_POST['enviar'])) {
        
        $usuario        = $_POST['usuario'];
        $clave           = $_POST['clave'];
        $fecha_inicio   = $_POST['fecha_inicio'];
        $cod_distri     = $_POST['cod_distribuidor'];
        $tipo_distri    = $_POST['tipo_distribuidor'];
        $nombre         = $_POST['nombre'];
        $ape_paterno    = $_POST['ape_paterno'];
        $ape_materno    = $_POST['ape_materno'];
        $direccion      = $_POST['dir_contacto'];
        $tlf_fijo       = $_POST['tlf_fijo'];
        $tlf_movil      = $_POST['tlf_movil'];
        $email          = $_POST['correo'];
        $emal_opcionl   = $_POST['correo_dos'];
        $dir_envio      = $_POST['dir_envio_paqueteria'];
        
        $query = "INSERT INTO distribuidores
         (
             usuario,
             clave,
             fecha_inicio,
             cod_distribuidor,
             tipo_distribuidor,
             nombre,
             ape_paterno,
             ape_materno,
             dir_contacto,
             tlf_fijo,
             tlf_movil,
             correo,
             correo_dos,
             dir_envio_paqueteria ) 
        VALUES ('" .$usuario. "','" .$clave. "','" .$fecha_inicio. "','" .$cod_distri. "','" .$tipo_distri. "','" .$nombre. "','" .$ape_paterno. "','" .$ape_materno. "','" .$direccion. "','" .$tlf_fijo. "','" .$tlf_movil. "','" .$email. "','" .$emal_opcionl. "','" .$dir_envio. "')";
        $result = mysqli_query($con, $query);
    }

    date_default_timezone_set("America/Mexico_City");
    $fecha = date("d/m/Y"); 
    $result_fecha = $fecha;
    ?>
    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
            <style>
            .icon-pencil:hover{
                color:black;
            }
            </style>
            <script  src="https://code.jquery.com/jquery-2.2.4.js"></script>
        </head>
        <body id="mimin" class="dashboard">
            <?php include('menu_header.php'); ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>

                <div id="content">
                    <br>
                    <div class="col-md-12">
                        <div class="col-md-12 panel">
                            <div class="col-md-12 panel-heading">
                                <h4 style="text-align: center; color: black;"> AGREGAR <strong style="color:crimson;">"DISTRIBUIDOR"</strong> </h4>
                                <br>
                            </div>
                            <form  enctype="multipart/form-data" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                                <div class="col-md-12 panel-body">
                                    <div class="col-md-12">
                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text" name="usuario" id="validate_firstname"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>USUARIO</label>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text"  name="clave"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>CLAVE</label>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text" >
                                                    <input type="text" class="form-text"  name="fecha_inicio" value="<?php echo $result_fecha; ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>FECHA INICIO</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text" name="cod_distribuidor"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>CODIGO DISTRIBUIDOR</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                            <label>TIPO DISTRIBUIDOR</label>
                                                <div class="form-group form-animate-text" style="margin-top:-10px !important;">
                                                    <select name="tipo_distribuidor" id="tipo_distribuidor" class="form-control">
                                                        <option value="MASTER">MASTER</option>
                                                        <option value="SENIOR">SENIOR</option>
                                                    </select>
                                                 </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text" >
                                                    <input type="text" class="form-text"  name="nombre"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>NOMBRE</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text" name="ape_paterno"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>APELLIDO PATERNO</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text"  name="ape_materno"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>APELLIDO MATERNO</label>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text" >
                                                    <input type="text" class="form-text"  name="dir_contacto"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>DIRECCION</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="col-md-3">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text" name="tlf_fijo"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>TELEFONO FIJO</label>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text"  name="tlf_movil"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>TELEFONO MOVIL</label>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group form-animate-text" >
                                                    <input type="text" class="form-text"  name="correo"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>EMAIL</label>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text" name="correo_dos"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>EMAIL OPCIONAL</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-5">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text"  name="dir_envio_paqueteria"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>DIRECCIÓN DE ENVIO</label>
                                                </div>
                                            </div>
                                            <div class="col-md-7" style="margin-top:10px !important;">
                                                <button class="btn ripple btn-raised btn-success" name="enviar">
                                                    <div>
                                                        <span>Agregar Distribuidor</span>
                                                    </div>
                                                </button>
                                            </div>

                                        </div>
                                    </div>

                            </form>
                        </div>
                    </div>

                    
                    <?php
                    $sql = ("SELECT * FROM distribuidores ORDER BY id DESC");
                   if($mostar = mysqli_query($con, $sql)){
                    $total_client = mysqli_num_rows($mostar) ;
                        ?>
                        <div class="col-md-12 top-20 padding-0">
                            <div class="col-md-12">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h4 style="text-align: center;">
                                            <?php echo " Hay un Total de <strong style='color:green; text-align: center;'>(" .$total_client. ')</strong>'; ?> Distribuidores
                                        </h4>
                                    </div>
                                    <div class="panel-body">
                                        <div class="responsive-table">
                                            <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                                                <thead>
                                                    <tr>
                                                        <th>USUARIO</th>
                                                        <th>CLAVE</th>
                                                        <th>COD DISTRIBUIDOR</th>
                                                        <th>NOMBRE</th>
                                                        <th>DIRECCIÓN</th>
                                                        <th>TLF MOVIL</th>
                                                        <th>EMAIL</th>
                                                        <th>DIR. DE ENVIO</th>
                                                        <th>OPCIONES</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                    while ($vcard = mysqli_fetch_array($mostar)) {
                                                     $table ="distribuidores";
                                                     $delet ="admin";
                                                     ?>
                                                        <tr>
                                                            <td><?php echo $vcard['usuario']; ?></td>
                                                            <td><?php echo $vcard['clave']; ?></td>
                                                            <td><?php echo $vcard['cod_distribuidor']; ?></td>
                                                            <td><?php echo $vcard['nombre']; ?></td>
                                                            <td><?php echo $vcard['dir_contacto']; ?></td>
                                                            <td><?php echo $vcard['tlf_movil']; ?></td>
                                                            <td><?php echo $vcard['correo']; ?></td>
                                                            <td><?php echo $vcard['dir_envio_paqueteria']; ?></td> 
                                                            <td style="text-align: center; font-size: 25px;">
                                                            <a href="edit_datos_distribuidor.php?id=<?php echo $vcard['id']; ?>"> 
                                                                <span class="fa icon-pencil" title="Editar Datos del Distribuidor"></span>
                                                            </a>
                                                            <a href="delete.php?id=<?php echo $vcard['id']; ?>&table=<?php echo $table; ?>&delete=<?php echo $delet; ?>"> 
                                                                <span class="fa fa-trash" title="Eliminar Notificación"></span>
                                                            </a>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div>
                        <?php
                    }
                    @mysqli_close($mostar);
                    ?>  
                </div>            
            </div>


            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse" > 
                <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

            <?php include('js.html'); ?>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#datatables-example').DataTable();
                });
            </script>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>













