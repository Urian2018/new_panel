<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
	$cod_vcard_cliente = $_GET['cod_vcard'];
    ?>
    
    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
            <style type="text/css">
                h3{
                    text-align: center;
                    color: #00afef;
                }
                .fa-eye:hover{
                    color: black;
                }

            </style>
            <script  src="https://code.jquery.com/jquery-2.2.4.js"></script>
            <script type="text/javascript" src="asset/js/ajax.js"></script>
            <script language="javascript">
            $(document).ready(function () {
                $("#mes").change(function () {
                    $('#cbx_localidad').find('option').remove().end().append('<option value="whatever"></option>').val('whatever');
                        mes = $(this).val();
                        $.get("recib_mes_visita.php", {mes: mes}, function (data) {
                            $("#meses").html(data);
                        });
                });
            });
            </script>
        </head>


        <body id="mimin" class="dashboard">
            <?php include('menu_header_user.php'); ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>
                <div id="content">
                    <?php
                    //comparar por el rcf y nombre por el nombre dela empresa
                    $Consultar_dos = ("SELECT * FROM visitas WHERE cod_vcard='".$cod_vcard_cliente."'");
                    $numero_servi = mysqli_query($con, $Consultar_dos);
                    $total_client = mysqli_num_rows($numero_servi);
                    ?>
                    <div class="col-md-12 top-20 padding-0">
                        <div class="col-md-12">
                            <div class="panel">

                                <div class="panel-heading">
                                    <?php
                                        $query = ("SELECT mes FROM visitas  WHERE cod_vcard='".$cod_vcard_cliente."' GROUP BY mes");
                                        $res = mysqli_query($con, $query);
                                        ?>
                                        <div class="col-md-3">
                                        <center><label for=""style="color: #333; font-size: 13px; text-align: center; font-weight: bold;">Meses</label></center>
                                          <select name="mes" id="mes" class="form-control">
                                            <option value="" selected>Seleccione</option>
                                            <?php
                                              while ($valores = mysqli_fetch_array($res)) {
                                                $mes  = $valores['mes'];
                                                if($mes == '01'){
                                                    $name_mes = 'ENERO';
                                                }elseif ($mes == '02') {
                                                    $name_mes = 'FEBRERO';
                                                }elseif ($mes == '03') {
                                                    $name_mes = 'MARZO';
                                                }elseif ($mes == '04') {
                                                    $name_mes = 'ABRIL';
                                                }elseif ($mes == '05') {
                                                    $name_mes = 'MAYO';
                                                }elseif ($mes == '06') {
                                                    $name_mes = 'JUNIO';
                                                }elseif ($mes == '07') {
                                                    $name_mes = 'JULIO';
                                                }elseif ($mes == '08') {
                                                    $name_mes = 'AGOSTO';
                                                }elseif ($mes == '09') {
                                                    $name_mes = 'SEPTIEMBRE';
                                                }elseif ($mes == '10') {
                                                    $name_mes = 'OCTUBRE';
                                                }elseif ($mes == '11') {
                                                    $name_mes = 'NOVIEMBRE';
                                                }else{
                                                    $name_mes = 'DICIEMBRE';
                                                }
                                            ?>
                                                <option value="<?php echo $mes; ?>"><?php echo $name_mes; ?></option>
                                                <?php
                                              }
                                            @mysqli_close($res);
                                            ?>
                                          </select>

                                      </div>

                                        <h3>  
                                            <strong style="font-size: 15px; text-align: center; ">RESUMEN DE MÉTRICAS</strong> 
                                        </h3>

                                        <h5 style="text-align: right;">
                                        <a href="visitas_clientes.php">
                                        <span class="icon-action-undo"></span>
                                        Volver
                                        </a>
                                    </h5>
                                </div>
                                    
                               
                                <div class="panel-body">
                                   <div class="table-responsive">
                                        <table class="table table-responsive table-bordered table-hover" id="datatables-example" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>C&oacute;digo</th>
                                                    <th>Nombre</th>
                                                    <th>Descargas de VCard</th>
                                                    <th>Compartió VCard</th>
                                                    <th>Visitas en Galeria</th>
                                                    <th>Mes</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                while ($visitas = mysqli_fetch_array($numero_servi)) {
                                                    $id =  $visitas['id'];
                                                    ?>
                                                    <tr>
                                                        <td style="text-align: center;"><?php echo $visitas['cod_vcard']; ?></td>
                                                        <td style="text-align: center;"><?php echo $visitas['nombre']; ?></td>
                                                        <td style="text-align: center;"><?php echo $visitas['visita_pag1']; ?></td>
                                                        <td style="text-align: center;"><?php echo $visitas['visita_pag2']; ?></td>
                                                        <td style="text-align: center;"><?php echo $visitas['visita_pag3']; ?></td>
                                                        <td style="text-align: center;"><?php 
                                                             $mes = $visitas['mes'];
                                                            if($mes == '01'){
                                                               echo $name_mes = 'ENERO';
                                                            }elseif ($mes == '02') {
                                                               echo $name_mes = 'FEBRERO';
                                                            }elseif ($mes == '03') {
                                                               echo $name_mes = 'MARZO';
                                                            }elseif ($mes == '04') {
                                                               echo $name_mes = 'ABRIL';
                                                            }elseif ($mes == '05') {
                                                               echo $name_mes = 'MAYO';
                                                            }elseif ($mes == '06') {
                                                               echo $name_mes = 'JUNIO';
                                                            }elseif ($mes == '07') {
                                                               echo $name_mes = 'JULIO';
                                                            }elseif ($mes == '08') {
                                                               echo $name_mes = 'AGOSTO';
                                                            }elseif ($mes == '09') {
                                                               echo $name_mes = 'SEPTIEMBRE';
                                                            }elseif ($mes == '10') {
                                                               echo $name_mes = 'OCTUBRE';
                                                            }elseif ($mes == '11') {
                                                               echo $name_mes = 'NOVIEMBRE';
                                                            }else{
                                                               echo $name_mes = 'DICIEMBRE';
                                                            } ?></td>
                                                       
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <?php
                        @mysqli_close($numero_servi);
                        ?>  
                    </div> 
                </div>
            </div>


            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse" > 
                <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

            <?php include('js.html'); ?>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#datatables-example').DataTable();
                });
            </script>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>                    