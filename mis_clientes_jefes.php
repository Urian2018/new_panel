<?php
session_start();
header("Content-Type: text/html;charset=utf-8");
include('config.php');
if (isset($_SESSION['user']) != "") {
    ?>
    
    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <style>
                .icon-pencil:hover{
                    color: black;
                }
                .fa-paste:hover{
                    color: black;
                }
                .icon-lock:hover{
                    color: black;
                }
                .icon-people:hover{
                    color: black;
                }
            </style>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
            <script  src="https://code.jquery.com/jquery-2.2.4.js"></script>
        </head>

        <body id="mimin" class="dashboard">
            <?php include('menu_header.php'); ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>

                <div id="content">
                    <br>
                    <div class="col-md-12">
                        <div>
                            <span>
                                <h5 style="text-align: right;">
                                    <a href="descargar_clientes_jefes.php" class="ripple btn-outline btn-primary">Descargar Todos mis Clientes Jefe</a>    
                                </h5>
                            </span>
                        </div>
                    </div>
                    
                    <?php
                    $sql = ("SELECT * FROM myclientes");
                   if($mostar = mysqli_query($con, $sql)){
                    $total_client = mysqli_num_rows($mostar) ;
                        ?>

                        <div class="col-md-12 top-20 padding-0">
                            <div class="col-md-12">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h4 style="text-align: center;">
                                            <?php echo " Hay un Total de <strong style='color:green; text-align: center;'>(" .$total_client. ')</strong>'; ?> Clientes
                                        </h4>
                                    </div>
                                    <div class="panel-body">   
                                            <div class="table-responsive">
                                              <table class="table table-responsive table-bordered table-hover" id="datatables-example" width="100%" cellspacing="0">
                                                <thead>
                                                    <tr>
                                                        <th>Codigo VCard</th>
                                                        <th>Nombre y Apelido</th>
                                                        <th>Cliente</th>
                                                        <th>Ciudad / Empresa</th>
                                                        <th>Estatus VCard</th>
                                                        <th>Acciones</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    while ($vcard = mysqli_fetch_array($mostar)) {
                                                        $id = $vcard['id'];
                                                        $table = "myclientes";
                                                        $cod_vcard = $vcard['cod_vcard'];
                                                        $nombres   = utf8_encode($vcard['nombre']);
                                                        $empresa   = utf8_encode($vcard['empresa']);
                                                        $ciudad    = utf8_encode($vcard['ciudad']);
                                                        $rang_img  = $vcard['rango_imagenes'];
                                                        $rfc       = $vcard['rfc'];
                                                        $rfc_empresa       = $vcard['rfc_empresa'];
                                                        $status_vcard = $vcard['estatus_vcard'];
                                                        $delet        = "admin";
                                                       ?>
                                                        <tr>
                                                            <td style="width: 100px;"><?php echo $cod_vcard; ?></td>
                                                            <td style="width: 170px;"><?php echo $nombres; ?></td>
                                                            <td style="width: 100px; text-align: center;"><?php if($rfc !=''){
                                                                echo '<span style="color:crimson">'.$rfc.'<span>'; } else{ echo 'Empleado'; } ?>
                                                            </td>
                                                            <td style="width: 200px;"><?php echo $ciudad .' / '. $empresa; ?> </td> 
                                                            <td style="width: 100px; text-align: center;"><?php echo  $status_vcard; ?></td>
                                                        <td style="text-align: center; font-size: 25px;">
                                                            <a href="edit_datos_clientes_jefe.php?id=<?php echo $id; ?>&cod=<?php echo $cod_vcard; ?>"> 
                                                                <span class="fa icon-pencil" title="Editar Datos del Cliente"></span>
                                                            </a>
                                                            <a href="delete.php?id=<?php echo $id; ?>&table=<?php echo $table; ?>&delete=<?php echo $delet; ?>&cod_vcard=<?php echo $cod_vcard; ?>"> 
                                                                <span class="fa fa-trash" title="Eliminar Registro"></span>
                                                            </a>
                                                           
                                                            <a href="migrar_cliente.php?id=<?php echo $id; ?>&cod_vcard=<?php echo $cod_vcard; ?>&empresa=<?php echo $empresa; ?>&name=<?php echo $nombres; ?>"> 
                                                                <span class="fa icon-action-redo" title="Migrar Cliente o Empleado"></span>
                                                            </a>
                                                            <a href="javascript:;" id="security<?php echo $cod_vcard; ?>" class="openBtn" data-id='<?php echo $cod_vcard; ?>' data-toggle="modal" data-target="#element"> 
                                                                <span class="fa icon-lock" title="Datos del Empleado"></span>
                                                            </a>
                                                            <a href="asociar_vcard.php?rfc_empresa=<?php echo $rfc_empresa; ?>&cod_vcard=<?php echo $cod_vcard; ?>">
                                                                <span class="fa icon-people" title="Adjuntar VCard a Empleado"></span> 
                                                            </a>
                                                            
                                                        </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div>
                        <?php
                    }
                    @mysqli_close($mostar);
                    ?>  
                </div>            
            </div>


            <div class="modal fade" id="element" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <center><h4 class="modal-title" id="myModalLabel">Datos del Cliente</h4></center>
                        </div>
                        <div class="modal-body">
                        <div class="container-fluid">
                            <div class="form-group input-group">

                                <span style="width:150px;"></span><!--imprimir cod_vcard-->
                               
                                <div id="datos"></div>
                                
                                <span style="width:150px;"> </span> <!---id del cliente seleccionado-->
                                <input type="hidden" style="width:350px;"  id="id">
                            </div>
                        </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
                        </div>
                    </div>
                </div>
            </div>

            

            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse" > 
                <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

            <?php include('js.html'); ?>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#datatables-example').DataTable();
                    $('[id^=security]').click(function(){
                        $('#id_dato').html('');
                        $('#id_dato').append('<spam>'+ $(this).attr('data-id') +'</spam>');
                            var id=$(this).attr('data-id');
                                                   
                            $('#edit').modal('show');
                            $('#id').val(id);

                            $("#datos").load("data.php?id="+id);                                                    
                    });
                });
            </script>

        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>