<?php
session_start();
include('config.php');
if (isset($_SESSION['user'])!="") {   ?>
<!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
        <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
    </head>

    <body id="mimin" class="dashboard">
        <?php include('menu_header.php'); ?>

        <div class="container-fluid mimin-wrapper">
            <?php include('menu_lateral_escritorio.php'); ?>

            <div id="content">
                <br><br>
                <div class="col-md-12">
                    <div class="col-md-12 panel">
                        <div class="col-md-12 panel-heading">
                            <h4 style="text-align: center; color: black;"> Actualizar Contenido de <strong style="color:crimson;">"BIENVENIDA"</strong>.</h4>
                            <br>
                        </div>
                        
                            <div class="col-md-12 panel-heading">
                                <iframe src="https://vcard.mx/_/editor_html/editor_html_contenido_welcome.php" width="100%" height="550px" frameborder="0" framespacing="0" scrolling="auto" border="0">
                                    Texto para cuando el navegador no conoce la etiqueta iframe</iframe>
                            </div>

                    </div>
                </div>
                
                <?php
                    $Consultar = ("SELECT * FROM contenido_welcome");
                    $number_img_welcome = mysqli_query($con, $Consultar);
                    ?>
                    <div class="col-md-12 top-20 padding-0">
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-heading"><h3 style="text-align: center;">Contenido de <strong style="color: crimson;">"BIENVENIDA"</strong></h3></div>
                                <div class="panel-body">
                                    <div class="responsive-table">
                                        <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>contenido</th>
                                                    <th>Acci&oacute;n</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                while ($vcard = mysqli_fetch_array($number_img_welcome)) {
                                                     $id = $vcard['id']; 
                                                     $table = "contenido_welcome";
                                                     $delet = "admin"; 
                                                    ?>
                                                    <tr>
                                                        <td style="text-align: center;">
                                                            <?php echo  $vcard['content']; ?>
                                                        </td>
                                                        <td style="text-align: center; font-size: 25px;">
                                                            <a href="delete.php?id=<?php echo $id; ?>&table=<?php echo $table; ?>"> 
                                                                <span class="fa fa-trash" title="Eliminar Contenido Html"></span>
                                                            </a>
                                                        </td>
                                                    </tr>
                                        <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <?php
                        @mysqli_close($number_img_welcome);
                        ?>  
                    </div>
                
            </div>
        </div>


        <!-- start: Mobile -->
        <div id="mimin-mobile" class="reverse" > 
            <?php include('menu_movil.php'); ?>
        </div>
        <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
            <span class="fa fa-bars"></span>
        </button>
        <!-- end: Mobile -->

        <?php include('js.html'); ?>
    </body>
</html>
    <?php
} else {
    include('error.php');
}
?>