<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
    $rfc_empresa =  $_SESSION['rfc_empresa'];
    $id_contenido_html = $_GET['id'];
    ?>
    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
            <style type="text/css">
                img{
                    width: 100%;
                    max-width: 100px !important;
                }
            </style>
        </head>

        <body id="mimin" class="dashboard">
            <?php include('menu_header.php'); ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>

                <div id="content">
                    <div class="col-md-12">
                        <div class="col-md-12 panel">
                            <div class="col-md-12 panel-heading">
                                    <h4 style="text-align: center; color: black;"> AGREGAR CONTENIDO <strong style="color:crimson;"> "HTML" </strong></h4>
                                <br>
                                
                                <iframe src="editor_html/editar_contenido_html.php?id=<?php echo $id_contenido_html; ?>" width="100%" height="700px" frameborder="0" framespacing="0" scrolling="auto" border="0">
                                    Texto para cuando el navegador no conoce la etiqueta iframe</iframe>
                            </div>
                        </div>
                    </div>
                    

                
            </div>
    </div>

            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse" > 
                <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

            <?php include('js.html'); ?>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>