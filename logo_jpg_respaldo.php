<?php 
$original = imagecreatefromjpeg($rtOriginal);
$logo_pequeño_jpg = $cod_vcard_cliente.$p.'.'.$fileExtension;

$max_ancho = 300; 
$max_alto = 300;

list($ancho,$alto)=getimagesize($rtOriginal);
$x_ratio = $max_ancho / $ancho;
$y_ratio = $max_alto / $alto;
//Proporciones
if(($ancho <= $max_ancho) && ($alto <= $max_alto) ){
    $ancho_final = $ancho;
    $alto_final = $alto;
}
else if(($x_ratio * $alto) < $max_alto){
    $alto_final = ceil($x_ratio * $alto);
    $ancho_final = $max_ancho;
}
else {
    $ancho_final = ceil($y_ratio * $ancho);
    $alto_final = $max_alto;
}

//Crear un lienzo
$lienzo=imagecreatetruecolor($ancho_final,$alto_final);
imagecopyresampled($lienzo,$original,0,0,0,0,$ancho_final, $alto_final,$ancho,$alto);
imagedestroy($original);
imagejpeg($lienzo,"Logos/".$logo_pequeño_jpg);

?>