<?php
require_once('tcpdf/tcpdf.php');
require('config.php');
class PDF extends TCPDF {

    public $orientacion = 'L';    

    public function Header() {
        $this->SetFont('helvetica', '', 7);
        $ds = DIRECTORY_SEPARATOR;
        //$logo = ROOT . $ds .'app'. $ds .'webroot'. $ds .'img'. $ds .'logo_mtc.jpg';
        //$this->Image($logo, 12, 10, 37, 20, '', '', '', false, 300, '', false);
    }

    public function Footer() { 
        $this->SetY(-20);
        $this->SetFont('helvetica', '', 7);
        $this->Cell(240, 0, 'Vcard', 0, 1, 'C');
        // Page number
        $this->Cell(253, 5, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }

}

$style = array(
    'position' => 'R',
    'align' => 'R',
    'stretch' => false,
    'fitwidth' => false,
    'cellfitalign' => '',
    'border' => false,
    'hpadding' => 'auto',
    'vpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255),
    'text' => true,
    'font' => 'helvetica',
    'fontsize' => 8,
    'stretchtext' => 4
    );

$pdf = new PDF('L', PDF_UNIT, 'A4', true, 'UTF-8', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Vcard');
$pdf->SetTitle('Vcard ');
$pdf->SetSubject('Vcard');
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

$pdf->SetMargins('10', '30', '10');
$pdf->SetHeaderMargin('10');
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);//set auto page breaks
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);//set image scale factor
$pdf->SetFont('helvetica', '', 8);//set pdf font
//$pdf->AliasNbPages('TPAG');


// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();
//$imagen1 = './assets/img/logo.png';
//$pdf->Image($imagen1, 20, 10, 60, 20, '', '', '', false, 300);


$pdf->SetFont('helvetica', '', '10');
$pdf->MultiCell($w = 258, $h = 0, $txt = 'VCARD | Lista de registros', $border = 0, $align = 'C', $fill = 0, $ln = 1, $x = '', $y = '30', $reseth = true);

//$pdf->write1DBarcode('fefe', 'C128', '', 15, '', 18, 0.4, $style, 'N');

$pdf->ln(28);


$pdf->SetFont('helvetica', '', '8');

$html='<table border="1">
        <tr>
            <td align="center"><b>CODIGO VCARD</b></td>
            <td align="center"><b>CODIGO DISTRIBUIDOR</b></td>
            <td align="center"><b>URL LOGO</b></td>
            <td align="center"><b>NOMBRE</b></td>
            <td align="center"><b>CARGO</b></td>
            <td align="center"><b>EMPRESA</b></td>
            <td align="center"><b>DIRECCION</b></td>
            <td align="center"><b>DIR L2</b></td>
            <td align="center"><b>DIR L3</b></td>
            <td align="center"><b>CODIGO POSTAL</b></td>
            <td align="center"><b>CIUDAD</b></td>
            <td align="center"><b>TELEFONO</b></td>
            <td align="center"><b>MOVIL</b></td>
            <td align="center"><b>WEB</b></td>
            <td align="center"><b>EMAIL</b></td>
            <td align="center"><b>URL PAGINA 1</b></td>
        </tr>';
$pdf->SetFont('helvetica', '', '7');
        $sql = ("SELECT * FROM clientes_temporales");
        $mostar = mysqli_query($con, $sql); 

        while ($row = mysqli_fetch_array($mostar)) { 
         $web = "https://vcard.mx/_/logos_g/";
         $logo_G = $row['logo_grand'];
         $url_logo_G  = $web.$logo_G;

            $html.='<tr>
            <td align="center">'.$row['cod_vcard'].'</td>
            <td align="center">'.$row['cod_distribuidor'].'</td>
            <td align="center">'.$url_logo_G.'</td>
            <td align="center">'.$row['name'].'</td>
            <td align="center">'.$row['cargo'].'</td>
            <td align="center">'.$row['empresa'].'</td>
            <td align="center">'.$row['direccion'].'</td>
            <td align="center">'.$row['dir_l2'].'</td>
            <td align="center">'.$row['dir_l3'].'</td>
            <td align="center">'.$row['cp'].'</td>
            <td align="center">'.$row['ciudad_compa'].'</td>
            <td align="center">'.$row['telefono'].'</td>
            <td align="center">'.$row['tlf_movil'].'</td>
            <td align="center">'.$row['web_cliente'].'</td>
            <td align="center">'.$row['email'].'</td>
            <td align="center">'.$row['pag1'].'</td>
            </tr>';
        }

$html.='</table>';


$html.='<br><br><br><br><br><br><br>';


$pdf->writeHTML($html, true, false, true, false, '');

// ---------------------------------------------------------
ob_end_clean();
// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('pdf_historiaTemporal.pdf', 'I');
?>