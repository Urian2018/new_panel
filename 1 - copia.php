<?php
require_once('config.php');
$codigo_vcard = isset($_POST['vc']) ? $_POST['vc'] : $_GET['vc'];

$sql = mysqli_query($con, "SELECT * FROM myclientes WHERE cod_vcard='" . $codigo_vcard . "' LIMIT 1 ");
$resultado_vcard = mysqli_num_rows($sql);
if ($resultado_vcard > 0) {
    while ($row = mysqli_fetch_array($sql)) {
        $id_logo_cliente = $row['id'];
        $cod_vcard       = $row['cod_vcard'];
        $name            = $row['nombre'];
        $name_client     = $row['nombre'];
        $cargo           = $row['cargo'];
        $empresa         = $row['empresa'];
        $telefono        = $row['telefono'];
        $tlf_movil       = $row['tlf_movil'];
        $email           = $row['email'];
        $logo_peq        = $row['logo_peque'];
        $logo_grande     = $row['logo_grand'];
        $logo_galeria    = $row['logo_galeria'];
        $texto_whasapp   = $row['texto_whatsapp'];
        $pagina_compartir   = $row['pagina_compartir'];
        $pagina_descarga    = $row['pagina_descarga'];
        $pagina_galeria     = $row['pagina_galeria'];
        $texto              = $row['texto'];
        $rfc_empresa        = $row['rfc_empresa'];
        $estatus_vcard      = $row['estatus_vcard'];   
    }

    //MIS VARIAVES
    $web_vcard = "https://vcard.mx/";
    $folder = "https://vcard.mx/_/logos_p/" . $logo_peq;
    $ruta_logo_peque = $folder;
    ?>

    <!DOCTYPE HTML>
    <html>
        <head>
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-50408658-5"></script>
            <script>
                window.dataLayer = window.dataLayer || [];
                function gtag() {
                    dataLayer.push(arguments);
                }
                gtag('js', new Date());
                gtag('config', 'UA-50408658-5');
            </script>
            <meta name="encoding" charset="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
            <link rel="shortcut icon" type="image/png" href="https://vcard.mx/favicon.png" />
            <meta name="author" content="VCard de México" />
            <meta name="description" content="VCard - Tarjeta de <?php echo $name; ?> - VCard de México - <?php echo $empresa ?> - Tarjeta de presentación electrónica - Tarjeta Digital - Tarjeta de Presentación Digital" />
            <meta name="keywords" content="VCard de México, <?php echo $empresa; ?>, VCard, Tarjetas de Preentación, Tarjetas de Presentación electrónicas, Tarjeta Digital - Tarjeta de Presentación Digital, Tecnología, Negocios, Empresarios, Posicionamiento web" />
            <meta name="twitter:card" content="summary"/>

            <!-- METAS FACEBOOK -->
            <meta content='<?php echo $name; ?>' property='og:title'/>
            <meta content='<?php echo $ruta_logo_peque; ?>' property='og:image'/>
            <meta content='<?php echo $cargo . ' - ' . $empresa; ?>' property='og:description'/>
            <style type="text/css">
                body:after {
                    display: none;
                }
                #main {
                    -moz-transform: none !important;
                    -webkit-transform: none !important;
                    -ms-transform: none !important;
                    transform: none !important;
                    opacity: 1 !important;
                }
                .boton_confirmar{
                    text-decoration: none;
                    padding: 8px;
                    font-weight: 500;
                    font-size: 18px;
                    color: #ffffff;
                    background-color: #1883ba;
                    border-radius: 4px;
                    border: 2px solid #0016b0;
                }
                .boton_confirmar:hover{
                    color: #1883ba;
                    background-color: #ffffff;
                }
            </style>
            <title><?php echo $cargo . ' ' . $empresa; ?></title>
            <!--<link rel="stylesheet" href="https://vcard.mx/tema/1/assets/css/main.css" />--->
            <link rel="stylesheet" href="https://vcard.mx/_/tema1/assets/css/main.css" />
        </head>
        <body class="is-preload" onLoad="alert('Abrir el archivo descargado y Click en Guardar');" onUnLoad="confirm('VCard de México,');">
            
            <div id="wrapper">
                <section id="main">
                    <header>
                        <span class="avatar"><img src="<?php echo $ruta_logo_peque; ?>" alt="" width="140px" /></span>
                        <h1><?php echo $name; ?></h1>
                        <p><?php echo $cargo; ?></p>
                        <a class="boton_confirmar" href="https://api.whatsapp.com/send?phone=<?php $wapp = substr($tlf_movil, 1); echo $wapp ?>&text=Buen día, confirmo de recibida su VCard.">CONFIRMAR RECEPCIÓN</a>
                        <p>&nbsp;</p>
                    </header>
                    <footer>
                        <ul class="icons">
                            <li><a href="<?php echo $pagina_descarga; ?>" class="fa-download">DESCARGAR</a></li>
                            <li><a href="<?php echo $pagina_compartir; ?>" class="fa-share-alt">COMPARTIR</a></li>
                            <li><a href="<?php echo $pagina_galeria; ?>" class="fa-image">MULTIMEDIA</a></li>
                            <li><a href="https://api.whatsapp.com/send?phone=<?php echo $wapp; ?>&text=Buen día, me interesa recibir información de sus servicios." class="fa-whatsapp">Enviar Whatsapp</a></li>
                            <li><a href="tel:<?php echo $telefono; ?>" class="fa-phone">Marcar</a></li>
                            <li><a href="tel:<?php echo $tlf_movil; ?>" class="fa-mobile">Marcar a Móvil</a></li>
                            <li><a href="mailto:<?php echo $email; ?>" class="fa-at">Enviar Correo</a></li>
                        </ul>
                    </footer>
                </section>

                <!-- Footer -->
                <footer id="footer">
                    <ul class="copyright">
                        <li>&copy; VCard 2018</li><li>VCard de México - <a href="https://vcard.mx">VCard.mx</a></li>
                    </ul>
                </footer>
            </div>
           <!----para forzar la descarga de la vcard y no usar el refresh como antes xqbloqeaba alguno elemento segun google-->
          <?php 
          if($estatus_vcard !="Inactiva"){ ?>
            <script>location.href="descargar_vcard.php?cod_vcard=<?php echo $cod_vcard; ?>&nombre=<?php echo $name; ?>"</script>
         <?php } ?> 
            
        </body>
    </html>
<?php
date_default_timezone_set("America/Mexico_City");
$fecha = date("d/m/Y"); 

//Verificando si existe el Cod_vcard si existe
$verificando_cod_vcard = mysqli_query($con, "SELECT * FROM visitas WHERE cod_vcard='" . $codigo_vcard . "' LIMIT 1 ");
$result_verifcar = mysqli_num_rows($verificando_cod_vcard);
if ($result_verifcar != '') {
//Actualizando el campo visita
$contador_visita = "1";
$update_contador = ("UPDATE visitas SET visita_pag1=visita_pag1 + '".$contador_visita."' WHERE cod_vcard='".$codigo_vcard."'");
$result_update = mysqli_query($con, $update_contador);
 } else{
$add_cod_vcard = "INSERT INTO visitas (cod_vcard, nombre, empresa, rfc_empresa, fecha) VALUES ('$codigo_vcard','$name_client','$empresa','$rfc_empresa','$fecha')";
$result_insert = mysqli_query($con, $add_cod_vcard);  
if($result_insert >0){
$contador_visita = "1";
$update_contador = ("UPDATE visitas SET visita_pag1=visita_pag1 + '".$contador_visita."' WHERE cod_vcard='".$codigo_vcard."'");
$result_update = mysqli_query($con, $update_contador);
 } }
} else {
	header("location:https://vcard.mx/inactiva/"); 
}
?>