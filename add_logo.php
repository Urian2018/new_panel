<?php
session_start();
include('config.php');
if (isset($_SESSION['user'])!="") {
$cod_vcard_cliente = $_SESSION['cod_vcard'];
$_rfc_empresa      = $_SESSION['rfc_empresa'];


if (isset($_POST['enviar'])) {
 $msj_exito = "";   
 $fileName   = $_FILES['file']['name'];
 $rtOriginal = $_FILES['file']['tmp_name'];

$check = getimagesize($rtOriginal);
$data = base64_encode(file_get_contents($_FILES["file"]["tmp_name"]));
$img_vcard = "," . $data . "";

if (!empty($_POST['todosVCard'])) {
$todosVCard = $_POST['todosVCard'];
    $q = ("UPDATE myclientes SET img_vcard='" . $img_vcard . "' WHERE rfc_empresa='".$_rfc_empresa."' ");
    $result = mysqli_query($con, $q);
 } 

$fileExtension = substr(strrchr($fileName, '.'), 1);
$mi_file = $cod_vcard_cliente.'.'.$fileExtension;

$png = 'png';
$PNG = 'PNG';
if ($fileExtension == $png || $fileExtension == $PNG ) {
$original =  imagecreatefrompng($rtOriginal);

//Ancho y alto máximo
$max_ancho = 400;
$max_alto = 400;

//Medir la imagen
list($ancho,$alto)=getimagesize($rtOriginal);
//Ratio
$x_ratio = $max_ancho / $ancho;
$y_ratio = $max_alto / $alto;

//Proporciones
if(($ancho <= $max_ancho) && ($alto <= $max_alto) ){
    $ancho_final = $ancho;
    $alto_final = $alto;
}
else if(($x_ratio * $alto) < $max_alto){
    $ancho_final = $max_ancho;
    $alto_final = ceil($x_ratio * $alto);
}
else {
    $ancho_final = $max_ancho;
    $alto_final = $max_alto;
    /*$ancho_final = ceil($y_ratio * $ancho);
    $alto_final = $max_alto;*/
}

//Crear un lienzo
$lienzo=imagecreatetruecolor($ancho_final,$alto_final);
//Copiar original en lienzo
imagecopyresampled($lienzo,$original,0,0,0,0,$ancho_final, $alto_final,$ancho,$alto);
//Destruir la original
imagedestroy($original);
//Crear la imagen y guardar en directorio logos_g/
imagepng($lienzo,"logos_g/".$mi_file);

$update_logo_G = ("UPDATE myclientes SET logo_grand='" . $mi_file . "' WHERE rfc_empresa='".$_rfc_empresa."'");
$result = mysqli_query($con, $update_logo_G);
$msj_exito = "
<div class='col-md-12'>
<div class='alert alert-success col-md-12 col-sm-12  alert-icon alert-dismissible fade in' role='alert'>
  <div class='col-md-2 col-sm-2 icon-wrapper text-center'>
    <span class='fa fa-check fa-2x'></span></div>
    <div class='col-md-10 col-sm-10'>
      <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
      <span aria-hidden='true'>x</span></button>
      <p><strong>Felicitaciones el Registro fue un Exito.</strong></p>
    </div>
  </div>
</div>";
include('add_logo_peque_reducido_png.php');
} else{
$original = imagecreatefromjpeg($rtOriginal);

$max_ancho = 400; 
$max_alto = 400;

list($ancho,$alto)=getimagesize($rtOriginal);
$x_ratio = $max_ancho / $ancho;
$y_ratio = $max_alto / $alto;
//Proporciones
if(($ancho <= $max_ancho) && ($alto <= $max_alto) ){
    $ancho_final = $ancho;
    $alto_final = $alto;
}
else if(($x_ratio * $alto) < $max_alto){
    $alto_final = ceil($x_ratio * $alto);
    $ancho_final = $max_ancho;
}
else {
    $ancho_final = ceil($y_ratio * $ancho);
    $alto_final = $max_alto;
}

//Crear un lienzo
$lienzo=imagecreatetruecolor($ancho_final,$alto_final);
imagecopyresampled($lienzo,$original,0,0,0,0,$ancho_final, $alto_final,$ancho,$alto);
imagedestroy($original);
imagejpeg($lienzo,"logos_g/".$mi_file);

$update_logo_G = ("UPDATE myclientes SET logo_grand='" . $mi_file . "' WHERE rfc_empresa='".$_rfc_empresa."'");
$result = mysqli_query($con, $update_logo_G);
$msj_exito = "
<div class='col-md-12'>
<div class='alert alert-success col-md-12 col-sm-12  alert-icon alert-dismissible fade in' role='alert'>
  <div class='col-md-2 col-sm-2 icon-wrapper text-center'>
    <span class='fa fa-check fa-2x'></span></div>
    <div class='col-md-10 col-sm-10'>
      <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
      <span aria-hidden='true'>x</span></button>
      <p><strong>Felicitaciones el Registro fue un Exito.</strong></p>
    </div>
  </div>
</div>";
include('add_logo_peque_reducido_jpg.php');
}
}
?>

<!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
        <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">   
            <!----js para mostrar msj--->
        <script  src="asset/js/jquery.min.js"></script>
        <script src="asset/js/msj.js"></script>
    </head>

    <body id="mimin" class="dashboard">
        <?php include('menu_header_user.php'); ?>

        <div class="container-fluid mimin-wrapper">
            <?php include('menu_lateral_escritorio.php'); ?>
            <div id="content">
                <br>
                <div class="col-md-12">
                    <div class="col-md-12 panel">
                        <div class="col-md-12 panel-heading">
                            <h4 style="text-align: center; color: black;"> Actualizar Imagen del <strong style="color:crimson;">"LOGO"</strong> de la empresa.</h4>
                            <br><br>
                        </div>
        <p>&nbsp;</p>	
		<div style="border: solid 2px #000000; ">				
						<h5 style="text-align: center; color: red;"> 
                                    <strong>REFERENCIA</strong>  <strong style='color:crimson;font-size: 16px;'></strong>
                                </h5>
								
						<h6 style="text-align: center; color: black;"> 
                                   - Tipo de archivos permitidos: <strong>".jpg, .jpeg, .png"</strong><strong style='color:crimson;font-size: 14px;'></strong>
                                </h6>
						
						<h6 style="text-align: center; color: black;"> 
                                   - Se recomienda que el LOGO a cargar sea de máximo <strong>350x350px</strong>, si no cuentas con esa medida, utiliza el siguiente servicio EXTERNO para redimensionar: <a href="https://picasion.com/es/resize-image/" target="_blank">CLICK AQUÍ</a>  <strong style='color:crimson;font-size: 14px;'></strong>
                                </h6>

						<h6 style="text-align: center; color: black;"> 
                                   - <strong>Este logotipo se utiliza en las páginas de COMPARTIR, DESCARGA Y GALERÍA, para modificar el logotipo que aparece en el contacto electrónico, es necesario hacerlo desde la sección "DATOS EN VCARD"</strong><strong style='color:crimson;font-size: 14px;'></strong>
                                </h6>								
        </div>
		<p>&nbsp;</p>
                        <div class="col-md-12 panel-body">
                                <div class="col-md-12">
                                <form class="cmxform" id="signupForm" enctype="multipart/form-data" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                                    <div class="col-md-5">
                                        <div class="form-group form-animate-text">
                                          <div class="input-group fileupload-v1">
                                            <input type="file" name="file" required="required" class="fileupload-v1-file hidden"  accept="image/*">
                                            <input type="text" class="form-control fileupload-v1-path" placeholder="Imagen . . . ." disabled>
                                            <span class="input-group-btn">
                                                <button class="btn fileupload-v1-btn" type="button"><i class="fa fa-folder"></i> Presione Examinar</button>
                                            </span>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="custom-control custom-radio">
                                          <input type="checkbox" class="custom-control-input" name="todosVCard" value="todosVCard" checked>
                                          <label class="custom-control-label" for="defaultGroupExample2"> Cargar imagen para todas las VCard</label>
                                        </div>
                                    </div>
                                        
                                    <div class="col-md-3">
                                        <div class="form-group form-animate-text">
                                          <button class="btn ripple btn-raised btn-success" name="enviar">
                                                <div>
                                                    <span>Agregar Imagen</span>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                </div>
                            </div>
                    </div>
                </div>



                <?php
                    $Consultar = ("SELECT logo_grand FROM myclientes WHERE cod_vcard='".$cod_vcard_cliente."' ORDER BY id DESC LIMIT 1");
                    $numero_servicios = mysqli_query($con, $Consultar);
                    ?>
                    <div class="col-md-12 top-20 padding-0">
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-heading"><h3 style="text-align: center;">Mi <strong style="color: crimson;">"LOGO"</strong> actual</h3></div>
                                <div class="panel-body">
                                    <div class="responsive-table">
                                        <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>Imagen</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                    <?php
                                                    while ($vcard = mysqli_fetch_array($numero_servicios)) {
                                                        ?>
                                                        <tr>
                                                            <td style="text-align: center;">
                                                                <?php
                                                                if (empty($vcard['logo_grand'])) {
                                                                    echo "<center><span style='color:red; font-size:13px;text-align:center;'>Sin Imagen</span></center>";
                                                                } else {
                                                                    ?>
                                                        <center>
                                                            <img src="logos_g/<?php echo $vcard['logo_grand'] ?>" id="tamano_img_table" style='width:300px; text-align: center;'>
                                                        </center>
                                                <?php } ?>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <?php
                        @mysqli_close($numero_servicios);
                        ?>  
                    </div>
                
                    <div class="contenedor_flotante">                         
                    <?php
                        echo isset($msj_exito) ? utf8_decode($msj_exito) : ''; 
                    ?>
                    </div>
            </div>
        </div>


        <!-- start: Mobile -->
        <div id="mimin-mobile" class="reverse" > 
            <?php include('menu_movil.php'); ?>
        </div>
        <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
            <span class="fa fa-bars"></span>
        </button>
        <!-- end: Mobile -->

        <?php include('js.html'); ?>
    </body>
</html>
            
 <?php
} else {
    include('error.php');
}
?>