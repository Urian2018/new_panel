<?php
session_start();
header("Content-Type: text/html;charset=utf-8");
include('config.php');
if (isset($_SESSION['user']) != "") {
    ?>
    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <style>
                .fa-refresh{
                    color: #222;
                }
                .fa-refresh:hover{
                    color: green;
                }
                a{
                    color: #fafafa;
                }
                a:hover{
                    color:#333;
                }
            </style>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
            <script  src="https://code.jquery.com/jquery-2.2.4.js"></script>
        </head>

        <body id="mimin" class="dashboard">
            <?php include('menu_header.php'); ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>

                <div id="content">
                    <br>
                    <div class="col-md-12">
                        <div>
                            <span>
                                <h5 style="text-align: right;">
                                    <button style="margin-top:0px !important;" class="btn-flip btn btn-3d btn-danger">
                                <div class="side">
                                    <a href="limpiar_papelera.php"> Limpiar Papelera </a>
                                     <span class="fa fa-trash"></span>
                                </div>
                                <span class="icon"></span>
                            </button>
                                </h5>
                            </span>
                        </div>
                        
                        
                    </div>
                    
                    <?php
                    $sql = ("SELECT * FROM papelera ORDER BY id");
                   if($mostar = mysqli_query($con, $sql)){
                    $total_client = mysqli_num_rows($mostar) ;
                        ?>
                        <div class="col-md-12 top-20 padding-0">
                            <div class="col-md-12">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h4 style="text-align: center;">
                                            <?php echo " Hay un Total de <strong style='color:green; text-align: center;'>(" .$total_client. ')</strong>'; ?> Registros en Papelera
                                        </h4>
                                    </div>
                                    <div class="panel-body">
                                        <div class="responsive-table">
                                            <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                                                <thead>
                                                    <tr>
                                                        <th>Codigo VCard</th>
                                                        <th>Nombre y Apelido</th>
                                                        <th>Cliente</th>
                                                        <th>Ciudad / Empresa</th>
                                                        <th>Estus VCard</th>
                                                        <th>Fecha de eliminación</th>
                                                        <th>Acción</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    while ($vcard = mysqli_fetch_array($mostar)) {
                                                        $id = $vcard['id'];
                                                        $table = "myclientes";
                                                        $cod_vcard = $vcard['cod_vcard'];
                                                        $nombres   = $vcard['nombre'];
                                                        $empresa   = $vcard['empresa'];
                                                        $ciudad    = $vcard['ciudad'];
                                                        $fecha_de_eliminacion  = $vcard['fecha_delete'];
                                                        $rfc       = $vcard['rfc'];
                                                        $status_vcard = $vcard['estatus_vcard'];
                                                        $delet     = "admin";
                                                       ?>
                                                        <tr>
                                                            <td><?php echo $cod_vcard; ?></td>
                                                            <td><?php echo $nombres; ?></td>
                                                            <td><?php if($rfc !=''){
                                                                echo '<span style="color:crimson">'.$rfc.'<span>'; } else{ echo 'Empleado'; } ?>
                                                            </td>
                                                            <td><?php echo $ciudad .' / '. $empresa; ?> </td> 
                                                            <td><?php echo  $status_vcard; ?></td> 
                                                        <td style="text-align: center;"><?php echo $fecha_de_eliminacion; ?></td>
                                                        <td style="text-align: center; font-size: 25px;">
                                                            <a href="restaurar_registro_papelera.php?id=<?php echo $id; ?>"> 
                                                                <span class="fa fa-refresh" title="Restaurar Registro"></span>
                                                            </a>
                                                  
                                                        </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div>
                        <?php
                    }
                    @mysqli_close($mostar);
                    ?>  
                </div>            
            </div>


            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse" > 
                <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

            <?php include('js.html'); ?>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#datatables-example').DataTable();
                });
            </script>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>