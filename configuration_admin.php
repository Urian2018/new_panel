<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") { ?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="VCARD">
<meta name="author" content="ALEJANDRO TORRES">
<meta name="keyword" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/png" href="../favicon.png" />
<title>VCARD</title>
<?php include('css.html'); ?>
<link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
<style> 
 .img_miniatura{
   width:300px; 
   max-width: 300px;
   min-width: 300px;
   max-height:150px;
   min-height: 150px;
   padding: 3px;
 }   
 .img_miniatura_seleccionada {
   width:300px; 
   max-width: 300px;
   min-width: 300px;
   max-height:150px;
   min-height: 150px;
   padding: 3px;
   background-color: #27C24C;
}
.noseleccion{
  display: block;
  padding: 40px;
}
.seleccion{
  display: block;
  padding: 40px;
  border: 2px solid black;
}

.ocultar_mostrar{
text-align: center;
margin: 0 auto;
}
.ocultar_mostrar:hover{
    cursor: pointer;
}
.img_fondo_view{
    display: block; 
    list-style: none; 
    margin: 0 auto;
    text-align: center;
 }
.img_fondo_view li { 
  display: inline;
  padding: 5px !important;
  position: relative;
} 
.img_fondo_view .fa-times-circle-o{
  font-size: 25px;
  position: absolute;
  color:#191919;
}
.img_fondo_view .fa-times-circle-o:hover{
    font-size: 25px;
  cursor: pointer;
  color: red;
}
.img_fondo_view li img {
border: 5px solid white;
cursor: pointer;

 }
.img_fondo_view li img:hover {
 border: 2px solid #27C24C !important; 
-webkit-transition: all 0.3s linear;
-moz-transition: all 0.3s linear;
transition: all 0.3s linear;
}
.img_fondo_view  div:hover {
cursor: pointer;
border: 2px solid black !important; 
-webkit-transition: all 0.3s linear;
-moz-transition: all 0.3s linear;
transition: all 0.3s linear;
}
</style>

<script  src="asset/js/jquery.min.js"></script>
<script  src="asset/js/funciones_tabs.js"></script>
<script type="text/javascript">
/************JQUERY QUE CAPTURA EL ID DE LA IMG SELECCIONADA Y LA ENVIA A UN PHP*************/
$(document).ready(function() {
$(".img_fondo_view li img").on("click", function () {
var id = $(this).attr('id');
var dataString = 'id=' + id;

var ruta = "recib_config_img_fondo_admin.php";
$('#tab1').html('<center><img src="img/cargandoo.gif"/><br/>Espere un momento, por favor...</center>');
$.ajax({
    url: ruta,
    type: "POST",
    data: dataString,
    complete:function(data){
      $("#msj_exito_img_fondo").delay(500).fadeIn("slow"); 
      $("#msj_exito_img_fondo").delay(3500).fadeOut("slow");
    },
    success: function(data){
          $("#tab1").html(data); // Mostrar la respuestas del script PHP.
    }
});
return false;
});
});

/********JQUE QUE AGREGA LA CLASE HOVE LA IMG ESTA SELECCIONADA*********/
$(function() {
$(".img_fondo_view img").click(function() {
    $('.img_miniatura_seleccionada').removeClass("img_miniatura_seleccionada").addClass("img_miniatura");
    $(this).addClass("img_miniatura_seleccionada");
  });
});  

/********JQUE QUE AGREGA LA CLASE HOVE AL COLOR QUE ESTA SELECCIONADA*********/
$(function() {
$(".img_fondo_view div").click(function() {
    $('.seleccion').removeClass("seleccion").addClass("noseleccion");
    $(this).addClass("seleccion");
  });
});    

/************JQUERY QUE CAPTURA EL ID DEL DIV LISTA DE COLORES PARA ENVIARLO A PHP*************/
$(document).ready(function() {
$(".img_fondo_view li div").on("click", function () {
var id = $(this).attr('id');
var dataString = 'id=' + id;

var ruta = "recib_config_color_fondo_admin.php";
$('#tab1').html('<center><img src="img/cargandoo.gif"/><br/>Espere un momento, por favor...</center>');

$.ajax({
    url: ruta,
    type: "POST",
    data: dataString,
    complete:function(data){
      $("#exito_color_fondo").delay(500).fadeIn("slow"); 
      $("#exito_color_fondo").delay(3500).fadeOut("slow");
    },
    success: function(data){
          $("#tab1").html(data); // Mostrar la respuestas del script PHP.
    }
});
return false;
});
});

/**********ELIMINAR TEMA ADMINISTRADOR****************/
$(document).ready(function() {
  $('a.fa-times-circle-o').click(function(e) {
      e.preventDefault();
      var id = $(this).attr('id');
      var ruta = $("input#ruta").val();
      var dataString = 'id=' + id + '&ruta=' + ruta;
      var parent = $(this).parent();

      $.ajax({
        type: 'get',
        url: 'delete_thema.php',
        data: dataString,
        beforeSend: function() {
          parent.animate({'backgroundColor':'#fafafa'},300);
        },
        complete:function(data){
        $("#delete_theme").delay(500).fadeIn("slow"); 
        $("#delete_theme").delay(3500).fadeOut("slow");
       },
      success: function(data){
        parent.slideUp(300,function() {
          parent.remove();
        });
      }
    });
  });
  return false;
});
</script>
</head>

<body id="mimin" class="dashboard">
<?php include('menu_header_user.php'); ?>

<div class="container-fluid mimin-wrapper">
    <?php include('menu_lateral_escritorio.php'); ?>

    <div id="content">
        <br>
        <div class="col-md-12">
            <div class="col-md-12 panel">

                <div class="col-md-12 panel-heading">
                    <h4 style="text-align: center; color: black; text-transform: uppercase;"> 
                        Configuraci&oacute;n de la Página Galeria  
                    </h4>
                </div>

                <!--------CAPAS Y MSJS DEL TAB CON EXITO-------->    
                <?php
                include('msj_exito.html'); 
                ?>


    <div class="col-md-12 tabs-area">
         
         <ul id="tabs-demo4" class="nav nav-tabs nav-tabs-v3" role="tablist">
          <li role="presentation" class="active">
            <a href="#tabs-demo4-area1" id="tabs-demo4-1" role="tab" data-toggle="tab" aria-expanded="true">CAMBIAR IMAGEN DE FONDO</a>
          </li>
          <li role="presentation" class="">
            <a href="#tabs-demo4-area2" role="tab" id="tabs-demo4-2" data-toggle="tab" aria-expanded="false">CAMBIAR COLOR DE LA WEB</a>
          </li>
        </ul>

    <div class="col-md-12">
        <div id="tabsDemo4Content" class="tab-content tab-content-v3">
    
        <!--section 1---->
        <div role="tabpanel" class="tab-pane fade active in" id="tabs-demo4-area1" aria-labelledby="tabs-demo4-area1">
            <br>
            <div id="capa_tab1_config_admin">
              <?php 
              $tipo = "admin";
              $sql_img_config = ("SELECT id,ruta,estatus FROM configuration WHERE tipo='".$tipo."' ");
              $query_config = mysqli_query($con, $sql_img_config);
              ?>

              <ul class="img_fondo_view">
              <?php
              while ($imgs_fondo = mysqli_fetch_array($query_config)) { 
                  $url_imgs_fondo = $imgs_fondo['ruta']; 
                  $estatus        = $imgs_fondo['estatus'];
                  if($estatus !='Activa'){  ?>
                  <li>
                    <a id="<?php echo $imgs_fondo['id']; ?>" class="fa fa-times-circle-o" title="Eliminar Theme"></a>
                      <img src="<?php echo $url_imgs_fondo; ?>" id="<?php echo $imgs_fondo['id']; ?>"  class="img_miniatura" title="Activar Theme"/>
                  </li>
              <?php } else{ ?>
                  <li>
                    <a id="<?php echo $imgs_fondo['id']; ?>" class="fa fa-times-circle-o"></a>
                      <img src="<?php echo $url_imgs_fondo; ?>" id="<?php echo $imgs_fondo['id']; ?>" class="img_miniatura_seleccionada" title="Theme Activo"/>
                  </li>
              <?php } ?>
              <input type="hidden" name="ruta" id="ruta" value="<?php echo $url_imgs_fondo; ?>"><?php } ?>
             </ul>
            </div>
            <br> 
            <br>
         
          <form enctype="multipart/form-data" method="post" id="config_formulario_tab1_admin">
                <br>          
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <label>CARGAR IMAGEN</label>
                                <div class="input-group fileupload-v1">
                                    <input type="file" name="img_fondo" required="required" id="img_fondo" class="fileupload-v1-file hidden"  accept="image/*">
                                    <input type="text" class="form-control fileupload-v1-path" placeholder="Imagen Vcard . . ." disabled>
                                    <span class="input-group-btn">
                                        <button class="btn fileupload-v1-btn" type="button"><i class="fa fa-folder"></i> Presione Examinar</button>
                                    </span>
                                </div>
                        </div>                       

                    <div class="col-md-6"  style="padding: 23px 0px 0px 0px;">
                             <input type="submit" id="config_botonenviar_tab1_admin" name="config_botonenviar_tab1_admin"  class="btn ripple btn-raised btn-success" value="Guardar Theme">
                        </div>
                    </div>
                    <br><br>

            </form>
        </div>
        <br><br>
        <br>

          <!--section 2---->
          <div role="tabpanel" class="tab-pane fade" id="tabs-demo4-area2" aria-labelledby="tabs-demo4-area2">
          <div id="capa_tab2_config_admin_color"> 
            
            <?php
            $sql_color_config = ("SELECT id,tipo,color,estatus FROM color WHERE tipo='".$tipo."' ");
            $query_config_color = mysqli_query($con, $sql_color_config); ?>

            <ul class="img_fondo_view" style="display: flex; flex-wrap: wrap;">

              <?php  while ($color_fondo = mysqli_fetch_array($query_config_color)) { 
                    $color_fondo_bd = $color_fondo['color']; 
                    $estado_color   = $color_fondo['estatus'];
                    if($estado_color !='Activo'){  ?>
                    <li>
                        <div class="noseleccion" id="<?php echo $color_fondo['id']; ?>" style=" background-color:<?php echo $color_fondo_bd; ?>" title="Activar Color"> </div>  
                    </li>
                <?php } else{ ?>
                    <li>
                      <div class="seleccion" id="<?php echo $color_fondo['id']; ?>" style="background-color:<?php echo $color_fondo_bd; ?> " title="Color Activado"> </div>  
                    </li>

                <?php }  } ?>
                </ul>
              </div>
            <br><br>    
            
             <form method="post" id="config_formulario_tab2_admin">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group form-animate-text" >
                                <input type="color" style="width: 40%; height: 100px;" name="color" id="color">
                            </div>
                        </div>
                        <div class="col-md-6"  style="padding: 35px 0px 0px 0px;">
                                <input type="submit" id="config_botonenviar_tab2_admin" name="config_botonenviar_tab2_admin" class="btn ripple btn-raised btn-success" value="Guardar Color">
                        </div>

                    </div>
            </form>
            <br><br>
          </div>


        </div>
      </div>   

    </div>



        </div>
    </div>
</div>


<!-- start: Mobile -->
<div id="mimin-mobile" class="reverse" > 
<?php include('menu_movil.php'); ?>
</div>
<button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
<span class="fa fa-bars"></span>
</button>
<!-- end: Mobile -->

<?php include('js.html'); ?>

</body>
</html>
<?php
} else {
include('error.php');
}
?>