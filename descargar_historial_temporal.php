    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <style>
              .stle_tbody{ 
                    font-size: 10px; 
                    color: black;
                    }   
             .color{
                  background-color: #9BB; 
                }
            </style>
        </head>

        <body>
        <?php
        header("Content-Type: text/html;charset=utf-8");
        require('config.php');
        header("Content-Type: text/html;charset=utf-8");
        header("Content-Type: application/vnd.ms-excel");
        header("Expires: 0");$filename = "Todos_mis_Clientes." . date('m-d-Y') . ".xls";
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Disposition: attachment; filename=" . $filename . "");

        $sql = ("SELECT * FROM clientes_temporales");
        $mostar = mysqli_query($con, $sql); 
        ?>
    <table style="color:#000099; width:100px;" border=1 align="center" cellpadding=1 cellspacing=1>
    <thead>
      <tr>
        <th class="color">CODIGO VCARD</th>
        <th class="color">CODIGO DISTRIBUIDOR</th>
        <th class="color">URL LOGO</th>
        <th class="color">NOMBRE</th>
        <th class="color">CARGO</th>
        <th class="color">EMPRESA</th>
        <th class="color">DIRECCION</th>
        <th class="color">DIR L2</th>
        <th class="color">DIR L3</th>
        <th class="color">CODIGO POSTAL</th>
        <th class="color">CIUDAD</th>
        <th class="color">TELEFONO</th>
        <th class="color">MOVIL</th>
        <th class="color">WEB</th>
        <th class="color">EMAIL</th>
        <th class="color">URL P&Aacute;GINA 1</th>
      </tr>
    </thead>
    <?php   
     while ($row = mysqli_fetch_array($mostar)) { 
         $web = "https://vcard.mx/_/logos_g/";
         $logo_G = $row['logo_grand'];
         $url_logo_G  = $web.$logo_G;
                echo "<tbody>"; 
                       echo "<tr>"; 
                            echo "<td class='style_tbody'>" . $row['cod_vcard'] . "</td>"; 
                            echo "<td class='style_tbody'>" . $row['cod_distribuidor'] . "</td>";
                            echo "<td class='style_tbody'>" . $url_logo_G . "</td>";
                            echo "<td class='style_tbody'>" . $row['name'] . "</td>";
                            echo "<td class='style_tbody'>" . $row['cargo'] . "</td>";
                            echo "<td class='style_tbody'>" . $row['empresa'] . "</td>";
                            echo "<td class='style_tbody'>" . $row['direccion'] . "</td>";
                            echo "<td class='style_tbody'>" . $row['dir_l2'] . "</td>";
                            echo "<td class='style_tbody'>" . $row['dir_l3'] . "</td>";     
                            echo "<td class='style_tbody'>" . $row['cp'] . "</td>";     
                            echo "<td class='style_tbody'>" . $row['ciudad_compa'] . "</td>";       
                            echo "<td class='style_tbody'>" . $row['telefono'] . "</td>";       
                            echo "<td class='style_tbody'>" . $row['tlf_movil'] . "</td>";      
                            echo "<td class='style_tbody'>" . $row['web_cliente'] . "</td>";        
                            echo "<td class='style_tbody'>" . $row['email'] . "</td>";      
                            echo "<td class='style_tbody'>" . $row['pag1'] . "</td>";      
                        echo "</tr>";      
                echo "</tbody>";   
                        }   
                ?>
            </table>                                      
        </body>
    </html>