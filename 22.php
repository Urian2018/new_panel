<?php
require_once('config.php');
$codigo_vcard = isset($_POST['vc']) ? $_POST['vc'] : $_GET['vc'];


$checkemail = mysqli_query($con, "SELECT * FROM myclientes WHERE cod_vcard='" . $codigo_vcard . "' LIMIT 1 ");
$resultado_vcard = mysqli_num_rows($checkemail);
if($resultado_vcard >0){
    while ($row = mysqli_fetch_array($checkemail)) {
        $id_logo_cliente = $row['id'];
        $cod_vcard = $row['cod_vcard'];
        $name = $row['nombre'];
        $cargo = $row['cargo'];
        $empresa = $row['empresa'];
        $ciudad = $row['ciudad'];
        $email = $row['email'];
        $telefono = $row['telefono'];
        $tlf_movil = $row['tlf_movil'];
        $cp = $row['cp'];
        $logo_peq = $row['logo_peque'];
        $logo_grande = $row['logo_grand'];
        $pagina_descarga = $row['pagina_descarga'];
        $pagina_galeria = $row['pagina_galeria'];
        $logo_galeria = $row['logo_galeria'];
        $ruta_icono_compartir = $row['ruta_icono_compartir'];
        $texto_whasapp = $row['texto_whatsapp'];
        $web_cliente = $row['web_cliente'];
        $codigo_pais = $row['codigo_pais'];
        $pais = $row['pais'];
        $direccion = $row['direccion'];
    }
    //MIS VARIABLES
    $web_vcard = "https://vcard.mx/";
    $folder = "https://vcard.mx/_/logos_p/" . $logo_peq;
    $ruta_logo_peque = $folder;
    ?>

    <!DOCTYPE HTML>
    <html>
        <head>
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-50408658-5"></script>
            <script>
                window.dataLayer = window.dataLayer || [];
                function gtag() {
                    dataLayer.push(arguments);
                }
                gtag('js', new Date());

                gtag('config', 'UA-50408658-5');
            </script>

            <meta name="encoding" charset="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
            <link rel="shortcut icon" type="image/png" href="http://vcard.mx/favicon.png" />
            <meta name="author" content="VCard de México" />
            <meta name="description" content="VCard - Tarjeta de <?php echo $name; ?> - <?php echo $empresa; ?> - Vcard de México - Tarjeta de presentación electrónica - Tarjeta de presentación digital" />
            <meta name="keywords" content="VCard de México, VCard, Tarjetas de Presentación, Tarjetas de Presentación electrónicas, Tarjetas de Presentación Digital, Tecnología, Negocios, Empresarios, Posicionamiento web" />
            <meta name="twitter:card" content="summary"/>

            <!-- METAS FACEBOOK -->
            <meta content='<?php echo $name; ?>' property='og:title'/>
            <meta content='<?php echo $ruta_logo_peque; ?>' property='og:image'/>
            <meta content='<?php echo $name; ?> - <?php echo $empresa; ?>' property='og:description'/>
            <title>VCard de <?php echo $name; ?></title>
            
            <link rel="stylesheet" href="https://vcard.mx/_/tema1/assets/css/main.css" />
           <!-- <link rel="stylesheet" href="https://vcard.mx/_/tema1/assets/css/noscript.css" />--->
            <!--<style type="text/css">
	body:after {
		display: none;
	}
	#main {
		-moz-transform: none !important;
		-webkit-transform: none !important;
		-ms-transform: none !important;
		transform: none !important;
		opacity: 1 !important;
	}
                
                /* Botones de Redes Sociales */
                .social_bookmarks_container {
                    width:100%;
                    height:65px;
                    padding:0;
                    position:relative;
                }
                .social_bookmarks {
                    position:relative;
                    float:left;
                    margin:0;
                    padding:0;
                }
                .social_bookmarks li {
                    margin:0 0 1px 1px;
                    height:57px;
                    width:57px;
                    list-style:none;
                    float:left;
                    padding:0;
                    -webkit-transition: all 0.4s ease-in-out;
                    -moz-transition: all 0.4s ease-in-out;
                    -o-transition: all 0.4s ease-in-out;
                    -ms-transition: all 0.4s ease-in-out; 
                    transition: all 0.4s ease-in-out;
                }
                .social_bookmarks li a {
                    float:left;
                    width:57px;
                    line-height:24px;
                    display: block;
                    text-indent: -99999px;
                    margin:0px;
                    outline: none;
                    padding:0;
                    min-height:57px;
                    height:100%;
                    text-decoration:none;
                }
                .social_bookmarks .iconwhats{background: #A4A4A4 url(//vcard.mx/tema/2/icon/icon-wapp.png) top left no-repeat;}
                .social_bookmarks .iconfacebook{background: #BDBDBD url(//lh5.googleusercontent.com/-nt-UdZG1ns0/U4vC1v57lwI/AAAAAAAALLQ/pAOWjEBSnSg/s114/icon-facebook.png) top left no-repeat;}
                .social_bookmarks .icontwitter{background: #D8D8D8 url(//lh6.googleusercontent.com/-4TshwJW7hIE/U4vC2JM25AI/AAAAAAAALLI/tcIkpheXrYU/s114/icon-twitter.png) top left no-repeat;}
                .social_bookmarks .iconrssmail{background: #E6E6E6 url(//lh5.googleusercontent.com/-H4iUybdCAyU/U4vC1yQWu1I/AAAAAAAALLE/fTpR7eN4cIw/s114/icon-rssmail.png) top left no-repeat;}
                .social_bookmarks .icondescarga{background: #F1F1F1 url(//vcard.mx/tema/2/icon/icon-descarga.png) top left no-repeat;}
                .social_bookmarks .iconwhats:hover{background-position:center -57px; background-color: #25d366;}
                .social_bookmarks .iconfacebook:hover{background-position:center -57px; background-color: #37589b;}
                .social_bookmarks .icontwitter:hover {background-position:center -57px; background-color: #46d4fe;}
                .social_bookmarks .icondescarga:hover {background-position:center -57px; background-color: #de5a49;}
                .social_bookmarks .iconrss:hover{background-position:center -57px; background-color: #ff6600;}
                .social_bookmarks .iconrssmail:hover{background-position:center -57px; background-color: #FFDE00;}
                .social_bookmarks li:hover a{background: transparent url(//lh4.googleusercontent.com/-XNviZ_JqvyU/U4vDMDFAX8I/AAAAAAAALLk/E4VIJr1onlo/s57/icon-social-reflect.png) no-repeat;
                }
            </style>--->
        </head>
        <body class="is-preload">

            <!-- Wrapper -->
            <div id="wrapper">

                <!-- Main -->
                <section id="main">
                    <header>
                        <span class="avatar"><img src="<?php echo $ruta_logo_peque; ?>" alt="" width="140px" /></span>
                        <h1><?php echo $name; ?></h1>
                        <p><?php echo $cargo; ?></p>
                        <p></p>
                        <ul class="icons">
                            <li><a href="<?php echo $pagina_galeria; ?>" class="fa-image">Galería</a></li>								
                        </ul>
                        <p>&nbsp;</p>
                        <h2>COMPARTIR</h2>
                        <center>							
                            <div class='social_bookmarks_container'>
                                <ul class='social_bookmarks'>
                                    <li class='iconwhats'>
                                        <a href='whatsapp://send?text=Pongo a su disposición mi tarjeta de presentación electrónica. Favor de dar *click* en el siguiente enlace: <?php echo $pagina_descarga; ?>'>
                                            Whatsapp
                                        </a>
                                    </li>
                                    <li class='iconfacebook'>
                                        <a href='http://www.facebook.com/sharer.php?u=<?php echo $pagina_descarga; ?>'>
                                            Facebook
                                        </a>
                                    </li>
                                    <li class='icontwitter'>
                                        <a href='http://twitter.com/intent/tweet?text=Tarjeta de presentación electrónica. Favor de dar click en el siguiente enlace: <?php echo $pagina_descarga; ?>'>
                                            Twitter
                                        </a>
                                    </li>
                                    <li class='iconrssmail'>
                                        <a href='mailto:?subject=Tarjeta%20de%20presentación%20electrónica&body=Pongo%20a%20su%20disposción%20mi%20tarjeta%20de%20presentación%20electrónica.%20Favor%20de%20dar%20click%20en%20el%20siguiente%20enlace:%20%20<?php echo $pagina_descarga; ?>'>
                                            E-Mail
                                        </a>
                                    </li>
                                    <li class='icondescarga'>
                                        <a href='<?php echo $pagina_descarga; ?>'>
                                            Descargar
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </center>
                        <!-- 1. Define some markup -->
                        <center>
                            <button class="btn">
                                <strong>
                                    <span style="font-size:12px;">COPIAR ENLACE</span>
                                </strong>
                            </button>
                        </center>

                        <!-- 2. Include library -->
                        <script src="https://vcard.mx/ctrlc/clipboard.min.js"></script>

                        <!-- 3. Instantiate clipboard -->
                        <script>
                var clipboard = new Clipboard('.btn', {
                    text: function () {
                        return '<?php echo $pagina_descarga; ?>';
                    }
                });

                clipboard.on('success', function (e) {
                    console.log(e);
                });

                clipboard.on('error', function (e) {
                    console.log(e);
                });
                        </script>						
                    </header>
                    <footer>
                        <p>&nbsp;</p>
                        <p style='text-align: center;'>
                            <span style='font-size:25px;'>
                                <font style='vertical-align: inherit;'>
                                <strong>
                                    VCARD OFFLINE
                                </strong>
                                </font>
                            </span>
                        </p>
                        <div id='output'></div>

                        <script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js'></script>
                        <script type='text/javascript' src='https://vcard.mx/_/asset/js/jquery.qrcode.min.js'></script>
                        <script>
                jQuery(function () {

                    jQuery('#output').qrcode('MECARD:N:<?php echo $name; ?>;TEL:<?php echo $telefono; ?>;TEL:<?php echo $tlf_movil; ?>;EMAIL:<?php echo $email; ?>;URL:<?php echo $pagina_descarga; ?>');
                })
                        </script>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>

                        <div align='center'>
                            <script type='text/javascript'>
                                window.setInterval(BlinkIt, 500);
                                var color = 'black';
                                function BlinkIt() {
                                    var blink = document.getElementById('blink');
                                    color = (color == '#ffffff') ? 'black' : '#ffffff';
                                    blink.style.color = color;
                                    blink.style.fontSize = '32px';
                                }
                            </script>
                        </div>
                    </footer>
                </section>

                <!-- Footer -->
                <div>
                    <footer id="footer">
                        <ul class="copyright">
                            <li>&copy; VCard 2018</li><li>VCard de México - <a href="https://vcard.mx">VCard.mx</a></li>
                        </ul>
                    </footer>
                </div>
                <!-- Scripts -->
                <script>
                    if ('addEventListener' in window) {
                        window.addEventListener('load', function () {
                            document.body.className = document.body.className.replace(/\bis-preload\b/, '');
                        });
                        document.body.className += (navigator.userAgent.match(/(MSIE|rv:11\.0)/) ? ' is-ie' : '');
                    }
                </script>
                
                <?php } else{ 
    include('error_404.html');
}  ?>
        </body>
    </html>