<?php
session_start();
require('config.php');
$id_cliente_vcard   = $_REQUEST['id'];
$cod_vcard_cliente  = $_SESSION['cod_vcard'];
$rfc_empresa        = $_SESSION['rfc_empresa'];

$name_actual = $_POST['filename'];
$file = $_FILES['img_vcard']['name'];
$nombre_sin_extension = pathinfo($name_actual, PATHINFO_FILENAME); //funcion para sber solo el nombre de un archivo
$rtOriginal = $_FILES['img_vcard']['tmp_name'];
$folder ="Galeria_Clientes/".$cod_vcard_cliente.'/';

$fileExtension = substr(strrchr($file, '.'), 1); //para saber l extencion de l img
$mi_file = $nombre_sin_extension.'.'.$fileExtension;

 //VERIFANDO SINO EXISTE LA CARPETA LA CREO
 if (!file_exists($folder)) {
     mkdir($folder, 0777, true);
 }

 
$png = 'png';
$PNG = 'PNG';
if ($fileExtension == $png || $fileExtension == $PNG ) {
$original =  imagecreatefrompng($rtOriginal);

//Ancho y alto máximo
$max_ancho = 800;
$max_alto = 600;

//Medir la imagen
list($ancho,$alto)=getimagesize($rtOriginal);

//Ratio
$x_ratio = $max_ancho / $ancho;
$y_ratio = $max_alto / $alto;

//Proporciones
if(($ancho <= $max_ancho) && ($alto <= $max_alto) ){
$ancho_final = $ancho;
$alto_final = $alto;
}
else if(($x_ratio * $alto) < $max_alto){
$alto_final = ceil($x_ratio * $alto);
$ancho_final = $max_ancho;
}
else {
$ancho_final = ceil($y_ratio * $ancho);
$alto_final = $max_alto;
}

//Crear un lienzo
$lienzo=imagecreatetruecolor($ancho_final,$alto_final);
//Copiar original en lienzo
imagecopyresampled($lienzo,$original,0,0,0,0,$ancho_final, $alto_final,$ancho,$alto);
//Destruir la original
imagedestroy($original);
//Crear la imagen y guardar en directorio logos_g/
imagepng($lienzo,$folder.$mi_file);

$query = ("UPDATE slider_cliente SET filename='".$mi_file."', cod_vcard='".$cod_vcard_cliente."', rfc_empresa='".$rfc_empresa."' WHERE id='".$id_cliente_vcard."' ");
$result = mysqli_query($con, $query); ?>
<META HTTP-EQUIV="REFRESH" CONTENT="0;URL=add_carrusel.php">
<?php } else{
$original = imagecreatefromjpeg($rtOriginal);

$max_ancho = 600; 
$max_alto = 400;

list($ancho,$alto)=getimagesize($rtOriginal);
$x_ratio = $max_ancho / $ancho;
$y_ratio = $max_alto / $alto;
//Proporciones
if(($ancho <= $max_ancho) && ($alto <= $max_alto) ){
 $ancho_final = $ancho;
 $alto_final = $alto;
}
else if(($x_ratio * $alto) < $max_alto){
 $alto_final = ceil($x_ratio * $alto);
 $ancho_final = $max_ancho;
}
else {
 $ancho_final = ceil($y_ratio * $ancho);
 $alto_final = $max_alto;
}

//Crear un lienzo
$lienzo=imagecreatetruecolor($ancho_final,$alto_final);
imagecopyresampled($lienzo,$original,0,0,0,0,$ancho_final, $alto_final,$ancho,$alto);
imagedestroy($original);
imagejpeg($lienzo,$folder.$mi_file);

$query_jpg = ("UPDATE slider_cliente SET filename='".$mi_file."', cod_vcard='".$cod_vcard_cliente."', rfc_empresa='".$rfc_empresa."' WHERE id='".$id_cliente_vcard."' ");
$result = mysqli_query($con, $query_jpg); ?>
<META HTTP-EQUIV="REFRESH" CONTENT="0;URL=add_carrusel.php">
<?php } ?>