<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
    $id_user    = $_SESSION['id'];
    $rango_user = $_SESSION['rango_users'];
    $rfc_empresa = $_SESSION['rfc_empresa'];
    $codEvento   = $_SESSION['user'];   
    ?>
<!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
            <style type="text/css">
                ul li {
                  list-style-type: none;
                }
                #enlace:hover{
                    text-decoration: underline;
                }
            </style>
            <!----js para mostrar msj--->
            <script  src="asset/js/jquery.min.js"></script>
            <script src="asset/js/msj.js"></script>
            <script type="text/javascript">
            $(function () {
                var nextinput = 0;
                var scntDiv = $('#dynamicDiv');
                $(document).on('click', '#addInput', function () {
                    nextinput++;
                    $('<p>'+
                            '<div class="col-md-6" id="eliminar">'+
                                '<label>CAMPO ' + nextinput + '</label>'+
                                    '<div class="form-group form-animate-text">'+
                                        '<input type="text" name=campo[] style="width:60%; color:#333; font-weight:bold;" />'+
                                            '<a style="margin:0px 0px 0px 5px;" class="btn btn-danger" href="javascript:void(0)" id="removerInput">'+
                                            '<span class="glyphicon glyphicon-minus" aria-hidden="true"></span> '+
                                            'Eliminar'+
                                        '</a>'+
                            '</div></div></p>').appendTo(scntDiv);



            if (nextinput == 1) {  
                $('<div><input type="submit" class="btn ripple btn-raised btn-success" value="Crear Formulario de Visitantes" name="Crear Formulario de Visitantes"/></div>').appendTo('#botonEnviar');
                }

            //limite de input
             if (nextinput == 20) { 
                //Primero se desabilita el boton y luego muestro un mensaje en la modal 
                 $('#addInput').click(function() {
                    $("#addInput").attr('disabled',true);//desabilitando el boton
                    $("#mostrarmodal").modal("show"); //muestro la modal
                return false; 
                });
             }   

            return false;
            });               

            //para eliminar un input
            $(document).on('click', '#removerInput', function () {
             //console.log(nextinput);
                $(this).parents('#eliminar').remove();
                return false;
                });
            });
        </script>
    </head>

        <body id="mimin" class="dashboard">

            <?php 
                if ($rango_user !="Administrador") {
                    include('menu_header_user.php');
                } else{
                    include('menu_header.php');
                }
             ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>

                <div id="content">
                    <br><br>
                    
                    <div class="col-md-12">
                        <div class="col-md-12 panel">
                            <div class="col-md-12 panel-heading">
                                <h4 style="text-align: center; color: black;"> Personaliza tu formulario de registros para <strong style="color:crimson;">"Visitantes"</strong></h4>
                                <br>
                                <a class="btn btn-primary" href="javascript:void(0)" id="addInput">
                                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                    Agregar Campo
                                </a>    

                                <?php
                                $ConsultarForm =  ("SELECT * FROM visitantexpo WHERE cod_expo='".$codEvento. "' ORDER BY id DESC Limit 1");
                                $mostar = mysqli_query($con, $ConsultarForm);
                                $Cant = mysqli_num_rows($mostar);
                                if ($Cant >= 1 ) {
                                    echo '<a class="btn btn-primary" href="edit_FormVisitantes.php">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        Editar Campos
                                    </a>';

                                   echo '<a style="float:right;" href="https://vcard.mx/expo/" target="_blank" id="enlace"> Ver Formulario</a>';
                                }
                                ?>
                            </div>
<!----mensaje en modal---->
<div class="modal fade" id="mostrarmodal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>Notificación</h3>
     </div>
         <div class="modal-body">
            <h4>Ha consumido el total de campos para el formulario,</h4>
            solo se´permiten 20 campos para construir el formulario.
     </div>
         <div class="modal-footer">
        <a href="#" data-dismiss="modal" class="btn btn-danger">Cerrar</a>
     </div>
      </div>
   </div>
</div>
<!----fin dela modal---->




                            <form  enctype="multipart/form-data" method="post" action="recib_CrearFormVisitante.php">

                                <div class="col-md-12 panel-body">
                                    <div class="col-md-12">
                                        <div id="dynamicDiv">

                                           
                                        </div>
                                           
                                <input type="hidden" name="cod_expo" value="<?php echo $codEvento?>">
                                <div id="botonEnviar">
                                    
                                </div>
                                 
                                </div>
                                </div>
                            </form>
            
        
                        </div>

                    </div>
                </div>

                <div class="contenedor_flotante">                         
                <?php
                if (!empty($_GET['msj'])) {  ?>
            <div class='col-md-12'>
            <div class='alert alert-success col-md-12 col-sm-12  alert-icon alert-dismissible fade in' role='alert'>
                <div class='col-md-2 col-sm-2 icon-wrapper text-center'>
                    <span class='fa fa-check fa-2x'></span></div>
                        <div class='col-md-10 col-sm-10'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>
                        <p><strong>Felicitaciones el Formulario para Visitantes fue creado Correctamente</strong></p>
                        </div>
                </div>
            </div> 
            <?php } ?> 
            </div>



            </div>

            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse"> 
    <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

    <?php include('js.html'); ?>

        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>