<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
$id_user = $_SESSION['id'];
$cod_vcard_session = $_SESSION['cod_vcard'];
$rfc_empresa_session = $_SESSION['rfc_empresa']; 
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="VCARD">
<meta name="author" content="ALEJANDRO TORRES">
<meta name="keyword" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/png" href="../favicon.png" />
<title>VCARD</title>
<?php include('css.html'); ?>
<link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
<style> 
 .img_miniatura{
   width:300px; 
   max-width: 300px;
   min-width: 300px;
   max-height:150px;
   min-height: 150px;
   padding: 3px;
 }   
 .img_miniatura_seleccionada {
   width:300px; 
   max-width: 300px;
   min-width: 300px;
   max-height:150px;
   min-height: 150px;
   padding: 3px;
   background-color: #27C24C;
}
.noseleccion{
  display: block;
  padding: 40px;
}
.seleccion{
  display: block;
  padding: 40px;
  border: 2px solid black;
}
.texto{
color:#2196F3;
text-align: center;
margin: 0 auto;
text-transform: uppercase;
font-weight: bold;
}
.previsualizacion { 
background-color: #27C24C;
font-family: "Roboto", Helvetica, Arial, sans-serif;
font-weight: 800;
color: #fff;
padding: 10px 35px;
border-radius: 50px;
float: right; 
text-align: center; 
text-transform: uppercase;
}
.previsualizacion:hover{
  color:#333;
}
.img_fondo_view{
    display: block; 
    list-style: none; 
    margin: 0 auto;
    text-align: center;
 }
.img_fondo_view li { display: inline; padding: 5px !important;} 
.img_fondo_view li img { border: 5px solid white; cursor: pointer; }
.img_fondo_view li img:hover {
 border: 2px solid #27C24C !important; 
-webkit-transition: all 0.3s linear;
-moz-transition: all 0.3s linear;
transition: all 0.3s linear;
}
.img_fondo_view  div:hover {
cursor: pointer;
border: 2px solid black !important; 
-webkit-transition: all 0.3s linear;
-moz-transition: all 0.3s linear;
transition: all 0.3s linear;
}
/*.img_fondo_view li img.hover { border: 2px solid #27C24C !important; background-color: #27C24C; }*/
</style>

<script  src="asset/js/jquery.min.js"></script>
<script  src="asset/js/funciones_tabs.js"></script>
<script src="asset/js/msj.js"></script>
<script type="text/javascript">
/************JQUERY QUE CAPTURA EL ID DE LA IMG SELECCIONADA Y LA ENVIA A UN PHP*************/
$(document).ready(function() {
$(".img_fondo_view li img").on("click", function () {
var id = $(this).attr('id');
var cod_vcard = $("input#cod_vcard").val();
var rfc_empresa = $("input#rfc_empresa").val();

var dataString = 'cod_vcard=' + cod_vcard + '&rfc_empresa=' + rfc_empresa + '&id=' + id;

var ruta = "recib_config_img_fondo.php";
$('#tab1').html('<center><img src="img/cargandoo.gif"/><br/>Espere un momento, por favor...</center>');
$.ajax({
    url: ruta,
    type: "POST",
    data: dataString,
    complete:function(data){
      $("#msj_exito_img_fondo").delay(500).fadeIn("slow"); 
      $("#msj_exito_img_fondo").delay(3500).fadeOut("slow");
    },
    success: function(data){
          $("#tab1").html(data); // Mostrar la respuestas del script PHP.
    }
});
return false;
});
});

/********JQUE QUE AGREGA LA CLASE HOVE LA IMG ESTA SELECCIONADA*********/
$(function() {
$(".img_fondo_view img").click(function() {
    $('.img_miniatura_seleccionada').removeClass("img_miniatura_seleccionada").addClass("img_miniatura");
    //$(".img_fondo_view img").not(this).removeClass(".img_fondo_view hover");
    $(this).addClass("img_miniatura_seleccionada");
    //$(this).toggleClass("hover");
  });
});   


/********JQUE QUE AGREGA LA CLASE HOVE AL COLOR QUE ESTA SELECCIONADA*********/
$(function() {
$(".img_fondo_view div").click(function() {
    $('.seleccion').removeClass("seleccion").addClass("noseleccion");
    $(this).addClass("seleccion");
  });
});    

/************JQUERY QUE CAPTURA EL ID DEL DIV LISTA DE COLORES PARA ENVIARLO A PHP*************/
$(document).ready(function() {
$(".img_fondo_view li div").on("click", function () {
var id = $(this).attr('id');
var dataString = 'id=' + id;

var ruta = "recib_config_color_fondo_cliente.php";
$('#capa_tab2_config_cliente_color').html('<center><img src="img/cargandoo.gif"/><br/>Espere un momento, por favor...</center>');

$.ajax({
    url: ruta,
    type: "POST",
    data: dataString,
    complete:function(data){
      $("#exito_color_fondo").delay(500).fadeIn("slow"); 
      $("#exito_color_fondo").delay(3500).fadeOut("slow");
    },
    success: function(data){
          $("#capa_tab2_config_cliente_color").html(data); // Mostrar la respuestas del script PHP.
    }
});
return false;
});
});
</script>
</head>

<body id="mimin" class="dashboard">
<?php include('menu_header_user.php'); ?>

<div class="container-fluid mimin-wrapper">
    <?php include('menu_lateral_escritorio.php'); ?>

    <div id="content">
        <br>
        <div class="col-md-12">
            <div class="col-md-12 panel">

                <div class="col-md-12 panel-heading">
                    <h4 style="text-align: center; color: black;text-transform: uppercase;"> 
                        Configuraci&oacute;n de la Página Galeria  
                    </h4><?php 
                    $enlace     = "https://vcard.mx/_/3.php?vc=";
                    $url_enlace = $enlace.''.$cod_vcard_session; ?>
                    <a class="previsualizacion" target="_blank" href=<?php echo $url_enlace; ?>> previsualizacion</a>
                </div>


                <!--------CAPAS Y MSJS DEL TAB CON EXITO-------->    
                <?php
                include('msj_exito.html'); 
                ?>


    <div class="col-md-12 tabs-area">
         
         <ul id="tabs-demo4" class="nav nav-tabs nav-tabs-v3" role="tablist">
          <li role="presentation" class="active">
            <a href="#tabs-demo4-area1" id="tabs-demo4-1" role="tab" data-toggle="tab" aria-expanded="true">CAMBIAR IMAGEN DE FONDO</a>
          </li>
          <li role="presentation" class="">
            <a href="#tabs-demo4-area2" role="tab" id="tabs-demo4-2" data-toggle="tab" aria-expanded="false">CAMBIAR COLOR DE LA WEB</a>
          </li>
          <!---<li role="presentation">
            <a href="#tabs-demo4-area3" id="tabs-demo4-3" role="tab" data-toggle="tab" aria-expanded="false">X</a>
          </li>
          <li role="presentation" class="">
            <a href="#tabs-demo4-area4" role="tab" id="tabs-demo4-4" data-toggle="tab" aria-expanded="false">Y</a>
          </li>--->
        </ul>

    <div class="col-md-12">
        <div id="tabsDemo4Content" class="tab-content tab-content-v3">
    
        <!--section 1---->
        <div role="tabpanel" class="tab-pane fade active in" id="tabs-demo4-area1" aria-labelledby="tabs-demo4-area1">
          <br>
          <?php 
          /*******VERIFICANDO SI EL CLIENTE TIENE IMAGEN PROPIA DE FONDO**********/
          $sql_img_fondo = ("SELECT cod_vcard,ruta,rfc_empresa FROM configuration WHERE cod_vcard='".$cod_vcard_session."' AND rfc_empresa='".$rfc_empresa_session."' ");
          $query_img_fondo = mysqli_query($con, $sql_img_fondo);
          $total_img_fondo = mysqli_num_rows($query_img_fondo) ;
          $row_resul_img   = mysqli_fetch_array($query_img_fondo);

          $tipo = "admin";
          $sql_img_config = ("SELECT * FROM configuration WHERE tipo='".$tipo."' OR cod_vcard='".$cod_vcard_session."' AND rfc_empresa='".$rfc_empresa_session."' ");
          $query_config = mysqli_query($con, $sql_img_config);

          if($total_img_fondo >0){ ?>

               <ul class="img_fondo_view">
                <?php
                while ($imgs_fondo = mysqli_fetch_array($query_config)) { 
                    $url_imgs_fondo = $imgs_fondo['ruta']; 
                    $estatus        = $imgs_fondo['estatus'];
                    if($imgs_fondo['cod_vcard'] == $cod_vcard_session && $estatus ="Activa"){ ?>
                    <li>
                        <img src="<?php echo $url_imgs_fondo; ?>" id="<?php echo $imgs_fondo['id']; ?>"  class="img_miniatura_seleccionada" title="Theme Activo"/>
                    </li>
                <?php } else{  ?>
                    <li>
                        <img src="<?php echo $url_imgs_fondo; ?>" id="<?php echo $imgs_fondo['id']; ?>" class="img_miniatura" title="Activar Theme"/>
                    </li>

                <?php } } ?>
                </ul>
                    <input type="hidden" name="cod_vcard" id="cod_vcard" value="<?php echo $cod_vcard_session; ?>">
                    <input type="hidden" name="rfc_empresa" id="rfc_empresa" value="<?php echo $rfc_empresa_session; ?>">
         <?php }else{ ?>
          <ul class="img_fondo_view">
                <?php
                while ($imgs_fondo = mysqli_fetch_array($query_config)) { 
                    $url_imgs_fondo = $imgs_fondo['ruta']; 
                    $estatus        = $imgs_fondo['estatus'];
                    if($estatus =='Activa' &&  $imgs_fondo['tipo'] == "admin"){ ?>
                    <li>
                        <img src="<?php echo $url_imgs_fondo; ?>" id="<?php echo $imgs_fondo['id']; ?>"  class="img_miniatura_seleccionada" title="Theme Activo"/>
                    </li>
                <?php } else{  ?>
                    <li>
                        <img src="<?php echo $url_imgs_fondo; ?>" id="<?php echo $imgs_fondo['id']; ?>" class="img_miniatura"  title="Activar Theme"/>
                    </li>

                <?php } } ?>
                </ul>
                    <input type="hidden" name="cod_vcard" id="cod_vcard" value="<?php echo $cod_vcard_session; ?>">
                    <input type="hidden" name="rfc_empresa" id="rfc_empresa" value="<?php echo $rfc_empresa_session; ?>">

        <?php } ?> 
        <br><br>       

 
            <center>
                <p class="texto">Agregar imagen de fondo Personalizada</p>
            </center>
            <!--</div>  -->
            <br>
         
                <form enctype="multipart/form-data" method="post" id="config_formulario_tab1_cliente" action="recib_config_tab1.php">
                <br>          
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <label>CARGAR IMAGEN</label>
                                <div class="input-group fileupload-v1">
                                    <input type="file" name="img_fondo" required="required" id="img_fondo" class="fileupload-v1-file hidden"  accept="image/*">
                                    <input type="text" class="form-control fileupload-v1-path" placeholder="Imagen Vcard . . ." disabled>
                                    <span class="input-group-btn">
                                        <button class="btn fileupload-v1-btn" type="button"><i class="fa fa-folder"></i> Presione Examinar</button>
                                    </span>
                                </div>
                        </div>
                        <input type="hidden" name="cod_vcard" value="<?php echo $cod_vcard_session; ?>">
                        <input type="hidden" name="rfc_empresa" value="<?php echo $rfc_empresa_session; ?>">
                       

                    <div class="col-md-6"  style="padding: 23px 0px 0px 0px;">
                      <input type="submit" id="config_botonenviar" name="config_botonenviar"  class="btn ripple btn-raised btn-success" value="Guardar Theme">
                    </div>
                    </div>
                    <br><br>
            </form>
        </div>
        <br><br>
        <br>
 


          <!--section 2---->
          <div role="tabpanel" class="tab-pane fade" id="tabs-demo4-area2" aria-labelledby="tabs-demo4-area2">
            
          <div id="capa_tab2_config_cliente_color"> 
          <?php 
          /*******VERIFICANDO SI EL CLIENTE TIENE IMAGEN PROPIA DE FONDO**********/
          $sql_colo_fondo = ("SELECT cod_vcard,estatus,rfc_empresa FROM color WHERE cod_vcard='".$cod_vcard_session."' AND rfc_empresa='".$rfc_empresa_session."' ");
          $query_color_fondo = mysqli_query($con, $sql_colo_fondo);
          $total_color_fondo = mysqli_num_rows($query_color_fondo) ;
          $row_resul_color   = mysqli_fetch_array($query_color_fondo);


          $tipo_admin = "admin";
          $sql_color_config = ("SELECT id,tipo,color,estatus,cod_vcard FROM color WHERE tipo='".$tipo_admin."' OR cod_vcard='".$cod_vcard_session."' AND rfc_empresa='".$rfc_empresa_session."' ");
          $query_config_color = mysqli_query($con, $sql_color_config);

          if($total_color_fondo >0){ ?>

              <ul class="img_fondo_view" style="display: flex; flex-wrap: wrap;">

                <?php  while ($color_fondo = mysqli_fetch_array($query_config_color)) { 
                      $color_fondo_bd = $color_fondo['color']; 
                      $estado_color   = $color_fondo['estatus'];
                      $cod_vcard_color = $color_fondo['cod_vcard'];
                      if($color_fondo['cod_vcard'] == $cod_vcard_session && $estado_color ="Activo"){ ?>
                      <li>
                          <div class="seleccion" id="<?php echo $color_fondo['id']; ?>" style=" background-color:<?php echo $color_fondo_bd; ?>" title="Color Activado"> </div>  
                      </li>
                  <?php } else{ ?>
                      <li>
                          <div class="noseleccion" id="<?php echo $color_fondo['id']; ?>" style="background-color:<?php echo $color_fondo_bd; ?> " title="Activar Color"> </div>  
                      </li>
                  <?php }  } ?>
                  </ul>
              <?php }else{ ?>
                      <ul class="img_fondo_view" style="display: flex; flex-wrap: wrap;">

                <?php  while ($color_fondo = mysqli_fetch_array($query_config_color)) { 
                      $color_fondo_bd = $color_fondo['color']; 
                      $estado_color   = $color_fondo['estatus'];
                      $cod_vcard_color = $color_fondo['cod_vcard'];
                      if($estado_color =='Activo' &&  $color_fondo['tipo'] == "admin"){ ?>
                      <li>
                          <div class="seleccion" id="<?php echo $color_fondo['id']; ?>" style=" background-color:<?php echo $color_fondo_bd; ?>" title="Color Activado"> </div>  
                      </li>
                  <?php } else{ ?>
                      <li>
                          <div class="noseleccion" id="<?php echo $color_fondo['id']; ?>" style="background-color:<?php echo $color_fondo_bd; ?> "  title="Activar Color"> </div>  
                      </li>
                  <?php }  } ?>
                  </ul>

              <?php } ?>
                </div>
              
                <br><br>

             <form method="post" id="config_formulario_tab2">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                <input type="color" style="width: 40%; height: 100px;" name="color" id="color">
                                <input type="hidden" name="cod_vcard" value="<?php echo $cod_vcard_session; ?>">
                                <input type="hidden" name="rfc_empresa" value="<?php echo $rfc_empresa_session; ?>">
                            </div>
                        </div>
                        <div class="col-md-6"   style="padding: 45px 0px 0px 0px;">
                                <input type="submit" id="config_botonenviar_tab2" name="config_botonenviar_tab2" class="btn ripple btn-raised btn-success" value="Guardar Color">
                        </div>
                    </div>
            </form>
            </div>


        <!--section 3---->
          <!---<div role="tabpanel" class="tab-pane fade" id="tabs-demo4-area3" aria-labelledby="tabs-demo4-area3">
            
           <h2>hola . . .</h2>
          </div>--->

        
        <!--section 4---->
        <!---<div role="tabpanel" class="tab-pane fade" id="tabs-demo4-area4" aria-labelledby="tabs-demo4-area4">
           <form method="post" id="formulario" name="formulario" action="recib_update_datos_mis_empleados.php">
                    <h2>4</h2>
            </form>
          </div>--->



        </div>
      </div>   

    </div>



    </div>
</div>



<div class="contenedor_flotante">   
  <?php
  if (!empty($_GET['msj'])) {
    ?>
    <div class='col-md-12'>
    <div class='alert alert-success col-md-12 col-sm-12  alert-icon alert-dismissible fade in' role='alert'>
      <div class='col-md-2 col-sm-2 icon-wrapper text-center'>
        <span class='fa fa-check fa-2x'></span></div>
        <div class='col-md-10 col-sm-10'>
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
          <span aria-hidden='true'>x</span></button>
          <p><strong>La Imagen de Fondo Fue Registrada Correctamente.</strong></p>
        </div>
      </div>
    </div>
    <?php } ?>
</div>    

</div>
</div>


<!-- start: Mobile -->
<div id="mimin-mobile" class="reverse" > 
<?php include('menu_movil.php'); ?>
</div>
<button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
<span class="fa fa-bars"></span>
</button>
<!-- end: Mobile -->

<?php include('js.html'); ?>

</body>
</html>
<?php
} else {
include('error.php');
}
?>