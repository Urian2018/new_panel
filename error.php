<!DOCTYPE html>
<html>
    <head>
        <title>VCARD</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" type="image/png" href="../favicon.png" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <style>
            * {
                margin: 0;
                padding: 0;
                box-sizing: border-box;
            }
            body {
                background: white;
                font-family: 'Open Sans', sans-serif;
            }
            section{
                width: 100%;
                height: 350px;
                background: #2196F3;
                color: #fff;
                margin-top: 110px;
                position: absolute;

            }
            p{
                font-size: 20px;
                text-align: center;
                margin-top: 90px;
                font-weight: bold;
                color: red;
            }
            h1{
                text-align: center;
                color: black;
            }
            img{
                width:150px; 
                border-radius: 50%;
                display: block;
                position: absolute;
                left: 50%;
                transform: translate(-50%, -50%);
                font-size: 30px;
                border: 2px solid grey;
            }

        </style>

        <script language="JavaScript">
            var totalTiempo = 2;
            var url = "index.php";
            function updateReloj()
            {
                document.getElementById('CuentaAtras').innerHTML = " " + totalTiempo + " ";
                if (totalTiempo == 0)
                {
                    window.location = url;
                } else {
                    /* Restamos un segundo al tiempo restante */
                    totalTiempo -= 1;
                    /* Ejecutamos nuevamente la función al pasar 1000 milisegundos (1 segundo) */
                    setTimeout("updateReloj()", 1000);
                }
            }
            window.onload = updateReloj;

        </script>
    </head>
    <body>

        <section>
            <img src="asset/img/img_vcard.png" alt="">
            
            <p>Usted debe Iniciar Sessi&oacute;n</p>
            <h1 id='CuentaAtras'>
                
            </h1>
        </section>
    </body>
</html>

