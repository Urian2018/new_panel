<?php
session_start();
header("Content-Type: text/html;charset=utf-8");
include('config.php');
if (isset($_SESSION['user']) != "") {
    $rango_usuario 			= $_SESSION['rango_users'];
    $rfc_empresa 			= $_GET['rfc_empresa'];
    $cod_vcard_cliente		= $_GET['cod_vcard'];
    ?>
    
    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
            <style type="text/css">
                  .lista_vcards {
                      list-style-type: none;
                      margin: 0;
                      padding: 0;
                    }

                    .lista_vcards li {
                      float: left;
                      text-align: center;
                      width: 15em;
                      padding: 6px;
                    }
            </style>
            <link rel="stylesheet" type="text/css" href="asset/css/bootstrap-multiselect.css">
        </head>

        <body id="mimin" class="dashboard">
            <?php include('menu_header.php'); ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>

                <div id="content">
            <?php
                $sql_jefe_asoc_vcard = ("SELECT jefe_asoc_vcard, migrar_descarga_vcard FROM asociar_vcard WHERE jefe_asoc_vcard='".$cod_vcard_cliente."' "); 
                $numero_sql_jefe = mysqli_query($con, $sql_jefe_asoc_vcard);
                $total = mysqli_fetch_array($numero_sql_jefe);
                $codigo_jefe 	  = $total["jefe_asoc_vcard"];
                $cod_migrar_vcard = $total["migrar_descarga_vcard"];
                if($codigo_jefe !=""){ ?>
                <br>
				 <div class="col-md-12">
                        <div class="col-md-12 panel">
                            <div class="col-md-12 panel-heading">
                                <h4 style="text-align: center; color: black;">Editar la Descargar de la <strong style="color:crimson;">"VCard"</strong> <?php echo $cod_vcard_cliente; ?></h4>
                                <br>
                                <p style="text-align:right;">
                                    <a href="busqueda_myclientes.php" title="Volver">
                                        <span class="icon-action-undo" title="Volver">Volver</span>
                                    </a>

                                    <button style="float: right; padding: 5px 20px;margin: 0px 0px 5px 10px;" title="Marcar Todos" type="button" class="btn btn-gradient btn-info">
                                    <input type="checkbox" onclick="marcar(this);" /> 
                                    </button>
                                </p>
                            </div>
                            
                            <?php
                            $sqlx = ("SELECT * FROM asociar_vcard WHERE migrar_descarga_vcard='".$cod_migrar_vcard."' AND cod_vcard !='".$cod_vcard_cliente."'");
                            $mostar = mysqli_query($con, $sqlx);
        
                            $estatus        = "Descarga_Inactiva";
                            $desc_activa    = "Descarga_Activa";
                            $suma = ("SELECT * FROM asociar_vcard WHERE estatus='".$desc_activa."' AND migrar_descarga_vcard='".$cod_migrar_vcard."' AND jefe_asoc_vcard!='".$cod_vcard_cliente."' "); 
                            $numero_vis = mysqli_query($con, $suma);
                            $total_client = mysqli_num_rows($numero_vis);
                            ?>
                           
                            <form  method="post" action="recib_editar_asociar_vcard.php">
                                <div class="col-md-12 panel-body">

                                        <div class="col-md-12">
                                            <ul class="lista_vcards">
											    <?php
											     while ($row = mysqli_fetch_array($mostar)) { ?>
                                                    <li>
                                                    <?php if($row['estatus']=="Descarga_Inactiva"){ ?>
                                                        <input type="checkbox" name="cod_vcard[]" value="<?php echo $row['cod_vcard']; ?>" >
                                                        <?php echo $row['cod_vcard']; 
                                                    } else{ ?>
                                                    <input type="checkbox" name="cod_vcard[]" checked value="<?php echo $row['cod_vcard']; ?>" >
                                                    <?php echo $row['cod_vcard']; 
                                                } ?>
                                                </li>
                                                <?php 
                                            }
											?>
                                            </ul>
                                        <input type="hidden" name="cantidad_desc_activa" value="<?php echo $total_client; ?>">
                                        <input type="hidden" name="rfc_empresa" value="<?php echo $rfc_empresa; ?>">
                                        <input type="hidden" name="cod_vcard_cliente" value="<?php echo $cod_vcard_cliente; ?>">
                                        <input type="hidden" name="migrar_descarga_vcard" value="<?php echo $cod_migrar_vcard; ?>">
									</div>                                        
                                        
                                
                                 <div class="col-md-12 panel-body">
                                     <div class="col-md-12">
                                        <div class="col-md-4"  style="float:right;">
                                                <button class="btn ripple btn-raised btn-success" name="enviar">
                                                    <div>
                                                        <span>Guardar VCard Adjunta</span>
                                                    </div>
                                                </button>
                                            </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                <?php } else{
			    $query_cod_vcard = ("SELECT cod_vcard, rfc_empresa,nombre FROM myclientes WHERE rfc_empresa = '" .$rfc_empresa. "' AND cod_vcard !='".$cod_vcard_cliente."' ");
			    $result_cod_vcard = mysqli_query($con, $query_cod_vcard);

				?>
					<br>
                    <div class="col-md-12">
                        <div class="col-md-12 panel">
                            <div class="col-md-12 panel-heading">
                                <h4 style="text-align: center; color: black;">Asociar VCard a la <strong style="color:crimson;">"Descarga"</strong> <?php echo $cod_vcard_cliente; ?></h4>
                                <br>
                                <p style="text-align:right;">
                                    <a href="busqueda_myclientes.php" title="Volver">
                                        <span class="icon-action-undo" title="Volver">Volver</span>
                                    </a>

                                <button style="float: right; padding: 5px 20px;margin: 0px 0px 5px 10px;" title="Marcar Todos" type="button" class="btn btn-gradient btn-info">
                                    <input type="checkbox" onclick="marcar(this);" /> 
                                </button>
                                </p>

                            </div>
                            
                            <form  method="post" action="recib_asociar_vcard.php">
                                <div class="col-md-12 panel-body">
                                        <div class="col-md-12">

                                            <ul class="lista_vcards">
										    <?php
										    while ($valores = mysqli_fetch_array($result_cod_vcard)) { ?>
                                                <li>
                                                    <input type="checkbox" name="cod_vcard[]" value="<?php echo $valores['cod_vcard']; ?>" >
                                                    <?php echo $valores['cod_vcard']; ?>
                                                </li>
											    
                                                <?php 
                                                  }
											    @mysqli_close($result_cod_vcard);
											    ?>
                                            </ul>
                                    </div>  
										<input type="hidden" name="rfc_empresa" value="<?php echo $rfc_empresa; ?>">
										<input type="hidden" name="cod_vcard_cliente" value="<?php echo $cod_vcard_cliente; ?>">
                                </div>                                        
                                        
                                
                                 <div class="col-md-12 panel-body">
                                     <div class="col-md-12">
                                        <div class="col-md-4"  style="float:right;">
                                                <button class="btn ripple btn-raised btn-success" name="enviar">
                                                    <div>
                                                        <span>Adjuntar VCard</span>
                                                    </div>
                                                </button>
                                            </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                <?php } ?>
                </div>
            </div>

            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse" > 
    <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

    <?php include('js.html'); ?>
            <script  type="text/javascript" src="asset/js/my_js.js"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
		<!-- Initialize the plugin: -->
<script type="text/javascript">
    $(document).ready(function() {
        $('#example-getting-started').multiselect({
          includeSelectAllOption: true,
        });
    });
</script>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>