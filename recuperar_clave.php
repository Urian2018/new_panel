<?php
session_start();
if (isset($_SESSION['user']) != "") {
    header("Location: home.php");
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="VCARD">
        <meta name="author" content="ALEJANDRO TORRES">
        <meta name="keyword" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/png" href="../favicon.png" />
        <title>VCARD</title>
    <?php include('css.html'); ?>
        <style type="text/css" media="screen">
            .hover_a:hover{
                color: black !important;
                transition: linear 0.4s;
                margin-right: 15px;
            }
        </style>
        <script  src="asset/js/jquery.min.js"></script>
    </head>

    <body id="mimin" class="form-signin-wrapper lock-screen-v1">

        <div class="container">
            <form class="form-signin" action="recib_recuperar_clave.php" method="POST" autocomplete="off">
                <div class="panel periodic-login">
                    <div class="panel-body text-center">
                        <h1 class="atomic-symbol">
                            <img src="asset/img/img_vcard.png" class="img-responsive img-circle" />
                        </h1>

                        <div class="form-group form-animate-text">
                            <input type="text" name="cod_vcard" class="form-text" required>
                            <span class="bar"></span>
                            <label>Codigo VCard</label>
                        </div>

                        <div class="form-group form-animate-text">
                            <input type="text" name="email" class="form-text" required>
                            <span class="bar"></span>
                            <label>Email</label>
                        </div>

                        <input style="font-weight: bold; color: #222;" type="submit" class="btn col-md-12" value="Recuperar Clave"/>
                    </div>

                    <p style="text-align: center; padding: 0px 0px 15px 0px;">
                        <a class="hover_a" href="index.php">
                            <span class="fa fa-arrow-left" title="volver"></span>
                            Volver
                        </a>
                    </p>
                </div>
            </form>
        </div>

<?php include('js.html'); ?>
    </body>
</html>