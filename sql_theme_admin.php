<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<script  src="asset/js/jquery.min.js"></script>
<script type="text/javascript">
/************JQUERY QUE CAPTURA EL ID DE LA IMG SELECCIONADA Y LA ENVIA A UN PHP*************/
$(document).ready(function() {
$(".img_fondo_view li img").on("click", function () {
var id = $(this).attr('id');
var dataString = 'id=' + id;

var ruta = "recib_config_img_fondo_admin.php";
$('#tab1').html('<center><img src="img/cargandoo.gif"/><br/>Espere un momento, por favor...</center>');
$.ajax({
    url: ruta,
    type: "POST",
    data: dataString,
    complete:function(data){
      $("#msj_exito_img_fondo").delay(500).fadeIn("slow"); 
      $("#msj_exito_img_fondo").delay(3500).fadeOut("slow");
    },
    success: function(data){
          $("#tab1").html(data); // Mostrar la respuestas del script PHP.
    }
});
return false;
});
});

/********JQUE QUE AGREGA LA CLASE HOVE LA IMG ESTA SELECCIONADA*********/
$(function() {
$(".img_fondo_view img").click(function() {
    $('.img_miniatura_seleccionada').removeClass("img_miniatura_seleccionada").addClass("img_miniatura");
    $(this).addClass("img_miniatura_seleccionada");
  });
});  

/********JQUE QUE AGREGA LA CLASE HOVE AL COLOR QUE ESTA SELECCIONADA*********/
$(function() {
$(".img_fondo_view span").click(function() {
    $('.seleccion').removeClass("seleccion").addClass("noseleccion");
    $(this).addClass("seleccion");
  });
});    


/**********ELIMINAR TEMA ADMINISTRADOR****************/
$(document).ready(function() {
  $('a.fa-times-circle-o').click(function(e) {
      e.preventDefault();
      var id = $(this).attr('id');
      var ruta = $("input#ruta").val();
      var dataString = 'id=' + id + '&ruta=' + ruta;
      var parent = $(this).parent();

      $.ajax({
        type: 'get',
        url: 'delete_thema.php',
        data: dataString,
        beforeSend: function() {
          parent.animate({'backgroundColor':'#fafafa'},300);
        },
        complete:function(data){
        $("#delete_theme").delay(500).fadeIn("slow"); 
        $("#delete_theme").delay(3500).fadeOut("slow");
       },
      success: function(data){
        parent.slideUp(300,function() {
          parent.remove();
        });
      }
    });
  });
  return false;
});
</script>
</head>
<body>
	<div id="capa_tab1_config_admin">
	<?php 
	$tipo = "admin";
	$sql_img_config = ("SELECT id,ruta,estatus FROM configuration WHERE tipo='".$tipo."' ");
	$query_config = mysqli_query($con, $sql_img_config);
	?>

	<ul class="img_fondo_view">
	<?php
	while ($imgs_fondo = mysqli_fetch_array($query_config)) { 
	    $url_imgs_fondo = $imgs_fondo['ruta']; 
	    $estatus        = $imgs_fondo['estatus'];
	    if($estatus !='Activa'){  ?>
	    <li>
	    	<a id="<?php echo $imgs_fondo['id']; ?>" class="fa fa-times-circle-o" title="Eliminar Theme"></a>
	        <img src="<?php echo $url_imgs_fondo; ?>" id="<?php echo $imgs_fondo['id']; ?>"  class="img_miniatura" title="Activar Theme"/>
	    </li>
	<?php } else{ ?>
	    <li>
			<a id="<?php echo $imgs_fondo['id']; ?>" class="fa fa-times-circle-o"></a>
	        <img src="<?php echo $url_imgs_fondo; ?>" id="<?php echo $imgs_fondo['id']; ?>" class="img_miniatura_seleccionada"  title="Theme Activo"/>
	    </li>
      <?php } ?>
      <input type="hidden" name="ruta" id="ruta" value="<?php echo $url_imgs_fondo; ?>"><?php } ?>
		</ul>
	</div>
	
</body>
</html>