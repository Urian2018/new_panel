<?php
session_start();
header("Content-Type: text/html;charset=utf-8");
include('config.php');
if (isset($_SESSION['user']) != "") {
$rango_usuario = $_SESSION['rango_users'];
?>

<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="VCARD">
<meta name="author" content="ALEJANDRO TORRES">
<meta name="keyword" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/png" href="../favicon.png" />
<title>VCARD</title>
<?php include('css.html'); ?>
<style type="text/css">

input[type=checkbox] {
position: relative;
cursor: pointer;
}
input[type=checkbox]:before {
content: "";
display: block;
position: absolute;
width: 16px;
height: 16px;
top: 0;
left: 0;
border: 2px solid #555555;
border-radius: 1px;
background-color: white;
}
input[type=checkbox]:checked:after {
content: "";
display: block;
width: 5px;
height: 10px;
border: solid green;
border-width: 0 2px 2px 0;
-webkit-transform: rotate(45deg);
-ms-transform: rotate(45deg);
transform: rotate(45deg);
position: absolute;
top: 2px;
left: 6px;
}
</style>
<link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
<script  src="https://code.jquery.com/jquery-2.2.4.js"></script>
<script type="text/javascript">
$(function () {
$("input[name^=img_vcard]").on("change", function () {
var id = $(this).attr('rel');
var formData = new FormData($("#formulariocero" + id)[0]);
var ruta = "cambiar_img_logo_pg_admin.php";
$.ajax({
url: ruta,
type: "POST",
data: formData,
contentType: false,
processData: false,
success: function (datos)
{
$("#respuesta").html(datos);
}
});
});

});
</script>
</head>

<body id="mimin" class="dashboard">
<?php include('menu_header.php'); ?>

<div class="container-fluid mimin-wrapper">
<?php include('menu_lateral_escritorio.php'); ?>

<div id="capalogos">    
<div id="content">
<br>

<?php
$checkemail = mysqli_query($con, "SELECT * FROM myclientes WHERE cod_vcard='' ORDER BY id");
$check_mail = mysqli_num_rows($checkemail);
if (($check_mail > 0) && ($rango_usuario == 'Administrador')) {
?>
<div class="col-md-12">
<form action="creando_vcard_and_urls.php" id="form" method="post">
<div class="col-md-12 top-20 padding-0">
<div class="col-md-12">
    <div class="panel">
        <div class="panel-heading">
            <h4 style="text-align: center;">
                <?php echo "Total de Clientes sin Codigos VCARD  <strong style='color:green;'>(" . $check_mail . ')</strong>'; ?>
            </h4>
        </div>
        <div class="panel-body">
           <div class="table-responsive">
                <table class="table table-responsive table-bordered table-hover" id="datatables-example" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Codigo VCard</th>
                            <th>Nombre y Apelido</th>
                            <th>Ciudad / Empresa</th>
                            <th>Logo G</th>
                            <th>Logo P</th>
                            <th>Img VCard</th>
                            <th>
                                <button  title="Marcar Todos" type="button" class="btn btn-gradient btn-info">
                                    <input type="checkbox" onclick="marcar(this);" /> 
                                </button>
                                
                                <input type="submit" class="btn btn-gradient btn-primary" title="Crear VCard y URL" value="VCard y URL" style="padding-left: 5px; padding-right:5px; padding: 5px 3px; font-size: 12px;">
                                
                                <input type="button" id="logoP"     class="btn btn-gradient btn-warning" value="Logo P" style="padding: 5px 3px;font-size: 12px;">

                                <input type="button" id="logoG"     class="btn btn-gradient btn-success"    title="Cargar Logos Grandes y Pequeño" value="Logos" style="padding-left: 5px; padding-right:5px; padding: 5px 3px; font-size: 12px;">

                                <input type="button" id="img_vcards" class="btn btn-gradient btn-success"   title="Cargar Imagenes de la VCARD" value="Img Vcard" style="padding: 5px 3px;padding-left: 5px; padding-right:5px; font-size: 12px;">
                                
                                <input type="button" id="grupal"    class="btn btn-gradient btn-success"    title="Cargar Imagenes de Forma Grupal" value="Todo" style="padding: 5px 3px;padding-left: 5px; padding-right:5px; font-size: 12px;">

                                <input type="button" id="eliminarRegis"  class="btn btn-gradient btn-success"    title="Eliminar Registro" value="Eliminar" style="padding: 5px 3px;padding-left: 5px; padding-right:5px; font-size: 12px;">

                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        while ($vcard = mysqli_fetch_array($checkemail)) {
                            $id = $vcard['id'];
                            $table = "myclientes";
                            $cod_vcard = $vcard['cod_vcard'];
                            $nombres   = ($vcard['nombre']);
                            $empresa   = ($vcard['empresa']);
                            $ciudad    = ($vcard['ciudad']);
                            $img_vcard = $vcard['img_vcard'];
                            ?>
                            <tr>
                                <td><?php echo $cod_vcard; ?></td>
                                <td><?php echo $nombres; ?></td>
                                <td><?php echo $ciudad . ' / ' . $empresa; ?> </td> 
                            <td><center>
                            <?php
                            if (empty($vcard['logo_grand'])) {
                                echo "<span style='color:red; font-size:13px;'>Sin Imagen</span>";
                            } else {
                            ?>
                                <img src="logos_g/<?php echo $vcard['logo_grand'] ?>" style='width:80px; height: 80px; object-fit: cover; text-align: center;'>
                            </center>
                            </td>
                            <?php } ?>
                            <td style="text-align: center;" id="respuesta"><center>
                            <?php
                            if (empty($vcard['logo_peque'])) {
                                echo "<span style='color:red; font-size:13px;'>Sin Imagen</span>";
                            } else { ?>
                                <img src="logos_p/<?php echo $vcard['logo_peque'] ?>" style='width:80px; height: 80px; object-fit: cover;'>
                            </center></td>
                            <?php } ?>
                            <td>
                            <?php
                            $var = "data:image/jpg/png;base64";
                            $ruta_img_vcard = $var . $img_vcard;
                            if (empty($img_vcard)) {
                                echo "<span style='color:red; font-size:13px;'>Sin Imagen</span>";
                            } else {
                                $img = file_get_contents($ruta_img_vcard);
                                $imdata = base64_encode($img);
                                echo "<img src='data:image/jpg/png;base64," . $imdata . "' style='width:80px;'/>";
                            }
                            ?>
                        </td>
                        <td>
                            <input type="checkbox" name="id[]" value="<?php echo $id; ?>" title="Seleccione" style="margin-top:19px"/>
                                <?php //echo $id; ?>
                            </form>


                            <div id="chekee" style='float:right;'>
                                <form enctype="multipart/form-data" id="formulariocero<?php echo $id; ?>" method="post">
                                    <input  type="hidden" name="id" id="id" value="<?php echo $id; ?>">
                                    <input  type="hidden" name="filename" id="filename">
                                    <label style='float:left;'>Cambiar Imagen</label><br>
                                    <input type="file" id="img_vcard<?php echo $id; ?>" name="img_vcard" accept="image/*" rel="<?php echo $id; ?>" style="color: transparent;">
                                </form>
                            </div>

                        </td>
                        </tr>
                    <?php
                    }
                    @mysqli_close($checkemail);
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>  
</div>

<?php } else {
echo '<h4 style="text-align:center; margin-top:100px;">NO HAY REGISTRO PENDIENTE, TODOS LOS CLIENTES YA TIENEN CODIGO VCARD ASIGNADO.</h4>';                                            
}
?>  
</div>            
</div>
</div>
</div>


<!-- start: Mobile -->
<div id="mimin-mobile" class="reverse" > 
<?php include('menu_movil.php'); ?>
</div>
<button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
<span class="fa fa-bars"></span>
</button>
<!-- end: Mobile -->

<?php include('js.html'); ?>
<script  type="text/javascript" src="asset/js/my_js.js"></script>

</body>
</html>
<?php
} else {
include('error.php');
}
?>