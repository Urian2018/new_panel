-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 16-02-2019 a las 02:39:39
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `new_vcard`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `myclientes`
--

CREATE TABLE IF NOT EXISTS `myclientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_vcard` varchar(255) NOT NULL,
  `correlativo` int(11) DEFAULT NULL,
  `nombre` varchar(255) NOT NULL,
  `rfc` varchar(100) NOT NULL,
  `empresa` varchar(255) CHARACTER SET utf32 NOT NULL,
  `cargo` varchar(255) NOT NULL,
  `logo_peque` text CHARACTER SET latin1 NOT NULL,
  `texto` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `extension` varchar(20) CHARACTER SET latin1 NOT NULL,
  `binario` longblob NOT NULL,
  `pagina_descarga` text NOT NULL,
  `pagina_compartir` text NOT NULL,
  `pagina_galeria` text NOT NULL,
  `redireccionamiento_contacto` text NOT NULL,
  `redireccionamiento_galeria` text NOT NULL,
  `logo_galeria` text NOT NULL,
  `ruta_icono_compartir` text CHARACTER SET latin1 NOT NULL,
  `texto_whatsapp` text NOT NULL,
  `uso_tlf` text CHARACTER SET latin1 NOT NULL,
  `telefono` varchar(255) CHARACTER SET latin1 NOT NULL,
  `uso_tlf_dos` text CHARACTER SET latin1 NOT NULL,
  `tlf_dos` text CHARACTER SET latin1 NOT NULL,
  `uso_tlf_tres` text CHARACTER SET latin1 NOT NULL,
  `tlf_tres` text CHARACTER SET latin1 NOT NULL,
  `uso_tlf_cuatro` text CHARACTER SET latin1 NOT NULL,
  `tlf_cuatro` text CHARACTER SET latin1 NOT NULL,
  `tlf_movil` text CHARACTER SET latin1 NOT NULL,
  `whatsapp` text CHARACTER SET latin1 NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_dos` text CHARACTER SET latin1 NOT NULL,
  `email_tres` text CHARACTER SET latin1 NOT NULL,
  `ciudad` varchar(255) NOT NULL,
  `ciudad_compa` text CHARACTER SET latin1 NOT NULL,
  `direccion` text NOT NULL,
  `cp` text CHARACTER SET latin1 NOT NULL,
  `pais` varchar(255) NOT NULL,
  `web_cliente` text NOT NULL,
  `web_cliente_dos` text NOT NULL,
  `facebook` text CHARACTER SET latin1 NOT NULL,
  `twitter` text CHARACTER SET latin1 NOT NULL,
  `instagram` text CHARACTER SET latin1 NOT NULL,
  `youtube` text NOT NULL,
  `linkedin` text CHARACTER SET latin1 NOT NULL,
  `gogle_mas` text CHARACTER SET utf32 NOT NULL,
  `nota` text NOT NULL,
  `codigo_pais` varchar(100) CHARACTER SET latin1 NOT NULL,
  `tipo_galeria` text CHARACTER SET latin1 NOT NULL,
  `img_vcard` text CHARACTER SET latin1 NOT NULL,
  `extra_uno` text CHARACTER SET latin1 NOT NULL,
  `extra_dos` text CHARACTER SET latin1 NOT NULL,
  `extra_tres` text CHARACTER SET latin1 NOT NULL,
  `extra_cuatro` text CHARACTER SET latin1 NOT NULL,
  `extra_cinco` text CHARACTER SET latin1 NOT NULL,
  `extra_seis` text CHARACTER SET latin1 NOT NULL,
  `cod_ciudad` varchar(100) CHARACTER SET latin1 NOT NULL,
  `tipo_cliente` varchar(100) NOT NULL,
  `rango_imagenes` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
