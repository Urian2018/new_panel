<?php 
sleep(1);
require('config.php');
$tipo		= "admin";
$estatus    = "Inactiva";
$ruta_file  = 'Configuration/themes/';


$fileName = $_FILES['img_fondo']['name'];
$rtOriginal = $_FILES['img_fondo']['tmp_name'];
$fileExtension = substr(strrchr($fileName, '.'), 1);

$png = 'png';
$PNG = 'PNG';


//contar imagen de un directorio
$sql_contar_theme = ("SELECT MAX(id) AS id FROM configuration");
$query_theme = mysqli_query($con, $sql_contar_theme);
$themes = mysqli_fetch_array($query_theme);
$id_theme = $themes['id'];
$new_total_img = $id_theme + 1;


/*$total_imagenes = count(glob('Configuration/themes/{*.jpg,*.gif,*.png,*.jpeg,*.PNG,*.JPG}',GLOB_BRACE));
$new_total_img = $total_imagenes + 1;*/

if ($fileExtension == $png || $fileExtension == $PNG) {
$original = imagecreatefrompng($rtOriginal);

$name_archivo = "theme_".''.$new_total_img;
$img_fondo_png = $name_archivo.$fileExtension;
$ruta_img_completa = $ruta_file .$img_fondo_png;


//Ancho y alto máximo
$max_ancho = 900;
$max_alto = 900;

//Medir la imagen
list($ancho, $alto) = getimagesize($rtOriginal);
//Ratio
$x_ratio = $max_ancho / $ancho;
$y_ratio = $max_alto / $alto;

//Proporciones
if (($ancho <= $max_ancho) && ($alto <= $max_alto)) {
    $ancho_final = $ancho;
    $alto_final = $alto;
} else if (($x_ratio * $alto) < $max_alto) {
    $ancho_final = $max_ancho;
    $alto_final = ceil($x_ratio * $alto);
} else {
    $ancho_final = $max_ancho;
    $alto_final = $max_alto;
}

$lienzo = imagecreatetruecolor($ancho_final, $alto_final);
imagecopyresampled($lienzo, $original, 0, 0, 0, 0, $ancho_final, $alto_final, $ancho, $alto);
imagedestroy($original);
imagepng($lienzo, $ruta_file . $img_fondo_png);

$sql_insert = ("INSERT INTO configuration (ruta, tipo, estatus) VALUES ('$ruta_img_completa','$tipo','$estatus')");
$result_insert = mysqli_query($con, $sql_insert);

/***CONDICON CUANDO LA IMG ES JPG******/
} else {
$original = imagecreatefromjpeg($rtOriginal);

$name_archivo_jpg = "theme_".''.$new_total_img.'.';
$img_fondo_jpg = $name_archivo_jpg.$fileExtension;
$ruta_img_completa_jpg = $ruta_file .$img_fondo_jpg;


$max_ancho = 900;
$max_alto  = 900;

list($ancho, $alto) = getimagesize($rtOriginal);
$x_ratio = $max_ancho / $ancho;
$y_ratio = $max_alto / $alto;
//Proporciones
if (($ancho <= $max_ancho) && ($alto <= $max_alto)) {
    $ancho_final = $ancho;
    $alto_final = $alto;
} else if (($x_ratio * $alto) < $max_alto) {
    $alto_final = ceil($x_ratio * $alto);
    $ancho_final = $max_ancho;
} else {
    $ancho_final = ceil($y_ratio * $ancho);
    $alto_final = $max_alto;
}

//Crear un lienzo
$lienzo = imagecreatetruecolor($ancho_final, $alto_final);
imagecopyresampled($lienzo, $original, 0, 0, 0, 0, $ancho_final, $alto_final, $ancho, $alto);
imagedestroy($original);
imagejpeg($lienzo, $ruta_file . $img_fondo_jpg);

$sql_insert_jpg = ("INSERT INTO configuration (ruta, tipo, estatus) VALUES ('$ruta_img_completa_jpg','$tipo','$estatus')");
$result_insert = mysqli_query($con, $sql_insert_jpg);
}

include('sql_theme_admin.php'); 

?>