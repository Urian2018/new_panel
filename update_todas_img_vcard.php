<?php
session_start();
include('config.php');
if (isset($_SESSION['user'])!="") {
    ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="description" content="VCARD">
        <meta name="author" content="ALEJANDRO TORRES">
        <meta name="keyword" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/png" href="../favicon.png" />
        <title>VCARD</title>
        <?php include('css.html'); ?>
        <script  src="https://code.jquery.com/jquery-2.2.4.js"></script>
        <script  type="text/javascript" src="asset/js/my_js.js"></script>
        <script type="text/javascript">
            function marcar(source)
            {
                checkboxes = document.getElementsByTagName('input'); //obtenemos todos los controles del tipo Input
                for (i = 0; i < checkboxes.length; i++) //recoremos todos los controles
                {
                    if (checkboxes[i].type == "checkbox") //solo si es un checkbox entramos
                    {
                        checkboxes[i].checked = source.checked; //si es un checkbox le damos el valor del checkbox que lo llamó (Marcar/Desmarcar Todos)
                    }
                }
            }
        </script>

       <!--- <script>
            $(function () {
                $("input[name^=img_vcard]").on("change", function () {
                    var id = $(this).attr('rel');
                    var formData = new FormData($("#formulariocero" + id)[0]);
                    var ruta = "img_vcard.php";
                    $.ajax({
                        url: ruta,
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false,
                        success: function (datos)
                        {
                            $("#respuesta").html(datos);
                        }
                    });
                });

            });
        </script>-->
    </head>

    <body id="mimin" class="dashboard">
        <?php include('menu_header.php'); ?>

        <div class="container-fluid mimin-wrapper">
            <?php include('menu_lateral_escritorio.php'); ?>

            <div id="content">
                <?php
                require('config.php');
                if (isset($_POST['enviar'])) {

                    $check = getimagesize($_FILES["img_vcard"]["tmp_name"]);
                    if ($check !== false) {
                        $data = base64_encode(file_get_contents($_FILES["img_vcard"]["tmp_name"]));
                        $img_vcard = "," . $data . "";
                        $q = ("UPDATE myclientes SET img_vcard='" . $img_vcard . "'");
                        $result = mysqli_query($con, $q);
                        if ($result) {
                            $var = "data:image/jpg/png;base64";
                            $ruta_img_vcard = $var . $img_vcard;

                            $img = file_get_contents($ruta_img_vcard);
                            $imdata = base64_encode($img);
                            echo "<center><img src='data:image/jpg/png;base64," . $imdata . "' style='width:200px; object-fit: cover;'/></center>";
                        } else {
                            header('Location: update_todas_img_vcard.php');
                            echo "<h2 style='color:red; text-align:center;'>La Imagen no Fue Registrada, seguramente la imagen pesa mucho.</h2>";
                        }
                    }
                }
                ?>

                <div class="col-md-12">
                    <div class="col-md-6">

                    </div>

                    <div class="col-md-6" style="text-align: right; margin: 0px 0px 5px 0px">
                        <button class="btn ripple btn-raised btn-success" style="width:300px; text-align: center">
                            <div>
                                <span>
                                    <a  style="color:white; " href="vcard.php">Volver Atras</a>
                                </span>
                            </div>
                        </button>
                    </div>
                </div>


                <div class="col-md-12">
                    <div class="col-md-12 panel">
                        <div class="col-md-12 panel-heading">
                            <h4 style="text-align: center;"> Actualizar las Imagens de la VCARD</h4>
                        </div>
                        <form  enctype="multipart/form-data" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                            <div class="col-md-12 panel-body">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="input-group fileupload-v1">
                                            <input type="file" name="img_vcard" id="img_vcard" required="required" class="fileupload-v1-file hidden"  accept="image/*">
                                            <input type="text" class="form-control fileupload-v1-path" placeholder="Imagen . . . ." disabled>
                                            <span class="input-group-btn">
                                                <button class="btn fileupload-v1-btn" type="button"><i class="fa fa-folder"></i> Presione Examinar</button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="col-md-6">
                                            <button class="btn ripple btn-raised btn-success" name="enviar">
                                                <div>
                                                    <span>Actualizar Imagenes</span>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <!-- start: Mobile -->
        <div id="mimin-mobile" class="reverse" > 
            <?php include('menu_movil.php'); ?>
        </div>
        <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
            <span class="fa fa-bars"></span>
        </button>
        <!-- end: Mobile -->

        <?php include('js.html'); ?>
    </body>
</html>
    <?php
} else {
    include('error.php');
}
?>