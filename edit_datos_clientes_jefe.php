<?php
session_start();
include('config.php');
header("Content-Type: text/html;charset=utf-8");
if (isset($_SESSION['user']) != "") {
    $id_user            = $_SESSION['id'];
    $id_cliente_jefe    = $_GET['id'];
    $vc                 = $_GET['cod'];
    ?>
<!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
        </head>

        <body id="mimin" class="dashboard">
            <?php include('menu_header.php'); ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>

                <div id="content">
                    <br><br>

                    <div class="col-md-12">
                        <div class="col-md-12 panel">
                            <div class="col-md-12 panel-heading">
                                <h4 style="text-align: center; color: black;"> Actualizar Datos del 
                                    <strong style="color:crimson;">"CLIENTE"</strong>
                                    <br><br>
                                    <strong style="color:crimson; font-size:20px;"><?php echo $vc; ?></strong>
                                </h4>
                                
                                <h5 style="text-align: right;"><a href="busqueda_myclientes.php">
                                        <span class="icon-action-undo"></span>
                                        Volver
                                    </a>
                                </h5>
                                <br>
                            </div>
                            <?php
                            $sql = ("SELECT * FROM myclientes WHERE id='".$id_cliente_jefe."'");
                            $mostar = mysqli_query($con, $sql);
                              while ($row = mysqli_fetch_array($mostar)) {?>
                            <form  enctype="multipart/form-data" method="post" action="recib_update_datos_cliente_jefes.php">
                                <div class="col-md-12 panel-body">
                                    <div class="col-md-12">

                                        <div class="col-md-12 panel-body">
                                            <div class="col-md-6">
                                                <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                    <input type="text" class="form-text" id="validate_firstname" name="nombre" value="<?php 
                                                    $na = ($row['nombre']);
                                                    if (mb_detect_encoding($na, 'UTF-8', true) =='UTF-8') {
                                                           echo  ($row['nombre']);
                                                        }else {
                                                            echo  utf8_encode($row['nombre']);
                                                        }  ?>"  autocomplete="off">
                                                    <input type="hidden"  name="cod" value="<?php echo $vc; ?>">
                                                    <span class="bar"></span>
                                                    <label>NOMBRE Y APELLIDOS</label>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                    <input type="text" class="form-text" id="validate_firstname" name="empresa" value="<?php 
                                                    $emp = $row['empresa'];
                                                    if (mb_detect_encoding($emp, 'UTF-8', true) =='UTF-8') {
                                                           echo ($row['empresa']);
                                                        }else {
                                                            echo utf8_encode($row['empresa']);
                                                        } ?>"  autocomplete="off">
                                                    <input type="hidden" class="form-text" id="validate_firstname" name="id" value="<?php echo $id_cliente_jefe; ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>EMPRESA</label>
                                                </div>
                                            </div>
                                           
                                    </div>
                                    <div class="col-md-12 panel-body">                                       
                                            <div class="col-md-3">
                                                <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                    <input type="text" class="form-text" id="validate_firstname" name="ciudad" value="<?php 
                                                        $ciudad = ($row['ciudad']);
                                                     if (mb_detect_encoding($ciudad, 'UTF-8', true) =='UTF-8') {
                                                           echo ($row['ciudad']);
                                                        }else {
                                                            echo utf8_encode($row['ciudad']);
                                                        } ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>CIUDAD</label>
                                                </div>
                                            </div>
                                            
                                        <div class="col-md-3">
                                              <label>ESTADO DE LA VCARD</label>
                                                <div class="form-group form-animate-text" style="margin-top:15px !important;">
                                                    <select name="estatus_vcard" id="estatus_vcard" class="form-control">
                                                        <option value=""> Seleccione</option>
                                                        <option value="Activa" <?php if ($row['estatus_vcard']=="Activa") {
                                                            echo 'selected';
                                                        } ?> >Activa</option>
                                                        <option value="Inactiva" <?php if ($row['estatus_vcard']=="Inactiva") {
                                                            echo 'selected';
                                                        } ?> >Inactiva</option>
                                                    </select>
                                                 </div>
                                            </div>
                                            <div class="col-md-3">
                                              <label>ESTADO DE LA M&Eacute;TRICA</label>
                                                <div class="form-group form-animate-text" style="margin-top:15px !important;">
                                                    <select name="estatus_metrica" id="estatus_metrica" class="form-control">
                                                        <option value=""> Seleccione</option>
                                                        <option value="SI" <?php if ($row['estatus_metrica']=="SI") {
                                                            echo 'selected';
                                                        } ?> >SI</option>
                                                        <option value="NO" <?php if ($row['estatus_metrica']=="NO") {
                                                            echo 'selected';
                                                        } ?> >NO</option>
                                                    </select>
                                                 </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                    <input type="text" class="form-text" id="validate_firstname" name="rango_imagenes" value="<?php echo $row['rango_imagenes']; ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>RANGO DE IMAGENES</label>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="col-md-12 panel-body"> 
                                        <div class="col-md-4">
                                                <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                    <input type="text" class="form-text" id="validate_firstname" name="rango_video" value="<?php echo $row['rango_video']; ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>RANGO DE VIDEOS</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                    <input type="text" class="form-text" id="validate_firstname" name="rango_audios" value="<?php echo $row['rango_audios']; ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>RANGO DE AUDIOS</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                    <input type="text" class="form-text" id="validate_firstname" name="rango_arch_descargables" value="<?php echo $row['rango_arch_descargables']; ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>RANGO DE ARCHIVOS DESCARGABLES</label>
                                                </div>
                                            </div>
                                        
                                    </div>
                                        
                                    <div class="col-md-12 panel-body">                                       
                                            <div class="col-md-6">
                                                <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                    <input type="text" class="form-text" id="validate_firstname" name="cargo" value="<?php
                                                    $carg = $row['cargo'];
                                                    if (mb_detect_encoding($carg, 'UTF-8', true) =='UTF-8') {
                                                           echo ($row['cargo']);
                                                        }else {
                                                            echo utf8_encode($row['cargo']);
                                                        } ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>CARGO</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                    <input type="text" class="form-text" id="validate_firstname" name="tlf_movil" value="<?php echo $row['tlf_movil']; ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>TELEFONO MOVIL</label>
                                                </div>
                                            </div>
                                    </div>
                                        
                                    <div class="col-md-12 panel-body">                                       
                                            <div class="col-md-6">
                                                <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                    <input type="text" class="form-text" id="validate_firstname" name="email" value="<?php echo $row['email']; ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>EMAIL</label>
                                                </div>
                                            </div>
                                        <div class="col-md-6">
                                                <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                    <input type="text" class="form-text" id="validate_firstname" name="whatsapp" value="<?php echo $row['whatsapp']; ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>WHATSAPP</label>
                                                </div>
                                            </div>
                                    </div>
                                        
                                    <div class="col-md-12 panel-body">                                       
                                            <div class="col-md-6">
                                                <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                    <input type="text" class="form-text" id="validate_firstname" name="web_cliente" value="<?php echo $row['web_cliente']; ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>WEB DEL CLIENTE</label>
                                                </div>
                                            </div>
                                        
                                            <div class="col-md-6">
                                                <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                    <input type="text" class="form-text" id="validate_firstname" name="direccion" value="<?php
                                                        $addres =  ($row['direccion']); 
                                                    if (mb_detect_encoding($addres, 'UTF-8', true) =='UTF-8') {
                                                           echo ($row['direccion']);
                                                        }else {
                                                            echo utf8_encode($row['direccion']);
                                                        } ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>DIRECCI&Oacute;N</label>
                                                </div>
                                            </div>
                                    </div> 
                                        
                                     <div class="col-md-12 panel-body">   
                                            <div class="col-md-6">
                                                <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                    <input type="text" class="form-text" id="validate_firstname" name="facebook" value="<?php echo $row['facebook']; ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>FACEBOOK</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                    <input type="text" class="form-text" id="validate_firstname" name="twitter" value="<?php echo $row['twitter']; ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>TWITTER</label>
                                                </div>
                                            </div>
                                    </div>
                                        
                                     <div class="col-md-12 panel-body">
                                         <div class="col-md-6">
                                                <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                    <input type="text" class="form-text" id="validate_firstname" name="instagram" value="<?php echo $row['instagram']; ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>INSTAGRAM</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                    <input type="text" class="form-text" id="validate_firstname" name="youtube" value="<?php echo $row['youtube']; ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>YOUTUBE</label>
                                                </div>
                                            </div>
                                            
                                    </div>
                                    <div class="col-md-12 panel-body">  
                                        <div class="col-md-6">
                                                <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                    <input type="text" class="form-text" id="validate_firstname" name="linkedin" value="<?php echo $row['linkedin']; ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>LINKEDIN</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                    <input type="text" class="form-text" id="validate_firstname" name="gogle_mas" value="<?php echo $row['gogle_mas']; ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>GOOGLE +</label>
                                                </div>
                                            </div>
                                    </div>    
                                        
                                </div>
                                </div>
                                
                                 <div class="col-md-12 panel-body">
                                     <div class="col-md-12">
                                        <div class="col-md-6"  style="float:right;">
                                                <button class="btn ripple btn-raised btn-success" name="enviar">
                                                    <div>
                                                        <span>Actualizar Información</span>
                                                    </div>
                                                </button>
                                            </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                            <?php } ?>
                    </div>
                </div>
            </div>


            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse" > 
    <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

    <?php include('js.html'); ?>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>