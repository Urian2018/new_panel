<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
    $id_user = $_SESSION['id'];
    $id_empleado     = $_GET['id'];
    $cod_vcard       = $_GET['cod_vcard'];
    ?>
<!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <style type="text/css" media="screen">
                .icon-action-undo:hover{
                    color: #333;
                }
                h5 a:hover{
                    color: #333;
                }
            </style>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
            <script  src="asset/js/jquery.min.js"></script>
            <script  src="asset/js/funciones_tabs.js"></script>
        </head>

        <body id="mimin" class="dashboard">
            <?php include('menu_header_user.php'); ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>

                <div id="content">
                    <br>
                    <div class="col-md-12">
                        <div class="col-md-12 panel">

                            <div class="col-md-12 panel-heading">
                                <h4 style="text-align: center; color: black;"> 
                                    Actualizar Datos del Cliente  <strong style='color:crimson;font-size: 20px;'>
                                    <?php echo $cod_vcard; ?></strong>
                                </h4>
								
                                <h5 style="text-align: right;">
                                    <a href="mis_empleados.php">
                                        <span class="icon-action-undo"></span>
                                        Volver
                                    </a>
                                </h5>
                            </div>
                            <!--------CAPAS Y MSJS DEL TAB CON EXITO-------->    
                            <?php
                            include('msj_exito.html'); 
                            ?>


                <div class="col-md-12 tabs-area">
                     
                     <ul id="tabs-demo4" class="nav nav-tabs nav-tabs-v3" role="tablist">
                      <li role="presentation" class="active">
                        <a href="#tabs-demo4-area1" id="tabs-demo4-1" role="tab" data-toggle="tab" aria-expanded="true">INFORMACIÓN GENERAL</a>
                      </li>
                      <li role="presentation" class="">
                        <a href="#tabs-demo4-area2" role="tab" id="tabs-demo4-2" data-toggle="tab" aria-expanded="false">TELÉFONOS</a>
                      </li>
                      <li role="presentation">
                        <a href="#tabs-demo4-area3" id="tabs-demo4-3" role="tab" data-toggle="tab" aria-expanded="false">DIRECCIONES</a>
                      </li>
                      <li role="presentation" class="">
                        <a href="#tabs-demo4-area4" role="tab" id="tabs-demo4-4" data-toggle="tab" aria-expanded="false">REDES SOCIALES</a>
                      </li>
                    </ul>

                <div class="col-md-12">
                    <div id="tabsDemo4Content" class="tab-content tab-content-v3">
                
                <!--section 1---->
                <?php
                $tab1 = ("SELECT * FROM myclientes WHERE id='".$id_empleado."' LIMIT 1");
                $mostar_tab1 = mysqli_query($con, $tab1);
                        while ($row = mysqli_fetch_array($mostar_tab1)) { ?>
                    <div role="tabpanel" class="tab-pane fade active in" id="tabs-demo4-area1" aria-labelledby="tabs-demo4-area1">

                        <form   enctype="multipart/form-data"  method="post" id="formulario_tab1">
                            	<p></p>
                            		<div style="border: solid 2px #000000; ">				
                						<h5 style="text-align: center; color: red;"> 
                                            <strong>REFERENCIA</strong>  <strong style='color:crimson;font-size: 16px;'></strong>
                                        </h5>
                						<h6 style="text-align: center; color: black;"> 
                                            - En el campo "Dirección", favor de incluir: Calle, Número y Colonia únicamente<strong style='color:crimson;font-size: 14px;'></strong>
                                        </h6>
            								<p></p>
                						<h6 style="text-align: center; color: black;"> 
                                            - La IMAGEN VCARD a cargar es de máximo <strong>350x350px y 50kb</strong>, si no cuentas con esa medida, utiliza el siguiente servicio EXTERNO para redimensionar: <a href="https://picasion.com/es/resize-image/" target="_blank">CLICK AQUÍ</a>  <strong style='color:crimson;font-size: 14px;'></strong>
                                        </h6>		
                                    </div>
                            	<p></p>      

                                    <div class="col-md-12">
                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                    <input type="text" class="form-text" id="validate_firstname" name="nombre" value="<?php 
                                                    $na = ($row['nombre']);
                                                    if (mb_detect_encoding($na, 'UTF-8', true) =='UTF-8') {
                                                           echo  ($row['nombre']);
                                                        }else {
                                                            echo  utf8_encode($row['nombre']);
                                                        } ?>" autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>NOMBRE COMPLETO</label>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                    <input type="text"   class="form-text" id="validate_firstname" name="cargo" value="<?php 
                                                    $carg = $row['cargo'];
                                                    if (mb_detect_encoding($carg, 'UTF-8', true) =='UTF-8') {
                                                           echo ($row['cargo']);
                                                        }else {
                                                            echo utf8_encode($row['cargo']);
                                                        } ?>"  autocomplete="off">
                                                    <input type="hidden" class="form-text" id="validate_firstname" name="id" value="<?php echo $id_empleado; ?>"  autocomplete="off">
                                                    <input type="hidden" class="form-text" id="validate_firstname" name="cod_vcard" value="<?php echo $cod_vcard; ?>"  autocomplete="off">                                                    
                                                    <span class="bar"></span>
                                                    <label>CARGO</label>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                            <br>
                                            <label>IMAGEN VCARD<em> (Máximo 350x350px. y 50kb.)</em></label>
                                                <div class="input-group fileupload-v1">
                                                    <input type="file" name="img_vcard" id="img_vcard" class="fileupload-v1-file hidden"  accept="image/*">
                                                    <input type="text" class="form-control fileupload-v1-path" placeholder="Imagen Vcard . . ." disabled>
                                                    <span class="input-group-btn">
                                                        <button class="btn fileupload-v1-btn" type="button"><i class="fa fa-folder"></i> Presione Examinar</button>
                                                    </span>
                                                </div>
                                            </div>
                                    </div>                            
                                    <p></p>
                                    <div class="col-md-12">                                       
                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text" id="validate_firstname" name="empresa" value="<?php 
                                                    $emp = $row['empresa'];
                                                    if (mb_detect_encoding($emp, 'UTF-8', true) =='UTF-8') {
                                                           echo ($row['empresa']);
                                                        }else {
                                                            echo utf8_encode($row['empresa']);
                                                        } ?>" autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>EMPRESA</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text" id="validate_firstname" name="direccion" value="<?php  
                                                    $addres =  ($row['direccion']); 
                                                    if (mb_detect_encoding($addres, 'UTF-8', true) =='UTF-8') {
                                                           echo ($row['direccion']);
                                                        }else {
                                                            echo utf8_encode($row['direccion']);
                                                        } ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>DIRECCIÓN</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text" id="validate_firstname" name="ciudad" value="<?php
                                                     $ciudad = ($row['ciudad']);
                                                     if (mb_detect_encoding($ciudad, 'UTF-8', true) =='UTF-8') {
                                                           echo ($row['ciudad']);
                                                        }else {
                                                            echo utf8_encode($row['ciudad']);
                                                        } ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>CIUDAD</label>
                                                </div>
                                            </div>
                                    </div>

                                    <div class="col-md-12">                                       
                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text" id="validate_firstname" name="pais" value="<?php 
                                                    $country = $row['pais'];
                                                    if (mb_detect_encoding($country, 'UTF-8', true) =='UTF-8') {
                                                           echo ($row['pais']);
                                                        }else {
                                                            echo utf8_encode($row['pais']);
                                                        } ?>" autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>PAIS</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text" id="validate_firstname" name="cp" value="<?php echo $row['cp']; ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>CODIGO POSTAL</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text" id="validate_firstname" name="email" value="<?php echo $row['email']; ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>EMAIL</label>
                                                </div>
                                            </div>
                                    </div>

                                    <div class="col-md-12">  
                                     <div class="col-md-4">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text" id="validate_firstname" name="email_dos" value="<?php echo $row['email_dos']; ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>EMAIL 2</label>
                                                </div>
                                        </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text" id="validate_firstname" name="email_tres" value="<?php echo $row['email_tres']; ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>EMAIL 3</label>
                                                </div>
                                            </div>

                                             <div class="col-md-4">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text" id="validate_firstname" name="web_cliente" value="<?php echo $row['web_cliente']; ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>WEB 1<em> (URL completa)</em></label>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                    <div class="col-md-12">                                     
                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text" id="validate_firstname" name="web_cliente_dos" value="<?php echo $row['web_cliente_dos']; ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>WEB 2<em> (URL completa)</em></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-animate-text">
                                                    <input type="text" class="form-text" id="validate_firstname" name="nota" value="<?php
                                                    $not = $row['nota']; 
                                                    if (mb_detect_encoding($not, 'UTF-8', true) =='UTF-8') {
                                                           echo  ($row['nota']);
                                                        }else {
                                                            echo  utf8_encode($row['nota']);
                                                        } ?>"  autocomplete="off">
                                                    <span class="bar"></span>
                                                    <label>Notas o Palabras Clave</label>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-4"  style="float:right; margin-top:10px !important;">
                                                 <input type="submit" id="botonenviar_tab1" name="botonenviar_tab1"  class="btn ripple btn-raised btn-success" value="Actualizar Información">
                                            </div>

                                    </div>
                        </form>

                      </div>
                  <?php } ?>



                <?php
                $tab2 = ("SELECT * FROM myclientes WHERE id='".$id_empleado."' LIMIT 1");
                $mostar_tab2 = mysqli_query($con, $tab2);
                        while ($row_tab2 = mysqli_fetch_array($mostar_tab2)) { 
                            $cod_pais = $row_tab2['codigo_pais']; ?>
                    <!--section 2---->
                      <div role="tabpanel" class="tab-pane fade" id="tabs-demo4-area2" aria-labelledby="tabs-demo4-area2">
                        
                        <form method="post" id="formulario_tab2">
                        	<p></p>
                        		<div style="border: solid 2px #000000; ">				
            						<h5 style="text-align: center; color: red;"> 
                                        <strong>REFERENCIA</strong>  <strong style='color:crimson;font-size: 16px;'></strong>
                                    </h5>
            						
            						<h6 style="text-align: center; color: black;"> 
                                        - El "+52" es mostrado por defecto como código país en México, pero puede ser cambiado  <strong style='color:crimson;font-size: 14px;'></strong>
                                    </h6>
        							<p></p>
            						<h6 style="text-align: center; color: black;"> 
                                        - Ingresa sólo números a 10 dígitos  <strong style='color:crimson;font-size: 14px;'></strong>
                                    </h6>		
                                </div>
                        		<p></p>
                                <div class="col-md-12">  
                                    <div class="col-md-3">
                                        <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                            <input type="hidden" class="form-text" id="validate_firstname" name="id" value="<?php echo $id_empleado; ?>"  autocomplete="off">
                                            <input type="text" class="form-text" id="validate_firstname" name="tlf_movil" maxlength="16" value="<?php
                                            $cant = strlen($row_tab2['tlf_movil']);
                                            $tlf_movil = $row_tab2['tlf_movil']; 
                                            if($cant > 6){
                                                echo $tlf_movil; 
                                                }elseif($tlf_movil ==''){
                                                    echo $cod_pais . ' 1 ';
                                                    } else{
                                                    
                                                } ?>"   autocomplete="off">
                                            <span class="bar"></span>
                                            <label>MOVIL<em> (10 Dígitos)</em></label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                            <input type="text" class="form-text" id="validate_firstname" maxlength="14" name="telefono" value="<?php 
                                            $tlf = $row_tab2['telefono'];
                                            $cant = strlen($tlf);
                                            if($cant == '10'){
                                                echo  $cod_pais.' '.$tlf; 
                                                }elseif($cant > 10 ){
                                                echo  $tlf;
                                                }elseif($cant ==''){
                                                echo  $cod_pais;
                                                } else{
                                                echo  $tlf;
                                                } ?> "  autocomplete="off">
                                            <span class="bar"></span>
                                            <label>TEL 1<em> (10 Dígitos)</em></label>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group form-animate-text">
                                         <p style="margin-top:55px !important;">  // Ext. </p>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                            <input type="text" class="form-text" id="validate_firstname" name="ext_telefono" maxlength="5" value="<?php echo $row_tab2['ext_telefono']; ?>"  autocomplete="off">
                                            <span class="bar"></span>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                            <input type="text" class="form-text" id="validate_firstname" name="tlf_dos" maxlength="14" value="<?php
                                            $tlf2 = $row_tab2['tlf_dos'];
                                            $cant = strlen($tlf2);
                                            if($cant == '10'){
                                                echo  $cod_pais.' '. $tlf2; 
                                                }elseif($cant > 10 ){
                                                echo  $tlf2;
                                                }elseif($cant ==''){
                                                echo  $cod_pais;
                                                } else{
                                                echo  $tlf2;
                                                } ?>"  autocomplete="off">
                                            <span class="bar"></span>
                                            <label>TEL 2<em> (10 Dígitos)</em></label>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group form-animate-text">
                                         <p style="margin-top:55px !important;">  // Ext. </p>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                            <input type="text" class="form-text" id="validate_firstname" name="ext_tlf_dos" maxlength="5" value="<?php echo $row_tab2['ext_tlf_dos']; ?>"  autocomplete="off">
                                            <span class="bar"></span>
                                        </div>
                                    </div>

                                </div>
                                        
                                <div class="col-md-12"> 
                                        <div class="col-md-3">
                                            <div class="form-group form-animate-text">
                                                <input type="text" class="form-text" id="validate_firstname" name="tlf_tres" maxlength="14" value="<?php 
                                            $tlf3 = $row_tab2['tlf_tres'];
                                            $cant = strlen($tlf3);
                                            if($cant == '10'){
                                                echo  $cod_pais.' '.$tlf3; 
                                                }elseif($cant > 10 ){
                                                echo  $tlf3;
                                                }elseif($cant ==''){
                                                echo  $cod_pais;
                                                } else{
                                                echo  $tlf3;
                                                } ?>"  autocomplete="off">
                                                <span class="bar"></span>
                                                <label>TEL 3<em> (10 Dígitos)</em></label>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                        <div class="form-group form-animate-text">
                                         <p style="margin-top:30px !important;">  // Ext. </p>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group form-animate-text">
                                            <input type="text" class="form-text" id="validate_firstname" name="ext_tlf_tres" maxlength="5" value="<?php echo $row_tab2['ext_tlf_tres']; ?>"  autocomplete="off">
                                            <span class="bar"></span>
                                        </div>
                                    </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-animate-text">
                                                <input type="text" class="form-text" id="validate_firstname" name="tlf_cuatro" maxlength="14" value="<?php 
                                            $tlf4 = $row_tab2['tlf_cuatro'];
                                            $cant = strlen($tlf4);
                                            if($cant == '10'){
                                                echo  $cod_pais.' '.$tlf4; 
                                                }elseif($cant > 10 ){
                                                echo  $tlf4;
                                                }elseif($cant ==''){
                                                echo  $cod_pais;
                                                } else{
                                                echo  $tlf4;
                                                } ?>"  autocomplete="off">
                                                <span class="bar"></span>
                                                <label>TEL 4<em> (10 Dígitos)</em></label>
                                            </div>
                                        </div> 
                                        <div class="col-md-1">
                                        <div class="form-group form-animate-text">
                                         <p style="margin-top:30px !important;">  // Ext. </p>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group form-animate-text">
                                            <input type="text" class="form-text" id="validate_firstname" name="ext_tlf_cuatro" maxlength="5" value="<?php echo $row_tab2['ext_tlf_cuatro']; ?>"  autocomplete="off">
                                            <span class="bar"></span>
                                        </div>
                                    </div>   
                                        <div class="col-md-2">
                                            <div class="form-group form-animate-text">
                                                <input type="hidden" class="form-text" name="codigo_pais" value="<?php echo  $cod_pais; ?>"  autocomplete="off">
                                                <input type="text" class="form-text" id="validate_firstname" maxlength="16" name="whatsapp" value="<?php 
                                            $tlf5 = $row_tab2['whatsapp'];
                                            $cant = strlen($tlf5);
                                            if($cant == '10'){
                                                echo  $cod_pais.' 1 '.$tlf5; 
                                                }elseif($cant == 15 ){
                                                echo  '+'. $tlf5;
                                                }elseif($cant > 10 ){
                                                echo  $tlf5;
                                                }elseif($cant ==''){
                                                echo $cod_pais . ' 1 ';
                                                } else{
                                                echo  $tlf5;
                                                } ?>"  autocomplete="off">
                                                <span class="bar"></span>
                                                <label>WHATSAPP</label>
                                            </div>
                                        </div>
                                    </div>

                                <div class="col-md-12" style="padding: 0px 0px 30px 0px;"> 
                                    <div class="col-md-4" style="float:right;">
                                        <input type="submit" id="botonenviar_tab2" name="botonenviar_tab2" class="btn ripple btn-raised btn-success" value="Actualizar Información">
                                    </div>
                                </div>
                             

                        </form>
                      </div>
                      <br>
                        <?php } ?>



                <?php
                $tab3 = ("SELECT * FROM myclientes WHERE id='".$id_empleado."' LIMIT 1");
                $mostar_tab3 = mysqli_query($con, $tab3);
                        while ($row_tab3 = mysqli_fetch_array($mostar_tab3)) { ?>
                        <!--section 3---->
                      <div role="tabpanel" class="tab-pane fade" id="tabs-demo4-area3" aria-labelledby="tabs-demo4-area3">
                        
                             <form method="post" id="formulario_tab3">  
                                <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                <input type="hidden" class="form-text" id="validate_firstname" name="id" value="<?php echo $id_empleado; ?>"  autocomplete="off">
                                                <input type="text" class="form-text" id="validate_firstname" name="direccion_dos" value="<?php 
                                                $dir2 =  $row_tab3['direccion_dos'];
                                                if (mb_detect_encoding($dir2, 'UTF-8', true) =='UTF-8') {
                                                           echo ($row_tab3['direccion_dos']);
                                                        }else {
                                                            echo utf8_encode($row_tab3['direccion_dos']);
                                                        } ?>"  autocomplete="off">
                                                <span class="bar"></span>
                                                <label>DIRECCIÓN 2 <em>(Calle y #, Colonia, Delegación, C.P.)</em></label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                <input type="text" class="form-text" id="validate_firstname" name="direccion_tres" value="<?php 
                                                $dir3 = $row_tab3['direccion_tres'];
                                                    if (mb_detect_encoding($dir3, 'UTF-8', true) =='UTF-8') {
                                                           echo ($row_tab3['direccion_tres']);
                                                        }else {
                                                            echo utf8_encode($row_tab3['direccion_tres']);
                                                        } ?>"  autocomplete="off">
                                                <span class="bar"></span>
                                                <label>DIRECCIÓN 3 <em>(Calle y #, Colonia, Delegación, C.P.)</em></label>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                <input type="text" class="form-text" id="validate_firstname" name="direccion_cuatro" value="<?php 
                                                $dir4 = $row_tab3['direccion_cuatro']; 
                                                    if (mb_detect_encoding($dir4, 'UTF-8', true) =='UTF-8') {
                                                           echo ($row_tab3['direccion_cuatro']);
                                                        }else {
                                                            echo utf8_encode($row_tab3['direccion_cuatro']);
                                                        } ?>"  autocomplete="off">
                                                <span class="bar"></span>
                                                <label>DIRECCIÓN 4 <em>(Calle y #, Colonia, Delegación, C.P.)</em></label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-animate-text" style="margin-top:25px !important;">
                                                <input type="text" class="form-text" id="validate_firstname" name="direccion_cinco" value="<?php 
                                                $dir5 = $row_tab3['direccion_cinco'];
                                                    if (mb_detect_encoding($dir5, 'UTF-8', true) =='UTF-8') {
                                                           echo ($row_tab3['direccion_cinco']);
                                                        }else {
                                                            echo utf8_encode($row_tab3['direccion_cinco']);
                                                        } ?>"  autocomplete="off">
                                                <span class="bar"></span>
                                                <label>DIRECCIÓN 5 <em>(Calle y #, Colonia, Delegación, C.P.)</em></label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="col-md-4"  style="float:right; margin-top:35px !important;">
                                        <div class="form-group form-animate-text">
                                            <input type="submit" id="botonenviar_tab3" name="botonenviar_tab3" class="btn ripple btn-raised btn-success" value="Actualizar Información">
                                        </div>
                                    </div>
                                    </div>

                                </form>


                      </div>
                      <br>
                       <?php } ?>



                    
                <?php
                $tab4 = ("SELECT * FROM myclientes WHERE id='".$id_empleado."' LIMIT 1");
                $mostar_tab4 = mysqli_query($con, $tab4);
                        while ($row_tab4 = mysqli_fetch_array($mostar_tab4)) {
                        $http = "http://";  ?>
                        <!--section 4---->
                    
                      <div role="tabpanel" class="tab-pane fade" id="tabs-demo4-area4" aria-labelledby="tabs-demo4-area4">
                      
                       <form method="post" id="formulario_tab4">					   
                        		<p></p>	
                        		<div style="border: solid 2px #000000; ">				
                        						<h5 style="text-align: center; color: red;"> 
                                                            <strong>REFERENCIA</strong>  <strong style='color:crimson;font-size: 16px;'></strong>
                                                        </h5>
                        						
                        						<h6 style="text-align: center; color: black;"> 
                                                           - Favor de ingresar URL´s completas  <strong style='color:crimson;font-size: 14px;'></strong>
                                                        </h6>
                        								
                                </div>
                        		<p></p>					   
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <div class="form-group form-animate-text" style="margin-top:29px !important;">
                                            <input type="hidden" class="form-text" id="validate_firstname" name="id" value="<?php echo $id_empleado; ?>"  autocomplete="off">
                                            <input style="font-size: 14px" type="text" class="form-text" id="validate_firstname" name="facebook" value="<?php
                                            $face = substr($row_tab4['facebook'], 0, 11);
                                            $face_www = substr($row_tab4['facebook'], 0, 4);
                                            //http://www. = 11 == $face
                                            //http://www.facebook/urian/065tggd..  == $facebook
                                            $facebook = $row_tab4['facebook'];
                                            if($face == 'http://www.'){
                                                echo $facebook;
                                            }elseif($face == 'https://www.'){
                                                echo $facebook;
                                            }elseif($face_www =='www.'){
                                            //www.facebook.com/urian/05tggd..
                                            $ruta_face = $http.$facebook;
                                            //http://www.facebook/urian/065tggd.. == $ruta_face
                                            $facek_update = ("UPDATE myclientes SET facebook='" . $ruta_face . "'  WHERE id='".$id_empleado."'");
                                            $result_face = mysqli_query($con, $facek_update);
                                            echo $ruta_face;
                                            } else{
                                            echo $facebook;
                                            } ?>"  autocomplete="off">
                                            <span class="bar"></span>
                                            <label>FACEBOOK<em> (URL completa)</em></label>
                                        </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-animate-text" style="margin-top:29px !important;">
                                                <input style="font-size: 14px" type="text" class="form-text" id="validate_firstname" name="twitter" value="<?php 
                                                $twi = substr($row_tab4['twitter'], 0, 11); 
                                                $twitter_www = substr($row_tab4['twitter'], 0, 4);
                                                $twitter  = $row_tab4['twitter'];
                                                if($twi == 'http://www.'){
                                                    echo $twitter;
                                                }elseif($twi == 'https://www.'){
                                                    echo $twitter;
                                                }elseif($twitter_www =='www.'){
                                                $ruta_twitter = $http.$twitter;
                                                $twitter_update = ("UPDATE myclientes SET twitter='" . $ruta_twitter . "'  WHERE id='".$id_empleado."'");
                                                $result_twitter = mysqli_query($con, $twitter_update);
                                                echo $ruta_twitter;
                                                }else{
                                                echo $twitter;
                                                } ?>"  autocomplete="off">
                                                <span class="bar"></span>
                                                <label>TWITTER<em> (URL completa)</em></label>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-3">
                                            <div class="form-group form-animate-text" style="margin-top:29px !important;">
                                                <input style="font-size: 14px" type="text" class="form-text" id="validate_firstname" name="instagram" value="<?php
                                                $inst = substr($row_tab4['instagram'], 0, 11); 
                                                $instagram_www = substr($row_tab4['instagram'], 0, 4);
                                                $instagram = $row_tab4['instagram'];
                                                if($inst == 'http://www.'){
                                                    echo $instagram;
                                                }elseif($inst == 'https://www.'){
                                                    echo $instagram;
                                                }elseif($instagram_www =='wwww.'){
                                                $ruta_instagram = $http.$instagram;
                                                $instagram_update = ("UPDATE myclientes SET instagram='" . $ruta_instagram . "'  WHERE id='".$id_empleado."'");
                                                $result_instagram = mysqli_query($con, $instagram_update);
                                                echo $ruta_instagram;
                                                }else{
                                                echo $instagram;
                                                } ?>"  autocomplete="off">
                                                <span class="bar"></span>
                                                <label>INSTAGRAM<em> (URL completa)</em></label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-animate-text" style="margin-top:29px !important;">
                                                <input style="font-size: 14px" type="text" class="form-text" id="validate_firstname" name="youtube" value="<?php 
                                                $yout = substr($row_tab4['youtube'], 0, 11); 
                                                $youtube_www = substr($row_tab4['youtube'], 0, 4);
                                                $youtube = $row_tab4['youtube'];
                                                if($yout == 'http://www.'){
                                                    echo $youtube;
                                                }elseif($yout == 'https://www.'){
                                                    echo $youtube;
                                                }elseif($youtube_www =='www.'){
                                                $ruta_youtube = $http.$youtube;
                                                $youtube_update = ("UPDATE myclientes SET youtube='" . $ruta_youtube . "'  WHERE id='".$id_empleado."'");
                                                $result_youtube = mysqli_query($con, $youtube_update);
                                                echo $ruta_youtube;
                                                } else{
                                                echo $youtube;
                                                }  ?>"  autocomplete="off">
                                                <span class="bar"></span>
                                                <label>YOUTUBE<em> (URL completa)</em></label>
                                            </div>
                                        </div>

                                </div>
                                        
                                <div class="col-md-12">
                                        <div class="col-md-4">
                                            <div class="form-group form-animate-text">
                                                <input style="font-size: 14px" type="text" class="form-text" id="validate_firstname" name="linkedin" value="<?php 
                                                $linke = substr($row_tab4['linkedin'], 0, 11); 
                                                $linke_www = substr($row_tab4['linkedin'], 0, 4);
                                                $linkedin = $row_tab4['linkedin'];
                                                if($linke == 'http://www.'){
                                                echo $linkedin;
                                                }elseif($linke == 'https://www.'){
                                                echo $linkedin;
                                                }elseif($linke_www =='www.'){
                                                $ruta_linkedin = $http.$linkedin;
                                                $linkedin_update = ("UPDATE myclientes SET linkedin='" . $ruta_linkedin . "'  WHERE id='".$id_empleado."'");
                                                $result_linkedin = mysqli_query($con, $linkedin_update);
                                                echo $ruta_linkedin;
                                                } else{
                                                echo $linkedin;
                                                } ?>"  autocomplete="off">
                                                <span class="bar"></span>
                                                <label>LINKEDIN<em> (URL completa)</em></label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group form-animate-text">
                                                <input  style="font-size: 14px" type="text" class="form-text" id="validate_firstname" name="gogle_mas" value="<?php 
                                                //OJO GOOGLE + YA NO EXISTE
                                                $goog = substr($row_tab4['gogle_mas'], 0, 11);
                                                $google_www = substr($row_tab4['gogle_mas'], 0, 4); 
                                                $google = $row_tab4['gogle_mas'];
                                                if($goog == 'http://www.'){
                                                echo $google;
                                                }elseif($goog == 'https://www.'){
                                                echo $google;
                                                }elseif($google_www =='www.'){
                                                $ruta_gogle_mas = $http.$google;
                                                $google_update = ("UPDATE myclientes SET gogle_mas='" . $ruta_gogle_mas . "'  WHERE id='".$id_empleado."'");
                                                $result_google = mysqli_query($con, $google_update);
                                                echo $ruta_gogle_mas;
                                                } else{
                                                echo $google;
                                                } ?>"  autocomplete="off">
                                                <span class="bar"></span>
                                                <label>OTRA<em> (URL completa)</em></label>
                                            </div>
                                        </div>
                                         <div class="col-md-4" style="float:right; margin-top:5px !important;">
                                            <div class="form-group form-animate-text">
                                                <input type="submit" id="botonenviar_tab4" name="botonenviar_tab4" class="btn ripple btn-raised btn-success" value="Actualizar Información">
                                              </div>
                                          </div>
                                        
                                    </div>
                                        
                                </form>

                                <!------------>
                                <br>
                               
                                <!--------->

                      </div>
                    <?php } ?>



                    </div>
                  </div>   

                </div>



                    </div>
                </div>
            </div>


        <!-- start: Mobile -->
        <div id="mimin-mobile" class="reverse" > 
            <?php include('menu_movil.php'); ?>
        </div>
        <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
            <span class="fa fa-bars"></span>
        </button>
        <!-- end: Mobile -->

        <?php include('js.html'); ?>

        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>