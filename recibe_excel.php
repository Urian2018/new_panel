<?php
require('config.php');
include('functions.php');
$tipo    = $_FILES['archivo']['type'];
$tamanio = $_FILES['archivo']['size'];
$archivotmp = $_FILES['archivo']['tmp_name'];
$lineas  = file($archivotmp);

ini_set('max_execution_time','1000'); 

$i = 0;
//IMPORTANTISIMO EL FOREACH DEBE IR ASI Y NO ASI $lineas as $linea_num => $linea PORQUE SINO DEJA DE FUNCIONAR
foreach ($lineas as $linea) {
    $cantidad_registros = count($lineas);
    $cantidad_regist_agregados =  ($cantidad_registros - 1);

    if ($i != 0) {

        //print_r($linea); 
        $datos = explode(";", $linea);
        
        //usamos la función utf8_encode para leer correctamente los caracteres especiales
       
        $cod_vcard                      = !empty($datos[0])  ? ($datos[0]) : '';
        $cod_distribuidor               = !empty($datos[1])  ? ($datos[1]) : '';
        $origenCliente                  = !empty($datos[2])  ? ($datos[2]) : '';
        $StandCliente                   = !empty($datos[3])  ? ($datos[3]) : '';
        $estatus_vcard                  = !empty($datos[4])  ? ($datos[4]) : ''; 
        $estatus_metrica                = !empty($datos[5])  ? ($datos[5]) : ''; 
        $nombre                         = utf8_encode($datos[6]); 
        $rfc                            = !empty($datos[7])  ? ($datos[7]) : ''; 
        $empresa                        = utf8_encode($datos[8]);
        $rfc_empresa                    = !empty($datos[9])  ? ($datos[9]) : ''; 
        $ciudad                         = utf8_encode($datos[10]);
        $cod_ciudad                     = !empty($datos[11]) ? ($datos[11]) : ''; 
        $rango_imagenes                 = !empty($datos[12]) ? ($datos[12]) : ''; 
        $rango_video                    = !empty($datos[13]) ? ($datos[13]) : ''; 
        $rango_audios                   = !empty($datos[14]) ? ($datos[14]) : ''; 
        $rango_arch_descargables        = !empty($datos[15]) ? ($datos[15]) : ''; 
        $cargo                          = utf8_encode($datos[16]); 
        $texto                          = utf8_encode($datos[17]); 
        $pagina_descarga                = !empty($datos[18]) ? ($datos[18]) : ''; 
        $pagina_compartir               = !empty($datos[19]) ? ($datos[19]) : ''; 
        $pagina_galeria                 = !empty($datos[20]) ? ($datos[20]) : ''; 
        $redireccionamiento_contacto    = !empty($datos[21]) ? ($datos[21]) : ''; 
        $redireccionamiento_galeria     = !empty($datos[22]) ? ($datos[22]) : ''; 
        $logo_galeria                   = !empty($datos[23]) ? ($datos[23]) : ''; 
        $ruta_icono_compartir           = !empty($datos[24]) ? ($datos[24]) : ''; 
        $texto_whatsapp                 = utf8_encode($datos[25]);
        $uso_tlf                        = !empty($datos[26]) ? ($datos[26]) : ''; 
        $telefono                       = !empty($datos[27]) ? ($datos[27]) : ''; 
        $ext_telefono                   = !empty($datos[28]) ? ($datos[28]) : ''; 
        $uso_tlf_dos                    = !empty($datos[29]) ? ($datos[29]) : ''; 
        $tlf_dos                        = !empty($datos[30]) ? ($datos[30]) : ''; 
        $ext_tlf_dos                    = !empty($datos[31]) ? ($datos[31]) : ''; 
        $uso_tlf_tres                   = !empty($datos[32]) ? ($datos[32]) : ''; 
        $tlf_tres                       = !empty($datos[33]) ? ($datos[33]) : ''; 
        $ext_tlf_tres                   = !empty($datos[34]) ? ($datos[34]) : ''; 
        $uso_tlf_cuatro                 = !empty($datos[35]) ? ($datos[35]) : ''; 
        $tlf_cuatro                     = !empty($datos[36]) ? ($datos[36]) : ''; 
        $ext_tlf_cuatro                 = !empty($datos[37]) ? ($datos[37]) : ''; 
        $tlf_movil                      = !empty($datos[38]) ? ($datos[38]) : ''; 
        $whatsapp                       = !empty($datos[39]) ? ($datos[39]) : ''; 
        $email                          = !empty($datos[40]) ? ($datos[40]) : ''; 
        $email_dos                      = !empty($datos[41]) ? ($datos[41]) : ''; 
        $email_tres                     = !empty($datos[42]) ? ($datos[42]) : ''; 
        $ciudad_compa                   = !empty($datos[43]) ? ($datos[43]) : '';
        $direccion                      = utf8_encode($datos[44]);
        $dir_l2                         = !empty($datos[45]) ? ($datos[45]) : ''; 
        $dir_l3                         = !empty($datos[46]) ? ($datos[46]) : ''; 
        $cp                             = !empty($datos[47]) ? ($datos[47]) : ''; 
        $pais                           = utf8_encode($datos[48]); 
        $web_cliente                    = !empty($datos[49]) ? ($datos[49]) : ''; 
        $web_cliente_dos                = !empty($datos[50]) ? ($datos[50]) : ''; 
        $facebook                       = !empty($datos[51]) ? ($datos[51]) : ''; 
        $twitter                        = !empty($datos[52]) ? ($datos[52]) : ''; 
        $instagram                      = !empty($datos[53]) ? ($datos[53]) : ''; 
        $youtube                        = !empty($datos[54]) ? ($datos[54]) : ''; 
        $linkedin                       = !empty($datos[55]) ? ($datos[55]) : ''; 
        $google_mas                     = !empty($datos[56]) ? ($datos[56]) : '';
        $nota                           = utf8_encode($datos[57]); 
        $codigo_pais                    = !empty($datos[58]) ? ($datos[58]) : ''; 
        $tipo_galeria                   = !empty($datos[59]) ? ($datos[59]) : ''; 
        $extra_uno                      = !empty($datos[60]) ? ($datos[60]) : ''; 
        $extra_dos                      = !empty($datos[61]) ? ($datos[61]) : ''; 
        $extra_tres                     = !empty($datos[62]) ? ($datos[62]) : ''; 
        $extra_cuatro                   = !empty($datos[63]) ? ($datos[63]) : ''; 
        $extra_cinco                    = !empty($datos[64]) ? ($datos[64]) : ''; 
        $extra_seis                     = !empty($datos[65]) ? ($datos[65]) : ''; 
        $url_imagen                     = !empty($datos[66]) ? ($datos[66]) : ''; 

        if( !empty($cod_vcard) ){
            $checkemail_duplicidad = ("SELECT rfc, cod_ciudad, rfc_empresa FROM myclientes WHERE cod_vcard='".trim($cod_vcard)."' ");
            $ca_dupli = mysqli_query($con, $checkemail_duplicidad);
            $cant_duplicidad = mysqli_num_rows($ca_dupli);
        }else{
            $cant_duplicidad = 0;
        }   

        if ( $cant_duplicidad == 0 ) { 

            $insertar = "INSERT INTO myclientes( 
            cod_vcard,
            cod_distribuidor,
            origenCliente,
            StandCliente,
            estatus_vcard,
            estatus_metrica,
            nombre,
            rfc,
            empresa,
            rfc_empresa,
            ciudad,
            cod_ciudad,
            rango_imagenes,
            rango_video,
            rango_audios,
            rango_arch_descargables,
            cargo,
            texto,
            pagina_descarga,
            pagina_compartir,
            pagina_galeria,
            redireccionamiento_contacto,
            redireccionamiento_galeria,
            logo_galeria,
            ruta_icono_compartir,
            texto_whatsapp,
            uso_tlf,
            telefono,
            ext_telefono,
            uso_tlf_dos,
            tlf_dos,
            ext_tlf_dos,
            uso_tlf_tres,
            tlf_tres,
            ext_tlf_tres,
            uso_tlf_cuatro,
            tlf_cuatro,
            ext_tlf_cuatro,
            tlf_movil,
            whatsapp,
            email,
            email_dos,
            email_tres,
            ciudad_compa,
            direccion,
            dir_l2,
            dir_l3,
            cp,
            pais,
            web_cliente,
            web_cliente_dos,
            facebook,
            twitter,
            instagram,
            youtube,
            linkedin,
            gogle_mas,
            nota,
            codigo_pais,
            tipo_galeria,
            extra_uno,
            extra_dos,
            extra_tres,
            extra_cuatro,
            extra_cinco,
            extra_seis
        ) VALUES(
            '$cod_vcard',
            '$cod_distribuidor',
            '$origenCliente',
            '$StandCliente',
            '$estatus_vcard',
            '$estatus_metrica',
            '$nombre',
            '$rfc',
            '$empresa',
            '$rfc_empresa',
            '$ciudad',
            '$cod_ciudad',
            '$rango_imagenes',
            '$rango_video',
            '$rango_audios',
            '$rango_arch_descargables',
            '$cargo',
            '$texto',
            '$pagina_descarga',
            '$pagina_compartir',
            '$pagina_galeria',
            '$redireccionamiento_contacto',
            '$redireccionamiento_galeria',
            '$logo_galeria',
            '$ruta_icono_compartir',
            '$texto_whatsapp',
            '$uso_tlf',
            '$telefono',
            '$ext_telefono',
            '$uso_tlf_dos',
            '$tlf_dos',
            '$ext_tlf_dos',
            '$uso_tlf_tres',
            '$tlf_tres',
            '$ext_tlf_tres',
            '$uso_tlf_cuatro',
            '$tlf_cuatro',
            '$ext_tlf_cuatro',
            '$tlf_movil',
            '$whatsapp',
            '$email',
            '$email_dos',
            '$email_tres',
            '$ciudad_compa',
            '$direccion',
            '$dir_l2',
            '$dir_l3',
            '$cp',
            '$pais',
            '$web_cliente',
            '$web_cliente_dos',
            '$facebook',
            '$twitter',
            '$instagram',
            '$youtube',
            '$linkedin',
            '$google_mas',
            '$nota',
            '$codigo_pais',
            '$tipo_galeria',
            '$extra_uno',
            '$extra_dos',
            '$extra_tres',
            '$extra_cuatro',
            '$extra_cinco',
            '$extra_seis'
        )";

            $result = mysqli_query($con, $insertar);


        
            if($result && !empty($url_imagen) ){

                $r = mysqli_query($con, "select id, cod_distribuidor, origenCliente from myclientes order by id desc limit 1");
                $row = mysqli_fetch_assoc($r);           
                $remote_image_url = trim($url_imagen);
                $type = get_type($remote_image_url);
                
                if(empty($type)){
                    $url = explode('.', $remote_image_url);
                    $type = $url[count($url -1)];
                }

                $logo = $row['id'].'_'.$row['cod_distribuidor'].'_'.$row['origenCliente'].'.'.$type;
                
                upload_image( $remote_image_url, $logo , 400, 400, 'logos_g' );
                upload_image( $remote_image_url, $logo , 300, 300, 'logos_p' );
                
                $update =  "UPDATE myclientes SET logo_grand = '" . $logo . "', logo_peque = '" . $logo . "' where id = ". $row['id'];
                $res = mysqli_query($con, $update);
            }
                
        } else{

                $update =  ("UPDATE myclientes SET nombre='" .$nombre. "',"
                        . " estatus_vcard='" .$estatus_vcard. "',"
                        . " estatus_metrica='" .$estatus_metrica. "',"
                        . " empresa='" .$empresa. "',"
                        . " ciudad='" .$ciudad. "',"
                        . " cod_ciudad='" .$cod_ciudad. "',"
                        . " cod_distribuidor='" .$cod_distribuidor. "',"
                        . " origenCliente='" .$origenCliente. "',"  
                        . " StandCliente='" .$StandCliente. "',"
                        . " rango_imagenes='" .$rango_imagenes. "',"
                        . " rango_video='" .$rango_video. "',"
                        . " rango_audios='" .$rango_audios. "',"
                        . " rango_arch_descargables='" .$rango_arch_descargables. "',"
                        . " cargo='" .$cargo. "',"
                        . " texto='" .$texto. "',"
                        . " pagina_descarga='" .$pagina_descarga. "',"
                        . " pagina_compartir='" .$pagina_compartir. "',"
                        . " pagina_galeria='" .$pagina_galeria. "',"
                        . " redireccionamiento_contacto='" .$redireccionamiento_contacto. "',"
                        . " redireccionamiento_galeria='" .$redireccionamiento_galeria. "',"                        
                        . " logo_galeria='" .$logo_galeria. "',"
                        . " ruta_icono_compartir='" .$ruta_icono_compartir. "',"
                        . " texto_whatsapp='" .$texto_whatsapp. "',"
                        . " uso_tlf='" .$uso_tlf. "',"
                        . " telefono='" .$telefono. "',"
                        . " ext_telefono='" .$ext_telefono. "',"
                        . " uso_tlf_dos='" .$uso_tlf_dos. "',"
                        . " tlf_dos='" .$tlf_dos. "',"
                        . " ext_tlf_dos='" .$ext_tlf_dos. "',"
                        . " uso_tlf_tres='" .$uso_tlf_tres. "',"
                        . " tlf_tres='" .$tlf_tres. "',"
                        . " ext_tlf_tres='" .$ext_tlf_tres. "',"
                        . " uso_tlf_cuatro='" .$uso_tlf_cuatro. "',"
                        . " tlf_cuatro='" .$tlf_cuatro. "',"
                        . " ext_tlf_cuatro='" .$ext_tlf_cuatro. "',"
                        . " tlf_movil='" .$tlf_movil. "',"
                        . " whatsapp='" .$whatsapp. "',"
                        . " email='" .$email. "',"
                        . " email_dos='" .$email_dos. "',"
                        . " email_tres='" .$email_tres. "',"
                        . " ciudad_compa='" .$ciudad_compa. "',"
                        . " direccion='" .$direccion. "',"
                        . " dir_l2='" .$dir_l2. "',"
                        . " dir_l3='" .$dir_l3. "',"
                        . " cp='" .$cp. "',"
                        . " pais='" .$pais. "',"
                        . " web_cliente='" .$web_cliente. "', "
                        . " web_cliente_dos='" .$web_cliente_dos. "',"
                        . " facebook='" .$facebook. "', "
                        . " twitter='" .$twitter. "',"
                        . " instagram='" .$instagram. "',"
                        . " youtube='" .$youtube. "',"
                        . " linkedin='" .$linkedin. "', "
                        . " gogle_mas='" .$google_mas. "', "
                        . " nota='" .$nota. "', "
                        . " codigo_pais='" .$codigo_pais. "',"
                        . " tipo_galeria='" .$tipo_galeria. "',"
                        . " extra_uno='" .$extra_uno. "',"
                        . " extra_dos='" .$extra_dos. "',"
                        . " extra_tres='" .$extra_tres. "',"
                        . " extra_cuatro='" .$extra_cuatro. "',"
                        . " extra_cinco='" .$extra_cinco. "',"
                        . " extra_seis='" .$extra_seis. "'  WHERE cod_vcard='".$cod_vcard."'");
                $result_update = mysqli_query($con, $update);
        } 
    }
    $i++;
}

//FECHA ACTUAL
date_default_timezone_set("America/Mexico_City");
$hora = date('h:i A', time() - 3600 * date('I'));
$fecha = date("d/m/Y"); 
$result_fecha = $fecha. " : ". $hora;

 /*********SQL CONSULTAR ULTIMO REGISTROo*************/
$consul =  ("SELECT MAX(id) AS id_cliente FROM myclientes ");
$mostar = mysqli_query($con, $consul);
if ($row_mostar = mysqli_fetch_row($mostar)) {
    $ultimo_id_suario =  ($row_mostar[0]);
}
$id_anterior =  ($ultimo_id_suario - $cantidad_regist_agregados );

//INSERTANDO HISTORIAL DE REGISTRO
$insert = "INSERT INTO historial_registros(desde,hasta,total_subidos,total_registros,fecha) VALUES ('" .$id_anterior . "', '" .$ultimo_id_suario. "', '" .$cantidad_regist_agregados. "', '" .$ultimo_id_suario. "', '" .$result_fecha. "')";
$resultado = mysqli_query($con, $insert);


header('Location: vcard.php');

?>