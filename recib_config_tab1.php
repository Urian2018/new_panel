<?php 
sleep(1);
require('config.php');
$cod_vcard     		= $_POST['cod_vcard'];
$rfc_empresa      = $_POST['rfc_empresa'];
$tipo				      = "cliente";
$estatus          = "Activa";
$ruta_file ='Configuration/themes/';


/**VERIFICANDO SI EL CLIENTE YA TIENE IMG DE FONDO PERSONALIZADA**/
$sql_verificar = ("SELECT cod_vcard FROM configuration WHERE cod_vcard='".$cod_vcard."' AND rfc_empresa='".$rfc_empresa ."' ");
$number  = mysqli_query($con, $sql_verificar);
$array   = mysqli_fetch_array($number);
$dato_bd = $array['cod_vcard'];


$fileName = $_FILES['img_fondo']['name'];
$rtOriginal = $_FILES['img_fondo']['tmp_name'];
$fileExtension = substr(strrchr($fileName, '.'), 1);

$png = 'png';
$PNG = 'PNG';

if ($fileExtension == $png || $fileExtension == $PNG) {
$original = imagecreatefrompng($rtOriginal);
$img_fondo_png = $cod_vcard .'.' . $fileExtension;
$ruta_img_completa = $ruta_file .$img_fondo_png;

//Ancho y alto máximo
$max_ancho = 900;
$max_alto = 900;

//Medir la imagen
list($ancho, $alto) = getimagesize($rtOriginal);
//Ratio
$x_ratio = $max_ancho / $ancho;
$y_ratio = $max_alto / $alto;

//Proporciones
if (($ancho <= $max_ancho) && ($alto <= $max_alto)) {
    $ancho_final = $ancho;
    $alto_final = $alto;
} else if (($x_ratio * $alto) < $max_alto) {
    $ancho_final = $max_ancho;
    $alto_final = ceil($x_ratio * $alto);
} else {
    $ancho_final = $max_ancho;
    $alto_final = $max_alto;
}

$lienzo = imagecreatetruecolor($ancho_final, $alto_final);
imagecopyresampled($lienzo, $original, 0, 0, 0, 0, $ancho_final, $alto_final, $ancho, $alto);
imagedestroy($original);
imagepng($lienzo, $ruta_file . $img_fondo_png);

if($dato_bd ==""){
  	$sql_insert = ("INSERT INTO configuration (ruta, cod_vcard, rfc_empresa, tipo, estatus) VALUES ('$ruta_img_completa','$cod_vcard','$rfc_empresa','$tipo','$estatus')");
    $result_insert = mysqli_query($con, $sql_insert);
  } else{
	$update_img_fondo = ("UPDATE configuration SET ruta='" . $ruta_img_completa . "', estatus='" . $estatus . "' WHERE cod_vcard='".$cod_vcard."' ");
	$result_fondo = mysqli_query($con, $update_img_fondo);
}


/***CONDICON CUANDO LA IMG ES JPG******/
} else {
$original = imagecreatefromjpeg($rtOriginal);
$img_fondo_jpg = $cod_vcard . '.' . $fileExtension;
$ruta_img_completa_jpg = $ruta_file .$img_fondo_jpg;

$max_ancho = 900;
$max_alto  = 900;

list($ancho, $alto) = getimagesize($rtOriginal);
$x_ratio = $max_ancho / $ancho;
$y_ratio = $max_alto / $alto;
//Proporciones
if (($ancho <= $max_ancho) && ($alto <= $max_alto)) {
    $ancho_final = $ancho;
    $alto_final = $alto;
} else if (($x_ratio * $alto) < $max_alto) {
    $alto_final = ceil($x_ratio * $alto);
    $ancho_final = $max_ancho;
} else {
    $ancho_final = ceil($y_ratio * $ancho);
    $alto_final = $max_alto;
}

//Crear un lienzo
$lienzo = imagecreatetruecolor($ancho_final, $alto_final);
imagecopyresampled($lienzo, $original, 0, 0, 0, 0, $ancho_final, $alto_final, $ancho, $alto);
imagedestroy($original);
imagejpeg($lienzo, $ruta_file . $img_fondo_jpg);

if($dato_bd ==""){
  	$sql_insert_jpg = ("INSERT INTO configuration (ruta, cod_vcard, rfc_empresa, tipo, estatus) VALUES ('$ruta_img_completa_jpg','$cod_vcard','$rfc_empresa','$tipo','$estatus')");
    $result_insert = mysqli_query($con, $sql_insert_jpg);
  } else{
	$update_img_fondo_jpg = ("UPDATE configuration SET ruta='" . $ruta_img_completa_jpg . "', estatus='" . $estatus . "' WHERE cod_vcard='".$cod_vcard."' AND rfc_empresa='".$rfc_empresa."' ");
	$result_fondo = mysqli_query($con, $update_img_fondo_jpg);
	}
}

$msj = "exito";
header("location:configuration.php?msj=".$msj);
exit();
?>