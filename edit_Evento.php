<?php
session_start();
include('config.php');
date_default_timezone_set("America/Mexico_City");
$fecha = date("d/m/Y"); 
if (isset($_SESSION['user']) != "") {    
$idEvento    = $_GET['id'];
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="VCARD">
<meta name="author" content="ALEJANDRO TORRES">
<meta name="keyword" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/png" href="../favicon.png" />
<title>VCARD</title>
<?php include('css.html'); ?>
<link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
<!----js para mostrar msj--->
<script  src="asset/js/jquery.min.js"></script>
</head>

<body id="mimin" class="dashboard">
<?php 
include('menu_header.php');
?>

<div class="container-fluid mimin-wrapper">
<?php include('menu_lateral_escritorio.php'); ?>

<div id="content">
<br>

<div class="col-md-12">
<div class="col-md-12 panel">
<div class="col-md-12 panel-heading">
  <h4 style="text-align: center; color: black;">
   Actualzar Datos del <strong style="color:crimson;">"EVENTO"</strong>
   <a href="add_evento.php"  style=" float: right; font-size: 13px;" title="Volver"><span class="icon-action-undo"></span>Volver</a>
 </h4>


  <br>
</div>
<?php
$sqlEvento = ("SELECT * FROM eventos WHERE id='".$idEvento."' LIMIT 1");
$mostarEvento = mysqli_query($con, $sqlEvento);
while ($row = mysqli_fetch_array($mostarEvento)) { ?>

<form action="recib_editEvento.php" method="post" enctype="multipart/form-data">
  <div class="col-md-12 panel-body">
      <div class="col-md-12">
          <div class="col-md-6">
              <label>NOMBRE DEL EVENTO</label>
                  <div class="form-group form-animate-text">
                    <input type="text" class="form-text" name="nameEvent" value="<?php echo $row['nameEvent']; ?>" autocomplete="off">
                    <input type="hidden" class="form-text" name="codExpot" value="<?php echo $row['codExpot']; ?>">
                    <span class="bar"></span>
                  </div>
          </div>
          
          <div class="col-md-2">
              <label>FECHA DE ALTA</label>
                <div class="form-group form-animate-text">
                    <input type="text" class="form-text" name="fecha_alta" value="<?php echo $row['fecha_alta']; ?>">
                    <span class="bar"></span>
                </div>
          </div>
          <div class="col-md-2">
              <label>FECHA INICIO</label>
                  <div class="form-group form-animate-text">
                      <input type="text" class="form-text" name="fechaInicio" value="<?php echo $row['fechaInicio']; ?>">
                      <span class="bar"></span>
                  </div>
          </div>
          <div class="col-md-2">
              <label>FECHA FIN</label>
                  <div class="form-group form-animate-text">
                      <input type="text" class="form-text" name="fecha_fin" value="<?php echo $row['fecha_fin']; ?>">
                      <span class="bar"></span>
                  </div>
          </div>

      </div>
      <div class="col-md-12">
          <div class="col-md-6">
              <label>DIRECCIÓN DEL EVENTO</label>
                  <div class="form-group form-animate-text">
                      <input type="text" class="form-text" name="direccionEvent" value="<?php echo $row['direccionEvent']; ?>" autocomplete="off">
                      <span class="bar"></span>
                  </div>
          </div>
          <div class="col-md-6">
              <label>DIRECCIÓN CON GOOGLE MAPS</label>
                  <div class="form-group form-animate-text">
                      <input type="text" class="form-text" name="gpsEvent" value="<?php echo $row['gpsEvent']; ?>" autocomplete="off">
                      <span class="bar"></span>
                  </div>
          </div>
        </div>
        <div class="col-md-12">
       <div class="col-md-6">
            <label>PAGINA WEB DEL EVENTO</label>
                <div class="form-group form-animate-text">
                    <input type="text" class="form-text" name="webEvento" value="<?php echo $row['webEvento']; ?>"  autocomplete="off" placeholder="www.eventoejemplo.mx">
                    <span class="bar"></span>
                </div>
        </div>
      <div class="col-md-6">
        <label>LOGO DEL EVENTO</label>
        <div class="form-group form-animate-text">
          <div class="input-group fileupload-v1">
            <input type="file" name="logoEvento" class="fileupload-v1-file hidden"  accept="image/*">
            <input type="text" class="form-control fileupload-v1-path" placeholder="Imagen . . . ." disabled>
            <span class="input-group-btn">
                <button class="btn fileupload-v1-btn" type="button"><i class="fa fa-folder"></i> Presione Examinar</button>
            </span>
        </div>
        </div>
    </div>
      </div>

        <div class="col-md-12">
          <div class="col-md-12">
            <p style="text-align: center; color: #2196F3;">DATOS DEL JEFE DE LA EXPO</p>
               <hr>
            </div>
        </div>
        
        <div class="col-md-12">
          <div class="col-md-4">
              <label>NOMBRE</label>
                  <div class="form-group form-animate-text">
                      <input type="text" class="form-text" name="name" value="<?php echo $row['name']; ?>" autocomplete="off">
                      <span class="bar"></span>
                  </div>
          </div>
          
          <div class="col-md-4">
              <label>APELLIDO PATERNO</label>
                <div class="form-group form-animate-text">
                    <input type="text" class="form-text" name="apePaterno" value="<?php echo $row['apePaterno']; ?>">
                    <span class="bar"></span>
                </div>
          </div>
          <div class="col-md-4">
              <label>APELLIDO MATERNO</label>
                <div class="form-group form-animate-text">
                    <input type="text" class="form-text" name="apeMater" value="<?php echo $row['apeMater']; ?>">
                    <span class="bar"></span>
                </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="col-md-4">
              <label>NOMBRE DE LA EMPRESA</label>
                  <div class="form-group form-animate-text">
                      <input type="text" class="form-text" name="empresa" value="<?php echo $row['empresa']; ?>" autocomplete="off">
                      <span class="bar"></span>
                  </div>
          </div>
          
          <div class="col-md-4">
              <label>CARGO</label>
                <div class="form-group form-animate-text">
                    <input type="text" class="form-text" name="cargo" value="<?php echo $row['cargo']; ?>">
                    <span class="bar"></span>
                </div>
          </div>
          <div class="col-md-4">
              <label>E-MAIL</label>
                <div class="form-group form-animate-text">
                    <input type="text" class="form-text" name="email" value="<?php echo $row['email']; ?>">
                    <span class="bar"></span>
                </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="col-md-4">
              <label>TELEFONO FIJO</label>
                  <div class="form-group form-animate-text">
                      <input type="text" class="form-text" name="tlf_fijo" value="<?php echo $row['tlf_fijo']; ?>" autocomplete="off">
                      <span class="bar"></span>
                  </div>
          </div>
          
          <div class="col-md-4">
              <label>TELEFONO MOVIL</label>
                <div class="form-group form-animate-text">
                    <input type="text" class="form-text" name="tlf_movil" value="<?php echo $row['tlf_movil']; ?>">
                    <span class="bar"></span>
                </div>
          </div>
          <input type="hidden" name="id" value="<?php echo $idEvento; ?>">
          <div class="col-md-4">
               <div class="form-group form-animate-text">
                  <input style="margin-top:30px !important; float: right; padding: 10px 80px; font-size: 14px;" type="submit" id="crearplan" name="crearplan" class="btn btn-primary btn-sm crear" value="Guardar Modificaciones">
               </div>
          </div>
        </div>

  </div>
  
</form>
<?php } ?>
</div>
</div>
</div>


</div> 
</div>


<!-- start: Mobile -->
<div id="mimin-mobile" class="reverse" > 
<?php include('menu_movil.php'); ?>
</div>
<button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
<span class="fa fa-bars"></span>
</button>
<!-- end: Mobile -->

<?php include('js.html'); ?>

</body>
</html>
<?php
} else {
include('error.php');
}
?>