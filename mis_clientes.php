<?php
session_start();
header("Content-Type: text/html;charset=utf-8");
include('config.php');
if (isset($_SESSION['user']) != "") {
    ?>
    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <style>
                .icon-pencil:hover{
                    color: black;
                }
                .fa-paste:hover{
                    color: black;
                }
                thead tr th{
                    font-size: 10px;
                }
                a img:hover {
                filter: sepia(60%);
                }
            </style>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
            <script  src="https://code.jquery.com/jquery-2.2.4.js"></script>

<script type="text/javascript">
//FUNCION CHEXBOX SELECCION MULTIPLE
function marcar(source)
{
    checkboxes = document.getElementsByTagName('input'); //obtenemos todos los controles del tipo Input
    for (i = 0; i < checkboxes.length; i++) //recoremos todos los controles
    {
        if (checkboxes[i].type == "checkbox") //solo si es un checkbox entramos
        {
            checkboxes[i].checked = source.checked; //si es un checkbox le damos el valor del checkbox que lo llamó (Marcar/Desmarcar Todos)
        }
    }
}

//CREAR VCARD Y URLS CON RESPECTO A LOS ID SELECCIONADOS.
$(function () {
    /*$("input[name^=img_vcard]").on("change", function () {
        var id = $(this).attr('rel');
        var formData = new FormData($("#formulariocero" + id)[0]);
        var ruta = "img_vcard.php";
        $.ajax({
            url: ruta,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function (datos)
            {
                $("#respuesta").html(datos);
            }
        });
    });*/

//CREAR LOGOS DE LA EMPRESA CON RESPECTO A LOS ID SELECCIONADOS
    $("#logos").on("click", function () {
        var id = $(this).attr('rel');
        var formData = new FormData($("#form")[0]);
        var ruta = "add_logo_admin_ajax.php";
        $.ajax({
            url: ruta,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function (datos)
            {
                $("#content").html(datos);
            }
        });
    });
    
//CREAR IMG VCARD DE LA EMPRESA CON RESPECTO A LOS ID SELECCIONADOS
    $("#img_vcards").on("click", function () {
        var id = $(this).attr('rel');
        var formData = new FormData($("#form")[0]);
        var ruta = "add_vcard_img_seleccionadas_ajax.php";
        $.ajax({
            url: ruta,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function (datos)
            {
                $("#content").html(datos);
            }
        });
    });

});
</script>
        </head>

        <body id="mimin" class="dashboard">
            <?php include('menu_header.php'); ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>

                <div id="content">
                    <br>
                    <?php
                    $sql = ("SELECT * FROM myclientes ");
                   if($mostar = mysqli_query($con, $sql)){
                    $total_client = mysqli_num_rows($mostar) ;
                        ?>
                        <div class="col-md-12 top-20 padding-0">
                            <div class="col-md-12">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h4 style="text-align: center;">
                                            <?php echo " TODOS MIS CLIENTES PENDIENTES(" .$total_client. ')</strong>'; ?> 
                                        </h4>
                                    </div>
                                    <div class="panel-body">
                                        <div class="responsive-table">
                                        <form enctype="multipart/form-data" id="form" method="post">
                                            <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                                                <thead>
                                                    <tr style="color: #fff; background-color: #337ab7; border-color: #337ab7;">
                                                    <th>Codigo VCard</th>
                                                    <th>Nombre y Apelido</th>
                                                    <th>Ciudad / Empresa</th>
                                                    <!--<th>Tipo Cliente</th>-->
                                                    <th>Logo Empresa</th>
                                                    <th>Img VCard</th>
                                                    <th>VCARD</th>
                                                    <th>
                                                        <button  title="Marcar Todos" type="button" class="btn btn-gradient btn-info">
                                                            <input type="checkbox" onclick="marcar(this);" /> 
                                                        </button>

                                                        <input type="button" id="logos" class="btn btn-gradient btn-success" title="Cargar Logo" value="Logo" style="padding-left: 5px; padding-right:5px;">

                                                        <input type="button" id="img_vcards" class="btn btn-gradient btn-success" title="Cargar Imagenes de la VCARD" value="img Vcard" style="padding-left: 5px; padding-right:5px;">
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    while ($row = mysqli_fetch_array($mostar)) {
                                                        $id = $row['id'];
                                                        $cod_vcard = $row['cod_vcard'];
                                                        $nombres   = utf8_encode($row['nombre']);
                                                        $empresa   = utf8_encode($row['empresa']);
                                                        $rfc = $row['rfc'];
                                                        $ciudad    = utf8_encode($row['ciudad']);
                                                        $img_vcard = $row['img_vcard'];
                                                        $logo_g = $row['logo_grand'];
                                                       ?>
                                                        <tr>
                        <td id="td_center"><?php echo $cod_vcard; ?></td>
                        <td id="td_center"><?php echo $nombres; ?></td>
                        <td id="td_center"><?php echo $ciudad .' / '. $empresa; ?> </td> 
                        <!--<td id="td_center"><?php //if($rfc !=""){ echo "Jefe";} ?></td>--->
                        <td id="td_center">
                            <?php if(empty($row['logo_grand'])){
                                echo "<p style='color:red;'>Sin Logo</p>";
                            } else { ?> <!---download="Logo"-->
                             <a href="download_logo_grande.php?name=<?php echo $row['logo_grand']; ?>" download="Logo" title="Descargar Logo">
                                <img src="logos_g/<?php echo $row['logo_grand']; ?>" style='width:70px; object-fit: cover; text-align: center;'>
                            </a>
                           <?php } ?>
                        </td>
                        <td <?php //echo $t;   ?> id="respuesta">
                            <center><?php
                            $var = "data:image/jpg/png;base64";
                            $ruta_img_vcard = $var . $img_vcard;
                            if (empty($img_vcard)) {
                                echo "<span style='color:red; font-size:13px;'>Sin imagen</span>";
                            } else {
                                $img = file_get_contents($ruta_img_vcard);
                                $imdata = base64_encode($img);
                                echo "<img src='data:image/jpg/png;base64," . $imdata . "' style='width:40px;'/>";
                            }
                            ?></center>
                        </td>
                        <td style="text-align: center;">
                            <a href="descargar_vcard.php?cod_vcard=<?php echo $cod_vcard; ?>&nombre=<?php echo $nombres;?>">
                                <span class="fa fa-download" title="Descargar VCARD"></span>
                            </a>
                        </td>
                        <td>
                        <input type="checkbox" name="id[]" value="<?php echo $id; ?>" title="Seleccione para Crear Pagina"/>
                        </form>
                        </td>
                    </tr>
                         <?php
                        }
                        ?>
                                                  
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div>
                        <?php
                    }
                    @mysqli_close($mostar);
                    ?>  
                </div>            
            </div>


            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse" > 
                <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

            <?php include('js.html'); ?>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#datatables-example').DataTable();
                });
            </script>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>
