<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Mi Panel</title>
    <style type="text/css" media="screen">
    @import 'https://fonts.googleapis.com/css?family=Open+Sans';
    *{
    margin: 0;
    padding: 0;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

body{
    font-family: 'Open Sans', sans-serif;
    background: #fff;
}

/****/
    input {
     width: 250px;
     padding: 5px;
   }
   .msj{
    color: green;
    font-size: 14px;
    text-align: center;
   }
    .boton_2{
        text-decoration: none;
        padding: 15px;
        font-family: arial;
        text-transform: uppercase;
        font-size: 14px;
        color: black;
        border: none;
        background-color: #DB2828;
      }
      .boton_2:hover{
        cursor: pointer;
        color: white;
        background-color: #9b0e0e;
        text-decoration: none;
      }
.contenedor {
    width: 100%;
        background-color: grey;
        display: flex;
        align-items: center;
}
.contenido {
        background-color: yellow;
        margin: 0 auto; /* requerido para alineación horizontal */
        padding: 5px;
}
h3{
    text-align: center;
}
</style>

    <script src="jquery-3.1.1.min.js"></script>
    <script src="main.js"></script>
</head>
<body>

            <?php
            if (isset($_POST['btn_envio'])) {
                $exito  = '';
                $error   = '';
                foreach ($_FILES["archivo"]['tmp_name'] as $key => $tmp_name) {
                    //Validamos que el archivo exista
                    if ($_FILES["archivo"]["name"][$key]) {
                        $filename = $_FILES["archivo"]["name"][$key]; //Obtenemos el nombre original del archivo
                        $source = $_FILES["archivo"]["tmp_name"][$key]; //Obtenemos un nombre temporal del archivo
                        $directorio = $_POST["carpeta"];
                        //Validamos si la ruta de destino existe, en caso de no existir la creamos
                        if (!file_exists($directorio)) {
                            $base_dir = __DIR__;
                            $directorio = $base_dir;
                           // mkdir($directorio, 0777) or die("No se puede crear el directorio de extracci&oacute;n");
                        }

                        $dir=opendir($directorio); //Abrimos el directorio de destino
                        $target_path = $directorio.'/'.$filename; //Indicamos la ruta de destino, así como el nombre del archivo

                        //Movemos y validamos que el archivo se haya cargado correctamente
                        //El primer campo es el origen y el segundo el destino
                        if (move_uploaded_file($source, $target_path)) {
                            echo "El archivo $filename se ha almacenado en forma exitosa.<br>";
                            $exito = "El Archivo Fue Subido con Exito en la Ruta <span style='color:#DB2828; font-size:26px;'>" .$directorio ."</span>";
                        } else {
                            $error = "Error en el Registro . .";
                        }
                            closedir($dir); //Cerramos el directorio de destino
                    }
                }
            }
            ?>
         <?php
         //echo $server_url = $_SERVER["SERVER_NAME"]; 
        //echo $doc_root = $_SERVER["DOCUMENT_ROOT"]; 
       //echo define('ROOTPATH', __DIR__);
      //echo $base_dir = __DIR__;
         
/*
 * $base_dir = __DIR__;
// server protocol
$protocol = empty($_SERVER['HTTPS']) ? 'http' : 'https';
// domain name
$domain = $_SERVER['SERVER_NAME'];
// base url
$base_url = preg_replace("!^${doc_root}!", '', $base_dir);
// server port
$port = $_SERVER['SERVER_PORT'];
$disp_port = ($protocol == 'http' && $port == 80 || $protocol == 'https' && $port == 443) ? '' : ":$port";
// put em all together to get the complete base URL
$url = "${protocol}://${domain}${disp_port}${base_url}";
echo $url;
*/
        ?>
<div class="contenedor">
    <div class="contenido">
        <h5>Para guardar cualquier archivo en carpeta se debe escribir la carpeta en el input, el nombre de la carpeta nada mas</h5>
<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data">
  <b>Elejir el Archivo</b>
    <input type="file" class="form-control" id="archivo[]" name="archivo[]" multiple=""><br />
   <label>Nombre de la Carpeta </label>
   <input type="text" name="carpeta">
   <input type="submit" name="btn_envio" value="Enviar" class="boton_2" />
</form>
</div>
</div>
</body>
</html>