<?php
session_start();
header("Content-Type: text/html;charset=utf-8");
include('config.php');

//require_once ("config.php");

$action = 'ajax';

if ($action == 'ajax') {
    $q = mysqli_real_escape_string($con, (strip_tags($_REQUEST['q'], ENT_QUOTES)));
    $sTable = "myclientes";
    $sWhere = "";
    $sWhere.="WHERE id<=2000";
    if ($_GET['q'] != "") {
        $sWhere.= " AND nombre like '%$q%' OR cod_distribuidor like '%$q%' OR empresa like '%$q%' OR cod_vcard like '%$q%' OR rfc_empresa like '%$q%' ";
    }


    $sWhere.=" ORDER BY cod_vcard ASC";
    include 'pagination.php'; //include pagination file
    //pagination variables

    $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
    $per_page = 10; //how much records you want to show
    $adjacents = 10; //gap between pages after number of adjacents
    $offset = ($page - 1) * $per_page;
    //Count the total number of row in your table*/
    $count_query = mysqli_query($con, "SELECT count(*) AS numrows FROM $sTable  $sWhere");
    $row = mysqli_fetch_array($count_query);

    $numrows = $row['numrows'];

    $total_pages = ceil($numrows / $per_page);

    $reload = 'busqueda_myclientes.php';

    //main query to fetch the data

    $sql = "SELECT * FROM  $sTable $sWhere LIMIT $offset,$per_page";

    $query = mysqli_query($con, $sql);

    //loop through fetched data

    if ($numrows > 0) {

        echo mysqli_error($con);

        ?>

        <div class="panel-body">   

            <div class="table-responsive">

              <table class="table table-responsive table-bordered table-hover" id="datatables-example" width="100%" cellspacing="0">

                    <thead>

                        <tr style="color: #fff; background-color: #337ab7; border-color: #337ab7;">
                            <th>Codigo VCard</th>
                            <th>Nombre y Apelido</th>
                            <th>Cliente</th>
                            <th>Ciudad / Empresa</th>
                            <th>Estatus VCard</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                <?php
                while ($vcard = mysqli_fetch_array($query)) {
                    $id = $vcard['id'];
                    $table = "myclientes";
                    $cod_vcard = $vcard['cod_vcard'];
                    $nam   = ($vcard['nombre']);
                   if (mb_detect_encoding($nam, 'UTF-8', true) =='UTF-8') {
                            $nombres    = ($vcard['nombre']);
                        }else {
                            $nombres    = utf8_encode($vcard['nombre']);
                     }

                    $empre              = ($vcard['empresa']);
                   if (mb_detect_encoding($empre, 'UTF-8', true) =='UTF-8') {
                            $empresa    = ($vcard['empresa']);
                        }else {
                            $empresa    = utf8_encode($vcard['empresa']);
                     }

                    $city               = ($vcard['ciudad']);
                   if (mb_detect_encoding($city, 'UTF-8', true) =='UTF-8') {
                            $ciudad    = ($vcard['ciudad']);
                        }else {
                            $ciudad    = utf8_encode($vcard['ciudad']);
                     }

                    $rang_img  = $vcard['rango_imagenes'];
                    $rfc       = $vcard['rfc'];
                    $rfc_empresa       = $vcard['rfc_empresa'];
                    $status_vcard = $vcard['estatus_vcard'];

                    $delet        = "admin";
                    $url_datos_users = "datos_users.php?cod_vcard=" . $cod_vcard;
                    ?>

                    <tr>

                        <td style="width: 100px;"><?php echo $cod_vcard; ?></td>

                        <td style="width: 170px;"><?php echo $nombres; ?></td>

                        <td style="width: 100px; text-align: center;"><?php if($rfc !=''){

                            echo '<span style="color:crimson">'.$rfc.'<span>'; } else{ echo 'Empleado'; } ?>

                        </td>

                        <td style="width: 200px;"><?php echo $ciudad .' / '. $empresa; ?> </td> 

                        <td style="width: 100px; text-align: center;"><?php echo  $status_vcard; ?></td>

                    <td style="text-align: center; font-size: 25px;">

                        <a href="edit_datos_clientes_jefe.php?id=<?php echo $id; ?>&cod=<?php echo $cod_vcard; ?>"> 

                            <span class="fa icon-pencil" title="Editar Datos del Cliente"></span>

                        </a>

                        <a href="delete.php?id=<?php echo $id; ?>&table=<?php echo $table; ?>&delete=<?php echo $delet; ?>&cod_vcard=<?php echo $cod_vcard; ?>"> 

                            <span class="fa fa-trash" title="Eliminar Registro"></span>

                        </a>

                       

                        <a href="migrar_cliente.php?id=<?php echo $id; ?>&cod_vcard=<?php echo $cod_vcard; ?>&empresa=<?php echo $empresa; ?>&name=<?php echo $nombres; ?>"> 

                            <span class="fa icon-action-redo" title="Migrar Cliente o Empleado"></span>

                        </a>

                        <?php

                        if($rfc !=""){  ?>

                        <a onclick="Cargar_datos_cliente('resultados', '<?php echo $url_datos_users; ?>', '')" >

                            <span class="fa fa-unlock-alt" id="candado" title="Ver Datos del Empleado"></span>

                        </a>

                        <?php } else{ ?>

                        <span class="fa fa-unlock-alt" title="" disabled></span>

                        <?php } ?>

                        <a href="asociar_vcard.php?rfc_empresa=<?php echo $rfc_empresa; ?>&cod_vcard=<?php echo $cod_vcard; ?>">

                            <span class="fa icon-people" title="Adjuntar VCard a Empleado"></span> 

                        </a>

                        <a href="pdf_vcard.php?cod_vcard=<?php echo $cod_vcard;?>&logo_peque=<?php echo $vcard['logo_peque']; ?>">

                            <span class="fa fa-file-pdf-o" title="Desacargar VCard en PDF"></span> 

                        </a>

                        

                    </td>

                    </tr>

                         <?php

                        }

                        ?>

                <tr>

                    <td colspan='7'>

                        <span class="pull-right">

                        <?php  echo paginate($reload, $page, $total_pages, $adjacents);?>

                        </span>

                    </td>

                </tr>

            </table>

        </div>

    </div>

        <?php

    }

} 

?>

