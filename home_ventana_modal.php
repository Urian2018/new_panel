<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {   ?>
    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta http-equiv="refresh" content="20; url=vcard.php">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <style>
                 /**VENTANA MODAL***/
                 #notificacion {
                    background-color: #2196F3;
                    color: white;
                    height: 60%;
                    width: 100%;
                    font-size: 20px;
                    text-align: center; 
                    margin: 0 auto;
                    position: absolute;
                    right: 0;
                    left: 0;
                    margin-top: 55px;
                    padding-top: 5px;
                    display:none;
                    z-index:999;
                    opacity: 0.9;
                }
                #notificacion a{
                    text-align: center;
                    color: white;
                    opacity: none;
                }
                #notificacion a:hover{
                    text-decoration: underline;
                    cursor: pointer;
                    color: crimson;
                }
                .pedido {
                    margin-top: 150px;
                    background-color: #fff;
                    display: inline-block;
                    width: 350px;
                    border-radius: 5px; 
                    font-weight: bold;
                    margin-left: 10px;
                    border: 1px solid black;
                    padding: 0 5px;
                }
                .cerrar {
                    margin-top: -49px;
                    background-color: black;
                    display: inline-block;
                    width: 100px;
                    border-radius: 3px;
                    float: right;
                    margin-right: 2px;
                    font-weight: bold;
                    font-size: 12px;
                    color: white;
                    padding: 10px;
                }
            </style>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
            <!-- El códido JQUERY empieza aquí -->
            <script type="text/javascript">
                $(document).ready(function () {
                    $("#notificacion").delay(500).slideDown("slow");
                    function cerrarNotificacion() {
                        $("#notificacion").click(function () {
                            $(this).slideUp("fast");
                        });
                    }
                    $(".cerrar").click(cerrarNotificacion);

                    setTimeout(function () {
                        $("#notificacion").fadeOut(2000);
                    }, 8000);

                });
            </script>
        </head>

        <body id="mimin" class="dashboard">
            <!---Mensaje de notificacion--->
            <div id="notificacion">
                <br><br>
                <!----------IMPORTANTE SE USA EL ATRIBUTO download="Cliente"  EN EL ENLACE PARA EVITAR RECARGAR LA WEB Y FORZAR LA DESCARGA---------->
                 <center>
                     <a href="descargar_historial_temporal.php" style="color:white; font-size: 14px;" download="Cliente">
                         DESCARGAR MIS NUEVOS REGISTROS EN EXCEL
                     </a>
                     <br>
                    <a href="pdf_hitorialTemporal.php" style="color:white; font-size: 14px;" target="_blank">
                         DESCARGAR MIS NUEVOS REGISTROS PDF
                     </a>
                 </center>
                
                <span class="cerrar">
                    <a href="#">Cerrar</a>
                </span>
            </div>
            <!---fin del mensaje de notificacion-->
            
            
            
            <?php include('menu_header.php'); ?>
            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>
                <div id="content">
                </div>
            </div>


            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse" > 
                <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

            <?php include('js.html'); ?>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>