<?php

require_once('tcpdf/tcpdf.php');
require_once('tcpdf/fpdi.php');

require("config.php");

ob_end_clean();
  
/*$usuario  = "root";
$password = "12345678";
$servidor = "localhost";
$basededatos = "new_panel";
$con = mysqli_connect($servidor, $usuario, $password) or die("No se ha podido conectar al Servidor");
$db = mysqli_select_db($con, $basededatos) or die("Upps! Error en conectar a la Base de Datos");
*/

class MYPDF extends TCPDF
{
  //Header personalizado
  public function Header() {
    //imagen en header
    /*$logo = 'img/php-logo.png';
    $this->Image($logo, 25, 10, 25, '', 'PNG', '', '', false, 300, '', false, false, 0, false, false, false);
        
    $this->SetFont('helvetica', 'B', 20);
    $this->Cell(0, 0, 'Titulo de pรกgina', 0, false, 'C', 0, '', 0, false, 'T', 'M');
    */
  }

}
  
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
//establecer margenes
$pdf->SetMargins(10, 5, 10);
$pdf->SetHeaderMargin(5);
$pdf->setPrintFooter(false);
$pdf->setPrintHeader(false); //para eliminar la linea superio del pdf por defecto y tambien
//$pdf->SetAutoPageBreak(false);  //IMPORTANTISIMO,permite bajar un elemento y eliminar el crear otra otra.
$pdf->SetAutoPageBreak(true);
$pdf->SetFont('Helvetica', 'B', 10, '', 'false');
$pdf->SetTextColor(0, 0, 0);



$checkemail = mysqli_query($con, "
SELECT t1 . * , t2 . * 
FROM myclientes AS t1
INNER JOIN temporal_vcard_pdf AS t2 ON t1.cod_vcard = t2.cod_vcard");

$cant = mysqli_num_rows($checkemail);

$x = 14.2;
$y = 10;
$w = 80;
$h = 50;
$c = 1;

$aux = array();
$inv = array();

while ($vcard = mysqli_fetch_assoc($checkemail)) {

    $id = $vcard['id'];
    $cod_vcard = $vcard['cod_vcard'];
    $nombre= $vcard['nombre'];
    $cargo = $vcard['cargo'];

    if(($c-1) % 10 == 0 ){
        $pdf->AddPage('P', 'A4');  
        $x = 14.2;
        $y = 10;
    }

        $folders = "logos_g/";
        $ruta_logo_grand = $vcard['logo_grand'];
	    $url_final = $folders.$ruta_logo_grand;

        $horizontal_alignments = array('C', 'C');
        $vertical_alignments = array('M', 'M');
	
        $fitbox = $horizontal_alignments[0].' ';
        $fitbox[1] = $vertical_alignments[0];

        $pdf->Rect($x-4, $y, 87, 53, 'D');
    
        if( empty($ruta_logo_grand) ){
                    $url_final = 'logos_g/nopic.png';
                }

        $imagen = getimagesize($url_final);
        $ancho = $imagen[0];              
        $alto = $imagen[1];
        $alto_mm = ( $alto * 25.4 ) / 72;
        $ancho_mm = ( $ancho * 25.4 ) / 72;

        if($ancho_mm > 67){
            $pct = 6700 / $ancho_mm;
            $alto_mm = ($alto_mm * $pct) / 100;
            $ancho_mm = 67;
        }

        if($alto_mm > 35){
            $dif_top = 10;
            $pct_y = 3500 / $alto_mm;
            $ancho_mm = ($ancho_mm * $pct_y) / 100;
            $alto_mm = 35;
        }else{
            $dif_top = (55 - $alto_mm) / 2;
        }

        $width  = ($ancho_mm * 72) / 25.4;
        $height = ($alto_mm  * 72) / 25.4;
                        
        $pdf->writeHTMLCell(67, 35, $x+6.5, $y+$dif_top, $html='<img src="'. $url_final .'" align="center" width="'. $width .'" height="'.$height.'" border="0" />', $border=0, $ln=0, $fill=false, $reseth=true, $align='C', $autopadding=false);
    
        $x += 102.5;

        if($c % 2 == 0){   
            $x = 14.2;
            $y += 55.80; // new row
            $inv[] = $vcard;
            $inv[] = $aux;
        }else{
            $aux = $vcard;

            if($c == $cant){
                $inv[] = $aux;
            }
        }
    
    $c++;
}


    $d = 1;
    $x = 14.2;
    $y = 10;
    $w = 80;
    $h = 50;

foreach ($inv as $value) {
  
    $id = $value['id'];
    $cod_vcard = $value['cod_vcard'];
    $nombre= $value['nombre'];
    $cargo = $value['cargo'];
    $pagina = $value['pagina_compartir'];
    $empresa = $value['empresa'];

    if(($d-1) % 10 == 0 ){
        $pdf->AddPage('P', 'A4');  
        $x = 14.2;
        $y = 10;
    }
    
    $folders = "logos_g/";
    $ruta_logo_grand = $value['logo_grand'];
    $url_final = $folders.$ruta_logo_grand;

    if(empty($ruta_logo_grand)){
        $url_final = 'logos_g/nopic.png';
    }else{
        if( !file_exists($url_final) ){
            $url_final = 'logos_g/nopic.png';
        }
    }

    $horizontal_alignments = array('C', 'C');
    $vertical_alignments = array('M', 'M');

    $style = array(
        'border' => false,
        'vpadding' => 'M',
        'hpadding' => 'C',
        'fgcolor' => array(0,0,0),
        'bgcolor' => false,
        'module_width' => 1, 
        'module_height' => 1 
    );

    $fitbox = $horizontal_alignments[0].' ';
    $fitbox[1] = $vertical_alignments[0];

    $pdf->Rect($x-4, $y, 87, 53, 'D');

    $pdf->SetXY($x+6,$y+17);
    $pdf->StartTransform();
    $pdf->Rotate(-90);

    $pdf->SetFont('Helvetica', 'B', 7, '', 'false');

    $pdf->Cell(1,1, strtoupper($cod_vcard) ,0,0,'L',0,'');
    $pdf->StopTransform();

    $pdf->SetFont('Helvetica', 'B', 7, '', 'false');
    $pdf->write2DBarcode($pagina, 'QRCODE,Q', $x+8.5, $y+8.5, 30, 30, $style, 'N');
    $pdf->Text($x, $y-4, '');

    $pdf->Image($url_final, $x+50.6, $y+8.5, 20, null, '', '', '', false, 300, '', false, false, 0, false, false, false);

    $pdf->SetFont('Helvetica', 'B', 8, '', 'false');
    $pdf->MultiCell(40, 6, ucwords( strtolower($empresa) ), 0, 'C', 0, 1, $x+40.7, $y+30, true);


    $pdf->SetXY($x+27,$y+44);
    
    $pdf->SetFont('Helvetica', 'B', 8, '', 'false');
    $pdf->MultiCell(42, 6, ucwords( strtolower($nombre) ), 0, 'C', 0, 1, $x+2, $y+40, true);

    $pdf->SetFont('Helvetica', '', 7, '', 'false');
    $pdf->MultiCell(42, 8, ucwords( strtolower($cargo)) , 0, 'C', 0, 1, $x+2, $y+43.5, true);
    
    $pdf->Image('logos_g/vcard.png', $x+72, $y+46, 4, 4, '', '', '', false, 300, '', false, false, 0, false, false, false);
    
    $x += 102.5;         
    
    if($d % 2 == 0){   
        $x = 14.2;
        $y += 55.80;

    }

    if(($d) == (count($inv) - 1 ) && ( ($d ) % 2) == 0 ){
        $x = 116.7;
    }
        
    $d++;
   
}

$pdf->Output('VCARD.'.date('dmy').'.pdf', 'I'); //la D es para forzar la descargarnd del pdf y La I funciona como un target

?>