<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
    $rango_usuario = $_SESSION['rango_users'];
    ?>
    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
        </head>

        <body id="mimin" class="dashboard">
            <?php 
            if ($rango_usuario == 'Administrador') {
                    include('menu_header.php'); ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>

                <div id="content">
                    <div class="panel box-shadow-none content-header">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <h3 class="animated fadeInLeft" style="text-align: center">Cargar Nuevo Excel</h3>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="col-md-12 panel">
                            <div class="col-md-12 panel-heading">
                                <h4> &nbsp;</h4>
                            </div>
                            <form action="recibe_excel.php" method="post" enctype="multipart/form-data">
                                <div class="col-md-12 panel-body">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="input-group fileupload-v1">
                                                <input type="file" name="archivo" id="archivo" class="fileupload-v1-file hidden" accept=".csv" required="required"/>
                                                <input type="text" class="form-control fileupload-v1-path" placeholder="Archivo . . . ." disabled>
                                                <input type="hidden" name="MAX_FILE_SIZE" value="20000"/>
                                                <span class="input-group-btn">
                                                    <button class="btn fileupload-v1-btn" type="button"><i class="fa fa-folder"></i> Presione Examinar</button>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-md-6">
                                                <button class="btn ripple btn-raised btn-success">
                                                    <div>
                                                        <span>Subir Excel</span>
                                                    </div>
                                                </button>
                                            </div>
                                            <br><br>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                </div>
                <?php
                    } else { ?>
            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>
                <div id="content">
                <?php    
                include('menu_header_user.php');
                $Consultar = ("SELECT * FROM contenido_welcome ORDER BY id DESC LIMIT 1");
                $numero_servicios = mysqli_query($con, $Consultar);
                
                while ($vcard = mysqli_fetch_array($numero_servicios)) { ?>
                    <div style="display: flex;align-items: center;justify-content: center; margin-top: 80px;">
                        <center><?php echo  $vcard['content']; ?></center>
                </div>
            </div> 
            <?php } } ?>
            </div>  
            


            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse"> 
                <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

            <?php include('js.html'); ?>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>