<?php
session_start();
if (isset($_SESSION['user']) != "") {
header("Content-Type: text/html;charset=utf-8");
include('config.php');
    ?>
    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <style>
                .icon-pencil:hover{
                    color: black;
                }
                .fa-paste:hover{
                    color: black;
                }
                .icon-lock:hover{
                    color: black;
                }
                .icon-people:hover{
                    color: black;
                }
                #candado:hover{
                    cursor: pointer;
                    color: black;
                }
            </style>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
            <script  src="https://code.jquery.com/jquery-2.2.4.js"></script>
        </head>


        <body id="mimin" class="dashboard">
            <?php include('menu_header.php'); ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); 
$query_myclientes = ("SELECT * FROM myclientes");
$re_myclientes = mysqli_query($con, $query_myclientes);
$cant_clientes = mysqli_num_rows($re_myclientes); ?>

            <div id="content">
                <br>
                <div class="col-md-12">
                    <div class="col-md-12 panel">
                        <div class="col-md-12 panel-heading">
                            <h4 style="text-align: center; color: black;"> Buscar 
                                <strong style="color:crimson;">"Cliente"</strong>  Total <strong>(<?php echo $cant_clientes; ?>)</strong>
                            </h4>
                            <h5 style="text-align: right;"><a href="mis_clientes_jefes.php">
                                        <span class="icon-action-undo"></span>
                                        Busquedad Antigua
                                    </a>
                                </h5>
                        </div>
                        <br><br>
                        <br><br>
                        <br>
                                   
                    <form class="form-horizontal" role="form" id="datos_cotizacion">
                        <div class="form-group row">
                        <div class="col-md-12">
                            <div class="col-md-7">
                                <input type="text" class="form-control" id="q" placeholder="Nombre, Cod VCard, Empresa, rfc empresa, Cod Distribuidor" onkeyup='load(1);'>
                            </div>

                            <div class="col-md-5">
                                <button type="button" class="btn btn-default" onclick='load(1);'>
                                <span class="glyphicon glyphicon-search" ></span> Buscar</button>
                                <span id="loader"></span>
                            </div>
                        </div>
                    </div>
                    </form>
                    
                    <div id="resultados"></div><!-- Carga los datos ajax -->
                    <div class='outer_div'> </div><!-- Carga los datos ajax -->
                </div>

            </div>
             </div>
            </div>

<!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse" > 
                <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

            <?php include('js.html'); ?>
            <script type="text/javascript" src="asset/js/buscar.js"></script>
            <script src="asset/js/ajax.js" type="text/javascript"></script>           

        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>
