<style>
    ul li{
      border-bottom: 1px solid #e5eef1;
    }
    li ul li{
      border-bottom: 1px solid #00afef;
      padding: 10px 1px !important;
    }
</style>
<?php
include('config.php');
if (isset($_SESSION['user']) != "") {
    $rango_usuario   = $_SESSION['rango_users'];
    $estatus_metrica = $_SESSION['estatus_metrica'];
    $cd = $_SESSION['cod_vcard'];
    $idSession =  $_SESSION['id'];

    if ($rango_usuario == 'Administrador') {
        ?>
        <div id="left-menu">
            <div class="sub-left-menu scroll">
                <ul class="nav nav-list">
                    <li><div class="left-bg"></div></li>
                   
                    <li class="ripple">
                        <a href="add_bienvenida_img.php" class="nav-header">
                            <span class="fa icon-picture"></span> 
                            CONT. BIENVENIDA
                        </a>
                    </li>
                    <li class="ripple">
                      <a class="tree-toggle nav-header"><span class="fa fa-cloud-upload"></span> REG VCARD 
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                      <ul class="nav nav-list tree">
                        <li>
                            <a href="home.php">
                            <span class="fa fa-users"></span>
                            Nuevos Registros</a>
                        </li>
                          <li>
                            <a href="new_prepago.php">
                            <span class="fa fa-credit-card"></span> 
                            Crear Prepago</a>
                          </li>
                        <li>
                            <a href="vcard.php">
                            <span class="fa fa-file-text-o"></span>
                            Registros Pendientes</a>
                        </li>
                          <li>
                            <a href="form_vcard_pdf.php">
                            <span class="fa fa-file-pdf-o"></span>
                            VCard PDF</a>
                          </li>
                          <li>
                            <a href="historial_registros_excel.php">
                            <span class="fa fa-file-text"></span>
                            Historial</a>
                          </li>
                      </ul>
                    </li>

                    <li class="ripple">
                      <a class="tree-toggle nav-header"><span class="fa fa-users"></span> CLIENTES
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                      <ul class="nav nav-list tree">
                        <!--<li>
                            <a href="mis_clientes_jefes.php">
                            <span class="fa icon-people"></span>
                            VC Clientes</a>
                          </li>-->
                          <li>
                            <a href="busqueda_myclientes.php">
                            <span class="fa icon-people"></span>
                            VC Clientes</a>
                          </li>
                          <li>
                            <a href="mis_clientes.php">
                            <span class="fa fa-file-image-o"></span>
                            Logos Clientes</a>
                          </li>
                          <li>
                            <a href="visitas_admin.php">
                            <span class="fa fa-bar-chart"></span>
                            Métricas</a>
                          </li>
                          <li>
                            <a href="papelera.php">
                            <span class="fa icon-trash"></span>
                            Papelera</a>
                          </li>
                      </ul>
                    </li>
                    
                    <li class="ripple">
                        <a href="distribuidor.php" class="nav-header">
                            <span class="fa fa-user"></span> 
                            DISTRIBUIDOR 
                        </a>
                    </li>


                     <li class="ripple">
                      <a class="tree-toggle nav-header"><span class="fa icon-settings"></span> HERRAMIENTAS
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                      <ul class="nav nav-list tree">
                        <li>
                            <a href="logos.php">
                            <span class="fa fa-pencil-square"></span>
                            Logos</a>
                          </li>
                          <li>
                            <a href="configuration_admin.php">
                            <span class="fa fa-cog"></span>
                            Configuración Web</a>
                          </li>
                          <li>
                            <a href="add_evento.php">
                            <span class="fa icon-bell"></span>
                            Agregar Evento</a>
                          </li>
                          
                          <li>
                            <a href="notificacion.php">
                            <span class="fa icon-bell"></span>
                            Notificaciones</a>
                          </li>
                          <li>
                            <a href="respaldo.php">
                            <span class="fa fa-database"></span>
                            Respaldo BD</a>
                          </li>
                      </ul>
                    </li>                  

                    <li class="ripple">
                      <a class="tree-toggle nav-header"><span class="fa fa-users"></span> USUARIO 
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                      <ul class="nav nav-list tree">
                          <li>
                            <a href="new_user.php">Agregar Usuario</a>
                          </li>
                          <li>
                            <a href="new_clave.php">Cambiar Clave</a>
                          </li>
                          <li>
                            <a href="cuentas_users.php">Lista de Usuarios</a>
                          </li>
                          <li>
                            <a href="UsersOnline.php">Usuarios Online</a>
                          </li>
                      </ul>
                    </li>
    
                    <li class="ripple">
                        <a href="exit.php" class="nav-header">
                            <span class="fa  icon-lock-open"></span>  
                            SALIR 
                        </a>
                    </li>

                    <!---<li class="ripple">
                        <a href="limpiar_tablas.php" class="nav-header">
                            <span class="fa fa-pencil-square"></span> 
                            LIMPIAR TABLAS 
                        </a>
                    </li>-->
                </ul>
            </div>
        </div>

    <?php }elseif ($rango_usuario == 'AdminExpo') { ?>
          <div id="left-menu">
            <div class="sub-left-menu scroll">
                <ul class="nav nav-list">
                    <li><div class="left-bg"></div></li>
                    <li class="active ripple">
                        <a href="home.php" class="nav-header">
                            <span class="fa-home fa"></span> 
                            INICIO
                        </a>
                    </li>

                    <li class="ripple">
                      <a class="tree-toggle nav-header">
                        <span class="fa fa-file-image-o"></span> PAGINA EVENTO 
                      </a>
                    </li>

                    <li class="ripple">
                        <a href="subir_clientesExpo.php" class="nav-header">
                            <span class="fa fa-users"></span> 
                            SUBIR CLIENTES 
                        </a>
                    </li>
                    <li class="ripple">
                        <a href="ClientesExpot.php" class="nav-header">
                            <span class="fa fa-users"></span> 
                             CLIENTES EXPO
                        </a>
                    </li>
                    <li class="ripple">
                          <a href="CrearformularioVisitante.php" class="nav-header">
                              <span class="fa fa-bar-chart"></span> FORMULARIO  
                          </a>
                      </li>
                    <li class="ripple">
                          <a href="#" class="nav-header">
                              <span class="fa fa-bar-chart"></span> ESTADISTICAS  
                          </a>
                      </li>
                      <li class="ripple">
                          <a href="downloadClientesExpot.php?codExpot=<?php echo $_SESSION['user']; ?>" class="nav-header">
                            <span class="fa fa-database"></span> DESCARGA BD</a>
                          </li>
                    <li class="ripple">
                        <a href="new_clave.php" class="nav-header">
                            <span class="fa icon-key"></span> CAMBIAR CLAVE  
                        </a>
                    </li>

                    <li class="ripple">
                        <a href="exit.php" class="nav-header">
                            <span class="fa  icon-lock-open"></span> 
                            SALIR 
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    <?php }elseif ($rango_usuario == 'ClienteExpo') { ?>
          <div id="left-menu">
            <div class="sub-left-menu scroll">
                <ul class="nav nav-list">
                    <li><div class="left-bg"></div></li>
                    <li class="active ripple">
                        <a href="home.php" class="nav-header">
                            <span class="fa-home fa"></span> 
                            INICIO EXPO
                        </a>
                    </li>
                    <li class="ripple">
                        <a href="add_logo.php" class="nav-header">
                            <span class="fa-home  icon-camera"></span> 
                            LOGOTIPO 
                        </a>
                    </li>

                    <li class="ripple">
                      <a class="tree-toggle nav-header"><span class="fa fa-file-image-o"></span> GALERIA 
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                      <ul class="nav nav-list tree">
                          <li>
                            <a href="add_carrusel.php">Agregar Imagen</a>
                          </li>
                          <li>
                            <a href="add_video.php">Agregar Video</a>
                          </li>
                          <li>
                            <a href="arch_descargable.php">Archivos Descargables</a>
                          </li>
                          <li>
                            <a href="add_audio.php">Agregar Audio</a>
                          </li>
                          <li>
                            <a href="iframe.php" class="nav-header"> Editor Html</a>
                          </li>
                          <li>
                            <a href="list_html.php" class="nav-header"> Lista Html</a>
                          </li>
                          <li>
                            <a href="configuration.php" class="nav-header"> Configuración Web</a>
                          </li>
                      </ul>
                    </li>

                    <li class="ripple">
                        <a href="mis_empleados.php" class="nav-header">
                            <span class="fa fa-users"></span> 
                            DATOS EN VCARD 
                        </a>
                    </li> 
                    <?php
                      $sqlLink = ("SELECT 
                              my.cod_vcard,
                              my.StandCliente,
                              u.user,
                              u.id
                          FROM myclientes my, users u
                          WHERE u.user = my.cod_vcard
                          AND u.id='".$idSession."' ");
                      $mostarLink = mysqli_query($con, $sqlLink);
                      $datosLink = mysqli_fetch_array($mostarLink);
                      $StandClienteLink = $datosLink["StandCliente"];
                      ?>
                    <li class="ripple">
                        <a href="https://www.vcard.mx/xp/index.php?cd=<?php echo  $cd; ?>&st=<?php echo $StandClienteLink;?>" class="nav-header" target="_blank">
                            <span class="fa fa-file-image-o"></span>
                            SCAND VISITANTES
                        </a>
                    </li> 

                    <?php if($estatus_metrica !="NO"){ ?>
                        <li class="ripple">
                            <a href="visitas_clientes.php" class="nav-header">
                                <span class="fa fa-bar-chart"></span> MÉTRICAS  
                            </a>
                        </li>
                    <?php } ?>
                    

                     <li class="ripple">
                        <a href="notif_user.php" class="nav-header">
                            <span class="fa icon-bell"></span>NOTIFICACIONES
                            <?php
                            $var = 'NO';
                            $cantidad_notificaciones = ("SELECT * FROM notificaciones WHERE (leido='" . $var . "' AND para='Todos') OR (para='" . $_SESSION['user'] . "' AND leido='" . $var . "') ORDER BY id ASC");
                            $total_notificaciones = mysqli_query($con, $cantidad_notificaciones);
                            $total = mysqli_num_rows($total_notificaciones);
                            ?>

                            <span style=" width: 100%; background-color: grey; border-radius: 50%; text-align: center; padding: 2px 6px;font-weight: bold; color: white;"><?php echo $total; ?></span>  
                        </a>
                    </li>

                    <li class="ripple">
                        <a href="new_clave.php" class="nav-header">
                            <span class="fa icon-key"></span> CAMBIAR CLAVE  
                        </a>
                    </li>

                    <li class="ripple">
                        <a href="exit.php" class="nav-header">
                            <span class="fa  icon-lock-open"></span> 
                            SALIR 
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    <?php } else { ?>
        <div id="left-menu">
            <div class="sub-left-menu scroll">
                <ul class="nav nav-list">
                    <li><div class="left-bg"></div></li>
                    <li class="active ripple">
                        <a href="home.php" class="nav-header">
                            <span class="fa-home fa"></span> 
                            INICIO
                        </a>
                    </li>
                    <li class="ripple">
                        <a href="add_logo.php" class="nav-header">
                            <span class="fa-home  icon-camera"></span> 
                            LOGOTIPO 
                        </a>
                    </li>

                    <li class="ripple">
                      <a class="tree-toggle nav-header"><span class="fa fa-file-image-o"></span> GALERIA 
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                      <ul class="nav nav-list tree">
                          <li>
                            <a href="add_carrusel.php">Agregar Imagen</a>
                          </li>
                          <li>
                            <a href="add_video.php">Agregar Video</a>
                          </li>
                          <li>
                            <a href="arch_descargable.php">Archivos Descargables</a>
                          </li>
                          <li>
                            <a href="add_audio.php">Agregar Audio</a>
                          </li>
                          <li>
                            <a href="iframe.php" class="nav-header"> Editor Html</a>
                          </li>
                          <li>
                            <a href="list_html.php" class="nav-header"> Lista Html</a>
                          </li>
                          <li>
                            <a href="configuration.php" class="nav-header"> Configuración Web</a>
                          </li>
                      </ul>
                    </li>

                    <li class="ripple">
                        <a href="mis_empleados.php" class="nav-header">
                            <span class="fa fa-users"></span> 
                            DATOS EN VCARD 
                        </a>
                    </li>                   
                    <?php if($estatus_metrica !="NO"){ ?>
                        <li class="ripple">
                            <a href="visitas_clientes.php" class="nav-header">
                                <span class="fa fa-bar-chart"></span> MÉTRICAS  
                            </a>
                        </li>
                    <?php } ?>

                     <li class="ripple">
                        <a href="notif_user.php" class="nav-header">
                            <span class="fa icon-bell"></span>NOTIFICACIONES
                            <?php
                            $var = 'NO';
                            $cantidad_notificaciones = ("SELECT * FROM notificaciones WHERE (leido='" . $var . "' AND para='Todos') OR (para='" . $_SESSION['user'] . "' AND leido='" . $var . "') ORDER BY id ASC");
                            $total_notificaciones = mysqli_query($con, $cantidad_notificaciones);
                            $total = mysqli_num_rows($total_notificaciones);
                            ?>

                            <span style=" width: 100%; background-color: grey; border-radius: 50%; text-align: center; padding: 2px 6px;font-weight: bold; color: white;"><?php echo $total; ?></span>  
                        </a>
                    </li>

                    <li class="ripple">
                        <a href="new_clave.php" class="nav-header">
                            <span class="fa icon-key"></span> CAMBIAR CLAVE  
                        </a>
                    </li>

                    <li class="ripple">
                        <a href="exit.php" class="nav-header">
                            <span class="fa  icon-lock-open"></span> 
                            SALIR 
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <?php
    }
}
?>



