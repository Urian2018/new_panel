<?php

include('UploadImage.php');

function upload_image($url = null, $file = null, $maxwidth = null, $maxheight = null, $folder = null, $encode64 = null){

	$remote_image_url = $url;
	$local_image_folder = $folder;


	$medidasimagen = getimagesize($remote_image_url);

	$max_ancho = $maxwidth;
	$max_alto  = $maxheight;

	$nf = explode('/', $remote_image_url);
	$count = count($nf);

	$name = $nf[$count-2].$nf[$count-1];

	$nf = explode('.', $name);

	$count = count($nf);

	$name = $nf[$count-2].'.'.$nf[$count-1];

	$nombrearchivo = $name;

	$rtOriginal = $remote_image_url;

	$info = @GetImageSize($remote_image_url);


	$mime = $info['mime'];
	$type = substr(strrchr($mime, '/'), 1);

	if($type=='jpeg'){
		$original = imagecreatefromjpeg($rtOriginal);
	}
	else if($type=='png'){
		$original = imagecreatefrompng($rtOriginal);
	}
	else if($type=='gif'){
		$original = imagecreatefromgif($rtOriginal);
	}

	list($ancho,$alto)=getimagesize($rtOriginal);


	$x_ratio = $max_ancho / (float) $ancho;
	$y_ratio = $max_alto /  (float) $alto;


	if( ($ancho <= $max_ancho) && ($alto <= $max_alto) ){
	    $ancho_final = $ancho;
	    $alto_final = $alto;
	}
	elseif (($x_ratio * $alto) < $max_alto){
	    $alto_final = ceil($x_ratio * $alto);
	    $ancho_final = $max_ancho;
	}
	else{
	    $ancho_final = ceil($y_ratio * $ancho);
	    $alto_final = $max_alto;
	}

	$lienzo=imagecreatetruecolor($ancho_final,$alto_final); 

	imagecopyresampled($lienzo,$original,0,0,0,0,$ancho_final, $alto_final,$ancho,$alto);
	 
	//imagedestroy($original);
	 
	$cal=8;

	if(!empty($file)){ $nombrearchivo = $file; }
	
	if($type=='jpeg'){
		imagejpeg($lienzo, $local_image_folder.'/'.$nombrearchivo);
	}
	else if($type=='png'){
		imagepng($lienzo, $local_image_folder.'/'.$nombrearchivo);
	}
	else if($type=='gif'){
		imagegif($lienzo, $local_image_folder.'/'.$nombrearchivo);
	}


	if($encode64 != null){

		$data = base64_encode(file_get_contents($local_image_folder.'/'.$nombrearchivo));
		unlink($local_image_folder.'/'.$nombrearchivo);
		return $data;

	}
}

function upload($url = null, $width = null, $height = null){

	include('UploadImage.php');

	$remote_image_url = 'https://pbs.twimg.com/profile_images/1116054518397935616/LCfplNPQ_400x400.png';
	$local_image_folder = './carga';

	// You can set the prefered upload method.  
	// If you do not set it, then it will try all of them until it can use one!
	//$upload_method = 'curl';
	//$upload_method = 'gd';
	//$upload_method = 'fopen';
	//$upload_method = 'sockets';
	$upload_method = 'sockets';

}

function get_type($remote_image_url){
	$info = @getimagesize($remote_image_url);

    $mime = $info['mime'];
    $type = substr(strrchr($mime, '/'), 1);
    return $type;
}




//if($t !=''){

//$data = base64_encode(file_get_contents('carga/'.$nombrearchivo));
//unlink('imagenes/'.$nombrearchivo);
// Fin de codigo nuevo


//$data = base64_encode(file_get_contents($_FILES["img_vcard"]["tmp_name"]));
//$img_vcard = "," . $data . "";





// initialize the class
//$image = new UploadImage($local_image_folder);

//$get = $image->uploadRemoteImage($remote_image_url, $upload_method);


?>