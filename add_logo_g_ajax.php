<?php
$original =  imagecreatefrompng($rtOriginal);
//Ancho y alto máximo
$max_ancho = 400;
$max_alto = 400;
list($ancho,$alto)=getimagesize($rtOriginal);

$x_ratio = $max_ancho / $ancho;
$y_ratio = $max_alto / $alto;

if(($ancho <= $max_ancho) && ($alto <= $max_alto) ){
    $ancho_final = $ancho;
    $alto_final = $alto;
}
else if(($x_ratio * $alto) < $max_alto){
    $alto_final = ceil($x_ratio * $alto);
    $ancho_final = $max_ancho;
}
else {
    $ancho_final = $max_ancho;
    $alto_final = $max_alto;
}

$lienzoo=imagecreatetruecolor($ancho_final,$alto_final);
imagecopyresampled($lienzoo,$original,0,0,0,0,$ancho_final, $alto_final,$ancho,$alto);
imagedestroy($original);
imagepng($lienzoo,"logos_p/".$mi_file);
?>
<META HTTP-EQUIV="REFRESH" CONTENT="0;URL=vcard.php">
<?php 
exit();
?>
