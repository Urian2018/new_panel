<?php
session_start();
include('config.php');
date_default_timezone_set("America/Mexico_City");
$fecha = date("d/m/Y"); 
if (isset($_SESSION['user']) != "") { 
$codEvento = $_SESSION['user'];   
?>
<!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
            <!----js para mostrar msj--->
            <script  src="asset/js/jquery.min.js"></script>
            <script src="asset/js/msj.js"></script>
        </head>

        <body id="mimin" class="dashboard">
            <?php 
               include('menu_header.php');
             ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); 
                
                $sql = ("SELECT * FROM myclientes WHERE origenCliente='".$codEvento."' ");
                if($mostar = mysqli_query($con, $sql)){
                $total_client = mysqli_num_rows($mostar) ;
                ?>

                <div id="content">
                    <br>
                    <div class="col-md-12">
                        <div class="col-md-12 panel">
                            <div class="col-md-12 panel-heading">
                                <h4 style="text-align: center; color: black;"> Lista de Clientes de la Expo <strong style="color:crimson;"><?php echo $codEvento;  ?></strong>
                                    <strong style='color:#2196F3; font-size: 13px; float:right;'> Total (<?php echo $total_client;?>)
                                        <a href="downloadClientesExpot.php?codExpot=<?php echo $codEvento; ?>" class="ripple btn-outline btn-primary">Descargar Clientes Expo</a> 
                                    </strong>
                                </h4>
                            </div>
                        </div>
                    </div>


<div class="col-md-12 top-20 padding-0" id="capa">
<div class="col-md-12">
    <div class="panel">
        <div class="panel-body">
            <div class="responsive-table">
                <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Nombre y Apellido</th>
                            <th>Empresa</th>
                            <th>Cargo</th>
                            <th>Ciudad</th>
                            <th>Telefono</th>
                            <th>Email</th>
                            <th>Stand</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        while ($vcard = mysqli_fetch_array($mostar)) {
                           ?>
                            <tr>
                                <td><?php echo $vcard['nombre']; ?></td>
                                <td><?php echo $vcard['empresa'];?></td>
                                <td><?php echo $vcard['cargo'];?></td>
                                <td><?php echo $vcard['ciudad'];?></td>
                                <td><?php echo $vcard['telefono'];?></td>
                                <td><?php echo $vcard['email'];?></td>
                                <td><?php echo $vcard['StandCliente'];?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>  
</div>
<?php
}
@mysqli_close($mostar);
?> 

 
        </div> 
    </div>


            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse" > 
    <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

<?php include('js.html'); ?>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#datatables-example').DataTable();
            });
        </script>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>