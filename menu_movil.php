<?php
include('config.php');
if (isset($_SESSION['user']) != "") {
    $rango_usuario   = $_SESSION['rango_users'];
    $estatus_metrica = $_SESSION['estatus_metrica'];
    if ($rango_usuario == 'Administrador') {
        ?>
        <div class="mimin-mobile-menu-list">
            <div class="col-md-12 sub-mimin-mobile-menu-list animated fadeInLeft">
              
                <ul class="nav nav-list">
                <li><div class="left-bg"></div></li>
                    <li class="active ripple">
                        <a href="home.php" class="nav-header">
                            <span class="fa-home fa"></span> 
                            NUEVO REGISTRO
                        </a>
                    </li>

                    <li class="ripple">
                        <a href="add_bienvenida_img.php" class="nav-header">
                            <span class="fa icon-picture"></span> 
                            CONT. BIENVENIDA
                        </a>
                    </li>

                    <li class="ripple">
                        <a href="vcard.php" class="nav-header">
                            <span class="fa fa-credit-card"></span> 
                            REG. PENDIENTES 
                        </a>
                    </li>

                    <li class="ripple">
                        <a href="busqueda_myclientes.php" class="nav-header">
                            <span class="fa fa-pencil-square"></span> 
                            CLIENTES 
                        </a>
                    </li>
                    
                    <li class="ripple">
                        <a href="logos.php" class="nav-header">
                            <span class="fa fa-pencil-square"></span> 
                            LOGOS 
                        </a>
                    </li>

                    <li class="ripple">
                        <a href="mis_empresas.php" class="nav-header">
                            <span class="fa icon-folder-alt"></span> 
                            EMPRESAS 
                        </a>
                    </li>

                    <li class="ripple">
                        <a href="notificacion.php" class="nav-header">
                            <span class="fa icon-bell"></span>NOTIFICACIONES
                        </a>
                    </li>

                    <li class="ripple">
                        <a href="historial_registros_excel.php" class="nav-header">
                            <span class="fa fa-users"></span> 
                            HISTORIAL 
                        </a>
                    </li>

                    <li class="ripple">
                        <a href="mis_clientes_jefes.php" class="nav-header">
                            <span class="fa icon-people"></span> 
                            MODIFICAR DATOS 
                        </a>
                    </li>    

                    <li class="ripple">
                        <a href="new_clave.php" class="nav-header">
                            <span class="fa icon-key"></span> CAMBIAR CLAVE  
                        </a>
                    </li>

                    <li class="ripple">
                        <a href="exit.php" class="nav-header">
                            <span class="fa  icon-lock-open"></span>  
                            Salir 
                        </a>
                    </li>

                   <!-- <li class="ripple">
                        <a href="limpiar_tablas.php" class="nav-header">
                            <span class="fa fa-pencil-square"></span> 
                            LIMPIAR TABLAS 
                        </a>
                    </li> -->

                </ul>
            </div>
        </div> 
        <?php } else { ?>        

          <div class="mimin-mobile-menu-list">
            <div class="col-md-12 sub-mimin-mobile-menu-list animated fadeInLeft">
                <ul class="nav nav-list">
                    <li><div class="left-bg"></div></li>
                    <li class="active ripple">
                        <a href="home.php" class="nav-header">
                            <span class="fa-home fa"></span> 
                            INICIO 
                        </a>
                    </li>
                    <li class="ripple">
                        <a href="add_logo.php" class="nav-header">
                            <span class="fa-home  icon-camera"></span> 
                            LOGOTIPO 
                        </a>
                    </li>

                    <li class="ripple">
                      <a class="tree-toggle nav-header"><span class="fa fa-file-image-o"></span> GALERIA 
                        <span class="fa-angle-right fa right-arrow text-right"></span>
                      </a>
                      <ul class="nav nav-list tree">
                          <li>
                            <a href="add_carrusel.php">Agregar Imagen</a>
                          </li>
                          <li>
                            <a href="add_video.php">Agregar Video</a>
                          </li>
                          <li>
                            <a href="arch_descargable.php">Archivos Descargables</a>
                          </li>
                          <li>
                            <a href="add_audio.php">Agregar Audio</a>
                          </li>
                          <li>
                            <a href="iframe.php" class="nav-header"> Editor Html</a>
                          </li>
                           <li>
                            <a href="list_html.php" class="nav-header"> Lista Html</a>
                          </li>
                      </ul>
                    </li>

                    <li class="ripple">
                        <a href="mis_empleados.php" class="nav-header">
                            <span class="fa fa-users"></span> 
                            DATOS EN VCARD 
                        </a>
                    </li>                   
                    <?php if($estatus_metrica !="NO"){ ?>
                        <li class="ripple">
                            <a href="visitas_clientes.php" class="nav-header">
                                <span class="fa fa-bar-chart"></span> MÉTRICAS  
                            </a>
                        </li>
                    <?php } ?>
                    

                     <li class="ripple">
                        <a href="notif_user.php" class="nav-header">
                            <span class="fa icon-bell"></span>NOTIFICACIONES
                            <?php
                            $var = 'NO';
                            $cantidad_notificaciones = ("SELECT * FROM notificaciones WHERE (leido='" . $var . "' AND para='Todos') OR (para='" . $_SESSION['user'] . "' AND leido='" . $var . "') ORDER BY id ASC");
                            $total_notificaciones = mysqli_query($con, $cantidad_notificaciones);
                            $total = mysqli_num_rows($total_notificaciones);
                            ?>

                            <span style=" width: 100%; background-color: grey; border-radius: 50%; text-align: center; padding: 2px 6px;font-weight: bold; color: white;"><?php echo $total; ?></span>  
                        </a>
                    </li>

                    <li class="ripple">
                        <a href="new_clave.php" class="nav-header">
                            <span class="fa icon-key"></span> CAMBIAR CLAVE  
                        </a>
                    </li>

                    <li class="ripple">
                        <a href="exit.php" class="nav-header">
                            <span class="fa  icon-lock-open"></span> 
                            SALIR 
                        </a>
                    </li>
                </ul>

        </div>
        </div>

        <?php
    }
}
?>
