<style>
    .style_tbody{
        font-size: 10px;
        color: black;
    }
    .color{
        background-color: #9BB;  
    }
</style>
<?php
require('config.php');
$rfc_empresa = $_GET['rfc_empresa'];
header("Content-Type: application/vnd.ms-excel");
header("Expires: 0");
$filename = "Mis_Metricas." . date('m-d-Y') . ".xls";
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Disposition: attachment; filename=" . $filename . "");
                        
$sql = ("SELECT * FROM visitas WHERE rfc_empresa='".$rfc_empresa."'");
$mostar = mysqli_query($con, $sql);
?>
<table style="color:#000099; width:100px;" border=1 align="center" cellpadding=1 cellspacing=1>
    <thead>
        <tr>
            <th class="color">C&oacute;digo</th>
            <th class="color">Nombre</th>
             <th class="color">Empresa</th>
            <th class="color">Descargas de VCard</th>
            <th class="color">Compartir VCard</th>
            <th class="color">Visitas en Galeria</th>
            <th class="color">Fecha</th>
        </tr>
    </thead>
    <?php
    while ($row = mysqli_fetch_array($mostar)) {
        echo "<tbody>";
        echo "<tr>";
        echo "<td class='style_tbody'>" . $row['cod_vcard'] . "</td>";
        echo "<td class='style_tbody'>" . $row['nombre'] . "</td>";
        echo "<td class='style_tbody'>" . $row['empresa'] . "</td>";
        echo "<td class='style_tbody'>" . $row['visita_pag1'] . "</td>";
        echo "<td class='style_tbody'>" . $row['visita_pag2'] . "</td>";
        echo "<td class='style_tbody'>" . $row['visita_pag3'] . "</td>";
        echo "<td class='style_tbody'>" . $row['fecha'] . "</td>";
        echo "</tr>";
        echo "</tbody>";
    }
    ?>
</table>