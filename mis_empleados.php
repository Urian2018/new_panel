<?php
session_start();
include('config.php');
if (isset($_SESSION['user']) != "") {
$rfc_empresa = $_SESSION['rfc_empresa'];   ?>
    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="description" content="VCARD">
            <meta name="author" content="ALEJANDRO TORRES">
            <meta name="keyword" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" type="image/png" href="../favicon.png" />
            <title>VCARD</title>
            <?php include('css.html'); ?>
            <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
            <style>
                .icon-pencil:hover{
                    cursor: pointer;
                    color: black;
                }
                .icon-credit-card:hover{
                   cursor: pointer;
                   color: black; 
                }
            </style>

            <!----js para mostrar msj--->
        <script  src="asset/js/jquery.min.js"></script>
        <script src="asset/js/msj.js"></script>
        </head>

        <body id="mimin" class="dashboard">
            <?php include('menu_header_user.php'); ?>

            <div class="container-fluid mimin-wrapper">
                <?php include('menu_lateral_escritorio.php'); ?>

                <div id="content">
                <br>
                    <?php
                    //comparar por el rcf y nombre por el nombre dela empresa
                    $Consultar_dos = ("SELECT * FROM myclientes  WHERE rfc_empresa='". $rfc_empresa ."' ");
                    $numero_servi = mysqli_query($con, $Consultar_dos);
                    $total_client = mysqli_num_rows($numero_servi);
                    ?>
                    <div class="col-md-12 top-20 padding-0">
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 style="text-align: center;">MIS 
                                    <strong style="color: #00afef;">VCARD</strong>  
                                    </h3>
                                    <div style="text-align: right; color: green; font-weight: bold;">Total (<?php echo $total_client; ?>)</div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-responsive table-bordered table-hover" id="datatables-example" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>Código</th>
                                                    <th>Nombre</th>
                                                    <th>Empresa</th>
                                                    <th>Cargo</th>
                                                    <th>Ciudad</th>
                                                    <th>Imagen Vcard</th>
                                                    <th>Opciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                while ($vcard = mysqli_fetch_array($numero_servi)) {
                                                    $table = "myclientes";
                                                    $id = $vcard['id'];
                                                    $cod_vacrd = $vcard['cod_vcard'];
                                                    $rfc_empresa = $vcard['rfc_empresa'];
                                                    //$var_asoc_vcard = $vcard['migrar_descarga_vcard'];
                                                    ?>
                                                    <tr>
                                                        <td style="text-align: center;"><?php echo $vcard['cod_vcard']; ?></td>
                                                        <td style="text-align: center;"><?php   
                                                          $na              = ($vcard['nombre']);
                                                        if (mb_detect_encoding($na, 'UTF-8', true) =='UTF-8') {
                                                               echo $name    = ($vcard['nombre']);
                                                                }else {
                                                               echo $name    = utf8_encode($vcard['nombre']);
                                                             } ?></td>

                                                        <td style="text-align: center;">
                                                            <?php
                                                            $emp = $vcard['empresa'];
                                                            if (mb_detect_encoding($emp, 'UTF-8', true) =='UTF-8') {
                                                               echo $empresa    = ($vcard['empresa']);
                                                                }else {
                                                               echo $empresa    = utf8_encode($vcard['empresa']);
                                                             }  ?></td>
                                                        <td style="text-align: center;"><?php echo $vcard['cargo']; ?></td>

                                                        <td style="text-align: center;">
                                                            <?php
                                                            $city = $vcard['ciudad'];
                                                            if (mb_detect_encoding($emp, 'UTF-8', true) =='UTF-8') {
                                                               echo $ciudad    = ($vcard['ciudad']);
                                                                }else {
                                                               echo $ciudad    = utf8_encode($vcard['ciudad']);
                                                             } ?></td>
                                                        <td style="width:200px; text-align: center; font-size: 25px;">
                                                        <?php
                                                        $var = "data:image/jpg/png;base64";
                                                        $img_vcard2 = $vcard['img_vcard'];
                                                        $img_vcard = $var.$img_vcard2;

                                                        /**evaluado la variable que sea distinta de vacia**/
                                                        if (!empty($img_vcard2)) {
                                                        $img = file_get_contents($img_vcard);
                                                        $imdata = base64_encode($img);
                                                        echo "<img src='data:image/jpg;base64,".$imdata."' style='width:50%;'/>";
                                                        } else{
                                                       echo "<span style='color:red; font-size:13px;'>No tiene Imagen</span>";
                                                        }  ?> 
                                                        </td>
                                                        <td style="text-align: center; font-size: 25px;">
                                                            <a href="edit_datos_mis_empleados.php?id=<?php echo $id; ?>&cod_vcard=<?php echo $cod_vacrd; ?>"> 
                                                                <span class="fa icon-pencil" title="Editar Datos de mi Empleado"></span>
                                                            </a>
                                                            <?php /*if($var_asoc_vcard !=""){ ?>
                                                                 <a href="migrar_descarga_vcard.php?id=<?php echo $id; ?>&rfc_empresa=<?php echo $rfc_empresa; ?>&desactivar_asc_vcard=si"> 
                                                            <!--<a href="migrar_descarga_vcard.php?id=<?php echo $id; ?>&rfc_empresa=<?php echo $rfc_empresa; ?>"> -->
                                                                <span style="color: green;" class="fa icon-credit-card" title="Migrar VCard a mi Pagina de Descarga"></span>
                                                            </a>
                                                            <?php } else{ ?>
                                                                <a href="migrar_descarga_vcard.php?id=<?php echo $id; ?>&rfc_empresa=<?php echo $rfc_empresa; ?>"> 
                                                                <!--<a href="migrar_descarga_vcard.php?id=<?php echo $id; ?>&rfc_empresa=<?php echo $rfc_empresa; ?>&desactivar_asc_vcard=si"> -->
                                                                <span class="fa icon-credit-card" title="Desactivar Asociacion de VCard en Pagina de Descarga"></span>
                                                            </a>
                                                            <?php } */?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <?php
                        @mysqli_close($numero_servi);
                        ?>  
                    </div> 

                    <div class="contenedor_flotante">                         
                        <?php
                             if(!empty($_GET['msj'])){ ?>
                             <div class='col-md-12'>
                            <div class='alert alert-success col-md-12 col-sm-12  alert-icon alert-dismissible fade in' role='alert'>
                            <div class='col-md-2 col-sm-2 icon-wrapper text-center'>
                                <span class='fa fa-check fa-2x'></span></div>
                                <div class='col-md-10 col-sm-10'>
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                <span aria-hidden='true'>x</span></button>
                                <p><strong>Felicidades el Registro fue Modificado Correctamente.</strong></p>
                                </div>
                            </div>
                            </div>
                            <?php } ?>
                             </div>
                </div>
            </div>


            <!-- start: Mobile -->
            <div id="mimin-mobile" class="reverse" > 
                <?php include('menu_movil.php'); ?>
            </div>
            <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
                <span class="fa fa-bars"></span>
            </button>
            <!-- end: Mobile -->

            <?php include('js.html'); ?>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#datatables-example').DataTable();
                });
            </script>
        </body>
    </html>
    <?php
} else {
    include('error.php');
}
?>