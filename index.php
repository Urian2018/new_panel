<?php

if(session_start() == false){
	session_destroy();
	session_start();
}

include('config.php');
if (isset($_SESSION['user']) != "") {
    header("Location: home.php");
}


if (!empty($_POST)) {
    
    $usuario =  ($_POST['user']);
    $password = ($_POST['pass']);
    $error_sesion = '';
    $consulta = "SELECT * FROM users WHERE user COLLATE utf8_bin ='" .$usuario. "' AND pass COLLATE utf8_bin='" .$password. "'";
    $res = mysqli_query($con, $consulta);
    if ($row = mysqli_fetch_assoc($res)) {
        $_SESSION['id']     = $row['id']; //creamos la sesion "id" y le asignamos como valor el campo id
        $_SESSION['user']   = $row['user']; //se crea la sesion "user" y le asignamos como valor el campo user
        $_SESSION['nombre_apellido'] = $row['nombre_apellido']; //se crea la sesion "user" y le asignamos como valor el campo user
        $_SESSION['rango_users']     = $row['rango_users'];
        $_SESSION['cod_vcard']       = $row['cod_vcard'];
        $_SESSION['empresa']         = $row['empresa'];
        $_SESSION['ciudad_cliente']  = $row['ciudad_cliente'];
        $_SESSION['rfc']             = $row['rfc'];
        $_SESSION['rfc_empresa']     = $row['rfc_empresa'];
        $_SESSION['rango_imagenes']             = $row['rango_imagenes'];
        $_SESSION['rango_video']                = $row['rango_video'];
        $_SESSION['rango_audios']               = $row['rango_audios'];
        $_SESSION['rango_arch_descargables']    = $row['rango_arch_descargables'];
        $_SESSION['estatus_metrica']            = $row['estatus_metrica'];


        $_SESSION['rango_imagenes']             = $row['rango_imagenes'];
        $_SESSION['tipo_cliente_empresa']       = $row['tipo_cliente_empresa'];
        echo '<meta http-equiv="refresh" content="0;url=home.php">';

        //ACTUALIZANDO EL ESTADO DE CONEXION

    $stateconexion = "Activa";
    $updateStateConexion = ("UPDATE users SET StateConexion='" .$stateconexion. "' WHERE id='".$_SESSION['id']."' ");
    $stateUpdate = mysqli_query($con, $updateStateConexion);

    } else {
        $error_sesion                           = "Algunos de los Campos son Incorrectos . .";
    }
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="VCARD">
        <meta name="author" content="ALEJANDRO TORRES">
        <meta name="keyword" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/png" href="../favicon.png" />
        <title>VCARD</title>
        <?php include('css.html'); ?>
        <link rel="stylesheet" type="text/css" href="asset/css/my_style.css">
        <style type="text/css" media="screen">
            .hover_a:hover{
                color: black !important;
                transition: linear 0.4s;
                margin-left: 15px;
            }
        </style>
        <script  src="asset/js/jquery.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                setTimeout(function () {
                    $(".error_session").fadeOut(1500);
                }, 3000);
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                setTimeout(function () {
                    $(".contenedor_flotante").fadeOut(1500);
                }, 3000);
            });
        </script>
    </head>

    <body id="mimin" class="form-signin-wrapper lock-screen-v1">

        <div class="container">
            <form class="form-signin" action="<?php $_SERVER['PHP_SELF']; ?>" method="POST" autocomplete="off">
                <div class="panel periodic-login">
                    <div class="panel-body text-center">
                        <h1 class="atomic-symbol">
                            <img src="asset/img/img_vcard.png" class="img-responsive img-circle" />
                        </h1>

                        <div class="form-group form-animate-text">
                            <input type="text" name="user" class="form-text" required>
                            <span class="bar"></span>
                            <label>Usuario</label>
                        </div>

                        <div class="form-group form-animate-text">
                            <input type="password" name="pass" class="form-text" required>
                            <span class="bar"></span>
                            <label>Clave</label>
                        </div>

                        <input style="font-weight: bold; color: #222;" type="submit" class="btn col-md-12" value="Entrar al Sistema"/>
                    </div>

                    <div class="error_session">
            <?php echo isset($error_sesion) ? utf8_decode($error_sesion) : ''; ?>
                    </div>
                    <p style="text-align: center; padding: 0px 0px 15px 0px;">
                        <a class="hover_a" href="recuperar_clave.php">Solicitar o Recuperar Contraseña
                            <span class="fa fa-arrow-right" title="Solicitar o Recuperar Contraseña"></span>  
                        </a>
                    </p>
                </div>
            </form>



<div class="contenedor_flotante">                         
<?php
echo isset($msj_exito) ? utf8_decode($msj_exito) : '';
//isset($_GET['msj']) ? utf8_decode($_GET['msj']) : ''; 
if (!empty($_GET['msj'])) {
?>
<div class='col-md-12'>
<div class='alert alert-success col-md-12 col-sm-12  alert-icon alert-dismissible fade in' role='alert'>
<div class='col-md-2 col-sm-2 icon-wrapper text-center'>
    <span class='fa fa-check fa-2x'></span></div>
    <div class='col-md-10 col-sm-10'>
    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
    <span aria-hidden='true'>x</span></button>
    <p><strong>Por favor revise su correo y bandeja de ESPAM.</strong></p>
    </div>
</div>
</div>
<?php } 

if (!empty($_GET['error'])) {
?>
<div class='col-md-12'>
<div class='alert col-md-12 col-sm-12 alert-icon alert-danger alert-dismissible fade in' role='alert'>
    <div class='col-md-2 col-sm-2 icon-wrapper text-center'>
        <span class='fa fa-flash fa-2x'></span></div>
    <div class='col-md-10 col-sm-10'>
        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>
        <p><strong>Lo sentimos!, pero los datos de accesados no corresponden a la VCard solicitada.</strong></p>
    </div>
</div>
</div>
<?php } ?> 
</div>


</div>

<?php include('js.html'); ?>
    </body>
</html>